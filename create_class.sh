# this shell script creates an empty h and cpp file for a class
YEAR=2014
AUTHOR="Viktor Richter"
NAMESPACE="sitinf"
SRC_ROOT="src"

# check call parameter
if [ -z "$1" ]
  then
    echo "use: create_class.sh 'PathFromProjectRoot' 'Classname'" 
    exit
fi
if [ -z "$2" ]
  then
    echo "use: create_class.sh 'PathFromProjectRoot' 'Classname'" 
    exit
fi
CLASS=$2
PATH=${1%/}

# create include path
_TMP="$PATH/$CLASS.h"
INCLUDE="${_TMP#src/}"

# create test name
_TMP="$PATH/$CLASS/Test";
_TMP="${_TMP#src/}"
_TMP2="$IFS";
IFS='/';
TESTNAME=""
for word in $_TMP; do TESTNAME="${TESTNAME}${word}"; done
IFS="$_TMP2";

function padding(){
  pad=$(printf '%0.1s' " "{1..60})
  padlength=67
  printf '%s' "$1"
  printf '%*.*s' 0 $((padlength - ${#1})) "$pad"
  printf '**\n'
}

function license(){
  year="$1"
  author="$2"
  path="$3"
  class="$4"
  postfix="$5"
  printf '/********************************************************************\n'
  printf '**                                                                 **\n'
  padding "** Copyright (C) $year $author"
  printf '**                                                                 **\n'
  padding "** File   : $path/$class.$postfix"
  padding "** Authors: $author"
  printf '**                                                                 **\n'
  printf '**                                                                 **\n'
  printf '** GNU LESSER GENERAL PUBLIC LICENSE                               **\n'
  printf '** This file may be used under the terms of the GNU Lesser General **\n'
  printf '** Public License version 3.0 as published by the                  **\n'
  printf '**                                                                 **\n'
  printf '** Free Software Foundation and appearing in the file LICENSE.LGPL **\n'
  printf '** included in the packaging of this file.  Please review the      **\n'
  printf '** following information to ensure the license requirements will   **\n'
  printf '** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **\n'
  printf '**                                                                 **\n'
  printf '********************************************************************/\n\n'
}

function header(){
  namespace="$1"
  class="$2"
  printf '#pragma once\n\n'
  printf '#include <boost/shared_ptr.hpp>\n\n'
  printf 'namespace %s {\n\n' "$namespace"
  printf '  class %s\n' "$class"
  printf '  {\n'
  printf '  public:\n\n'
  printf '    /**\n'
  printf '     * @brief Ptr typedef for shared pointer type\n'
  printf '     */\n'
  printf '    typedef boost::shared_ptr<%s> Ptr;\n\n' "$class"
  printf '    %s(){}\n\n' "$class"
  printf '    ~%s(){}\n\n' "$class"
  printf '  };\n\n'
  printf '} // namespace %s\n' "$namespace"
}

function source(){
  namespace="$1"
  class="$2"
  path="$3"
  header_name="$path/$class.h"
  include="${header_name#src/}"
  printf '#include <%s>\n\n' "$include"
  printf '#include <utils/Types.h>\n\n'
  printf '#include <utils/Exception.h>\n\n'
  printf '#include <utils/Macros.h>\n\n'
  printf 'using namespace %s;\n\n' "$namespace"
}

function test(){
  include_path="$1"
  test_name="$2"
  class="$3"
  namespace="$4"
  printf '#include <%s> \n\n' "$include_path"
  printf '#include <utils/Exception.h>\n'
  printf '#include "gtest/gtest.h"\n\n'

  printf 'namespace {\n\n'

  printf '  // Testing class %s\n' "$test_name"
  printf '  class %s : public ::testing::Test {\n' "$test_name"
  printf '  protected:\n\n'

  printf '    // Set-up work for each test here.\n'
  printf '    %s()\n    {\n\n    }\n\n' "$test_name"

  printf '    // Do clean-up work that does not throw exceptions here.\n'
  printf '    virtual ~%s() {}\n\n' "$test_name"

  printf '    // This is called immediately after the constructor (right before each test).\n'
  printf '    virtual void SetUp() {\n    }\n\n'

  printf '    // This is called after each test (right before the destructor).\n'
  printf '    virtual void TearDown() {\n    }\n\n  };\n\n'

  printf '  // Tests that %ss are correctly created\n' "$class"
  printf '  TEST_F(%s, %sCorrectlyCreated) {\n' "$test_name" "$class"
  printf '    // create instance\n'
  printf '    %s::%s instance\n\n' "$namespace" "$class"

  printf '    // implement something useful\n'
  printf '    EXPECT_NO_THROW(throw %s::utils::NotImplementedException("Test not implemented."));\n  }\n\n' "$namespace"

  printf '}  // namespace\n\n'
}

echo "Creating class $CLASS in $PATH/$CLASS"

# check that files not already exist
if [ -e "$PATH/$CLASS.h" ]
then
  echo "  * header file $PATH/$CLASS.h already exists."
else
  # create header file
  echo "  * creating header file $PATH/$CLASS.h."
  license "$YEAR" "$AUTHOR" "$PATH" "$CLASS" "h" >> "$PATH/$CLASS.h"
  header "$NAMESPACE" "$CLASS" >> "$PATH/$CLASS.h"
fi
if [ -e "$PATH/$CLASS.cpp" ]
then
  echo "  * source file $PATH/$CLASS.cpp already exists."
else
  # create source file
  echo "  * creating source file $PATH/$CLASS.cpp."
  license "$YEAR" "$AUTHOR" "$PATH" "$CLASS" "cpp" >> "$PATH/$CLASS.cpp"
  source "$NAMESPACE" "$CLASS" "$PATH" >> "$PATH/$CLASS.cpp"
fi
if [ -e "test/$TESTNAME.cpp" ]
then
  echo "  * test file test/$TESTNAME.cpp already exists."
else
  # create test file
  echo "  * creating test file test/$TESTNAME.cpp."
  license "$YEAR" "$AUTHOR" "test" "$TESTNAME" "cpp" >> "test/$TESTNAME.cpp"
  test "$INCLUDE" "$TESTNAME" "$CLASS" "$NAMESPACE" >> "test/$TESTNAME.cpp"
fi
