#*********************************************************************
#**                                                                 **
#** File   : share/CMakeLists.txt                                   **
#** Authors: Viktor Richter                                         **
#**                                                                 **
#**                                                                 **
#** GNU LESSER GENERAL PUBLIC LICENSE                               **
#** This file may be used under the terms of the GNU Lesser General **
#** Public License version 3.0 as published by the                  **
#**                                                                 **
#** Free Software Foundation and appearing in the file LICENSE.LGPL **
#** included in the packaging of this file.  Please review the      **
#** following information to ensure the license requirements will   **
#** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
#**                                                                 **
#*********************************************************************

cmake_minimum_required(VERSION 2.8.2)

set(SHARED
  "situation.sensor.yaml"
  "situation.network.yaml"
  "addressee.sensor.yaml"
  "addressee.network.xml"
  "addressee.network.yaml"
  "addressee-meka.sensor.yaml"
  "addressee-meka.network.yaml"
  "sitinf_logging_default.conf"
  "sitinf_logging_develop.conf"
)

install(FILES ${SHARED} DESTINATION share/situation)
