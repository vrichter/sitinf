import QtQuick 1.0

ListView {
    id: aspectlistview
    height: parent.height
    width: parent.width

    delegate: Row {
        id: row
        height: aspectlistview.height / aspectlistview.model.count
        width: aspectlistview.width

        Rectangle {
            id: namerect
            width: row.width / 2
            height: row.height
            color: "lightsalmon"

            Text {
                text: model_data_aspect
                anchors.horizontalCenter: namerect.horizontalCenter;
                anchors.verticalCenter: namerect.verticalCenter;
            }
        }

        Menu {
            id: statemenu
            z: 0
            width: row.width / 2
            height: row.height
            currentState: model_data_current_state
            model: model_data_states
            onZChanged: parent.z = z;
            onCurrentStateChanged: {
                aspectlistview.model.setAspectState(model_data_aspect,currentState)
            }
        }
    }
}
