/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/learner.cpp                                        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <dai/alldai.h>
#include <iostream>
#include <fstream>
#include <string>
#include <boost/program_options.hpp>

#include <inference/BayesianFactorGraph.h>
#include <inference/ParameterEstimation.h>
#include <inference/SimpleParameterEstimation.h>
#include <io/EvidenceFileSource.h>
#include <utils/Exception.h>
#include <io/RsbSink.h>
#include <rst/bayesnetwork/BayesNetwork.pb.h>
#include <network/BayesianNetworkFactory.h>

#define _LOGGER "learner"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::io;
using namespace sitinf::network;
using namespace sitinf::inference;

int main(int n, char**ppc){
  SITINF_PROGRAM_OPTIONS(
    n,
    ppc,
    "https://staff.fnwi.uva.nl/j.m.mooij/libDAI/doc/fileformats.html#fileformats-factorgraph-factor",

    ("network,n",
     boost::program_options::value<std::string>()->default_value("../../share/situation.network.yaml"),
     "The path to the source network definition xml file in YAML format.")

      ("input-data,d",
      boost::program_options::value<std::string>()->default_value(""),
      "The evidence file holding correspoinding network evidence for learning.")

      ("network-publish,p",
       boost::program_options::value<std::string>()->default_value("/citec/csra/home/situation/override/network"),
       "The rsb scope to publish the resulting network.")
  )

  BayesianNetwork network = BayesianNetworkFactory::fromFile(program_options["network"].as<std::string>());
  PullSource<WeightedAspectStateEvidence>::Ptr evidence(new EvidenceFileSource(program_options["input-data"].as<std::string>()));
  ParameterEstimation::Ptr estimation(new SimpleParameterEstimation(network));

  std::vector<WeightedAspectStateEvidence> aspectEvidence;
  uint emptyEvidence = 0;
  while(true){
    try{
      WeightedAspectStateEvidence e = network.applicableEvidence(evidence->next());
      if(!e.currentState().empty()){
        aspectEvidence.push_back(e);
      } else {
        ++emptyEvidence;
      }
    } catch (NoMoreDataAvailableException& e){
      // Source is empty leave this loop
      break;
    }
  }
  SLOG_INFO << "Ignored non-applicable evidence: #" << emptyEvidence;

  SLOG_TRACE << "Estimation with " << aspectEvidence.size() << " observed network configurations.";

  EstimationResult result = estimation->estimateParameters(aspectEvidence);

  std::stringstream stream;
  BayesianNetworkFactory::toYaml(stream,result.network);

  SLOG_INFO << "Result network: " << stream.str() << std::endl;

  std::cout << stream.str() << std::endl;

  if(program_options["network-publish"].as<std::string>() != ""){
    rsb::Scope scope(program_options["network-publish"].as<std::string>());
    RsbSink<rst::bayesnetwork::BayesNetwork>(scope,result.network.asRstMessage());
  }
}
