/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/simulator.cpp                                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <QApplication>
#include <QtDeclarative/QDeclarativeEngine>
#include <QtDeclarative/QDeclarativeContext>

#include <qt/QtQuick1ApplicationViewer.h>
#include <qt/SituationModel.h>

#include <io/SituationInformer.h>
#include <inference/BayesianInference.h>
#include <io/RsbNetworkPublisher.h>
#include <io/RsbNetworkReceiver.h>

#include <boost/program_options.hpp>
#include <rsb/Factory.h>
#include <utils/Macros.h>

#define _LOGGER "simulator"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf::io;
using namespace sitinf::qt;
using namespace sitinf::utils;
using namespace sitinf::network;
using namespace sitinf::inference;

Mutex lock;
BayesianNetwork::Ptr network;
SituationModel model;
SituationInformer::Ptr informer;
RsbNetworkReceiver::Ptr receiver;
class Updater;
Shared<Updater>::Ptr updater;
std::string state = "";
InferenceResult::Ptr empty_result = InferenceResult::Ptr(new InferenceResult());

void publishData(){
  lock.lock();
  if(state == ""){
    state = model.learnState().toStdString();
  } else if (state != model.learnState().toStdString()){
    // reset
    informer->publish(network,empty_result,state);
    state = model.learnState().toStdString();
  }
  BayesianNetwork::Ptr ntw = network;
  InferenceResult::Ptr result = model.result();
  QString state = model.learnState();
  lock.unlock();
  informer->publish(ntw,result,state.toStdString());
}

class Updater : Observer<Shared<rst::bayesnetwork::BayesNetwork>::Ptr> {
public:
  Updater(Shared<RsbNetworkReceiver>::Ptr receiver)
    : m_Receiver(receiver)
  {
    m_Receiver->addObserver(this);
  }

  ~Updater(){
    m_Receiver->removeObserver(this);
  }

  void update(const Subject<Shared<rst::bayesnetwork::BayesNetwork>::Ptr>* subject,
              Shared<rst::bayesnetwork::BayesNetwork>::Ptr data)
  {
    if(subject == m_Receiver.get()){
      Locker l(lock);
      network = BayesianNetwork::Ptr(new BayesianNetwork(*data));
      SLOG_TRACE << "network: " <<network->asRstMessage()->DebugString();
      model.useNetwork(network);
    }
  }

private:
  Shared<RsbNetworkReceiver>::Ptr m_Receiver;
};

void init(boost::program_options::variables_map& args){
  Locker l(lock);
  network = BayesianNetwork::Ptr(new BayesianNetwork());
  receiver = RsbNetworkReceiver::Ptr(new RsbNetworkReceiver(args["network-scope"].as<std::string>()));
  informer = SituationInformer::Ptr(new SituationInformer(args["evidence-scope"].as<std::string>()));

  updater = Shared<Updater>::Ptr(new Updater(receiver));

  // set network as model-data and connect model callback to publishData function
  model.useNetwork(network);
  model.registerStateCallback(boost::bind(publishData));
}

int main(int n, char **ppc)
{
  SITINF_PROGRAM_OPTIONS(
        n,
        ppc,
        "This application connects to a situation recognition instance via rsb. Loads its \n"
        "BayesNetwork and can then be used to override its evidence if it is supported by \n"
        "the recognition.",

        ("network-scope,n",
         boost::program_options::value<std::string>()->default_value("/citec/csra/home/situation/network"),
         "The scope on which the network is published")

        ("evidence-scope,e",
         boost::program_options::value<std::string>()->default_value("/citec/csra/home/situation/override/evidence"),
         "The rsb scope on which the clamped evidence should be published.")
        )
  init(program_options);

  QApplication app(n, ppc);

  QtQuick1ApplicationViewer viewer;

  // register model
  QDeclarativeContext *ctxt = viewer.rootContext();
  ctxt->setContextProperty("aspectmodel", &model);

  qRegisterMetaType<QModelIndex>("QModelIndex");

  // start qml gui
  viewer.addImportPath(QLatin1String("modules"));
  viewer.setOrientation(QtQuick1ApplicationViewer::ScreenOrientationAuto);
  viewer.setMainQmlFile(QLatin1String("qrc:///simulator.qml"));
  viewer.setResizeMode(QDeclarativeView::SizeRootObjectToView);
  viewer.showExpanded();

  // run gui
  int end = app.exec();

  // cleanup
  informer->publish(network,empty_result,"learn"); // reset changes
  informer->publish(network,empty_result,"override"); // reset changes
  informer.reset();
  network.reset();
  receiver.reset();
  updater.reset();
  return end;
}
