/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/evidence-filter.cpp                                **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <iostream>
#include <fstream>
#include <string>
#include <boost/program_options.hpp>

#include <inference/ParameterEstimation.h>
#include <io/EvidenceFileSource.h>
#include <utils/Exception.h>
#include <network/BayesianNetworkFactory.h>

#include <utils/Macros.h>

#define _LOGGER "evidence-filter"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::io;
using namespace sitinf::network;
using namespace sitinf::inference;


class UniqueEvidence {

public:
  class TimeDuration{
    uint64_t m_Timestamp;
    uint64_t m_Duration;

    public:

    TimeDuration(uint64_t timestamp, uint64_t duration)
      : m_Timestamp(timestamp), m_Duration(duration) {}

    const uint64_t& timestamp() const {
      return m_Timestamp;
    }

    const uint64_t& duration() const {
      return m_Duration;
    }
  };

  UniqueEvidence(const AspectStateEvidence& ev=AspectStateEvidence())
    : m_State(ev)
  {}

  void addTimeDuration(uint64_t timestamp, uint64_t duration){
    m_Times.push_back(TimeDuration(timestamp, duration));
  }

  const AspectStateEvidence& state() const {
    return m_State;
  }

  const std::vector<TimeDuration>& times() const {
    return m_Times;
  }
  
  private:
    AspectStateEvidence m_State;
    std::vector<TimeDuration> m_Times;
};

void printSumDurations(const std::map<AspectStateEvidence,UniqueEvidence>& unique_map){
  for(auto it : unique_map){
    const UniqueEvidence& ue = it.second;
    uint64_t duration = 0;
    for(auto t : ue.times()){
        duration+=t.duration();
    }
    std::cout << ue.times().size() << " - " << duration/1000000. << " - " << ue.state().toString() << std::endl;
  }
}

void printEvidenceOutput(const std::vector<UniqueEvidence>& evidence){
  for(const UniqueEvidence& e : evidence){
    static Converter<Shared<rst::bayesnetwork::BayesNetworkEvidence>::Ptr,const AspectStateEvidence&> converter;
    auto networkEvidence = converter.convert(e.state());
    SASSERT_THROW(e.times().size() == 1, UnexpectedException,
                  "UniqueEvidence at this point should always have 1 time element. Found " << e.times().size());
    rst::timing::Timestamp* time = rst::timing::Timestamp::default_instance().New();
    time->set_time(e.times().at(0).timestamp());
    networkEvidence->set_allocated_time(time);
    networkEvidence->set_duration_microseconds(e.times().at(0).duration());
    std::cout << networkEvidence->ShortDebugString() << std::endl;
  }
}

int main(int n, char**ppc){
  SITINF_PROGRAM_OPTIONS(
    n,
    ppc,
    "Filters situation evidence for applicable aspect states.",

    ("network,n",
     boost::program_options::value<std::string>()->default_value("../../share/situation.network.yaml"),
     "The path to the network definition xml file in YAML format. This Network will be used as filter.")

    ("input-data,d",
     boost::program_options::value<std::string>()->default_value(""),
     "The evidence file holding correspoinding network evidence.")
  )

  BayesianNetwork network = BayesianNetworkFactory::fromFile(program_options["network"].as<std::string>());
  ReadFile file(program_options["input-data"].as<std::string>());

  BayesNetworkEvidenceStreamSource evidenceSource(&file);

  rst::bayesnetwork::BayesNetworkEvidence currentDatedEvidence = evidenceSource.next();
  std::map<AspectStateEvidence,UniqueEvidence> uniqueSet;
  std::vector<UniqueEvidence> notRepeated;
  int iterations = 0;
  int doubles = 0;
  while(true){
    if(!((++iterations)%1000)) {
      SLOG_TRACE << "processed " << iterations << " elements.";
    }
    try{
      rst::bayesnetwork::BayesNetworkEvidence nextDatedEvidence = evidenceSource.next();
      AspectStateEvidence currentEvidence;
      for(int i = 0; i < currentDatedEvidence.observations_size(); ++i){
        currentEvidence.newEvidence(currentDatedEvidence.observations(i).variable(),currentDatedEvidence.observations(i).state());
      }
      AspectStateEvidence nextEvidence;
      for(int i = 0; i < nextDatedEvidence.observations_size(); ++i){
        nextEvidence.newEvidence(nextDatedEvidence.observations(i).variable(),nextDatedEvidence.observations(i).state());
      }
      if(nextEvidence.equals(&currentEvidence)){
         ++doubles;
        continue;
      }
      auto it = uniqueSet.find(currentEvidence);
      uint64_t time = currentDatedEvidence.time().time();
      uint64_t duration = nextDatedEvidence.time().time()-currentDatedEvidence.time().time();
      if(it == uniqueSet.end()){ // add new evidence to uniqe set
        uniqueSet[currentEvidence] = UniqueEvidence(currentEvidence);
        it = uniqueSet.find(currentEvidence);
      }
      // add timepoint to evidence
      UniqueEvidence& ue = it->second;
      ue.addTimeDuration(time,duration);
      notRepeated.push_back(UniqueEvidence(currentEvidence));
      notRepeated.back().addTimeDuration(time,duration);
      currentDatedEvidence = nextDatedEvidence;
    } catch (NoMoreDataAvailableException& e){
      // Source is empty leave this loop
      break;
    }
  }

  SLOG_TRACE << "dat: " << iterations;
  SLOG_TRACE << "rep: " << doubles;
  SLOG_TRACE << "uni: " << uniqueSet.size();

  //printSumDurations(uniqueSet);
  printEvidenceOutput(notRepeated);

}
