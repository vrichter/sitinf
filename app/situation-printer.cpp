/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/situation-printer.cpp                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <boost/program_options.hpp>

#include <io/RsbSource.h>
#include <utils/Types.h>
#include <utils/Macros.h>

#include <rst/classification/ClassificationResultMap.pb.h>

#define _LOGGER "situation-printer"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

typedef sitinf::io::RsbSource<rst::classification::ClassificationResultMap> Situation;
typedef rst::classification::ClassificationResult Aspect;
typedef sitinf::utils::ObserverCallback<Situation::ResultType> SituationObserver;

const char* COLOR_OFF = "\x1b[0m";
const char* COLOR_ASPECT = "\x1b[0;37;3m";
const char* COLOR_DECIDED = "\x1b[31;3m";
const char* COLOR_OBSERVED = "\x1b[32;3m";

void printBarrier(){
  std::cout << " ------------------------- " << std::endl;
}

void printAspectName(const std::string& name){
  std::cout << COLOR_ASPECT << name << COLOR_OFF << "\t";
}

void printAspectState(const std::string& name, float confidence, bool decided){
  const char* color = COLOR_OFF;
  if (decided) {
    if (confidence < 1.) {
      color = COLOR_DECIDED;
    } else {
      color = COLOR_OBSERVED;
    }
  }
  std::cout << color << name << COLOR_OFF << std::fixed << std::setprecision(2) << " [ " << confidence << " ]\t";
}

void printResult(const Aspect& aspect, bool compact){
  if(compact || aspect.classes_size() == 0) {
    std::cout << COLOR_DECIDED << aspect.decided_class() << COLOR_OFF;
  } else {
    for(auto result : aspect.classes()){
      printAspectState(result.name(), result.confidence(), aspect.decided_class() == result.name());
    }
  }
}

void print(Situation::ResultType situation, bool compact){
  if(situation.get()){
    printBarrier();
    for(auto entry : situation->aspects()){
      printAspectName(entry.name());
      printResult(entry.result(),compact);
      std::cout << std::endl;
    }
    printBarrier();
  }
}

// exit will be set by signalhandler
bool kill_application = false;

void signal_handler(int num) {
  SLOG_INFO << "Received interupt signal (" << num << ") closing application.";
  if(kill_application){
    SLOG_FATAL << "Repeatedly received interupt signal but application still running.";
    // already called once exit.
    exit(num);
  }
  kill_application = true;
}


int main(int n, char**ppc){
  SITINF_PROGRAM_OPTIONS(
        n,
        ppc,
        "This application prints the current situation to std::out.",

        ("scope,s",
         boost::program_options::value<std::string>()->default_value("/citec/csra/home/situation/result"),
         "The rsb scope on which the situation is published.")

        ("compact,c",
         boost::program_options::bool_switch(),
         "Print compact representation.")

      );


  // init
  Situation situation(program_options["scope"].as<std::string>());
  bool compact = program_options["compact"].as<bool>();
  SituationObserver printer([compact] (Situation::ResultType a) -> void {print(a,compact);});
  situation.addObserver(&printer);
  printer.update(&situation,situation.current());

  // add signal handler
  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

  while(!kill_application){
    sitinf::utils::Thread::sleep(boost::get_system_time() + boost::posix_time::milliseconds(100));
  }

  situation.removeObserver(&printer);

  return 0;
}
