/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/convert-from-fg.cpp                                **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <boost/program_options.hpp>

#include <dai/factorgraph.h>
#include <io/File.h>
#include <utils/Exception.h>

#define _LOGGER "convert-from-fg"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;


int main(int n, char**ppc){
  SITINF_PROGRAM_OPTIONS(
        n,
        ppc,
        "This application takes a libdai-FactorGraph representation and prints the corresponding \n"
        "graphviz-dot file.",

        ("factor-graph,f",
         boost::program_options::value<std::string>()->default_value(""),
         "The path to the FactorGraph definition in fg-Format.")

        )
  std::string filename = program_options["factor-graph"].as<std::string>();
  // check whether file exists
  try {
    File(filename,false,true,File::regular);
  } catch (IOException& e){
    SLOG_ERROR << "Can't open file '" << filename << "'. Error: " << e.what();
    std::cout << "File: '" << filename << "' does not exist." << std::endl;
    return 1;
  }

  // Create Factor Graph from
  SLOG_INFO << "Creating factorgraph from file";
  dai::FactorGraph factor_graph;
  try{
    factor_graph.ReadFromFile(filename.c_str());
  } catch (dai::Exception& e) {
    SLOG_ERROR << "Could not read factor graph from file '" << filename << "'. Error: " << e.what();
    std::cout << "File: '" << filename << "' does not contain a valid FactorGraph. \nError: " << e.what() << std::endl;
    return 1;
  }

  SLOG_INFO << "Printing network " << filename << " as fg-dot";
  factor_graph.printDot(std::cout);

  return 0;
}
