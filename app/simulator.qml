import QtQuick 1.0

Rectangle {
    id: root
    width: 1080;
    height: 900;
    color: "lightyellow"

    Aspect{
        id: aspectview
        height: root.height * 0.8
        width: root.width * 0.8
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        model: aspectmodel
        interactive: false
    }


    Rectangle {
        id: learn
        width: root.width * 0.15;
        height: root.height * 0.02
        color: "lightblue"
        x: root.width * 0.05
        y: root.height * 0.05

        Text {
            id: learntext
            text: "Simulate"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
        }

        MouseArea {
            anchors.fill: parent;
            onClicked: {
                if(learn.state == "learn"){
                    learn.state = "override"
                    aspectmodel.setLearnState("override")
                } else {
                    learn.state = "learn"
                    aspectmodel.setLearnState("learn");
                }
            }
        }

        states: [
            State {
               name: "learn";
               changes:[
                PropertyChanges {
                    target: learn;
                    color: "#f57979"
                   },
                PropertyChanges {
                    target: learntext;
                    text: "Learn from this"
                }
               ]
            },
            State {
               name: "simulate";
               changes: [
                  PropertyChanges {
                      target: learn;
                      color: "lightblue";
                  },
                  PropertyChanges {
                      target: learntext;
                      text: "Override";
                  }
               ]
            }
       ]

    }
}
