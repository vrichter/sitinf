/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/situation.cpp                                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <rsb/Factory.h>
#include <rsb/Listener.h>
#include <rsb/Scope.h>

#include <boost/program_options.hpp>

#include <network/BayesianNetwork.h>
#include <inference/BayesianFactorGraph.h>
#include <sensor/SensorFactory.h>
#include <utils/Exception.h>
#include <inference/BayesianInference.h>
#include <io/File.h>
#include <io/MessageParser.h>
#include <network/BayesianNetworkStateEvidence.h>
#include <render/GraphLayout.h>
#include <signal.h>
#include <utils/Macros.h>
#include <io/Sink.h>
#include <io/RsbSink.h>
#include <io/Source.h>
#include <io/RsbSource.h>
#include <io/EvidenceFileSink.h>
#include <network/StateEvidenceCollector.h>
#include <network/BayesianNetworkFactory.h>

#define _LOGGER "situation"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::sensor;
using namespace sitinf::utils;
using namespace sitinf::network;
using namespace sitinf::inference;

typedef Source<Shared<rst::bayesnetwork::BayesNetwork>::Ptr> RstNetworkSource;
typedef Source<Shared<rst::classification::ClassificationResultMap>::Ptr> SituationSource;
typedef Sink<Shared<rst::bayesnetwork::BayesNetwork>::Ptr> RstNetworkSink;
typedef Sink<Shared<rst::classification::ClassificationResultMap>::Ptr> SituationSink;

// global fields of the application
BayesianInference::Ptr bayesInference;
ProtoFilesImporter::Ptr protoFiles;
std::vector<Sensor::Ptr> sensors;

Source<BayesianFactorGraph::Ptr>::Ptr factorGraphSource;
Source<const AspectStateEvidence&>::Ptr evidenceSource;
Source<Shared<rst::classification::ClassificationResultMap>::Ptr>::Ptr evidenceOverride;
Source<Shared<rst::classification::ClassificationResultMap>::Ptr>::Ptr evidenceLearn;

RstNetworkSink::Ptr networkPublish;
SituationSink::Ptr situationPublish;
EvidenceFileSink::Ptr writeEvidence;
Sink<const AspectStateEvidence&>::Ptr publishEvidence;

// min loop time
boost::posix_time::milliseconds sleep_milis(0);

// exit will be set by signalhandler
bool kill_application = false;

void init(const boost::program_options::variables_map& args){
  // create importer for message definitions
  protoFiles = ProtoFilesImporter::Ptr(new ProtoFilesImporter(args["proto"].as<std::string>()));

  // set the minimum calculation time (reduce max update rate)
  sleep_milis = boost::posix_time::milliseconds(args["sleep-milis"].as<uint>());

  // Initialize the BayesianFactorGraph. It can be primed with an existing BN but can accept remote networks too.
  BayesianNetwork::Ptr network = BayesianNetwork::Ptr(new BayesianNetwork());
  std::string networkString = args["network"].as<std::string>();
  if(networkString != ""){
    try {
     network = BayesianNetwork::Ptr(new BayesianNetwork(BayesianNetworkFactory::fromFile(networkString)));
     SLOG_INFO << "Using network from file: '" << networkString << "'";
    } catch (const Exception &e){
       SLOG_FATAL << "Could not read provided network '" << networkString
                  << " as XMLBIF. Error: " << e.what();
    }
  } else {
     SLOG_INFO << "No network is loaded from File.";
  }

  Source<BayesianNetwork::Ptr>::Ptr networkSource;
  std::string remoteNetworkString = args["network-override"].as<std::string>();
  if(remoteNetworkString == ""){
    networkSource = NoOpSource<BayesianNetwork::Ptr>::Ptr(new NoOpSource<BayesianNetwork::Ptr>(network));
    SLOG_INFO << "No remote network scope provided. Network will not be updated.";
  } else {
    try {
      Source<Shared<rst::bayesnetwork::BayesNetwork>::Ptr>::Ptr rstNetworkSource = RsbSource<rst::bayesnetwork::BayesNetwork>::New(
            rsb::Scope(remoteNetworkString), network->asRstMessage()
            );
      networkSource = SourceAdapterFactory::create<BayesianNetwork::Ptr,Shared<rst::bayesnetwork::BayesNetwork>::Ptr>(rstNetworkSource);
    } catch (const std::invalid_argument& e){
      SLOG_FATAL << "Could not create remote network source with scope: '" << remoteNetworkString
                 << "'. Error: " << e.what();
    }
      SLOG_INFO << "Network will be updated from '" << remoteNetworkString << "'";
  }
  SLOG_INFO << "Using network: " << network->asRstMessage()->DebugString();
  factorGraphSource = SourceAdapterFactory::create<BayesianFactorGraph::Ptr, BayesianNetwork::Ptr>(networkSource);

  // create sensors
  SensorFactory factory = SensorFactory(protoFiles);
  sensors = factory.createSensors(args["config"].as<std::string>());
  SLOG_INFO << "Sensors created #" << sensors.size();
  for(uint i = 0; i < sensors.size(); ++i){
    SLOG_INFO << sensors[i]->toString();
    // filter updates which do not shange state
    sensors[i] = Sensor::Ptr(new ChangeFilterSensor(sensors[i]));
  }

  // Create EvidenceState collector
  evidenceSource = StateEvidenceCollector::New(sensors);

  // create inference
  bayesInference = BayesianInference::Ptr(new BayesianInference());

  // init publishing and remote data
  Shared<rst::classification::ClassificationResultMap>::Ptr defaultSituation(
        rst::classification::ClassificationResultMap::default_instance().New()
        );
  situationPublish = RsbSink<rst::classification::ClassificationResultMap>::Ptr(
        new RsbSink<rst::classification::ClassificationResultMap>(args["scope"].as<std::string>(),defaultSituation));

  // create network publisher
  networkPublish = RsbSink<rst::bayesnetwork::BayesNetwork>::Ptr(
        new RsbSink<rst::bayesnetwork::BayesNetwork>(args["network-scope"].as<std::string>(),network->asRstMessage()));

  // create listener for evidence overrides used for testing
  evidenceOverride = RsbSource<rst::classification::ClassificationResultMap>::Ptr(
        new RsbSource<rst::classification::ClassificationResultMap>(args["evidence-override"].as<std::string>()
        + std::string("/override")));

  // create listener for evidence that should be used for learning
  evidenceLearn = RsbSource<rst::classification::ClassificationResultMap>::Ptr(
        new RsbSource<rst::classification::ClassificationResultMap>(args["evidence-override"].as<std::string>()
        + std::string("/learn")));

  // create evidence publisher
  if(!args["evidence-scope"].as<std::string>().empty()){
    auto evidenceSink = RsbSink<rst::bayesnetwork::BayesNetworkEvidence>::New(
          rsb::Scope(args["evidence-scope"].as<std::string>()));
    publishEvidence = SinkAdapterFactory::
        create<const AspectStateEvidence&,Shared<rst::bayesnetwork::BayesNetworkEvidence>::Ptr>(evidenceSink);
    evidenceSource->addObserver(publishEvidence.get());
  }

  // init evidence writer
  if(!args["evidence-write-file"].as<std::string>().empty()){
    writeEvidence = EvidenceFileSink::Ptr(new EvidenceFileSink(args["evidence-write-file"].as<std::string>()));
    evidenceSource->addObserver(writeEvidence.get());
    evidenceLearn->addObserver(writeEvidence.get());
  }
}

void run(){
  SLOG_INFO << "Starting calculation.";
  // this is used to save old states and for first inference.
  AspectStateEvidence state(evidenceSource->current());
  BayesianFactorGraph::Ptr factorGraph = factorGraphSource->current();
  bool recalculate = true;
  while(!kill_application){
    // timeout for sleep timer
    boost::system_time until = boost::get_system_time() + sleep_milis;
    // get current network state and override
    AspectStateEvidence currentState(evidenceSource->current());
    currentState.override(evidenceOverride->current());
    currentState.override(evidenceLearn->current());
    if(!currentState.equals(&state)){
      state = currentState;
      recalculate = true;
    }
    // get current BayesianFactorGraph
    BayesianFactorGraph::Ptr currentGraph = factorGraphSource->current();
    if(currentGraph.get() != factorGraph.get()){
      // graph has changed
      factorGraph = currentGraph;
      recalculate = true;
      networkPublish->publish(factorGraph->asNetwork().asRstMessage());
    }
    if(recalculate)
    {
      if(factorGraph.get() && !factorGraph->isEmpty()){
        InferenceResult::Ptr result;
        {
          TIMED_SCOPE(inferenceTime, "inference calculation");
          result = bayesInference->infer(factorGraph.get(),&state);
        }
        situationPublish->publish(result->toClassificationResultMap(factorGraph->asNetwork()));
      }
      recalculate = false;
    }
    // if calculation was faster than sleep miliseconds then wait until they passed.
    boost::this_thread::sleep(until);
  }
}

void signal_handler(int num) {
  SLOG_INFO << "Received interupt signal (" << num << ") closing application.";
  if(kill_application){
    SLOG_FATAL << "Repeatedly received interupt signal but application still running.";
    // already called once exit.
    exit(num);
  }
  kill_application = true;
}


int main(int n, char**ppc){
  SITINF_PROGRAM_OPTIONS(
        n,
        ppc,
        "This application uses a BayesNetwork and SensorConfiguration to perform \n"
        "situation recognition. The Recognition results are published and the network \n"
        "variable states can be overriden by a remote simulator.",

        ("network,n",
         boost::program_options::value<std::string>()->default_value("../../share/situation.network.yaml"),
         "The path to the network definition xml file in YAML format.")

        ("config,c",
         boost::program_options::value<std::string>()->default_value("../../share/situation.sensor.yaml"),
         "The path to a network-sensor configuration file.")

        ("proto,p",
         boost::program_options::value<std::string>()->default_value(RST_PROTO_PATH),
         "A path to proto files root folder. For example: /usr/share/rst/proto/")

        ("scope,s",
         boost::program_options::value<std::string>()->default_value("/citec/csra/home/situation/result"),
         "The rsb scope on which the result is to be published.")

        ("network-override,o",
         boost::program_options::value<std::string>()->default_value("/citec/csra/home/situation/override/network"),
         "The rsb scope on from which the internal network can be changed.")

        ("evidence-override,o",
         boost::program_options::value<std::string>()->default_value("/citec/csra/home/situation/override/evidence"),
         "The rsb scope on from which the sensors evidence can be changed.")

        ("network-scope,t",
         boost::program_options::value<std::string>()->default_value("/citec/csra/home/situation/network"),
         "The rsb scope on which the bayes network is to be published.")

        ("evidence-scope,v",
         boost::program_options::value<std::string>()->default_value("/citec/csra/home/situation/evidence"),
         "The rsb scope on which sensor evidence may be published.")

        ("sleep-milis,l",
         boost::program_options::value<uint>()->default_value(100),
         "The minimum time in milliseconds between two recalculations of the network state.")

        ("evidence-write-file,e",
         boost::program_options::value<std::string>()->default_value(""),
         "The file to write observed states to.")

        )

  // initialize everything
  init(program_options);
  // add signal handler
  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

  // create threads for all loops
  Thread t(&run);

  // block until run thread stops
  t.join();

  // cleanup
  evidenceSource->removeObserver(writeEvidence.get());
  SLOG_INFO << "Leaving application.";
  return 0;
}
