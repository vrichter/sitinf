import QtQuick 1.1
import MyTools 1.0

Rectangle{
    id: root
    width: 1080
    height: 400


    Rectangle{
        id: viewport
        width: aspectmodel.width
        height: aspectmodel.height
        color: "whitesmoke"

        transform: Scale {
            origin.x: 0;
            origin.y: 0;
            xScale: 1 / aspectmodel.width * root.width
            yScale: 1 / aspectmodel.width * root.width
        }

        function relx(x){
            return x / aspectmodel.width * width;

        }

        function rely(y){
            return y /aspectmodel.height * height;
        }

        // renders all edges
        Repeater {
            height: parent.height
            width: parent.width
            model: aspectmodel.edgeModel()
            delegate: Line {
                x1: model_end_x
                y1: model_end_y
                x2: model_start_x
                y2: model_start_y
                color: "darkslategray"
                smooth: true
            }
        }

        // renders a node for every aspect in aspectmodel
        Repeater{
            height: parent.height
            width: parent.width
            model: aspectmodel
            delegate: Column{
                //scale: 1.5
                width: childrenRect.width
                height: childrenRect.height
                x: viewport.relx(model_position_x) - width/2
                y: viewport.rely(model_position_y) - height/3

                // node with nodename
                Rectangle {
                    width: 200//nodeText.width
                    height: nodeText.height
                    color: "powderblue"
                    Text {
                        id: nodeText
                        text: model_name
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            console.log("clicked: " + model_name)
                            console.log("model: " + aspectmodel.stateModel(model_name).length)
                        }
                    }
                }

                // aspect states
                ListView{
                    width: 200
                    height: 90
                    model: aspectmodel.stateModel(model_name)
                    delegate: Rectangle {
                        id: background
                        width: 200
                        height: stateText.height
                        state: model_state

                        Text {
                            id: stateText
                            anchors.verticalCenter: background.verticalCenter
                            anchors.left: background.left
                            anchors.leftMargin: 10
                            text: model_name
                        }
                        Text {
                            anchors.verticalCenter: background.verticalCenter
                            anchors.right: background.right
                            anchors.rightMargin: 10
                            text: model_probability.toFixed(2)
                        }

                        states: [
                            State {
                                name: "Inactive"
                                PropertyChanges { target: background; color: "lightgrey"}
                            },
                            State {
                                name: "Active"
                                PropertyChanges { target: background; color: "darkgrey"}
                            },
                            State {
                                name: "Clamped"
                                PropertyChanges { target: background; color: "crimson"}
                            }
                        ]
                    }
                }
            }
        }
    }

   MouseArea {
        id: dragArea
        anchors.fill: parent
        drag.target: viewport
        drag.axis: Drag.XandYAxis
        property int layout: 0
        onDoubleClicked: {
            ++layout;
            if(layout >= aspectmodel.layoutTypes().length){ layout = 0; }
            console.log("setting layout to " + aspectmodel.layoutTypes()[layout] + " of " + aspectmodel.layoutTypes());
            aspectmodel.useLayoutType(aspectmodel.layoutTypes()[layout]);
        }
    }

    WheelArea {
        anchors.fill: parent
        onVerticalWheel: {
            var scale = (delta > 0) ? 1.03 : 0.97;
            viewport.scale = viewport.scale * scale
        }
    }
}
