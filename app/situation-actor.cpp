/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/situation-actor.cpp                                **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/


#include <boost/program_options.hpp>
#include <utils/Utils.h>
#include <sensor/SituationSensor.h>
#include <actor/ScreenActor.h>
#include <io/RsbSource.h>
#include <sensor/DelaySensor.h>
#include <actor/SpeakerActor.h>



#define _LOGGER "situation-actor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::sensor;
using namespace sitinf::actor;
using namespace sitinf::utils;

typedef rst::classification::ClassificationResultMap Situation;

// exit will be set by signalhandler
bool kill_application = false;

SituationSensor::Ptr createIdleSensor(const std::string& room = ""){
  std::string situation = (room.empty()) ? "Situation" : "Situation_" + room;
  std::vector<std::string> sensorStates;
  sensorStates.push_back("Idle");
  sensorStates.push_back("NotIdle");

  std::map<std::string,std::string> stateMap;
  stateMap["Idle"] = "Idle"; // everything else is not idle -> default

  SituationSensor::Ptr sensor = SituationSensor::Ptr(
        new SituationSensor(situation + "Sensor",sensorStates,situation,stateMap,"NotIdle")
        );

  return sensor;
}

std::vector<ScreenSaver::Ptr> createScreenActor(const std::string& room){
  std::vector<ScreenSaver::Ptr> result;
  if(room == "Hallway"){
    result.push_back(ScreenSaver::Ptr(new ScreenSaver("/citec/csra/home/hallway/entrance/screen/ctrl/")));
  } else if (room == "Kitchen"){
    result.push_back(ScreenSaver::Ptr(new ScreenSaver("/citec/csra/home/kitchen/assistance/screen/ctrl/")));
  } else if (room == "Living"){
    result.push_back(ScreenSaver::Ptr(new ScreenSaver("/citec/csra/home/living/hometheater/screen/ctrl/")));
    result.push_back(ScreenSaver::Ptr(new ScreenSaver("/citec/csra/home/living/lounge/screen/ctrl/")));
    result.push_back(ScreenSaver::Ptr(new ScreenSaver("/citec/csra/home/living/window/screen/ctrl/")));
    result.push_back(ScreenSaver::Ptr(new ScreenSaver("/citec/csra/home/living/info/screen/ctrl/")));
  } else if (room == "Robotroom"){
    result.push_back(ScreenSaver::Ptr(new ScreenSaver("/citec/csra/robotroom/screen/ctrl/")));
   } else if (room == "Control") {
    result.push_back(ScreenSaver::Ptr(new ScreenSaver("/citec/csra/control/introspection/screen/ctrl/")));
    result.push_back(ScreenSaver::Ptr(new ScreenSaver("/citec/csra/control/bco/screen/ctrl/")));
    result.push_back(ScreenSaver::Ptr(new ScreenSaver("/citec/csra/control/vdemo/screen/ctrl/")));
  }
  return result;
}

void signal_handler(int num) {
  SLOG_INFO << "Received interupt signal (" << num << ") closing application.";
  if(kill_application){
    SLOG_FATAL << "Repeatedly received interupt signal but application still running.";
    // already called once exit.
    exit(num);
  }
  kill_application = true;
}

int main(int n, char**ppc){
  SITINF_PROGRAM_OPTIONS(
        n,
        ppc,
        "This application uses results of a situation recognition to control devices.",

        ("scope,s",
         boost::program_options::value<std::string>()->default_value("/citec/csra/home/situation/result"),
         "The rsb scope on which the result is published.")

        ("idle-time,i",
         boost::program_options::value<double>()->default_value(600.),
         "The time until idle is assumed in seconds.")

        )

  // only one situation is needed for SituationSensors.
  RsbSource<Situation>::Ptr situation = RsbSource<Situation>::Ptr(
        new RsbSource<Situation>(program_options["scope"].as<std::string>())
      );

  std::vector<Sensor::Ptr> sensors;

  // screen savers
  std::vector<ScreenSaver::Ptr> screenSavers;
  for(auto room : {"Hallway", "Kitchen", "Living", "Bath", "Robotroom", "Control"}){
    auto sensor = createIdleSensor(room);
    auto delayedSensor = DelaySensor::Ptr(new DelaySensor(sensor,program_options["idle-time"].as<double>()*1000.,"Idle")); // 10 minutes
    auto screenActors = createScreenActor(room);
    for(auto saver : screenActors){
      delayedSensor->addObserver(saver.get());
    }
    situation->addObserver(sensor.get());
    sensors.push_back(delayedSensor);
    screenSavers.insert(screenSavers.end(), screenActors.begin(), screenActors.end());
  }

  // initialize all speaker actuators
  auto apartmentSensor = createIdleSensor();
  situation->addObserver(apartmentSensor.get());
  SpeakerKeepAlive keepAlive("/citec/csra/home/audio/control/speakers/switchon", "NotIdle", 60000); // 1 minute
  apartmentSensor->addObserver(&keepAlive);

  SLOG_INFO << "Ready.";

  Thread::sleep(boost::posix_time::pos_infin);

  SLOG_INFO << "Leaving application.";
  return 0;
}
