/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/validate-yaml-file.cpp                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/File.h>

#include <boost/program_options.hpp>
#include <utils/Macros.h>
#include <yaml-cpp/yaml.h>
#include <utils/Yavl.h>

#define _LOGGER "yaml-validate"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;

#define PRINT(text,depth)\
{\
  for(int i = 0; i < depth; ++i){\
    std::cout << "  ";\
  }\
  std::cout << text << std::endl;\
}

void printNode(YAML::Node node, int depth=0){
  switch(node.Type()){
    case YAML::NodeType::Null:
      PRINT("null",depth);
      break;
    case YAML::NodeType::Scalar:
      PRINT(node.as<std::string>(),depth);
      break;
    case YAML::NodeType::Undefined:
      PRINT("undefined",depth);
      break;
    case YAML::NodeType::Sequence:
      PRINT("- ",depth);
      for(YAML::Node::const_iterator it = node.begin(); it != node.end(); ++it){
        printNode(*it,depth+2);
      }
      break;
    case YAML::NodeType::Map:
      for(YAML::Node::const_iterator it = node.begin(); it != node.end(); ++it){
        PRINT(it->first.as<std::string>() << ": ",depth);
        printNode(it->second, depth+2);
      }
      break;
  }
}

int main(int n, char**ppc){
  SITINF_PROGRAM_OPTIONS(
        n,
        ppc,
        "This application reads a YAML file as input and prints the parsed data or errors to the console.",

        ("file,f",
         boost::program_options::value<std::string>()->default_value("../../share/situation.sensor.yaml"),
         "The file to be processed.")

        ("grammar,g",
         boost::program_options::value<std::string>()->default_value(""),
         "The grammar file to use to validate the YAML document. If not provded every valid yaml is accepted.")


        )

  std::string document_name = program_options["file"].as<std::string>();
  std::string grammar_name = program_options["grammar"].as<std::string>();

  YAML::Node document = YAML::LoadFile(document_name);

  if(!grammar_name.empty()){
    YAML::Node grammar = YAML::LoadFile(grammar_name);
    YAVL::Validator validator(grammar,document);
  }

  std::cout << YAML::Dump(document) << std::endl;

  return 0;
}
