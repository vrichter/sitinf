import QtQuick 1.0

Item {
    id: dropdownview
    height: parent.height
    width: parent.width
    property variant model: notset
    property int step: 50
    property string currentState: "not-set"
    state: ""

    function foreground(){
        z += step;
    }

    function background(){
        z -= step;
    }

    Rectangle {
        id: dropdown
        height: parent.height;
        width: parent.width;
        color: "lightsteelblue"

        Text {
            id: chosenItemText
            anchors.horizontalCenter: parent.horizontalCenter;
            anchors.verticalCenter: parent.verticalCenter;
            text: dropdownview.currentState
        }

        MouseArea {
            anchors.fill: parent;
            onClicked: {
                dropdownview.foreground()
                dropdownview.state = (dropdownview.state === "dropDown") ? "" : "dropDown";
            }
        }
    }

    Rectangle {
        id: dropdownList
        width: dropdown.width;
        height: 0;
        property int fullheight: dropdownview.height * dropdownview.model.length
        clip: true;
        anchors.verticalCenter: parent.verticalCenter;
        anchors.left: parent.left;
        color: "lightgray"
        z: dropdown.z+1;

        ListView {
            id: listView
            width: dropdownList.width
            height: dropdownList.fullheight
            model: dropdownview.model


            delegate: Item{
                width: listView.width;
                height: listView.height / listView.model.length;

                Text {
                    text: modelData
                    anchors.horizontalCenter: parent.horizontalCenter;
                    anchors.verticalCenter: parent.verticalCenter;
                }

                MouseArea {
                    anchors.fill: parent;
                    onClicked: {
                        dropdownview.state = "";
                        dropdownview.background()
                        dropdownview.currentState = modelData;
                        chosenItemText.text = modelData;
                    }
                }
            }
        }
    }

    states: State {
        name: "dropDown";
        PropertyChanges {
            target: dropdownList;
            height: dropdownList.fullheight
        }
    }
}
