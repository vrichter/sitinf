/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/generate-from-description.cpp                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <rsb/Factory.h>
#include <rsb/Listener.h>
#include <rsb/Scope.h>

#include <network/BayesianNetwork.h>
#include <inference/BayesianFactorGraph.h>
#include <network/BayesianNetworkFactory.h>
#include <io/AnyTypeParticipantConfig.h>
#include <io/ProtoFilesImporter.h>
#include <io/File.h>

#include <boost/program_options.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string/replace.hpp>

#include <utils/Exception.h>
#include <utils/Macros.h>

#define _LOGGER "convert-from-xml"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;
using namespace sitinf::network;
using namespace sitinf::inference;

void printGraphviz(BayesianNetwork::Ptr network);
std::string replaceVariables(const std::string& src, const std::vector<BayesianNetwork::Variable>& variables);

int main(int n, char**ppc){
  SITINF_PROGRAM_OPTIONS(
        n,
        ppc,
        "This application takes a BayesNetwork in XMLBIF or YAML format and can print different "
        "representations of it to std::out.",

        ("network,n",
         boost::program_options::value<std::string>()->default_value("../../share/situation.network.yaml"),
         "The path to the network definition xml or yaml file. The input format is inferred from the postfix.")

        ("output-format,o",
         boost::program_options::value<std::string>()->default_value("xml"),
         "(xml | yaml | json | fg | digraph | dot | adot) The format in which the network should be printed."
         "'xml' print the BayesNetwork as XMLBIF."
         "'yaml' print the BayesNetwork as YAML(BIF)."
         "'json' print the rst::bayesnetwork::Bayesnetwork representation as json."
         "'fg' print a FactorGraph representation of the network."
         "'digraph' print a graphviz digraph representation of the BayesNetwork."
         "'dot' print a graphviz dot representation of the a FactorGraph created from the BayesNetwork."
         "'adot' print a graphviz representation of the FactorGraph created from the BayesNetwork with "
         "the  variable lables from the BayesNetwork.")

        )

  BayesianNetwork network = BayesianNetworkFactory::fromFile(program_options["network"].as<std::string>());

  std::string output = program_options["output-format"].as<std::string>();
  if(output == "xml"){
    // print own XMLBIF version of network
    SLOG_INFO << "Printing network " << &network << " as XMLBIF";
    BayesianNetworkFactory::toXml(std::cout, network);
  } else if (output == "json") {
    SLOG_INFO << "Printing network " << &network << " as json";
    BayesianNetworkFactory::toJson(std::cout, network);
  } else if (output == "yaml") {
    SLOG_INFO << "Printing network " << &network << " as yaml";
    BayesianNetworkFactory::toYaml(std::cout, network);
  } else if (output == "fg") {
    // create and print the factor graph
    SLOG_INFO << "Printing network " << &network << " as FactorGraph";
    BayesianFactorGraph fg(network);
    std::cout << fg.asFactorGraph() << std::endl;
  } else if (output == "digraph") {
    // create a graphviz representation and print it.
    SLOG_INFO << "Printing network " << &network << " as digraph";
    BayesianNetworkFactory::toDot(std::cout, network);
  } else if (output == "dot") {
    // create a FactorGraph and print dot
    SLOG_INFO << "Printing network " << &network << " as fg-dot";
    BayesianFactorGraph fg(network);
    fg.asFactorGraph().printDot(std::cout);
   } else if (output == "adot") {
    // create a FactorGraph and get its dot representation as input stream
    SLOG_INFO << "Creating fg-dot of network " << &network << ".";
    BayesianFactorGraph fg(network);
    std::ostringstream ostream;
    fg.asFactorGraph().printDot(ostream);
    // replace variables with names from network a&nd print the result.
    std::cout << replaceVariables(ostream.str(),network.variables()) << std::endl;
  } else {
    SLOG_WARNING << "Parameter output-format = '" << output << "' not known.";
    return 1;
  }

  return 0;
}

std::string replaceVariables(const std::string& src, const std::vector<BayesianNetwork::Variable>& variables){
  std::string ret = src;
  // replace backwards so v10 will not be replacced by v1
  for(sint i = (sint) variables.size()-1; i >= 0; --i){
    SLOG_TRACE << "replacing " << "v" << i << " with " << variables.at(i).m_Name;
    std::stringstream v;
    v  << "v" << i;
    boost::regex re(v.str());
    ret = boost::regex_replace(ret,re,variables.at(i).m_Name);
  }
  // more graph transforming: set variable node size vor variables and disable overlap in graph
  boost::replace_all(ret,"node[shape=circle,width=0.4,fixedsize=true];","overlap = false;\nnode[shape=ellipse];");
  return ret;
}
