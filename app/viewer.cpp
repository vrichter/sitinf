/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/viewer.cpp                                         **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <qt/QtQuick1ApplicationViewer.h>
#include <QApplication>
#include <QtDeclarative/QDeclarativeEngine>
#include <QtDeclarative/QDeclarativeContext>

#include <qt/WheelArea.h>

#include <QDebug>

#include <boost/program_options.hpp>
#include <rsb/Factory.h>
#include <io/RsbNetworkReceiver.h>
#include <io/RsbSituationReceiver.h>
#include <qt/AspectModel.h>
#include <qt/ListModelMap.h>
#include <qt/StateModel.h>
#include <render/GraphLayout.h>
#include <utils/Exception.h>
#include <qt/SituationUpdater.h>
#include <qt/NetworkUpdater.h>

#define _LOGGER "viewer"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::qt;
using namespace sitinf::utils;

QApplication* appP;

void signal_handler(int num){
  static bool catched = false;
  if(!catched){
    SLOG_INFO << "Received interupt signal (" << num << ") closing application.";
    catched = true;
    appP->quit();
  } else {
    SLOG_FATAL << "Repeatedly received interupt signal but application still running.";
    exit(num);
  }
}

int main(int n, char **ppc)
{
  SITINF_PROGRAM_OPTIONS(
        n,
        ppc,
        "This application connects to a situation recognition instance via rsb. It can \n"
        "visualize its BayesNetwork and the current recognition results.",

        ("scope-network,n",
         boost::program_options::value<std::string>()->default_value("/citec/csra/home/situation/network"),
         "The rsb scope on which the Bayesnetwork can be found.")

        ("scope-situation,s",
         boost::program_options::value<std::string>()->default_value("/citec/csra/home/situation/result"),
         "The rsb scope on which the situation can be observed.")

        )

  // create an AspectModel
  AspectModel aspectModel;

  // create network uppdater and connect it to aspectState
  NetworkUpdater networkUpdater(program_options["scope-network"].as<std::string>());
  auto networkUpdatePrinter = networkUpdater.createCallbackObserver(
        [&](boost::shared_ptr<rst::bayesnetwork::BayesNetwork>){SLOG_INFO << "Received a new nework.";});
  networkUpdater.addObserver(&networkUpdatePrinter);
  QObject::connect(&networkUpdater, SIGNAL(networkChanged(utils::Shared<rst::bayesnetwork::BayesNetwork>::Ptr)),
                   &aspectModel, SLOT(updateNetwork(utils::Shared<rst::bayesnetwork::BayesNetwork>::Ptr)));
  // create situation updater and connect its signal to aspectmodels updateStates
  SituationUpdater stateUpdater(program_options["scope-situation"].as<std::string>());
  auto stateUpdatePrinter = stateUpdater.createCallbackObserver(
        [&](boost::shared_ptr<rst::classification::ClassificationResultMap>){SLOG_INFO << "Received a new Situation.";});
  stateUpdater.addObserver(&stateUpdatePrinter);
  QObject::connect(&stateUpdater, SIGNAL(situationChanged(utils::Shared<rst::classification::ClassificationResultMap>::Ptr)),
                   &aspectModel, SLOT(updateStates(utils::Shared<rst::classification::ClassificationResultMap>::Ptr)));

  QApplication app(n, ppc);

  // register mouse wheel
  qmlRegisterType<WheelArea>("MyTools", 1, 0, "WheelArea");

  QtQuick1ApplicationViewer viewer;

  // register models
  viewer.rootContext()->setContextProperty("aspectmodel",&aspectModel);

  viewer.addImportPath(QLatin1String("modules"));
  viewer.setOrientation(QtQuick1ApplicationViewer::ScreenOrientationAuto);
  viewer.setMainQmlFile(QLatin1String("qrc:///viewer.qml"));
  viewer.setResizeMode(QDeclarativeView::SizeViewToRootObject);
  //viewer.setResizeMode(QDeclarativeView::SizeRootObjectToView);
  viewer.showExpanded();
  appP = &app;
  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);
  int result = app.exec();
  SLOG_INFO << "Leaving application.";
  return result;
}
