/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/learner-comparison.cpp                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <algorithm>

#include <boost/program_options.hpp>

#include <inference/ParameterEstimation.h>
#include <inference/SimpleParameterEstimation.h>
#include <io/EvidenceFileSource.h>
#include <utils/Exception.h>
#include <utils/Types.h>
#include <utils/Macros.h>
#include <utils/ThreadPool.h>
#include <network/BayesianNetworkFactory.h>

#define _LOGGER "learner-comparison"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::io;
using namespace sitinf::network;
using namespace sitinf::inference;

std::vector<uint> parse_batches(const std::string& arg){
  return split_parse<uint>(arg);
}

std::vector<real> parse_stepsizes(const std::string& arg){
  return split_parse<real>(arg);
}

void printSpaces(uint have, uint need){
  while(have < need){
    std::cout << " ";
    ++have;
  }
}

utils::real calc_sum_weight(const std::vector<WeightedAspectStateEvidence>& data){
  utils::real result = 0.;
  for (auto d : data) {
    result += d.weight();
  }
  return result;
}

void prettyPrint(const std::map<std::string,EstimationResult> results){
  uint titleLength = 0;
  for (auto result : results){
    if(result.first.size() > titleLength) titleLength = result.first.size();
  }

  for (auto result : results){
    std::cout << result.first << " learning";
    printSpaces(result.first.size(),titleLength);
    for (auto likelihood : result.second.estimationLogLikelihoods){
      std::cout << std::fixed << "\t" << likelihood;
    }
    std::cout << "\n" << result.first << " testing ";
    printSpaces(result.first.size(),titleLength);
    for (auto likelihood : result.second.testLogLikelihoods){
      std::cout << std::fixed << "\t" << likelihood;
    }
    std::cout << std::endl;
  }

}

int main(int n, char**ppc){
  SITINF_PROGRAM_OPTIONS(
    n,
    ppc,
    "This application can be used to evaluate different parameter estimation algorithms against each other.",

    ("network,n",
     boost::program_options::value<std::string>()->default_value("../../share/situation.network.yaml"),
     "The path to the source network definition xml file in YAML format.")

    ("input-data,d",
     boost::program_options::value<std::string>()->default_value(""),
     "The evidence file holding correspoinding network evidence for learning.")

    ("batch-size,b",
     boost::program_options::value<std::string>()->default_value("0"),
     "The batch size to be used for StepwiseEM. Multiple values can be separated via ','.")

    ("step-size,s",
     boost::program_options::value<std::string>()->default_value("0.6"),
     "The step reduction size to be used for StepwiseEM. Multiple values can be separated via ','.")

    ("iterations,i",
     boost::program_options::value<utils::uint>()->default_value(30),
     "The maximum amount of learning iterations.")

    ("test-set-size,t",
     boost::program_options::value<utils::real>()->default_value(0.25),
     "The size of the test set, extracted from the input data. This efectively reduces the learning data set size. "
     "values 0 <= s < 1 are interpreted as proportion of the data set. "
     "0 means no test set. "
     "0.25 means test set is 25% of the training set. ")

    ("seed",
     boost::program_options::value<int>(),
     "The seed to use for data randomization. If not passed a pseudo-random seed will be used.")

    ("jobs,j",
     boost::program_options::value<utils::uint>()->default_value(std::thread::hardware_concurrency()),
     "How many threads to use for calculations. Default is the machines default - if can be retrieved.")
  )

  real dataSplit = program_options["test-set-size"].as<real>();
  SASSERT_THROW(dataSplit >= 0., IllegalArgumentException, "data-split can not be smaller than 0");
  SASSERT_THROW(dataSplit < 1., IllegalArgumentException, "data-split can not be greater or equal to 1");

  // load and re-initialze network
  BayesianNetwork network = BayesianNetworkFactory::fromFile(program_options["network"].as<std::string>()).uniformDistributed(1e-3,0);

  std::vector<WeightedAspectStateEvidence> data;
  uint emptyEvidence = 0;
  try {
    PullSource<WeightedAspectStateEvidence>::Ptr evidence(new EvidenceFileSource(program_options["input-data"].as<std::string>()));
    while(true){
      try{
        WeightedAspectStateEvidence e = network.applicableEvidence(evidence->next());
        if(!e.currentState().empty()){
          data.push_back(e);
        } else {
          ++emptyEvidence;
        }
      } catch (NoMoreDataAvailableException& e){
        // Source is empty leave this loop
        break;
      }
    }
  } catch (Exception &e){
    std::cout << "Could not load evidence from '" << program_options["input-data"].as<std::string>() << "'\n"
              << "Error: " << e.what() << std::endl;
    exit(-1);
  }

  SLOG_TRACE << "Working with a data set of " << data.size() << " examples. Ignored " << emptyEvidence;

  utils::real sum_weight = calc_sum_weight(data);
  // seed the random number generator
  if(program_options.count("seed")){
    std::srand(program_options["seed"].as<int>());
  } else {
    std::srand(std::time(0));
  }
  SLOG_TRACE << "RANDOM_NUMBERS: " << std::rand() << " - " << std::rand() << " - " << std::rand();
  // shuffle data
  std::random_shuffle(data.begin(),data.end());

  // split the data into different sets
  std::vector<WeightedAspectStateEvidence> learn;
  std::vector<WeightedAspectStateEvidence> test;
  if(dataSplit == 0.){
    // no test set
    learn = data;
  } else {
#if 1 // weighted
    // find split position
    utils::real learn_weight = sum_weight * (1.-dataSplit);
    uint split_position = 0;
    for (const WeightedAspectStateEvidence& e : data){
      learn_weight -= e.weight();
      if(learn_weight > 0) {
        split_position++;
      } else {
        break;
      }
    }
    SASSERT_THROW(split_position < data.size()-1, IllegalArgumentException,
                  "Can not learn from " << data.size() << " examples with a test set size of " << split_position);
    test.reserve(data.size() - split_position + 1);
    learn.reserve(split_position);
    test.insert(test.end(),data.begin(),data.begin()+split_position-1);
    WeightedAspectStateEvidence t = data[split_position];
    t.weight(-learn_weight); // add to test with overhanging weight
    test.push_back(t);
    WeightedAspectStateEvidence l = data[split_position];
    l.weight(l.weight() - t.weight());
    learn.push_back(l);
    learn.insert(learn.end(),data.begin()+split_position+1,data.end());

#else // counted
    uint testSize = (dataSplit < 1.) ? data.size() * dataSplit : dataSplit;
    SASSERT_THROW(testSize < data.size(), IllegalArgumentException,
                  "Can not learn from " << data.size() << " examples with a test set size of " << testSize);
    test.reserve(testSize);
    learn.reserve(data.size() - testSize);
    test.insert(test.end(),data.begin(),data.begin()+testSize);
    learn.insert(learn.end(),data.begin()+testSize,data.end());
#endif
  }
  // to save some memory
  data.clear();
  data.shrink_to_fit();
  SLOG_TRACE << "Data set split into " << learn.size() << " learning examples and "
              << test.size() << " test examples (" << "weights: " << calc_sum_weight(learn)
              << "/" << calc_sum_weight(test) << ")";

  // create parameter estimation algorithms:
  std::map<std::string,ParameterEstimation::Ptr> m_Algorithms;
  //TODO: create multiple,diffent estimations

  m_Algorithms["simple estimation"] = SimpleParameterEstimation::New(network);
  m_Algorithms["em estimation"] = EMBasedParameterEstimation::New(network);

  auto batches = parse_batches(program_options["batch-size"].as<std::string>());
  auto stepsizes = parse_stepsizes(program_options["step-size"].as<std::string>());
  dai::PropertySet config = EMAlgFactory::defaultConfig();
  dai::PropertySet termination = config.getAs<dai::PropertySet>("termination_properties");
  termination.set("max_iters",(size_t)program_options["iterations"].as<utils::uint>());
  termination.set("log_z_tol",(dai::Real)0.);
  config.set("termination_properties",termination);
  config.set("estimation",(std::string)"StepwiseCondProbEstimation");
  dai::PropertySet estProps = config.getAs<dai::PropertySet>("estimation_properties");
  for (auto stepsize : stepsizes){
    estProps.set("alpha",(dai::Real)stepsize);
    config.set("estimation_properties",estProps);
    for (auto batch : batches){
      if(batch <= learn.size()){
        std::stringstream name; name << "em estimation (" << batch << "," << stepsize << ")";
        m_Algorithms[name.str()] = StepwiseEMBasedParameterEstimation::New(network,config,batch);
      }
    }
  }

  uint threads = program_options["jobs"].as<uint>();
  SLOG_INFO << "Calculating with " << threads << " threads.";

  ThreadPool<EstimationResult>::TaskSet tasks;
  for(auto alg : m_Algorithms){
    auto func = [alg,&learn,&test] () -> EstimationResult { return alg.second->estimateParameters(learn,test); };
    tasks.push_back(ThreadPool<EstimationResult>::Task(func));
  }
  ThreadPool<EstimationResult> pool(tasks,threads,1000);
  std::vector<EstimationResult> estimationResults = pool.runBlocking();

  std::map<std::string,EstimationResult> results;
  auto resIt = estimationResults.begin();
  for(auto alg : m_Algorithms){
    results[alg.first] = *resIt++;
  }

  // for pretty printing
  prettyPrint(results);

}
