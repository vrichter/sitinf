/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/em-config-from-xml.cpp                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>
#include <iostream>

#include <network/BayesianNetwork.h>
#include <network/BayesianNetworkFactory.h>

#define _LOGGER "em-config-from-xml"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::network;

int main(int n, char**ppc){
  SITINF_PROGRAM_OPTIONS(
    n,
    ppc,
    "This application prints a default/example .em configuration for parameter \n"
    "learning on a provided BayesNetwork. For further information see \n"
    "https://staff.fnwi.uva.nl/j.m.mooij/libDAI/doc/fileformats.html#fileformats-factorgraph-factor",

    ("network,n",
     boost::program_options::value<std::string>()->default_value("../../share/situation.network.yaml"),
     "The path to the network definition xml file in YAML format.")

    ("maximization-steps,m",
     boost::program_options::value<uint>()->default_value(1),
     "The amount of maximization steps to be performed during expectation maxmization.")

    ("estimation-class,e",
     boost::program_options::value<std::string>()->default_value("CondProbEstimation"),
     "The class to be used for probability estimation.")

    ("pseudo-count,p",
     boost::program_options::value<uint>()->default_value(1),
     "The used pseudo-count.")
  )

  BayesianNetwork network = BayesianNetworkFactory::fromFile(program_options["network"].as<std::string>());
  uint steps = program_options["maximization-steps"].as<uint>();

  // first print number of steps followed by an empty line
  SLOG_TRACE << "Creating " << steps << " steps.";
  std::cout << steps << "\n\n";
  for(uint i = 0; i < steps; ++i){
    SLOG_TRACE << "Step #" << i;
    SLOG_TRACE << "Maximization Step block for " << network.variables().size() << " variables";
    std::cout << network.variables().size() << "\n";
    for(const BayesianNetwork::Definition& definition : network.definitions()){
      std::stringstream vars;
      vars << "[";
      for (uint v = 0; v < definition.m_Variables.size(); ++v){
        vars << definition.m_Variables.at(v);
        if(v == 0){
          if(definition.m_Variables.size() > 1){
            vars << "|";
          }
        } else if (v+1 >= definition.m_Variables.size()){
          // do nothing
        } else {
          vars << ",";
        }
      }
      vars << "]";
      SLOG_TRACE << "Shared parameters for variable: " << vars.str();
      std::cout << program_options["estimation-class"].as<std::string>() << " ["
          << "target_dim=" << network.variableDimension(definition.m_Variables.front()) << ","
          << "total_dim=" << definition.m_Table.size() << ","
          << "pseudo_count=" << program_options["pseudo-count"].as<uint>() << "]\n";
      std::cout << "1\n";
      std::cout << network.variableId(definition.m_Variables.front());
      for(std::string variable : definition.m_Variables){
        std::cout << " " << network.variableId(variable);
      }
      std::cout << std::endl;
    }
  }
}
