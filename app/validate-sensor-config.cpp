/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/validate-sensor-config.cpp                         **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/File.h>

#include <boost/program_options.hpp>
#include <utils/Macros.h>
#include <yaml-cpp/yaml.h>
#include <sensor/SensorFactory.h>

#define _LOGGER "validate-sensor-config"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;
using namespace sitinf::sensor;



int countManifestations(YAML::Node n){
    if(n){
      if(n.IsSequence()){
        return n.size();
      } else if(n.IsMap()){
        int result = 1;
        for(auto it : n){
          result *= countManifestations(it.second);
        }
        return result;
      } else {
        return 1;
      }
    } else {
      return 1;
    }
}

int getSensorCount(YAML::Node n){
  int expected_count = 0;
  for(auto it : n){
    if(it["sensor_type"] && (it["sensor_type"].as<std::string>() == "MutualGazeSensor")){
      auto config = it["config"];
      return 4 * config["persons"].as<int>();
    }
    int inner = 1;
    auto config = it["config"];
    if(config["sensors"]){
      inner = getSensorCount(config["sensors"]);
    }
    expected_count += inner * countManifestations(it["manifestations"]);
  }
  return expected_count;
}


int main(int n, char**ppc){
  SITINF_PROGRAM_OPTIONS(
        n,
        ppc,
        "This application generates sensors from a sensor configuration in yaml-format.",

        ("file,f",
         boost::program_options::value<std::string>()->default_value("../../share/situation.sensor.yaml"),
         "The file to be processed.")

        ("proto,p",
         boost::program_options::value<std::string>()->default_value(RST_PROTO_PATH),
         "A path to proto files root folder. For example: /usr/share/rst/proto/")

        )

  YAML::Node node = YAML::LoadFile(program_options["file"].as<std::string>());

  SLOG_INFO << "Create sensors from config: " << YAML::Dump(node);

  SensorFactory factory(ProtoFilesImporter::Ptr(new ProtoFilesImporter(program_options["proto"].as<std::string>())));
  std::vector<Sensor::Ptr> sensors = factory.createSensors(node);

  SLOG_INFO << "Created " << sensors.size() << " sensors";

  // calculate expected count == sum(manifestations)
  int expected_count = getSensorCount(node["sensors"]);

  SLOG_INFO << "Expected " << expected_count << " sensors";

  std::cout <<  "Created #" << sensors.size() << " sensors. Expected: " << expected_count << "." << std::endl;

  return 0;
}
