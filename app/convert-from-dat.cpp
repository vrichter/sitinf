/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/convert-from-dat.cpp                               **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <boost/program_options.hpp>

#include <io/File.h>
#include <utils/Exception.h>
#include <utils/Macros.h>
#include <boost/algorithm/string.hpp>
#include <rst/bayesnetwork/BayesNetworkEvidence.pb.h>
#include <network/BayesianNetworkFactory.h>

#define _LOGGER "convert-from-dat"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;
using namespace sitinf::network;

static const uint MAX_VARNAME_LENGTH = 1024; // 1KB
static const uint MAX_LINE_LENGTH = 1024*1024; // 1MB

void printEvidence(BayesianNetwork& network, ReadFile& data);
void printTab(BayesianNetwork& network, ReadFile& data);

int main(int n, char**ppc){
  SITINF_PROGRAM_OPTIONS(
        n,
        ppc,
        "This application takes a samiam sample data file and prints the corresponding data in the output format.",

        ("data-file,d",
         boost::program_options::value<std::string>()->default_value(""),
         "The path to the sample data file samiam sample format.")

        ("network,n",
         boost::program_options::value<std::string>()->default_value("../../share/Sprinkler.network.yaml"),
         "The path to the corresponding network in XMLBIF/YAML format. Needed for variable id reslving.")

        ("format,f",
         boost::program_options::value<std::string>()->default_value("evidence"),
         "The output format of data. This can currently be libdais 'tab' or sitinfs 'evidence'.")

        )

  std::string data_filename = program_options["data-file"].as<std::string>();
  std::string network_filename = program_options["network"].as<std::string>();
  // check whether files exist
  try {
    File(data_filename,false,true,File::regular);
    SLOG_INFO << "Data file can be opened.";
    File(network_filename,false,true,File::regular);
    SLOG_INFO << "Network file can be opened.";
  } catch (IOException& e){
    SLOG_ERROR << "Can't open file: " << e.what();
    return 1;
  }

  // acquire data
  BayesianNetwork network = BayesianNetworkFactory::fromFile(network_filename);
  ReadFile data(data_filename);

  if(program_options["format"].as<std::string>() == "evidence"){
    printEvidence(network,data);
  } else if (program_options["format"].as<std::string>() == "tab"){
    printTab(network,data);
  } else {
    STHROW(IOException,"Parameter 'format' must be either 'evidence' or 'tab'");
  }
}

void printEvidence(BayesianNetwork& network, ReadFile& data){
  std::string buffer;
  /*
   * The first line contains the variable names.
   * Create a lookup table column -> variable;
   */
  std::vector<std::string> variables;
  std::getline(data, buffer);
  boost::split(variables,buffer,boost::is_any_of(","));

  /*
   * for every data line create a BayesianNetworkEvidence and print it
   */
  std::vector<std::string> states;
  rst::bayesnetwork::BayesNetworkEvidence evidence;
  states.reserve(variables.size());
  uint line = 1;
  while(!data.eof()){
    ++line;
    buffer.clear();
    states.clear();
    evidence.Clear();
    std::getline(data,buffer);
    boost::split(states,buffer,boost::is_any_of(","));
    if(variables.size() != states.size()){
      SLOG_WARNING << "Line " << line << ": ignored because of unexpected number of columns " << states.size();
      continue;
    } else {
      for(uint i = 0; i < variables.size(); ++i){
        if(states[i] != "N/A"){
          auto variableState = evidence.add_observations();
          variableState->set_variable(variables[i]);
          variableState->set_state(states[i]);
        }
      }
      std::cout << evidence.ShortDebugString() << std::endl;
    }
  }
}

void printTab(BayesianNetwork& network, ReadFile& data){
  char buffer[MAX_LINE_LENGTH];
  /*
   * The first line contains the variable names.
   * 1. Print the corresponding ids.
   * 2. Create a lookup table array[number] = map<state,statenumber>;
   */
  std::vector<std::map<std::string,uint> > table;
  data.getline(buffer,MAX_LINE_LENGTH);
  SLOG_TRACE << "First line of dat file: " << buffer;
  std::istringstream variableLine(buffer);
  char variableName[MAX_VARNAME_LENGTH];
  do {
    variableLine.getline(variableName,MAX_VARNAME_LENGTH,',');
    SLOG_TRACE << "Got variable: " << variableName;
    try {
      std::map<std::string,uint> lut;
      for (uint i = 0; i < network.variable(variableName).m_Outcomes.size(); ++i){
        lut[network.variable(variableName).m_Outcomes[i]] = i;
      }
      table.push_back(lut);
      std::cout << network.variableId(variableName);
    } catch (NotFoundException& e){
      std::cout << "Variable '" << variableName << "' defined in first line of data "
                << "but does not exist in BayesNetwork" << std::endl;
      std::exit(1);
    }
    if(!variableLine.eof()){
      std::cout << "\t";
    }
  } while (!variableLine.eof());
  std::cout << "\n" << std::endl;

  /*
   * For every data line print the converted variable states.
   */
  char stateName[MAX_VARNAME_LENGTH];
  data.getline(buffer,MAX_LINE_LENGTH);
  uint data_line = 2;
  while(!data.eof()) {
    std::istringstream statesLine(buffer);
    uint variable = 0;
    do {
      statesLine.getline(stateName,MAX_VARNAME_LENGTH,',');
      if(variable >= table.size()){
        SLOG_FATAL << "Line " << data_line << " '" << buffer << "' contains more variables than defined ("
                   << table.size() << ").";
      }
      if(std::string(stateName) != "N/A"){
        if(table[variable].find(stateName) == table[variable].end()){
          SLOG_FATAL << "Variable state '" << stateName << "' in line " << data_line << ", column " << variable+1
                     << " is unknown by corresponding variable";
        }
        std::cout << table[variable][stateName];
      }
      if(!statesLine.eof()){
        std::cout << "\t";
      }
      ++variable;
    } while (!statesLine.eof());
    std::cout << std::endl;
    data.getline(buffer,MAX_LINE_LENGTH);
    ++data_line;
  };
}
