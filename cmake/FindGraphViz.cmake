#*********************************************************************
#**                                                                 **
#** File   : cmake/FindGraphViz.cmake                               **
#** Authors: Viktor Richter                                         **
#**                                                                 **
#**                                                                 **
#** GNU LESSER GENERAL PUBLIC LICENSE                               **
#** This file may be used under the terms of the GNU Lesser General **
#** Public License version 3.0 as published by the                  **
#**                                                                 **
#** Free Software Foundation and appearing in the file LICENSE.LGPL **
#** included in the packaging of this file.  Please review the      **
#** following information to ensure the license requirements will   **
#** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
#**                                                                 **
#*********************************************************************

# Output:
#   NAME_FOUND           - shows if the package was found
#   HAVE_NAME            - shows if the package was found too
#   NAME_INCLUDE_DIRS    - include directory
#   NAME_LIBRARIES       - all found libraries
#   NAME_VERSION         - the version of the package if found
#   HAVE_NAME            - this variable is added to the definitions
# Global input:
#  PKG_SEARCH_PATHS         - contains directories, which should be searched
#  ARCH_DEPENDENT_LIB_PATHS - suffixes should be used for the libraries search
include(FindDependency)

# this is a little inconvenient. libgvc needs libgraph for versions <= 2.30 and libcgraph for greater versions
find_dependency(NAME GRAPHVIZ_VERSION
  HEADERS "graphviz/graphviz_version.h"
  PATHS "${GRAPHVIZ_ROOT}"
)
if(GRAPHVIZ_VERSION_FOUND)
  execute_process(COMMAND cat "${GRAPHVIZ_VERSION_INCLUDE_DIRS}/graphviz/graphviz_version.h" OUTPUT_VARIABLE GRAPHVIZ_VERSION_HEADER)
  string(REGEX MATCH "[0-9][.][0-9]+[.][0-9]" GRAPHVIZ_VERSION_STRING ${GRAPHVIZ_VERSION_HEADER})
  string(REGEX MATCHALL "[0-9]+" GRAPHVIZ_VERSION_TOC ${GRAPHVIZ_VERSION_STRING})
  list(GET GRAPHVIZ_VERSION_TOC 1 GRAPHVIZ_VERSION_MINOR)
endif()

if(GRAPHVIZ_VERSION_MINOR GREATER 30)
# use libcgraph
  find_dependency(NAME GRAPHVIZ
    HEADERS "graphviz/gvc.h"
    LIBS "gvc;cgraph"
    PATHS "${GRAPHVIZ_ROOT}"
  )
  set(GRAPHVIZ_DEFINES "-DWITH_CGRAPH")
else()
# use libgraph
  find_dependency(NAME GRAPHVIZ
    HEADERS "graphviz/gvc.h"
    LIBS "gvc;graph"
    PATHS "${GRAPHVIZ_ROOT}"
  )
endif()

