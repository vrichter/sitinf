/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/sensor/DelaySensor.cpp                            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/DelaySensor.h>
#include <utils/Exception.h>
#include <functional>

#include "gtest/gtest.h"

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::sensor;
namespace pt = boost::posix_time;

namespace {

  struct SensorStateList : Observer<const std::string&> {
    std::vector<std::string> states;


    virtual void update(const Subject<const std::string&>* subject, const std::string& data){
      states.push_back(data);
    }

  };

  //pt::time_duration nowPlus(pt::time_duration plus){
  //  return (boost::get_system_time() + plus).time_of_day();
  //}

  SimpleSensor::Ptr createSimpleSensor(){
    std::vector<std::string> states;
    states.push_back("zero");
    states.push_back("one");
    states.push_back("two");
    states.push_back("three");
    return SimpleSensor::Ptr(new SimpleSensor("SimpleSensor",states,"zero"));
  }

  TEST(DelaySensorTest, Destructions) {
    SimpleSensor::Ptr inner = createSimpleSensor();
    inner->changeCurrentState(inner->states()[0]);
    DelaySensor* sensor = nullptr;
    sensor = new DelaySensor(inner,10,inner->states()[0]);
    delete sensor;
    sensor = new DelaySensor(inner,10,inner->states()[1]);
    delete sensor;

    pt::ptime now = pt::microsec_clock::local_time();
    sensor = new DelaySensor(inner,10000,inner->states()[1]);
    inner->changeCurrentState(inner->states()[1]);
    delete sensor;
    EXPECT_LE(pt::microsec_clock::local_time(), now + pt::seconds(5));
  }

  TEST(DelaySensorTest, CorrectDelay) {
    SimpleSensor::Ptr inner = createSimpleSensor();
    inner->changeCurrentState(inner->states()[0]);
    DelaySensor sensor(inner,10,inner->states()[1]);

    EXPECT_EQ(inner->states()[0],sensor.currentState());
    inner->changeCurrentState(inner->states()[2]);
    EXPECT_EQ(inner->states()[2],sensor.currentState());
    inner->changeCurrentState(inner->states()[1]);
    EXPECT_EQ(inner->states()[2],sensor.currentState()); // update should be delayed
    Thread::sleep(boost::get_system_time() + pt::milliseconds(11));
    EXPECT_EQ(inner->states()[1],sensor.currentState()); // now it should be changed
  }

 }

