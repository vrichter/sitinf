/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/sensor/SensorSensorFactory.cpp                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/SensorFactory.h>
#include <yaml-cpp/yaml.h>
#include "test/Config.h"

#include "gtest/gtest.h"

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::sensor;

namespace {

  YAML::Node createConfig(){
    std::string config = "\
---\n\
# The sensor configuration for the Testing.\n\
sensors:\n\
\n\
  - sensor_type: DayTimeSensor\n\
    config:\n\
      name: Time\n\
      times:\n\
        - name: Night\n\
          end: 06:00:00\n\
        - name: Morning\n\
          end: 12:00:00\n\
        - name: Afternoon\n\
          end: 18:00:00\n\
        - name: Evening\n\
          end: 00:00:00\n\
\n\
  - sensor_type: TypeSensor\n\
    config:\n\
      type: rst.test.TestProto\n\
      results:\n\
        - xpath: /message[count(faces)>0]\n\
          result: Some\n\
        - xpath: /message[count(faces)<1]\n\
          result: None\n\
    manifestations:\n\
      - name: Attention_Flobi_Wardrobe\n\
        scope: /home/wardrobe/faces\n\
      - name: Attention_Flobi_Kitchen\n\
        scope: /home/kitchen/faces\n\
\n\
...";
    std::cout << config << std::endl;
    return YAML::Load(config);
  }

  TEST(SensorFactoryTest, CreateFromConfig) {
    sensor::SensorFactory factory(io::ProtoFilesImporter::Ptr(
                                    new io::ProtoFilesImporter(test::config::testDataPrefix()
                                                               + std::string("proto/share/rst.test/proto/"))
                                    ));
    std::vector<Sensor::Ptr> sensors = factory.createSensors(createConfig());
    EXPECT_EQ(3,sensors.size());
  }

}

