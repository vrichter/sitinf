/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/sensor/DayTimeSensor.cpp                          **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/DayTimeSensor.h>
#include <utils/Exception.h>
#include <functional>

#include "gtest/gtest.h"

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::sensor;
namespace pt = boost::posix_time;

namespace {

  struct SensorStateList : Observer<const std::string&> {
    std::vector<std::string> states;


    virtual void update(const Subject<const std::string&>* subject, const std::string& data){
      states.push_back(data);
    }

  };

  pt::time_duration nowPlus(pt::time_duration plus){
    return (pt::microsec_clock::local_time() + plus).time_of_day();
  }

  // Test initialization from set of DayTimes
  TEST(DayTimeTest, Exceptions) {
    std::vector<DayTimeSensor::DayTime> times;
    times.push_back(DayTimeSensor::DayTime("1", pt::time_duration(pt::hours(1))));

    // empty name
    EXPECT_THROW(DayTimeSensor("", times),IllegalArgumentException);

    // empoty times
    times.clear();
    EXPECT_THROW(DayTimeSensor("TestSensor", times),IllegalArgumentException);

    // equal times
    times.push_back(DayTimeSensor::DayTime("1", pt::time_duration(pt::hours(1))));
    times.push_back(DayTimeSensor::DayTime("2", pt::time_duration(pt::hours(1))));
    EXPECT_THROW(DayTimeSensor("TestSensor", times),IllegalArgumentException);

  }

  TEST(DayTimeTest, DestructionDuringLongWait) {
    std::vector<DayTimeSensor::DayTime> times;

    pt::ptime now = pt::microsec_clock::local_time();
    pt::ptime end = now + pt::seconds(5);
    times.push_back(DayTimeSensor::DayTime("end", end.time_of_day()));

    DayTimeSensor* sensor = new DayTimeSensor("TestSensor", times);
    delete sensor;

    EXPECT_LT(pt::microsec_clock::local_time(),end);
  }

  TEST(DayTimeTest, CorrectStates) {
    std::vector<DayTimeSensor::DayTime> times;
    times.push_back(DayTimeSensor::DayTime("4", pt::time_duration(pt::hours(4))));
    times.push_back(DayTimeSensor::DayTime("1", pt::time_duration(pt::hours(1))));
    times.push_back(DayTimeSensor::DayTime("3", pt::time_duration(pt::hours(3))));
    times.push_back(DayTimeSensor::DayTime("2", pt::time_duration(pt::hours(2))));
    times.push_back(DayTimeSensor::DayTime("5", pt::time_duration(pt::hours(5))));
    times.push_back(DayTimeSensor::DayTime("5", pt::time_duration(pt::hours(6))));
    times.push_back(DayTimeSensor::DayTime("5", pt::time_duration(pt::hours(7))));

    DayTimeSensor sensor("TestSensor", times);

    EXPECT_EQ(5,sensor.states().size());
    EXPECT_EQ("1", sensor.states()[0]);
    EXPECT_EQ("2", sensor.states()[1]);
    EXPECT_EQ("3", sensor.states()[2]);
    EXPECT_EQ("4", sensor.states()[3]);
    EXPECT_EQ("5", sensor.states()[4]);

  }

  TEST(DayTimeTest, Updates) {
    const uint states = 3; // must be >=2
    std::vector<DayTimeSensor::DayTime> times;

    std::vector<pt::time_duration> durations(states);
    for(uint i = 0; i < states; ++i){
      durations[i] = nowPlus(pt::seconds(i));
    }
    for(uint i = 0; i < states; ++i){
      std::stringstream s;
      s << i;
      times.push_back(DayTimeSensor::DayTime(s.str(),durations[i]));
    }

    std::mt19937 g(0);
    std::shuffle(times.begin(), times.end(), g);

    SensorStateList observer;
    DayTimeSensor sensor("TestSensor", times);
    sensor.addObserver(&observer);

    boost::thread::sleep(boost::get_system_time()+pt::time_duration(pt::seconds(states)));

    EXPECT_EQ(states-1,observer.states.size());
    for(uint i = 0; i < states; ++i){
      if(i < states-2){
        std::stringstream s;
        s << i+2;
        EXPECT_EQ(s.str(),observer.states[i]);
      } else {
        EXPECT_EQ("0",observer.states.back());
      }
    }
  }

 }

