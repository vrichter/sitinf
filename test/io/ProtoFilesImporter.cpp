/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/io/ProtoFilesImporter.cpp                         **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/ProtoFilesImporter.h>
#include "test/Config.h"

#include <stdlib.h>

#include "gtest/gtest.h"

namespace {
  using namespace sitinf;
  using namespace sitinf::io;

  void runTest(const std::string& env_var,
               const std::string& testpath,
               const std::string& type)
  {
    const char* NAME = env_var.c_str();
    // save old value
    const char* old_val = std::getenv(NAME);
    setenv(NAME,testpath.c_str(),1);

    ProtoFilesImporter p;
    const google::protobuf::Descriptor* test = p.findDescriptor(type);

    // reset variable
    if(old_val != NULL){
      setenv(NAME,old_val,1);
    } else {
      unsetenv(NAME);
    }

    EXPECT_NE(nullptr,test);
  }

  TEST(ProtoFilesImporterTest, CreateWithRstEnv) {
    runTest("RST_PROTO_PATH",
            test::config::testDataPrefix() + std::string("/proto/share/rst.test/proto"),
            "rst.test.TestProto");
  }

  TEST(ProtoFilesImporterTest, CreateWithPREFIX) {
    runTest("PREFIX",
            test::config::testDataPrefix() + std::string("/proto"),
            "rst.test.TestProto");
  }

  TEST(ProtoFilesImporterTest, CreateWithPrefix) {
    runTest("prefix",
            test::config::testDataPrefix() + std::string("/proto"),
            "rst.test.TestProto");
  }
}

