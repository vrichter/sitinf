/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/io/SensorConfigs.cpp                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <utils/Exception.h>
#include <sensor/SensorFactory.h>
#include <network/BayesianNetworkFactory.h>
#include <test/Config.h>

#include "gtest/gtest.h"

namespace {

  void testSensor(std::string config_name){
    sitinf::sensor::SensorFactory factory(boost::make_shared<sitinf::io::ProtoFilesImporter>(RST_PROTO_PATH));
    std::vector<sitinf::sensor::Sensor::Ptr> sensors;
    try{
      sensors = factory.createSensors(test::config::shareDataPrefix() + config_name);
    } catch (const sitinf::utils::Exception& exception){
      EXPECT_STREQ("Should not have thrown.",exception.what());
    }

    EXPECT_FALSE(sensors.empty());

  }

  void testNetwork(std::string config_name){
      sitinf::network::BayesianNetwork network;
      try{
        network = sitinf::network::BayesianNetworkFactory::fromFile(test::config::shareDataPrefix() + config_name);
        //network.validate();
      } catch (const sitinf::utils::Exception& exception){
        EXPECT_STREQ("Should not have thrown.",exception.what());
      }

      EXPECT_GT(network.size(),0);
  }

  TEST(Configurations, situation_sensor_yaml) {
    testSensor("situation.sensor.yaml");
  }

  TEST(Configurations, attention_sensor_yaml) {
    testSensor("addressee.sensor.yaml");
  }

  TEST(Configurations, situation_network_yaml) {
    testNetwork("situation.network.yaml");
  }

  TEST(Configurations, attention_network_yaml) {
    testNetwork("addressee.network.yaml");
  }

}  // namespace

