/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/io/RsbSourceSink.cpp                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/RsbSource.h>
#include <io/RsbSink.h>
#include <utils/Exception.h>
#include <rsb/ParticipantConfig.h>
#include <rst/math/Vec2DInt.pb.h>

#include "gtest/gtest.h"

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;
using namespace google::protobuf;

namespace {

  // a simple listener holds the last result
  class MessageObserver : Observer<Shared<rst::math::Vec2DInt>::Ptr> {
  public:
    Shared<rst::math::Vec2DInt>::Ptr current;
    Subject<Shared<rst::math::Vec2DInt>::Ptr>* subject;

    MessageObserver(Source<Shared<rst::math::Vec2DInt>::Ptr>* subject){
      this->subject = subject;
      subject->addObserver(this);
      current = subject->current();
    }

    ~MessageObserver(){
      subject->removeObserver(this);
    }

    void update(const Subject<Shared<rst::math::Vec2DInt>::Ptr>* subject, Shared<rst::math::Vec2DInt>::Ptr data) override {
      std::cout << "update called" << std::endl;
      current = data;
    }

    void* wait(int milliseconds,void* ptr=NULL){
      boost::posix_time::ptime now = boost::posix_time::microsec_clock::universal_time();
      boost::posix_time::ptime end = now + boost::posix_time::milliseconds(milliseconds);
      while(true){
        if(current.get() != ptr){
          return current.get();
        } else if(now > end){
          return NULL;
        }
        now += boost::posix_time::milliseconds(1);
        boost::thread::sleep(now);
      }
    }

    void reset(){
      current.reset();
    }
  };

  // Testing class RsbSourceSinkTest
  class RsbSourceSinkTest : public ::testing::Test {
  protected:

    // Set-up work for each test here.
    RsbSourceSinkTest()
    {
      // use a participant config with only inprocess comunication
      rsb::ParticipantConfig config = rsb::getFactory().getDefaultParticipantConfig();
      for(auto transport : config.getTransports()){
        transport.setEnabled(false);
      }
      rsb::ParticipantConfig::Transport inprocess = config.getTransport("inprocess");
      inprocess.setEnabled(true);
      config.addTransport(inprocess);
      rsb::getFactory().setDefaultParticipantConfig(config);

      data1 = Shared<rst::math::Vec2DInt>::Ptr(new rst::math::Vec2DInt(rst::math::Vec2DInt::default_instance()));
      data2 = Shared<rst::math::Vec2DInt>::Ptr(new rst::math::Vec2DInt(rst::math::Vec2DInt::default_instance()));
      data1->set_x(0);
      data1->set_y(1);
      data2->set_x(2);
      data2->set_y(3);
    }

    // Do clean-up work that does not throw exceptions here.
    virtual ~RsbSourceSinkTest() {}

    // This is called immediately after the constructor (right before each test).
    virtual void SetUp() {
    }

    // This is called after each test (right before the destructor).
    virtual void TearDown() {
    }

  public:
    Shared<rst::math::Vec2DInt>::Ptr data1;
    Shared<rst::math::Vec2DInt>::Ptr data2;

  };

  // Tests that RsbSources are correctly created
  TEST_F(RsbSourceSinkTest, CorrectCreation) {
    // create instance without exceptions
    EXPECT_TRUE(data1.get() != NULL);
    EXPECT_TRUE(data2.get() != NULL);
    EXPECT_NO_THROW(RsbSource<rst::math::Vec2DInt> instance("/test"));
    EXPECT_NO_THROW(RsbSink<rst::math::Vec2DInt> instance("/test",data1));
    EXPECT_NO_THROW(RsbSink<rst::math::Vec2DInt> instance("/test",Shared<rst::math::Vec2DInt>::Ptr()));
  }

  // Tests that RsbSources are correctly created
  TEST_F(RsbSourceSinkTest, RightScope) {
    rsb::Scope scope("/test");
    RsbSource<rst::math::Vec2DInt> instance1(scope);
    //test whether scope is correct
    EXPECT_EQ(scope,instance1.scope());
    RsbSink<rst::math::Vec2DInt> instance2(scope,data1);
    //test whether scope is correct
    EXPECT_EQ(scope,instance2.scope());
  }

  // Test default behaviour first Sink then Source
  TEST_F(RsbSourceSinkTest, SinkThenSource) {
    // create Sink with data
    RsbSink<rst::math::Vec2DInt> Sink(rsb::Scope("/test"),data1);
    // create Source and check its result
    RsbSource<rst::math::Vec2DInt> getter(rsb::Scope("/test"));
    // wait until data is available
    MessageObserver observer(&getter);
    bool gotDataAfterOneSecond = observer.wait(10000);
    EXPECT_TRUE(gotDataAfterOneSecond);
    EXPECT_TRUE(getter.current().get() != NULL);
    EXPECT_TRUE(data1.get() != NULL);
    EXPECT_EQ(data1->x(),getter.current()->x());
    EXPECT_EQ(data1->y(),getter.current()->y());
  }

  // Test default behaviour first Source then Sink
  TEST_F(RsbSourceSinkTest, SourceThenSink) {
    // create Source and check its result
    RsbSource<rst::math::Vec2DInt> getter("/test");
    MessageObserver observer(&getter);
    EXPECT_TRUE(getter.current().get() == NULL);
    // create Sink with data
    RsbSink<rst::math::Vec2DInt> Sink(rsb::Scope("/test"),data1);
    bool gotDataAfterOneSecond = observer.wait(10000);
    EXPECT_TRUE(gotDataAfterOneSecond);
    EXPECT_TRUE(getter.current().get() != NULL);
    EXPECT_EQ(data1->x(),getter.current()->x());
    EXPECT_EQ(data1->y(),getter.current()->y());
  }

  // Test update behaviour
  TEST_F(RsbSourceSinkTest, Update) {
    // create Source and check its result
    RsbSource<rst::math::Vec2DInt> getter("/test");
    MessageObserver observer(&getter);
    RsbSink<rst::math::Vec2DInt> Sink(rsb::Scope("/test"),data1);
    void* gotFirstResult = observer.wait(10000);
    EXPECT_TRUE(gotFirstResult);
    // test update
    Sink.update(nullptr,data2);
    void* gotDataAfterOneSecond = observer.wait(10000,gotFirstResult);
    if(getter.current()->x() == data1->x()){
      // race happend. second event of first Sink. wait for another data
      gotDataAfterOneSecond = observer.wait(10000,gotDataAfterOneSecond);
    }
    // wait that the result changes.
    EXPECT_TRUE(gotDataAfterOneSecond);
    EXPECT_TRUE(getter.current().get() != NULL);
    EXPECT_EQ(data2->x(),getter.current()->x());
    EXPECT_EQ(data2->y(),getter.current()->y());
  }
}  // namespace

