/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/network/BayesianNetwork.cpp                       **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <network/BayesianNetwork.h>

#include "gtest/gtest.h"

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::network;

namespace {

    std::vector<BayesianNetwork::Variable> sprinklerVariables(){
      std::vector<BayesianNetwork::Variable> variables;
      BayesianNetwork::Variable tmp;
      tmp.m_Type = tmp.nature;
      tmp.m_Outcomes.push_back("False");
      tmp.m_Outcomes.push_back("True");

      tmp.m_Name = "Cloudy";
      tmp.m_Property = "position = (0, 50)";
      variables.push_back(tmp);
      tmp.m_Name = "Rain";
      tmp.m_Property = "position = (50, 25)";
      variables.push_back(tmp);
      tmp.m_Name = "Sprinkler";
      tmp.m_Property = "position = (50, 75)";
      variables.push_back(tmp);
      tmp.m_Name = "WetGrass";
      tmp.m_Property = "position = (100, 50)";
      variables.push_back(tmp);
      return variables;
    }

    std::vector<BayesianNetwork::Definition> sprinklerDefinitions(){
      std::vector<BayesianNetwork::Definition> definitions;
      BayesianNetwork::Definition tmp;

      tmp.m_Variables.push_back("Cloudy");
      tmp.m_Table.push_back(0.5);
      tmp.m_Table.push_back(0.5);
      definitions.push_back(tmp);
      tmp.m_Variables.clear();
      tmp.m_Table.clear();

      tmp.m_Variables.push_back("Rain");
      tmp.m_Variables.push_back("Cloudy");
      tmp.m_Table.push_back(0.8);
      tmp.m_Table.push_back(0.2);
      tmp.m_Table.push_back(0.2);
      tmp.m_Table.push_back(0.8);
      definitions.push_back(tmp);
      tmp.m_Variables.clear();
      tmp.m_Table.clear();

      tmp.m_Variables.push_back("Sprinkler");
      tmp.m_Variables.push_back("Cloudy");
      tmp.m_Table.push_back(0.5);
      tmp.m_Table.push_back(0.5);
      tmp.m_Table.push_back(0.9);
      tmp.m_Table.push_back(0.1);
      definitions.push_back(tmp);
      tmp.m_Variables.clear();
      tmp.m_Table.clear();

      tmp.m_Variables.push_back("WetGrass");
      tmp.m_Variables.push_back("Sprinkler");
      tmp.m_Variables.push_back("Rain");
      tmp.m_Table.push_back(1.0);
      tmp.m_Table.push_back(0.0);
      tmp.m_Table.push_back(0.1);
      tmp.m_Table.push_back(0.9);
      tmp.m_Table.push_back(0.1);
      tmp.m_Table.push_back(0.9);
      tmp.m_Table.push_back(0.01);
      tmp.m_Table.push_back(0.99);
      definitions.push_back(tmp);
      tmp.m_Variables.clear();
      tmp.m_Table.clear();

      return definitions;
    }

    std::string sprinklerName(){
      return "Sprinkler";
    }

    std::string sprinklerComment(){
      return "A BayesianNetwork describing probabilities for WetGrass given Sprinkler, Rain and Clound states.";
    }

    //BayesianNetwork bayesianNetwork(){
    //  return BayesianNetwork(sprinklerVariables(),sprinklerDefinitions(),sprinklerName(),sprinklerComment());
    //}

  // Tests that observers are correctly added
  TEST(BayesianNetworkTest, Initialisation) {
    auto name = sprinklerName();
    auto comment = sprinklerComment();
    auto variables = sprinklerVariables();
    auto definitions = sprinklerDefinitions();
    EXPECT_NO_THROW(BayesianNetwork(variables,definitions,name,comment));

    BayesianNetwork network(variables,definitions,name,comment);

    EXPECT_EQ(name, network.name());
    EXPECT_EQ(comment, network.comment());
    EXPECT_EQ(variables.size(),network.variables().size());
    EXPECT_EQ(definitions.size(),network.definitions().size());

    for (uint i = 0; i < variables.size(); ++i){
      EXPECT_EQ(variables.at(i),network.variable(i));
      EXPECT_EQ(definitions.at(i), network.definition(i));
    }

    EXPECT_EQ(network,BayesianNetwork(variables,definitions,name,comment));
 }


 TEST(BayesianNetworkTest, Uniformisation) {
   BayesianNetwork network(sprinklerVariables(),sprinklerDefinitions(),sprinklerName(),sprinklerComment());
   BayesianNetwork uniform = network.uniformDistributed(0.,0);

   EXPECT_NE(network,uniform);

   EXPECT_EQ(network.variables().size(), uniform.variables().size());
   EXPECT_EQ(network.definitions().size(), uniform.definitions().size());

   for(uint i = 0; i < network.variables().size(); ++i){
     EXPECT_EQ(network.variable(i), uniform.variable(i));
   }

   for(uint i = 0; i < network.definitions().size(); ++i){
     EXPECT_EQ(network.definition(i).m_Property,uniform.definition(i).m_Property);
     EXPECT_EQ(network.definition(i).m_Variables.size(),uniform.definition(i).m_Variables.size());
     for (uint j = 0; j < network.definition(i).m_Variables.size(); ++j){
       EXPECT_EQ(network.definition(i).m_Variables[j],uniform.definition(i).m_Variables[j]);
     }
   }

   // the only thing that changes are the tables. they must be uniformly distributed
   for(uint i = 0; i < uniform.definitions().size(); ++i){
     real one_div_dim  = 1. / (real) uniform.variable(uniform.definition(i).m_Variables.at(0)).m_Outcomes.size();
     for(real value : uniform.definition(i).m_Table){
       EXPECT_EQ(one_div_dim,value);
     }
   }
 }

 TEST(BayesianNetworkTest, UniformisationWithVariance) {
   uint seed  = 0;
   real noise = 0.01;
   BayesianNetwork network(sprinklerVariables(),sprinklerDefinitions(),sprinklerName(),sprinklerComment());
   BayesianNetwork uniform = network.uniformDistributed(noise,seed);

   EXPECT_NE(network,uniform);

   EXPECT_EQ(network.variables().size(), uniform.variables().size());
   EXPECT_EQ(network.definitions().size(), uniform.definitions().size());

   // now the tables should uniform plus small noise
   for(uint i = 0; i < uniform.definitions().size(); ++i){
     real uniform_distribution  = 1. / (real) uniform.variable(uniform.definition(i).m_Variables.at(0)).m_Outcomes.size();
     for(real value : uniform.definition(i).m_Table){
       EXPECT_NE(uniform_distribution,value);
       EXPECT_GT(noise,std::abs(uniform_distribution-value));
     }
   }
 }

}  // namespace
