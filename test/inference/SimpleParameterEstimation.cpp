/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/inference/SimpleParameterEstimation.cpp           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <inference/SimpleParameterEstimation.h>

#include <utils/Exception.h>
#include <inference/SimpleParameterEstimation.h>
#include <network/BayesianNetworkFactory.h>
#include "gtest/gtest.h"

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::network;
using namespace sitinf::inference;

namespace {

  BayesianNetwork createCoinNetwork(){
    static std::istringstream stream(BayesianNetworkFactory::Networks::coin_yaml());
    static BayesianNetwork result = BayesianNetworkFactory::fromYaml(stream).uniformDistributed(0,0);
    return result;
  }

  ParameterEstimation::Ptr createSimpleParameterEstimation(){
    return ParameterEstimation::Ptr(new SimpleParameterEstimation(createCoinNetwork()));
  }

  std::vector<WeightedAspectStateEvidence> createCoinEvidence(uint head_count=1, uint tail_count=1){
    std::vector<WeightedAspectStateEvidence> result;
    WeightedAspectStateEvidence heads;
    heads.newEvidence("Coin", "Heads");
    WeightedAspectStateEvidence tails;
    tails.newEvidence("Coin", "Tails");
    for(uint i = 0; i < head_count; ++i){
      result.push_back(heads);
    }
    for(uint i = 0; i < tail_count; ++i){
      result.push_back(tails);
    }
    return result;
  }


  TEST(SimpleParameterEstimationTest, Setup) {

    EXPECT_NO_THROW(createCoinNetwork());
    EXPECT_NO_THROW(createSimpleParameterEstimation());

  }


  TEST(SimpleParameterEstimationTest, ResultNetworkStructure) {

    uint heads = 1;
    uint tails = 9;

    BayesianNetwork coin = createCoinNetwork();
    ParameterEstimation::Ptr est = createSimpleParameterEstimation();
    std::vector<WeightedAspectStateEvidence> evidence = createCoinEvidence(heads,tails);

    EstimationResult result = est->estimateParameters(evidence);

    EXPECT_EQ(coin.variables().size(), result.network.variables().size());
    EXPECT_EQ(coin.definitions().size(), result.network.definitions().size());
    for (auto var : coin.variables()){
      uint varId = 0;
      EXPECT_NO_THROW(varId = result.network.variableId(var.m_Name));
      BayesianNetwork::Variable rvar;
      EXPECT_NO_THROW(rvar = result.network.variable(varId));
      EXPECT_EQ(var,rvar);

      BayesianNetwork::Definition def,rdef;
      EXPECT_NO_THROW(def = coin.definition(varId));
      EXPECT_NO_THROW(rdef = result.network.definition(varId));
      EXPECT_EQ(def.m_Property,rdef.m_Property);
      EXPECT_EQ(def.m_Variables,rdef.m_Variables);
    }
  }


  TEST(SimpleParameterEstimationTest, Probabilities) {

    EXPECT_NO_THROW(createCoinNetwork());
    EXPECT_NO_THROW(createSimpleParameterEstimation());

    uint heads = 10;
    uint tails = 90;

    BayesianNetwork coin = createCoinNetwork();
    ParameterEstimation::Ptr est = createSimpleParameterEstimation();
    std::vector<WeightedAspectStateEvidence> evidence = createCoinEvidence(heads,tails);

    EstimationResult result = est->estimateParameters(evidence);

    BayesianNetwork::Definition def = result.network.definitions()[0];
    real norm = (real) heads + (real) tails;
    EXPECT_NEAR(def.m_Table.at(0), heads / norm, 1./norm);
    EXPECT_NEAR(def.m_Table.at(1), tails / norm, 1./norm);

  }


  TEST(SimpleParameterEstimationTest, ProbabilitiesWeighted) {

    EXPECT_NO_THROW(createCoinNetwork());
    EXPECT_NO_THROW(createSimpleParameterEstimation());

    uint heads = 1000000; // whole lot of examples, thats the reason for weights
    uint tails = 9000000;

    BayesianNetwork coin = createCoinNetwork();
    ParameterEstimation::Ptr est = createSimpleParameterEstimation();
    // use only one of every evidence but give them different weights
    std::vector<WeightedAspectStateEvidence> evidence = createCoinEvidence(1,1);
    // set weights
    evidence.at(0).weight(heads);
    evidence.at(1).weight(tails);

    EstimationResult result = est->estimateParameters(evidence);

    BayesianNetwork::Definition def = result.network.definitions()[0];
    real norm = (real) heads + (real) tails;
    EXPECT_NEAR(def.m_Table.at(0), heads / norm, 1./norm);
    EXPECT_NEAR(def.m_Table.at(1), tails / norm, 1./norm);

  }

  TEST(SimpleParameterEstimationTest, WeightedResultsComparable) {

    EXPECT_NO_THROW(createCoinNetwork());
    EXPECT_NO_THROW(createSimpleParameterEstimation());

    uint heads = 10;
    uint tails = 90;

    BayesianNetwork coin = createCoinNetwork();
    ParameterEstimation::Ptr est = createSimpleParameterEstimation();
    ParameterEstimation::Ptr wEst = createSimpleParameterEstimation();
    // use only one of every evidence but give them different weights
    std::vector<WeightedAspectStateEvidence> evidence = createCoinEvidence(heads,tails);
    std::vector<WeightedAspectStateEvidence> weightedEvidence = createCoinEvidence(1,1);
    // set weights
    weightedEvidence.at(0).weight(heads);
    weightedEvidence.at(1).weight(tails);

    EstimationResult result = est->estimateParameters(evidence,evidence);
    EstimationResult wResult = wEst->estimateParameters(weightedEvidence,weightedEvidence);

    double precision = 1e-13; // to account for rounding errors
    EXPECT_EQ(result.network,wResult.network);
    EXPECT_EQ(result.estimationLogLikelihoods.size(),wResult.estimationLogLikelihoods.size());
    for(uint i = 0; i < result.estimationLogLikelihoods.size(); ++i){
      EXPECT_NEAR(result.estimationLogLikelihoods.at(i),wResult.estimationLogLikelihoods.at(i),precision);
    }
    EXPECT_EQ(result.testLogLikelihoods.size(),wResult.testLogLikelihoods.size());
    for(uint i = 0; i < result.testLogLikelihoods.size(); ++i){
      EXPECT_NEAR(result.testLogLikelihoods.at(i),wResult.testLogLikelihoods.at(i),precision);
    }
  }

}  // namespace

