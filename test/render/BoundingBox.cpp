/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/render/BoundingBox.cpp                            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <render/BoundingBox.h> 

#include <utils/Exception.h>
#include <utils/Types.h>
#include "gtest/gtest.h"

using namespace sitinf;
using namespace sitinf::render;
using namespace sitinf::utils;

namespace {

  // Testing class RenderBoundingBoxTest
  class BoundingBoxTest : public ::testing::Test {
  protected:

    // Set-up work for each test here.
    BoundingBoxTest()
    {

    }

    // Do clean-up work that does not throw exceptions here.
    virtual ~BoundingBoxTest() {}

    // This is called immediately after the constructor (right before each test).
    virtual void SetUp() {
    }

    // This is called after each test (right before the destructor).
    virtual void TearDown() {
    }

  };

  // Tests that BoundingBoxs are correctly created
  TEST_F(BoundingBoxTest, CorrectlyCreated) {
    // create instance
    EXPECT_NO_THROW(BoundingBox<sint> instance(Position<sint>(0,0),10,10));
    EXPECT_NO_THROW(BoundingBox<uint> instance(Position<uint>(-1,0),-10,10));
    EXPECT_NO_THROW(BoundingBox<real> instance(Position<real>(-1.,0.),-10.,10.));
  }

  // Tests that BoundingBoxs values are correct
  TEST_F(BoundingBoxTest, CorrectValues) {
    // create instance
    BoundingBox<sint> instance(Position<sint>(0,1),2,3);
    EXPECT_EQ(Position<sint>(0,1),instance.position());
    EXPECT_EQ(2,instance.width());
    EXPECT_EQ(3,instance.height());
  }


  // Tests that BoundingBoxs equals works correct
  TEST_F(BoundingBoxTest, CorrectEquals) {
    // create instance
    BoundingBox<sint> a(Position<sint>(0,1),2,3);
    BoundingBox<sint> b(Position<sint>(0,1),2,3);
    BoundingBox<sint> c(Position<sint>(1,1),2,3);

    EXPECT_TRUE(a == b);
    EXPECT_FALSE(a == c);
  }
}  // namespace

