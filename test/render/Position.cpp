/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/render/Position.cpp                               **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <render/Position.h> 

#include <utils/Exception.h>
#include "gtest/gtest.h"
#include <utils/Types.h>

using namespace sitinf;
using namespace sitinf::render;
using namespace sitinf::utils;

namespace {

  // Testing class RenderPositionTest
  class PositionTest : public ::testing::Test {
  protected:

    // Set-up work for each test here.
    PositionTest()
    {

    }

    // Do clean-up work that does not throw exceptions here.
    virtual ~PositionTest() {}

    // This is called immediately after the constructor (right before each test).
    virtual void SetUp() {
    }

    // This is called after each test (right before the destructor).
    virtual void TearDown() {
    }

  };

  // Tests that Positions are correctly created
  TEST_F(PositionTest, CorrectlyCreated) {
    // create instance
    EXPECT_NO_THROW(Position<sint> instance(-10,10));
    EXPECT_NO_THROW(Position<uint> instance(0,10));
    EXPECT_NO_THROW(Position<real> instance(-1.,0));
  }

  // Tests that Positions members are correctly set
  TEST_F(PositionTest, CorrectValues) {
    // create instance
    Position<sint> instance(-5,10);
    EXPECT_EQ(-5,instance.x());
    EXPECT_EQ(10,instance.y());
  }

  // Tests that Positions equals works correctly
  TEST_F(PositionTest, CorrectEquals) {
    // create instance
    Position<sint> a(-5,10);
    Position<sint> b(-5,10);
    Position<sint> c(5,10);
    EXPECT_TRUE(a == b);
    EXPECT_FALSE(b == c);
  }
}  // namespace

