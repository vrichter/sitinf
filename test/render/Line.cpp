/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/render/Line.cpp                                   **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <render/Line.h> 

#include <utils/Exception.h>
#include "gtest/gtest.h"

using namespace sitinf;
using namespace sitinf::render;

namespace {

  // Testing class RenderLineTest
  class LineTest : public ::testing::Test {
  protected:

    // Set-up work for each test here.
    LineTest()
      : start(0,50), end(100,200)
    {

    }

    // Do clean-up work that does not throw exceptions here.
    virtual ~LineTest() {}

    // This is called immediately after the constructor (right before each test).
    virtual void SetUp() {
    }

    // This is called after each test (right before the destructor).
    virtual void TearDown() {
    }
  public:
    // positions
    const Position<int> start;
    const Position<int> end;
  };

  // Tests that Lines are correctly created
  TEST_F(LineTest, ConstructorNoThrow) {
    // creation does not throw
    EXPECT_NO_THROW(Line<int> instance(start,end));
  }


  // Tests that Lines are correctly created
  TEST_F(LineTest, ConstructorCorrect) {
    // create instance
    Line<int> instance(start,end);
    EXPECT_EQ(start.x(),instance.start().x());
    EXPECT_EQ(start.y(),instance.start().y());
    EXPECT_EQ(end.x(),instance.end().x());
    EXPECT_EQ(end.y(),instance.end().y());
  }

}  // namespace

