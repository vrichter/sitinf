/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/render/GraphLayout.cpp                            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <render/GraphLayout.h> 
#include <utils/Exception.h>
#include <rst/bayesnetwork/BayesNetwork.pb.h>
#include <utils/Types.h>

#include "gtest/gtest.h"

using namespace sitinf;
using namespace sitinf::render;

namespace {

  // Testing class RenderGraphLayoutTest
  class GraphLayoutTest : public ::testing::Test {
  protected:

    // Set-up work for each test here.
    GraphLayoutTest()
      : network(rst::bayesnetwork::BayesNetwork::default_instance().New())
    {
      // setup network
      network->set_name("network");
      // create first variable
      rst::bayesnetwork::BayesVariable* root =  network->add_variable();
      root->set_name("root");
      root->set_type(rst::bayesnetwork::BayesVariable_Type_NATURE);
      *root->add_outcomes() = "on";
      *root->add_outcomes() = "off";
      root->add_probabilities(0.1);
      root->add_probabilities(0.9);

      rst::bayesnetwork::BayesVariable* child1 =  network->add_variable();
      child1->set_name("goodchild");
      child1->set_type(rst::bayesnetwork::BayesVariable_Type_NATURE);
      *child1->add_outcomes() = "on";
      *child1->add_outcomes() = "off";
      child1->add_probabilities(0.9);
      child1->add_probabilities(0.1);
      child1->add_probabilities(0.1);
      child1->add_probabilities(0.9);

      rst::bayesnetwork::BayesVariable* child2 =  network->add_variable();
      child2->set_name("badchild");
      child2->set_type(rst::bayesnetwork::BayesVariable_Type_NATURE);
      *child2->add_outcomes() = "on";
      *child2->add_outcomes() = "off";
      child2->add_probabilities(0.1);
      child2->add_probabilities(0.9);
      child2->add_probabilities(0.9);
      child2->add_probabilities(0.1);
    }

    // Do clean-up work that does not throw exceptions here.
    virtual ~GraphLayoutTest() {
      delete network;
    }

    // This is called immediately after the constructor (right before each test).
    virtual void SetUp() {
    }

    // This is called after each test (right before the destructor).
    virtual void TearDown() {
    }

    rst::bayesnetwork::BayesNetwork* network;
  };

  // Tests that GraphLayouts are correctly created
  TEST_F(GraphLayoutTest, ConstructorNoThrow) {
    // create instance with null pointer
    EXPECT_NO_THROW(GraphLayout inst(NULL));
    EXPECT_NO_THROW(GraphLayout inst(network));
  }

  // Tests that GraphLayouts are correctly created
  TEST_F(GraphLayoutTest, NullConstructorCorrect) {
    GraphLayout inst(NULL);
    EXPECT_EQ(0,inst.boundingBox().height());
    EXPECT_EQ(0,inst.boundingBox().width());
    EXPECT_EQ(0,inst.boundingBox().position().x());
    EXPECT_EQ(0,inst.boundingBox().position().y());
    EXPECT_EQ(0,inst.edges().size());
    EXPECT_EQ(0,inst.positions().size());
  }

  // Tests that GraphLayouts creates correct BoundingBox
  TEST_F(GraphLayoutTest, BoundingBox) {
    GraphLayout inst(network);
    // check bounding box
    EXPECT_EQ(Position<uint>(0,0),inst.boundingBox().position());
    EXPECT_NE(0U,inst.boundingBox().width());
    EXPECT_NE(0U,inst.boundingBox().height());
  }

  // Tests that GraphLayouts creates correct Positions count
  TEST_F(GraphLayoutTest, PositionsCount) {
    GraphLayout inst(network);
    // check positions size
    uint network_size = network->variable_size();
    EXPECT_EQ(network_size,inst.positions().size());
  }

  // Tests that GraphLayouts creates correct Positions
  TEST_F(GraphLayoutTest, PositionsNotZero) {
    GraphLayout inst(network);
    // check positions not zero
    for(auto pos : inst.positions()){
      EXPECT_NE(0U,pos.second.x());
      EXPECT_NE(0U,pos.second.y());
    }
  }
}  // namespace

