/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/utils/Observable.cpp                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <utils/Observable.h>

#include "gtest/gtest.h"

using namespace sitinf;
using namespace sitinf::utils;

namespace {

  // Testing class Observable
  class ObservableTest : public ::testing::Test {
  protected:

    // Set-up work for each test here.
    ObservableTest()
    {
      called[0] = false;
      called[1] = false;
      called[2] = false;
    }

    // Do clean-up work that doesn't throw exceptions here.
    //virtual ~ObservableTest() {

    //}

    // This is called immediately after the constructor (right before each test).
    virtual void SetUp() {
    }

    // This is called after each test (right before the destructor).
    virtual void TearDown() {

    }

  public:
    void setCalled(bool* i) {
      *i = true;
    }

  protected:
    // Objects used by all tests in the test case.
    Notifyer observable;
    bool called[3];

  };

  // Tests that observers are correctly added
  TEST_F(ObservableTest, ObserversCorrectlyAdded) {
    // add three observers
    uint o1 = observable.addObserver(boost::bind(&ObservableTest::setCalled,this,called));
    uint o2 = observable.addObserver(boost::bind(&ObservableTest::setCalled,this,called+1));
    uint o3 = observable.addObserver(boost::bind(&ObservableTest::setCalled,this,called+2));

    // check that ids are not equal
    EXPECT_NE(o1,o2);
    EXPECT_NE(o1,o3);
    EXPECT_NE(o2,o3);
  }

  // Tests that observers are correctly notified
  TEST_F(ObservableTest, ObserversCorrectlyNotified) {
    // add three observers
    observable.addObserver(boost::bind(&ObservableTest::setCalled,this,called));
    observable.addObserver(boost::bind(&ObservableTest::setCalled,this,called+1));
    observable.addObserver(boost::bind(&ObservableTest::setCalled,this,called+2));

    // notify
    observable.notifyObservers();

    // when calling notify setCalled should have set all three booleans to true
    EXPECT_TRUE(called[0]);
    EXPECT_TRUE(called[1]);
    EXPECT_TRUE(called[2]);
  }

  // Tests that observers are correctly removed
  TEST_F(ObservableTest, ObserversCorrectlyRemoved) {
    // add three observers
    uint o1 = observable.addObserver(boost::bind(&ObservableTest::setCalled,this,called));
    observable.addObserver(boost::bind(&ObservableTest::setCalled,this,called+1));
    uint o3 = observable.addObserver(boost::bind(&ObservableTest::setCalled,this,called+2));

    // now remove first and third
    observable.removeObserver(o1);
    observable.removeObserver(o3);

    // notify. only the registered (o2) should be notified
    observable.notifyObservers();

    // when calling notify setCalled should have set all called[1] booleans to true
    EXPECT_FALSE(called[0]);
    EXPECT_TRUE(called[1]);
    EXPECT_FALSE(called[2]);
  }

}  // namespace
