/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/utils/ThreadPool.cpp                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <utils/ThreadPool.h>
#include <functional>

#include "gtest/gtest.h"

using namespace sitinf;
using namespace sitinf::utils;

namespace {

  typedef ThreadPool<uint> IntPool;

  uint taskCount(){
    return 1000;
  }

  uint threadCount(){
    return 8;
  }

  std::function<int()> createTestFunction(uint i){
    //return std::bind([](int result) -> int {for(int i = 0; i < 1000000;) { ++i; } ;return result;},i);
    return std::bind([](int result) -> int {return result;},i);
  }

  IntPool::TaskSet createTaskSet(){
    IntPool::TaskSet tasks;
    for(uint i = 0; i < taskCount(); ++i){
      tasks.push_back(IntPool::Task(createTestFunction(i)));
    }
    return tasks;
  }

  std::vector<uint> createResultSet(){
    std::vector<uint> result;
    for (uint i = 0; i < taskCount(); ++i){
      result.push_back(createTestFunction(i)());
    }
    return result;
  }

  // Test initialization from set of tasks
  TEST(ThreadPoolTest, Constructor) {
    IntPool::TaskSet set;
    set = std::move(createTaskSet());

    for(uint i = 0; i < set.size(); ++ i){
      EXPECT_TRUE(set[i].valid());
    }

    EXPECT_NO_THROW(IntPool(set,threadCount(),10));
    // tasks should be moved into thread pool
    EXPECT_EQ(0,set.size());
  }

  // Test calculation withoud thread pool
  TEST(ThreadPoolTest, CalculationWithoutThreadPool) {
    auto tasks = createTaskSet();

    std::vector<uint> real_result = createResultSet();

    EXPECT_EQ(real_result.size(),tasks.size());
    for(uint i = 0; i < tasks.size(); ++i){
      std::future<uint> result = std::move(tasks[i].get_future());
      tasks[i]();
      EXPECT_EQ(real_result[i],result.get());
    }
  }

  // Test synchronous calculation
  TEST(ThreadPoolTest, BlockingCalculation) {
    auto tasks = createTaskSet();
    IntPool pool(tasks,threadCount(),10);
    IntPool::ResultSet result = pool.runBlocking();

    std::vector<uint> real_result = createResultSet();

    EXPECT_EQ(real_result.size(),result.size());
    for(uint i = 0; i < result.size(); ++i){
      EXPECT_EQ(real_result[i],result[i]);
    }
  }

  //
  TEST(ThreadPoolTest, PoolCalculation) {
    auto tasks = createTaskSet();
    IntPool pool(tasks,threadCount(),10);

    EXPECT_EQ(0,tasks.size());

    IntPool::FutureResultSet futureResult = pool.run();

    IntPool::ResultSet result = futureResult.get();

    std::vector<uint> real_result = createResultSet();

    EXPECT_EQ(real_result.size(),result.size());
    for(uint i = 0; i < result.size(); ++i){
      EXPECT_EQ(real_result[i],result[i]);
    }

  }

 }

