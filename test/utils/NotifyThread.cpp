/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : test/utils/NotifyThread.cpp                            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <utils/Exception.h>
#include <utils/NotifyThread.h>
#include <functional>

#include "gtest/gtest.h"

using namespace sitinf;
using namespace sitinf::utils;
namespace pt = boost::posix_time;

namespace {

  struct CallbackLogger {
    std::vector<pt::ptime> call_log;

    void call(){
      call_log.push_back(boost::get_system_time());
    }
  };

  // Test initialization and destruction. Callback not called.
  TEST(NotifyThreadTest, ConstructionDestruction) {
    CallbackLogger l;

    EXPECT_NO_THROW(NotifyThread([&l] {l.call();}));
    EXPECT_EQ(0,l.call_log.size());

    NotifyThread* t = new NotifyThread([&l] {l.call();});
    Thread::sleep(boost::get_system_time() + pt::milliseconds(10));
    delete t;
    EXPECT_EQ(0,l.call_log.size());

    t = new NotifyThread([&l] {l.call();});
    Thread::sleep(boost::get_system_time() + pt::milliseconds(10));
    t->stop();
    EXPECT_EQ(0,l.call_log.size());
    delete t;
    EXPECT_EQ(0,l.call_log.size());
  }

  TEST(NotifyThreadTest, DeleteBeforeCallback) {
    CallbackLogger l;
    NotifyThread* t = new NotifyThread([&l] {l.call();});
    t->call_in(10);
    EXPECT_EQ(0,l.call_log.size());
    Thread::sleep(boost::get_system_time() + pt::milliseconds(9));
    delete t;
    Thread::sleep(boost::get_system_time() + pt::milliseconds(10));
    EXPECT_EQ(0,l.call_log.size());
  }

  TEST(NotifyThreadTest, CallbackTimeCallIn) {
    CallbackLogger l;
    NotifyThread* t = new NotifyThread([&l] {l.call();});
    EXPECT_EQ(0,l.call_log.size());

    for(auto time : {0,10,20,50}){
      l.call_log.clear();
      t->call_in(time);
      Thread::sleep(boost::get_system_time() + pt::milliseconds(time-2));
      EXPECT_EQ(0,l.call_log.size());
      Thread::sleep(boost::get_system_time() + pt::milliseconds(3));
      EXPECT_EQ(1,l.call_log.size());
      Thread::sleep(boost::get_system_time() + pt::milliseconds(time+1));
      EXPECT_EQ(1,l.call_log.size());
    }
  }

  TEST(NotifyThreadTest, CallbackTimeCallAt) {
    CallbackLogger l;
    NotifyThread* t = new NotifyThread([&l] {l.call();});
    EXPECT_EQ(0,l.call_log.size());

    for(auto time : {0,10,20,50}){
      utils::uint64 time_at = utils::currentTimeMillis() + time;
      l.call_log.clear();
      t->call_at(time_at);
      Thread::sleep(boost::get_system_time() + pt::milliseconds(time-2));
      EXPECT_EQ(0,l.call_log.size());
      Thread::sleep(boost::get_system_time() + pt::milliseconds(3));
      EXPECT_EQ(1,l.call_log.size());
      Thread::sleep(boost::get_system_time() + pt::milliseconds(time+1));
      EXPECT_EQ(1,l.call_log.size());
    }
  }

 }

