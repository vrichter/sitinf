#*********************************************************************
#**                                                                 **
#** File   : test/CMakeLists.txt                                    **
#** Authors: Viktor Richter                                         **
#**                                                                 **
#**                                                                 **
#** GNU LESSER GENERAL PUBLIC LICENSE                               **
#** This file may be used under the terms of the GNU Lesser General **
#** Public License version 3.0 as published by the                  **
#**                                                                 **
#** Free Software Foundation and appearing in the file LICENSE.LGPL **
#** included in the packaging of this file.  Please review the      **
#** following information to ensure the license requirements will   **
#** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
#**                                                                 **
#*********************************************************************

cmake_minimum_required(VERSION 2.8.2)

# create config header
configure_file(
  "${PROJECT_SOURCE_DIR}/test/Config.h.in"
  "${PROJECT_BINARY_DIR}/test/test/Config.h"
)

# must include before google does its magic
include_directories(BEFORE SYSTEM "${PROJECT_SOURCE_DIR}/test/")
include_directories("${PROJECT_BINARY_DIR}/test/")
include_directories("${PROJECT_BINARY_DIR}/src/proto/")
include_directories("${PROJECT_SOURCE_DIR}/src/")
include_directories("${PROJECT_SOURCE_DIR}/dai/")
include_directories(${PQUERY_INCLUDE_DIRS})
include_directories(${Boost_INCLUDE_DIR})
include_directories(${ICL_INCLUDE_DIRS})
include_directories(BEFORE SYSTEM ${RSB_INCLUDE_DIRS})
include_directories(BEFORE SYSTEM ${RST_INCLUDE_DIRS})
include_directories(${PROTOBUF_INCLUDE_DIR})
include_directories("${GRAPHVIZ_INCLUDE_DIRS}")

# build gtest
add_subdirectory(gtest)

# add rsb-rst link directories
link_directories(${RSB_RUNTIME_LIBRARY_DIRS})
link_directories(${RST_RUNTIME_LIBRARY_DIRS})

#add icl defines
add_definitions(${ICL_DEFINES})

#add rst definitions
add_definitions(${RST_CFLAGS} ${RSTSANDBOX_CFLAGS})

# for every namespace subditectory
foreach(NAMESPACE "inference" "io" "network" "qt" "render" "sensor" "utils")
  message(STATUS "Creating tests for namespace ${NAMESPACE}")
  FILE(GLOB TESTS "${PROJECT_SOURCE_DIR}/test/${NAMESPACE}/*.cpp")

  foreach(TEST ${TESTS})
    STRING(REGEX REPLACE "/.*/" "" TEST ${TEST})
    STRING(REGEX REPLACE "[.]cpp" "" TEST ${TEST})
    message(STATUS "-- Adding test: ${TEST}")
  
    add_executable("${PROJECT_NAME}-${NAMESPACE}-${TEST}"
      "${PROJECT_SOURCE_DIR}/test/${NAMESPACE}/${TEST}.cpp"
    )
  
  target_link_libraries("${PROJECT_NAME}-${NAMESPACE}-${TEST}"
      dai
      ${PQUERY_LIBRARIES}
      ${GMP_LIBRARIES}
      ${PROJECT_NAME}
      ${ICL_LIBRARIES}
      ${RST_LIBRARIES}
      ${RSTSANDBOX_LIBRARIES}
      org-openbase::openbase-type
      ${RSB_LIBRARIES}
      ${QT_LIBRARIES}
      ${QT_QTDECLARATIVE_LIBRARY}
      gtest_main
    )
  add_test("Test::${NAMESPACE}::${TEST}" "${PROJECT_NAME}-${NAMESPACE}-${TEST}")
  endforeach(TEST)

endforeach(NAMESPACE)

