#!/bin/bash

function checkfile() {
  # ensure paths exist
  if [ -a "$1" ]; then
    if [ ! -f "$1" ]; then
      echo -e "Error: A file $1 exists but is not a regular file."
      usage
      exit 1
    fi
  fi
}


network=""
data=""
print_usage=0
tempnames="network"

function usage() {
 echo -e "This is a script to create a everything needed for libdai learning from a Bayesian Network and sensor data file."
 echo -e "Usage: "
 echo -e "\t -h       | --help           \tPrint this message."
 echo -e "\t -n <arg> | --network   <arg>\tThe network in XMLBIF format to be used."
 echo -e "\t -d <arg> | --data      <arg>\tThe data to be used."
 echo -e "\t -o <arg> | --outname   <arg>\tThe name of the output files. '${tempfiles}' per default."
 echo -e ""
}

while [ "$1" != "" ]; do
    case "$1" in
        -n | --network )        shift
                                network="$1"
                                ;;
        -d | --data )           shift
                                data="$1"
                                ;;
        -o | --outname )        shift
                                tempnames="$1"
                                ;;
        -h | --help )           print_usage=1
                                ;;
        * )                     print_usage=1
    esac
    shift
done

if [ "$network" == "" ]; then
  print_usage=1
fi

if [ "$data" == "" ]; then
  print_usage=1
fi

# print usage if requested
if [ "$print_usage" == 1 ]; then
  usage
  exit 0
fi

checkfile "$network"
checkfile "$data"

# create factor graph
./sitinf-convert-from-xml -n "$network" -o "fg" > "${tempnames}.fg"  
# create em algorithm
./sitinf-em-config-from-xml -n "$network" > "${tempnames}.em"
# create tab data file
./sitinf-convert-from-dat -n "$network" -d "$data" > "${tempnames}.tab"
# create factorgraph and bayes network visualizations
./sitinf-convert-from-fg -f "${tempnames}.fg" | dot -Kneato -Tsvg -o "${tempnames}_fg.svg"
./sitinf-convert-from-xml -n "$network" -o adot | dot -Kneato -Tsvg -o "${tempnames}_afg.svg"
./sitinf-convert-from-xml -n "$network" -o digraph | dot -Kneato -Tsvg -o "${tempnames}_bn.svg"
