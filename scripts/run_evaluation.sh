#/bin/bash

SOURCE_EVIDENCE=$work/share/Szenario2_1M_0.10.evidence
SET_SIZES="1000 5000 10000 50000 100000 200000 500000 1000000"
BATCH_SIZES="0,5,10,100,1000,10000"
STEP_SIZES="0.6,0.7,0.8,0.9,0.95,0.99,0.999"
JOBS=4


for n in $SET_SIZES; do
  head $SOURCE_EVIDENCE -n $n > ${n}.evidence
  ./sitinf-learner-comparison \
    -d "${n}.evidence" -b "${BATCH_SIZES}" -s "${STEP_SIZES}" -j $JOBS \
    --logging-configuration $work/share/sitinf_logging_default.conf \
    --default-log-file "./${n}.log" \
    > "${n}.results"
done
