/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/inference/SimpleParameterEstimation.h              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <inference/ParameterEstimation.h>
#include <inference/EMAlgFactory.h>
#include <inference/BayesianFactorGraph.h>
#include <dai/emalg.h>

namespace sitinf {
namespace inference {

  /**
   * @brief The StepwiseEMBasedParameterEstimation class provides a ParameterEstimation for incomplete
   * evidence using a predefined BayesianNetwork and a configuration..
   *
   * The structure of the used BayesianNetwork is used as is and the probability destribution as a bias.
   * If a batch size \f$ b < | evidence | \f$ is set the parameter estimation makes intermediate estimation
   * steps with subsets of the passed evidence.
   *
   */
  class StepwiseEMBasedParameterEstimation : public ParameterEstimation {
  public:

    /**
     * @brief Ptr typedef for shared pointer type
     */
    typedef boost::shared_ptr<StepwiseEMBasedParameterEstimation> Ptr;

    /**
     * @brief StepwiseEMBasedParameterEstimation creates all required resources for parameter estimation.
     * @param network A BayesianNetwork as structure reference and probability distribution bias.
     * @param em_config A expectation maximization configuration. See EMAlgFactory.
     * @param batch_size The size of evidence batches to use in estimation. If \f$ b == 0\ or\ b > | evidence |\f$
     *         an em iteration is computed on the whole evidence set. When \f$ b < | evidence |\f$ the evidence
     *         set will be split in \f$ | evidence |\ mod\ b \f$ sets and EM is performed on them consecutively.
     */
    StepwiseEMBasedParameterEstimation(const network::BayesianNetwork& network,
                                       dai::PropertySet em_config=EMAlgFactory::defaultConfig(),
                                       utils::uint batch_size=0);

    virtual ~StepwiseEMBasedParameterEstimation() {}

    static ParameterEstimation::Ptr New(const network::BayesianNetwork& network,
                                        dai::PropertySet em_config=EMAlgFactory::defaultConfig(),
                                        utils::uint batch_size=0)
    {
      return ParameterEstimation::Ptr(new StepwiseEMBasedParameterEstimation(network,em_config,batch_size));
    }

    virtual EstimationResult estimateParameters(
        const std::vector<network::WeightedAspectStateEvidence>& evidence,
        const std::vector<network::WeightedAspectStateEvidence>& test
        ) const override;

    /**
     * @brief calculateLogLikelihood calculates the normalized log likelihood of the passed evidence given the
     * current probability distribution. Internally converts the Evidence to dai::Evidence and calls the dai-version
     * of this function.
     *
     * This can be used to evaluate the quality of a resulting probability distribution using a test evidence set.
     *
     * @param evidence a IID evidence
     * @return normalized log likelihood of the passed evidence
     */
    const utils::real calculateLogLikelihood(const std::vector<network::WeightedAspectStateEvidence>& evidence) const;

    /**
     * @brief calculateLogLikelihood calculates the normalized log likelihood of the passed evidence given the
     * current probability distribution.
     *
     * This can be used to evaluate the quality of a resulting probability distribution using a test evidence set.
     *
     * @param evidence_start the starting evidence iterator
     * @param evidence_end past the end of the evidence iterator (element is not dereferenced)
     * @return normalized log likelihood of the passed evidence
     */
    const utils::real calculateLogLikelihood(dai::Evidence::const_iterator evidence_start,
                                             dai::Evidence::const_iterator evidence_end) const;

    private:
    /**
     * @brief m_BatchSize the learning atch size.
     */
    utils::uint m_BatchSize;
    /**
     * @brief m_FactorGraph the internally used network.
     */
    BayesianFactorGraph m_FactorGraph;
    /**
     * @brief m_Algorithm the algorithm used to estimate network parameters.
     */
    EMAlgFactory::EMAlgPtr m_Algorithm;

    //TODO remove this hack. add good termination class.
    uint m_Iterations;

  };


  /**
   * @brief The SimpleParameterEstimation class provides a simple ParameterEstimation for incomplete evidence using
   * a predefined BayesianNetwork.
   *
   * The structure of the used BayesianNetwork is used as fixed so no structure learning is done.
   * The probability distribution of the used BayesianNetwork is used as a bias when learning with
   * incomplete data.
   *
   */
  class SimpleParameterEstimation : public StepwiseEMBasedParameterEstimation
  {
  public:

    /**
     * @brief Ptr typedef for shared pointer type
     */
    typedef boost::shared_ptr<SimpleParameterEstimation> Ptr;

    /**
     * @brief SimpleParameterEstimation constructor creates a SimpleParameterEstimation instance using a
     * BayesianNetwork.
     *
     * @param network a const pointer to a BayesianNetwork.
     */
    SimpleParameterEstimation(const network::BayesianNetwork& network);

    virtual ~SimpleParameterEstimation() {}

    static ParameterEstimation::Ptr New(const network::BayesianNetwork& network){
      return ParameterEstimation::Ptr(new SimpleParameterEstimation(network));
    }


  };


  /**
   * @brief The EMBasedParameterEstimation class provides a ParameterEstimation for incomplete
   * evidence using predefined BayesianNetwork and a dai::EMAlg.
   *
   * The structure of the used BayesianNetwork is used as is and the probability destribution as a bias.
   */
  class EMBasedParameterEstimation : public StepwiseEMBasedParameterEstimation {
  public:

    /**
     * @brief Ptr typedef for shared pointer type
     */
    typedef boost::shared_ptr<SimpleParameterEstimation> Ptr;

    /**
     * @brief EMBasedParameterEstimation sets up all resources for parameter estimation.
     * @param network the network which structure and distribution bias.
     * @param em_config the configuration of the em algorithm to use.
     */
    EMBasedParameterEstimation(const network::BayesianNetwork& network,
                               dai::PropertySet em_config=EMAlgFactory::defaultConfig());

    virtual ~EMBasedParameterEstimation() {}

    static ParameterEstimation::Ptr New(const network::BayesianNetwork& network,
                                 dai::PropertySet em_config=EMAlgFactory::defaultConfig())
    {
      return ParameterEstimation::Ptr(new EMBasedParameterEstimation(network,em_config));
    }

  };


} // inference
} // namespace sitinf
