/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/inference/BayesianInference.cpp                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <inference/BayesianInference.h>
#include <utils/Exception.h>
#include <utils/Macros.h>

#define _LOGGER "inference.BayesianInference"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::network;
using namespace sitinf::inference;

void inference_junction_tree(dai::FactorGraph& fg,
                             dai::PropertySet& opts,
                             std::vector<size_t>& outcomes,
                             std::vector<dai::Factor>& probabilities,
                             size_t max_states){
  // bound width of junction tree check size
  try{
    dai::boundTreewidth(fg,&dai::eliminationCost_MinFill,max_states);
  } catch (dai::Exception& e){
    STHROW(Exception,"Junction Tree inference will not work because of memory of maximum states limit.");
  }

  // create jtree init and run
  dai::JTree alg(fg, opts("updates",std::string("HUGIN")));
  alg.init();
  alg.run();

  // set outcomes from algorithm
  outcomes = alg.findMaximum();

  // set probabilities ffrom algorithm
  probabilities.clear();
  for(size_t i = 0; i < alg.nrVars(); i++ ) probabilities.push_back(alg.belief(fg.var(i)));
}

void inference_bp_sumprod(dai::FactorGraph& fg,
                          dai::PropertySet& opts,
                          std::vector<size_t>& outcomes,
                          std::vector<dai::Factor>& probabilities)
{
  // create algorithm init and run
  dai::BP alg(fg, opts
              ("updates",std::string("SEQRND"))
              ("logdomain",false)
              );
  alg.init();
  alg.run();

  // set outcomes from algorithm
  outcomes = alg.findMaximum();

  // set probabilities ffrom algorithm
  probabilities.clear();
  for(size_t i = 0; i < alg.nrVars(); i++ ) probabilities.push_back(alg.belief(fg.var(i)));
}

void inference_bp_maxprod(dai::FactorGraph& fg,
                          dai::PropertySet& opts,
                          std::vector<size_t>& outcomes,
                          std::vector<dai::Factor>& probabilities)
{
  // create algorithm init and run
  dai::BP alg(fg, opts
              ("updates",std::string("SEQRND"))
              ("logdomain",false)
              ("inference",std::string("MAXPROD"))
              ("damping",std::string("0.1"))
              );
  alg.init();
  alg.run();

  // set outcomes from algorithm
  outcomes = alg.findMaximum();

  // set probabilities ffrom algorithm
  probabilities.clear();
  for(size_t i = 0; i < alg.nrVars(); i++ ) probabilities.push_back(alg.belief(fg.var(i)));
}

void inference_decimation(dai::FactorGraph& fg,
                          dai::PropertySet& opts,
                          std::vector<size_t>& outcomes,
                          std::vector<dai::Factor>& probabilities)
{
  // set additional options, init, run, return
  dai::DecMAP alg(fg, opts
                  ("reinit",true)
                  ("ianame",std::string("BP"))
                  ("iaopts",std::string("[damping=0.1,inference=MAXPROD,logdomain=0,maxiter=1000,tol=1e-9,updates=SEQRND,verbose=1]")
                   ));
  alg.init();
  alg.run();

  // set outcomes from algorithm
  outcomes = alg.findMaximum();

  // set probabilities ffrom algorithm
  probabilities.clear();
  for(size_t i = 0; i < alg.nrVars(); i++ ) probabilities.push_back(alg.belief((fg.var(i))));
}

BayesianInference::BayesianInference(InferenceAlgorithm algorithm,
                                     size_t maximum_states,
                                     size_t maximum_iterations,
                                     real tolerance,
                                     size_t verbosity)
  : m_Algorithm(algorithm), m_MaxStates(maximum_states), m_MaxIterations(maximum_iterations),
    m_Tolerance(tolerance), m_Verbosity(verbosity)
{ /* nothing to do */ }

BayesianInference::~BayesianInference()
{ /* nothing to do */ }

InferenceResult::Ptr BayesianInference::infer(BayesianFactorGraph* bayesianFactorGraph,
    AspectStateEvidence* evidence)
{
  SLOG_TRACE << "calculating inference";
  // get clamped factor graph
  if(!bayesianFactorGraph) STHROW(Exception,"Passed BayesianFactorGraph ins null");
  dai::FactorGraph fg = bayesianFactorGraph->getClampedFactorGraph(*evidence);
  SLOG_TRACE << "factor graph created and clamped";

  // create an options object
  dai::PropertySet opts;
  opts.set("maxiter",m_MaxIterations);
  opts.set("tol",m_Tolerance);
  opts.set("verbose",m_Verbosity);

  // init and run the algorithm
  std::vector<size_t> outcomes;
  std::vector<dai::Factor> probabilities;
  switch(m_Algorithm){
    case JunctionTree:
      SLOG_TRACE << "running JunctionTree inference";
      inference_junction_tree(fg,opts,outcomes,probabilities,m_MaxStates);
      break;
    case BeliefPropagationSumprod:
      SLOG_TRACE << "running BeliefPropagation-Sumproduct inference";
      inference_bp_sumprod(fg,opts,outcomes,probabilities);
      break;
    case BeliefPropagationMaxprod:
      SLOG_TRACE << "runnign BeliegPropagation-Maxproduct inference";
      inference_bp_maxprod(fg,opts,outcomes,probabilities);
      break;
    case Decimation:
      SLOG_TRACE << "running Decimation inference";
      inference_decimation(fg,opts,outcomes,probabilities);
      break;
    default:
      STHROW(Exception,"Unknown InferenceAlgorithm '" << m_Algorithm << "'");
  }

  // create result object
  const std::vector<BayesianNetwork::Variable>& variables = bayesianFactorGraph->asNetwork().variables(); // variables
  InferenceResult::Ptr ret = InferenceResult::Ptr(new InferenceResult()); // result
  for(uint i = 0; i < variables.size(); ++i){
    // create and set outcome
    InferenceResult::Outcomes outcome;
    // set max probable outcome
    outcome.m_MaxOutcome = outcomes.at(i);
    // add all outcome probabilities
    dai::Factor belief = probabilities.at(i);
    for(uint j = 0; j < belief.nrStates(); ++j){ outcome.m_Probabilities.push_back(belief.get(j)); }
    // add outcome
    ret->getOutcomes()[variables.at(i).m_Name] = outcome;
  }
  return ret;
}
