/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/inference/SimpleParameterEstimation.cpp            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <inference/SimpleParameterEstimation.h>
#include <utils/Types.h>
#include <utils/Exception.h>
#include <utils/Macros.h>

#define _LOGGER "inference.SimpleParameterEstimation"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::inference;


SimpleParameterEstimation::SimpleParameterEstimation(const network::BayesianNetwork& network)
  : StepwiseEMBasedParameterEstimation(network,EMAlgFactory::defaultConfig(),0)
{}


EMBasedParameterEstimation::EMBasedParameterEstimation(
    const network::BayesianNetwork& network,
    dai::PropertySet em_config)
  : StepwiseEMBasedParameterEstimation(network,em_config,0)
{}


StepwiseEMBasedParameterEstimation::StepwiseEMBasedParameterEstimation(const network::BayesianNetwork& network,
    dai::PropertySet em_config,
    utils::uint batch_size)
  : m_BatchSize(batch_size),
    m_FactorGraph(network),
    m_Algorithm(EMAlgFactory::create(m_FactorGraph.asFactorGraph(),em_config)),
    m_Iterations(10)
{
  //TODO: add good termination strategy
  if(em_config.hasKey("termination_properties")){
    dai::PropertySet estimationProps = em_config.getAs<dai::PropertySet>("termination_properties");
    if(estimationProps.hasKey("max_iters")){
      m_Iterations = estimationProps.getAs<size_t>("max_iters");
    }
  }
}

EstimationResult StepwiseEMBasedParameterEstimation::estimateParameters(
    const std::vector<network::WeightedAspectStateEvidence>& evidence,
    const std::vector<network::WeightedAspectStateEvidence>& test) const
{
  SASSERT_THROW(!evidence.empty(),IllegalArgumentException,"Can't estimate probability distribution "
                "for an empty evidence set.");
  TIMED_FUNC(LearningTimer);
  SLOG_TRACE << "Estimating network with " << evidence.size() << " learning and " << test.size() << " test cases.";
  EstimationResult result;

  std::vector<dai::Evidence::Observation> learnSet;
  learnSet.reserve(evidence.size());
  for (auto e : evidence) {
    learnSet.push_back(m_FactorGraph.convertEvidence(e));
  }
  std::vector<dai::Evidence::Observation> testSet;
  testSet.reserve(test.size());
  for (auto t : test) {
    testSet.push_back(m_FactorGraph.convertEvidence(t));
  }

  std::vector<dai::Evidence::const_iterator> batches;
  uint batchSize = (m_BatchSize == 0 || m_BatchSize > learnSet.size()) ? learnSet.size() : m_BatchSize;
  uint batchCount = learnSet.size() / batchSize;
  SLOG_TRACE
    << "Learning with " << batchCount << " batches of size " << batchSize
    << " ignoring the last " << learnSet.size() % batchSize << " samples.";
  for(uint i = 0; i < batchCount; ++i){
    batches.push_back(learnSet.begin() + batchSize * i);
  }

  PERFORMANCE_CHECKPOINT_WITH_ID(LearningTimer,"learning setup");

  // Iterate EM until convergence
  for(uint i = 0; i < m_Iterations; ++i){
    result.testLogLikelihoods.push_back(calculateLogLikelihood(testSet.begin(),testSet.end()));
    dai::Real l = 0.;
    for(auto batch : batches){
      l += m_Algorithm->iterate(batch, batchSize); // iterate returns the not normalized likelihood before estimation
      SLOG_INFO << "likelihood " << l << " evidence #" << batchSize;
      SLOG_INFO << "Iteration " << m_Algorithm->Iterations() << " likelihood: " << l;
      PERFORMANCE_CHECKPOINT_WITH_ID(LearningTimer,"learning batch");
    }
    result.estimationLogLikelihoods.push_back(l/(real) batchCount);
    PERFORMANCE_CHECKPOINT_WITH_ID(LearningTimer,"learning iteration");
  }

  // calculate final likelihoods
  result.testLogLikelihoods.push_back(calculateLogLikelihood(testSet.begin(),testSet.end()));
  result.estimationLogLikelihoods.push_back(calculateLogLikelihood(learnSet.begin(),learnSet.end()));

  // copy old network and update cpd's
  result.network = m_FactorGraph.updatedNetwork(m_Algorithm->eStep().fg());
  PERFORMANCE_CHECKPOINT_WITH_ID(LearningTimer,"update network");
  return result;
}

const utils::real StepwiseEMBasedParameterEstimation::calculateLogLikelihood(
    const std::vector<network::WeightedAspectStateEvidence>& evidence) const
{
  SASSERT_THROW(!evidence.empty(),IllegalArgumentException,"Can't caclulate likelihood for an empty evidence set.");
  TIMED_FUNC(LikelihoodEstimationTimer);
  std::vector<dai::Evidence::Observation> observations;
  observations.reserve(evidence.size());
  for (auto e : evidence) {
    observations.push_back(m_FactorGraph.convertEvidence(e));
  }
  PERFORMANCE_CHECKPOINT_WITH_ID(LikelihoodEstimationTimer,"likelihood setup");
  return calculateLogLikelihood(observations.begin(),observations.end());
}

const utils::real StepwiseEMBasedParameterEstimation::calculateLogLikelihood(
    dai::Evidence::const_iterator evidence_start,
    dai::Evidence::const_iterator evidence_end) const
{
  if(evidence_start == evidence_end){
    SLOG_TRACE << "Empty evidence set passed to likelihood estimation. Result will be -1";
    return -1.;
  }
  TIMED_FUNC(LikelihoodEstimationTimer);
  dai::InfAlg* alg = m_Algorithm->eStep().clone();
  alg->run();
  real logZ = alg->logZ();
  real likelihood = 0.;
  real sumWeight = 0.;
  uint count = 0;
  for (auto observation = evidence_start; observation != evidence_end; ++observation, ++count) {
    dai::InfAlg* clamped = alg->clone();
    // Apply evidence
    for(auto aspect : *observation) {
      if(aspect.second == (uint) -1) { // ignore unobserved/unknown values
        continue;
      }
      try {
        clamped->clamp(clamped->fg().findVar(aspect.first), aspect.second );
      } catch (const std::exception& e) {
          SLOG_WARNING << "Error while clamping dai::FactorGraph at "
                       << clamped->fg().findVar(aspect.first)
                       << " to " << aspect.second << ". Error: " << e.what();
      } catch (...) {
          SLOG_WARNING << "Error while clamping dai::FactorGraph at "
                       << clamped->fg().findVar(aspect.first)
                       << " to " << aspect.second;
      }
    }
    clamped->init();
    clamped->run();
    likelihood += (clamped->logZ() - logZ) * observation->getWeight();
    sumWeight += observation->getWeight();
    delete clamped;
  }
  delete alg;
  SLOG_TRACE << "Likelihood: " << likelihood << " in " << count << " samples with a combined weight of " << sumWeight;
  return likelihood / sumWeight;

}
