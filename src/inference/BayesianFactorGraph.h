/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/inference/BayesianFactorGraph.h                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <dai/alldai.h>

#include <network/BayesianNetwork.h>
#include <network/AspectStateEvidence.h>

namespace sitinf {
namespace inference {

  /**
   * This class combines the BayesianNetwork description with the FactorGraph representation.
   */
  class BayesianFactorGraph {
  public:

    /**
     * @brief Ptr typedef for shared pointer type
     */
    typedef boost::shared_ptr<BayesianFactorGraph> Ptr;

    /**
     * @brief BayesianFactorGraph constructor from a BayesianNetwork.
     * @param network used to create a factor graph representateion.
     */
    BayesianFactorGraph(const network::BayesianNetwork& network=network::BayesianNetwork());

    /**
     * @brief asFactorGraph returns a FactorGraph representation of this.
     * @return a dai::FactorGraph representation
     */
    const dai::FactorGraph& asFactorGraph() const;

    /**
     * @brief asNetwork returns a BaxesianNetwork representation of this.
     * @return a const BayesianNetwork::Ptr
     */
    const network::BayesianNetwork& asNetwork() const;

    /**
     * @brief getClampedFactorGraph creates a clamped FactorGraph representation of this using the passed Evidence.
     * @param network_state AspectStateEvidence holding known data about this Network
     * @return a clamped FactorGraph.
     */
    dai::FactorGraph getClampedFactorGraph(const network::AspectStateEvidence& network_state) const;

    /**
     * @brief updatedNetwork creates a new network with updated probabilities from the passed dai::FactorGraph.
     * @param factor_graph a matching dai::FactorGraph providing the probabilities for the network.
     * @return a copy of this BayesianNetwork with the probabilities from factor_graph.
     */
    network::BayesianNetwork updatedNetwork(const dai::FactorGraph& factor_graph) const;

    /**
     * @brief factorGraphVariable can be used to access the dai::Var corresponding to a BayesianNetwork::Variable.
     * @param variable_name the id/name of the BayesianNetwork::Variable.
     * @return the dai::Var in the dai::FactorGraph representing the Bayes variable.
     */
    dai::Var factorGraphVariable(const std::string& variable_name) const;

    /**
     * @brief factorGraphVariableState can be used to access the dai::FactorGraph representation of a
     * BayesianNetwork::Variables state.
     *
     * @param variable the id/name of the variable
     * @param value the value/state of the variable
     * @return the dai::FactorGraph representation of the variable-state
     */
    size_t factorGraphVariableState(const std::string& variable, const std::string& value) const;

    /**
     * @brief bayesianVariable can be used to get the name of the BayesianNetwork::Variable corresponding
     * to a dai::FactorGraph Var.
     *
     * @param variable a dai::FactorGraph variable
     * @return the name of the corresponding BayesianNetwork::Variable
     */
    std::string bayesianVariable(const dai::Var& variable) const;

    /**
     * @brief bayesianVariableState can be used to get the state name (outcome) of a BayesianNetwork::Variable
     * corresponding to a dai::Var and its state value
     * @param variable the variable which state is requested
     * @param value the state of the variable
     * @return the corresponding state in the BayesianNetwork
     */
    std::string bayesianVariableState(const dai::Var& variable, const size_t& value) const;

    /**
     * @brief isEmpty tells whether ihis BayesianFactorGraph is empty.
     * @return true when factors count is empty.
     */
    bool isEmpty() const;

    /**
     * @brief convertEvidence creates a dai::Evidence::Observation that reflects the passed AspectStateEvidence.
     * @param evidence the evidence to translate to dai::Evidence
     * @param throwing whether to throw an Exception when aspects or states in evidence so not exist in this Network.
     * @return a dai::Evidence representation of the known variables and states.
     */
    dai::Evidence::Observation convertEvidence(const network::AspectStateEvidence& evidence, bool throwing=false) const;

    /**
     * @brief convertEvidence creates a AspectStateEvidence that reflects the passed AspectStateEvidence.
     * @param evidence the evidence to translate to AspectStateEvidence
     * @param throwing whether to throw an Exception when variables or states in evidence do not exist in this Network.
     * @return a WeightedAspectStateEvidence representation of the known variables and states.
     */
    network::WeightedAspectStateEvidence convertEvidence(const dai::Evidence::Observation& evidence, bool throwing=false) const;

  private:
    /**
     * @brief m_Network holds the BayesianNetwork network
     */
    network::BayesianNetwork m_Network;

    /**
     * @brief m_Graph holds the internal FactorGraph.
     */
    dai::FactorGraph m_Graph;

    std::vector<dai::Permute> m_TableMapping;


  };

} // inference

namespace utils {

    template<>
    class Converter<inference::BayesianFactorGraph,const network::BayesianNetwork&> {

      public:

      /**
       * @brief Ptr a typedef to a shared instance of this.
       */
      typedef boost::shared_ptr<Converter<inference::BayesianFactorGraph,const network::BayesianNetwork&> > Ptr;

      virtual inference::BayesianFactorGraph convert(const network::BayesianNetwork& data) final;
    };

    template<>
    class Converter<inference::BayesianFactorGraph, network::BayesianNetwork::CPtr> {

      public:

      /**
       * @brief Ptr a typedef to a shared instance of this.
       */
      typedef boost::shared_ptr<Converter<inference::BayesianFactorGraph,network::BayesianNetwork::CPtr> > Ptr;

      virtual inference::BayesianFactorGraph convert(network::BayesianNetwork::CPtr data) final;
    };

    template<>
    class Converter<inference::BayesianFactorGraph::Ptr,network::BayesianNetwork::Ptr> {

      public:

      /**
       * @brief Ptr a typedef to a shared instance of this.
       */
      typedef boost::shared_ptr<Converter<inference::BayesianFactorGraph::Ptr,network::BayesianNetwork::Ptr> > Ptr;

      virtual inference::BayesianFactorGraph::Ptr convert(network::BayesianNetwork::Ptr data) final;
    };

} // namespace utils

} // namespace sitinf
