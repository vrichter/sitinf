/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/inference/BayesianFactorGraph.cpp                  **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <inference/BayesianFactorGraph.h>
#include <utils/Exception.h>
#include <utils/Macros.h>

#define _LOGGER "inference.BayesianFactorGraph"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::network;
using namespace sitinf::inference;

BayesianFactorGraph::BayesianFactorGraph(const BayesianNetwork& network)
  : m_Network(network)
{
  if(m_Network.variables().empty()) return; // nothing to do if no network provided
  // create new factors vector. reserve space
  std::vector<dai::Factor> factors;
  factors.reserve(m_Network.size());

  // create a factor for every node in the bayes network
  for(uint i = 0; i < m_Network.size(); ++i) {
      const BayesianNetwork::Definition& def = m_Network.definition(i);

      // number of factor members
      uint members = def.m_Variables.size();

      // get member ids
      std::vector<uint> variableIds = m_Network.variableIds(def.m_Variables);

      // get member dimensions
      std::vector<uint> variableDims = m_Network.variableDimensions(def.m_Variables);

      // create Factor
      std::vector<dai::Var> fVars;
      fVars.reserve(members);
      // sort the variables so that the BayesNet.Table conforms the factor graph table
      // the first variable is the one that changes its value the fastest
      fVars.push_back(dai::Var(variableIds.front(), variableDims.front()));
      // the other variables should be in reversed order
      for(uint j = members-1; j > 0; --j){
          fVars.push_back(dai::Var(variableIds[j], variableDims[j]));
        }
      dai::Factor factor(dai::VarSet(fVars.begin(),fVars.end(),fVars.size()),(real) 0.);

      // use libdais permutation object to get correct inexing
      m_TableMapping.push_back(dai::Permute(fVars));
      // set values of the factor table
      for(uint i = 0; i < def.m_Table.size(); ++i){
          factor.set(m_TableMapping.back().convertLinearIndex(i),def.m_Table[i]);
        }

      // add factor to graph
      factors.push_back(factor);
    }

    // create fator graph and fill it with factors
    m_Graph = dai::FactorGraph(factors);
}

const dai::FactorGraph& BayesianFactorGraph::asFactorGraph() const {
    return m_Graph;
}

const BayesianNetwork& BayesianFactorGraph::asNetwork() const {
  return m_Network;
}

dai::FactorGraph BayesianFactorGraph::getClampedFactorGraph(const AspectStateEvidence& network_state) const {
  // copy factor graph
  dai::FactorGraph ret = m_Graph;
  auto states = network_state.currentState();
  for(auto it : states){
    // ignore unobserved states and unknown variables
    if(it.second == "__unobserved__"){
      continue;
    } else if(!m_Network.knows(it.first,it.second)){
      SLOG_WARNING << "Ignoring unknown variable-state combination " << it.first << " = " << it.second << "'.";
      continue;
    }
    // clamp everything else
    uint id = m_Network.variableId(it.first);
    uint outcome = m_Network.variable(id).outcome(it.second);
    try {
      ret.clamp(id,outcome,false);
    } catch (const std::exception& e) {
        SLOG_WARNING << "Error while clamping dai::FactorGraph at " << id << "(" << it.first << ") to "
                     << outcome << "(" << it.second << ")" << ". Error: " << e.what();
    } catch (...) {
        SLOG_WARNING << "Error while clamping dai::FactorGraph at " << id << "(" << it.first << ") to "
                     << outcome << "(" << it.second << ")" << ".";
    }
  }
  return ret;
}

BayesianNetwork BayesianFactorGraph::updatedNetwork(const dai::FactorGraph& factor_graph) const {
  SASSERT_THROW(m_Network.definitions().size() == factor_graph.vars().size(),
                NetworkInconsistentException,
                "Passed FactorGraph is inconsistent with the BayesNetwork"
                );
  std::vector<BayesianNetwork::Definition> definitions = m_Network.definitions();
  for(uint i = 0; i < factor_graph.factors().size(); ++i){
    const dai::Factor& factor = factor_graph.factor(i);
    for(uint j = 0; j < definitions.at(i).m_Table.size(); ++j){
      definitions.at(i).m_Table.at(j) = factor.get(m_TableMapping.at(i).convertLinearIndex(j));
    }
  }
  return BayesianNetwork(m_Network.variables(),definitions,m_Network.name(),m_Network.comment());
}

dai::Var BayesianFactorGraph::factorGraphVariable(const std::string& variable_name) const{
  uint id = m_Network.variableId(variable_name);
  SASSERT_THROW(id < m_Graph.nrVars(),NotFoundException,
                "Variable with name '" << variable_name << "' not found in FactorGraph.");
  return m_Graph.var(id);
}

size_t BayesianFactorGraph::factorGraphVariableState(const std::string& variable, const std::string& value) const{
  return m_Network.variable(variable).outcome(value);
}

bool BayesianFactorGraph::isEmpty() const{
  return m_Graph.factors().size() == 0;
}

std::string BayesianFactorGraph::bayesianVariable(const dai::Var& variable) const{
  return m_Network.variable(variable.label()).m_Name;
}

std::string BayesianFactorGraph::bayesianVariableState(const dai::Var& variable, const size_t&value) const{
  const BayesianNetwork::Variable& var = m_Network.variable(variable.label());
  SASSERT_THROW(value < var.m_Outcomes.size(),NotFoundException,
                "Variable with name '" << var.m_Name << "' does not have an outcome with id '" << value << "'.");
  return var.m_Outcomes.at(value);
}

dai::Evidence::Observation BayesianFactorGraph::convertEvidence(const AspectStateEvidence& evidence, bool throwing) const{
  /*
   * convert AspectStateEvidence to the Evidence::Observation format of libdai
   *
   * AspectStateEvidence variables and states must me mapped to Variables and steates of
   * the FactorGraph.
   */
  dai::Evidence::Observation observation;
  for(auto it : evidence.currentState()){
    try {
      observation[factorGraphVariable(it.first)] = factorGraphVariableState(it.first,it.second);
    } catch (NotFoundException& e){
      if(!throwing){
        SLOG_WARNING << "Error while adding observation " << it.first << " = " << it.second << " : " << e.what();
      } else {
        throw;
      }
    }
  }
  // if evidence is weighted set the weight according to evidence
  try{
    const WeightedAspectStateEvidence& weightedEvidence = dynamic_cast<const WeightedAspectStateEvidence&>(evidence);
    observation.setWeight(weightedEvidence.weight());
  } catch (std::bad_cast& e) {
    // thats okay. not every eidence is weighted
  }
  return observation;
}

WeightedAspectStateEvidence BayesianFactorGraph::convertEvidence(const dai::Evidence::Observation& evidence, bool throwing) const{
  WeightedAspectStateEvidence observation;
  for(auto it = evidence.begin(); it != evidence.end(); ++it){
    try {
    observation.newEvidence(
          bayesianVariable(it->first),
          bayesianVariableState(it->first,it->second)
          );
    } catch (NotFoundException& e){
      if (!throwing){
        SLOG_WARNING << "Error while adding observation " << it->first << " = " << it->second << " : " << e.what();
      } else {
        throw;
      }
    }
  }
  observation.weight(evidence.getWeight());
  return observation;

}

BayesianFactorGraph Converter<BayesianFactorGraph, const BayesianNetwork&>::
convert(const BayesianNetwork& data){
  return BayesianFactorGraph(data);
}

BayesianFactorGraph Converter<BayesianFactorGraph, BayesianNetwork::CPtr>::
convert(BayesianNetwork::CPtr data){
  return BayesianFactorGraph(*data);
}

BayesianFactorGraph::Ptr Converter<BayesianFactorGraph::Ptr, BayesianNetwork::Ptr>::
convert(const BayesianNetwork::Ptr data){
  return BayesianFactorGraph::Ptr(new BayesianFactorGraph(*data));
}
