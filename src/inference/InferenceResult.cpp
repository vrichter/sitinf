/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/inference/InferenceResult.cpp                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <inference/InferenceResult.h>
#include <utils/Exception.h>

using namespace sitinf;
using namespace sitinf::inference;

// some typedefs for lesser writing

InferenceResult::InferenceResult(){/* nothing to do for now */}

InferenceResult::InferenceResult(const ClassificationResultMapPtr data, const network::BayesianNetwork& network){
  for(int i = 0; i < data->aspects_size(); ++i){
    // get data from the message
    auto entry = data->aspects(i);
    auto result = entry.result();
    const network::BayesianNetwork::Variable& variable = network.variable(entry.name());

    Outcomes outcome;
    // max outcome corresponds to decided class
    outcome.m_MaxOutcome = variable.outcome(result.decided_class());
    // the probabilities vector should represent all outcomes. when some are not
    // set in Situation they will stay at 0. probability
    outcome.m_Probabilities.resize(variable.m_Outcomes.size(),0.);
    for(int j = 0; j < result.classes_size(); ++j){
      auto clazz = result.classes(j);
      // set probability with id from variable
      outcome.m_Probabilities[variable.outcome(clazz.name())] = clazz.confidence();
    }
    m_Outcomes[entry.name()] = outcome;
  }
}

std::map<std::string,InferenceResult::Outcomes>& InferenceResult::getOutcomes(){
  return m_Outcomes;
}

InferenceResult::ClassificationResultMapPtr InferenceResult::toClassificationResultMap(const network::BayesianNetwork& network){
  // create the Situation object
  ClassificationResultMapPtr classificationMap(new ClassificationResultMap());
  // iterate over the results
  for(auto it = m_Outcomes.begin(); it != m_Outcomes.end(); ++it)
  {
    // get variable
    network::BayesianNetwork::Variable variable = network.variable(it->first);
    // create aspect classification
    auto aspect = classificationMap->add_aspects();
    InferenceResult::Outcomes& resultOutcome = it->second;
    // set aspect name from result
    aspect->set_name(it->first);
    // create classification
    auto state = aspect->mutable_result();
    // set class probabilities from outcomes
    for(uint j = 0; j < resultOutcome.m_Probabilities.size(); ++j){
      auto outcome = state->add_classes();
      // class name is the outcome of the variable
      outcome->set_name(variable.m_Outcomes.at(j));
      // confidence is the corresponding belief
      outcome->set_confidence(resultOutcome.m_Probabilities.at(j));
    }
    // set decided class from max
    //state->set_decided_class(variable.m_Outcomes.at(resultOutcome.m_MaxOutcome)); TODO: somehow this does not work. Check that!
    double most_probable = 0.;
    for (auto state_class : state->classes()) {
      if(state_class.confidence() > most_probable){
         state->set_decided_class(state_class.name());
         most_probable = state_class.confidence();
      }
    }
  }
  return classificationMap;
}

