/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/inference/EMAlgFactory.h                           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <inference/BayesianFactorGraph.h>
#include <dai/emalg.h>

namespace sitinf {
namespace inference {

  /**
   * @brief The EMAlgFactory class creates dai::EMAlg instances for a dai::FactorGraph and dai::PropertySet config.
   *
   * The resulting EMAlg consists of one maximization step without shared parameters and uses the ProbbilityEstimation
   * configured in the passed Property set.
   *
   */
  class EMAlgFactory {
  public:

    /**
     * @brief EMAlgPtr A shared dai::EMAlg instance.
     */
    typedef boost::shared_ptr<dai::EMAlg> EMAlgPtr;

    /**
     * @brief create s a dai::EMAlg with one Maximization step and without parameter sharing.
     * @param fg the dai::FactorGraph to be optimize.
     * @param config a configuration. see defaultConfig()
     * @param pseudo_counts a vector of pseudo counts. One for every factor. When this is empty all pseudo_counts will
     *         be i nitialized to the default_pseudo_count configured in config or 1 if not configured.
     * @return a run-ready dai::EMAlg
     */
    static utils::Shared<dai::EMAlg>::Ptr create(
       const dai::FactorGraph& fg,
        const dai::PropertySet& config = defaultConfig(),
        const std::vector<utils::real>& pseudo_counts = std::vector<utils::real>()
        );

    /**
     * @brief defaultConfig a default dai::PropertySet for the EMAlgFactory::create() method.
     *
     * The default configuration is the following:
     * \verbatim
       [
         inference = JTREE,
         inference_properties = [ verbose = 1, updates = HUGIN ],
         estimation = CondProbEstimation,
         estimation_properties = [ ],
         default_pseudo_count = 1,
         termination_properties = [ max_iters = 10 ]
       ]
      \endverbatim
     * @return a default configuration.
     */
    static dai::PropertySet defaultConfig();

  };

} // inference
} // sitinf
