/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/inference/InferenceResult.h                        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Types.h>
#include <network/BayesianNetwork.h>

#include <rst/classification/ClassificationResultMap.pb.h>

namespace sitinf {
namespace inference {

  /**
   * @brief The InferenceResult struct holds the result of an inference
   * calculation on a BayesianFactorGraph.
   */
  class InferenceResult{

  public:
    /**
     * @brief Ptr typedef for shared pointer type
     */
    typedef boost::shared_ptr<InferenceResult> Ptr;

    typedef rst::classification::ClassificationResultMap ClassificationResultMap;
    typedef boost::shared_ptr<ClassificationResultMap> ClassificationResultMapPtr;

    /**
     * @brief The Outcomes struct holds the maximum probable
     * outcome of a variable and a list of all outcome probabilities
     * sorted by the variable.
     */
    struct Outcomes{
      /**
       * @brief m_MaxOutcome the id of the maximum probable outcome.
       */
      uint m_MaxOutcome;
      /**
       * @brief m_Probabilities the probabilities of all outcomes of the variable.
       */
      std::vector<utils::real> m_Probabilities;
      /**
       * @brief Outcomes default constructor setting m_Maxoutcome to -1
       */
      Outcomes() : m_MaxOutcome(-1){}
    };


    /**
     * @brief InferenceResult creates an InferenceResult and fills it with information from data.
     * @param data ClassificationResultMapPtr data
     * @param network the corresponding BayesianNetwork.
     * @throw this constructor may throw if data and network do not match
     */
    InferenceResult(const ClassificationResultMapPtr data, const network::BayesianNetwork& network);

    /**
     * @brief InferenceResult default constructor creates an empty instance.
     */
    InferenceResult();

    /**
     * @brief getOutcomes a getter for the inner Outcomes map.
     * @return maps inference variables to their outcomes
     */
    std::map<std::string,Outcomes>& getOutcomes();

    /**
     * @brief toClassificationResultMap creates a ClassificationResultMapPtr representation of this
     * InferenceResult
     * @param network the network corresponding to this result
     * @return A ClassificationResultMapPtr containing the informaiton from network
     */
    ClassificationResultMapPtr toClassificationResultMap(const network::BayesianNetwork& network);

  private:
    /**
     * @brief m_Outcomes maps variable names to their Outcomes
     */
    std::map<std::string,Outcomes> m_Outcomes;
  };

} // inference
} // namespace sitinf
