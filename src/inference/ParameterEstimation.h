/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/inference/ParameterEstimation.h                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <network/BayesianNetwork.h>
#include <network/AspectStateEvidence.h>

namespace sitinf {
namespace inference {

  /**
   * @brief The EstimationResult class is created in ParameterEstimation::estimateParameters().
   *
   * It carries a BayesianNetwork that may have produced the passed evidence with a high probability
   * likelihoods of the passed evidence and likelihoods of the passed test data.
   */
  class EstimationResult {
  public:

    /**
     * @brief network a BayesianNetwork that explains the generation process of a set of evidence.
     */
    network::BayesianNetwork network;

    /**
     * @brief estimationLikelihoods The normalized log likelihood of the evidence set before and after network
     * estimation.
     *
     * In the case of multiple consecutive estimations - like in em - intermediate likelihoods should be retained
     * for further analysis.
     */
    std::vector<utils::real> estimationLogLikelihoods;

    /**
     * @brief testLogLikelihoods The normalized log likelihood of the test set before and after network estimation.
     *
     * In the case of multiple consecutive estimations - like in em - intermediate likelihoods should be retained
     * for further too analysis.
     */
    std::vector<utils::real> testLogLikelihoods;
  };

  /**
   * @brief The ParameterEstimation class can be used to estimate the probabilities of a BayesianNetwork
   * using a data set consisting of independent and identically distributed (IID) sampled AspectStateEvidence.
   */
  class ParameterEstimation
  {
  public:

    /**
     * @brief Ptr typedef for shared pointer type
     */
    typedef boost::shared_ptr<ParameterEstimation> Ptr;

    /**
     * @brief ~ParameterEstimation virtual destructor for nice cleanup
     */
    virtual ~ParameterEstimation() {}

    /**
     * @brief estimateParameters estimates a probability distribution given evidence.
     *
     * Assuming the passed evidence is independently and identically distributed sampled
     * from some BayesianNetwork \f$ B \f$ this method tries to estimate a BayesianNetwork \f$ B' \approx B \f$
     * using the evidence.
     *
     * @param evidence IID sampled evidence
     * @param test IID sampled test data to calculate test set performance of the estimated network. May be empty.
     * @return An EstimationResult with the resulting network and likelihoods.
     */
    virtual EstimationResult estimateParameters(
        const std::vector<network::WeightedAspectStateEvidence>& evidence,
        const std::vector<network::WeightedAspectStateEvidence>& test = std::vector<network::WeightedAspectStateEvidence>()
        ) const = 0 ;


  };

} // inference
} // namespace sitinf
