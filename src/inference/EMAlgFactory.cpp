/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/inference/EMAlgFactory.cpp                         **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <inference/EMAlgFactory.h>
#include <utils/Types.h>
#include <utils/Exception.h>
#include <utils/Macros.h>

#define _LOGGER "inference.EMAlgFactory"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::inference;

std::vector<dai::SharedParameters> createSharedParameters(
  dai::FactorGraph fg, const std::string& estimation, const dai::PropertySet& estimation_properties,
  const std::vector<real>& pseudo_counts
)
{
  /*
   *
   * Create a SharetParameters for every Factor with
   * - Parameterestimation = estimation
   * -- target dimension = dimension of first variable = dimension of variable corresp. to this Definition
   * -- total dimension = size of the cpt = |variable| * |parent_1| * ... * |parent_n
   * -- orientations according to variable ordering in fg = this, parents
   *
   */
  std::vector<dai::SharedParameters> result;
  result.reserve(fg.factors().size());
  for(uint i = 0; i < fg.factors().size(); ++i) {
    auto& factor = fg.factor(i);
    dai::PropertySet estimationProperties(estimation_properties);
    estimationProperties.set("target_dim",fg.var(i).states());
    for (uint f = 0; f < factor.vars().size(); ++f) {
      SLOG_TRACE << fg.var(i).label() << " - " << fg.var(i).states() << " - " << fg.factor(i).nrStates();
    }
    estimationProperties.set("total_dim",(size_t)fg.factor(i).nrStates());
    estimationProperties.set("pseudo_count",pseudo_counts.at(i));

    // only one orientation per factor -> no parameter sharing
    dai::SharedParameters::FactorOrientations orientations;
    std::vector<dai::Var> orientation;
    orientation.reserve(fg.factor(i).vars().size());
    orientation.push_back(fg.var(i));
    for (auto var = fg.factor(i).vars().begin(); var != fg.factor(i).vars().end(); ++var) {
      if(var->label() != fg.var(i).label()) {
        orientation.push_back(*var);
      }
    }
    orientations[i] = orientation;

    dai::SharedParameters parameters(
      orientations,
      dai::ParameterEstimation::construct(estimation,estimationProperties),
      true
    );
    SLOG_TRACE << "SharedParameters " << i << ": " << estimation << estimationProperties;

    result.push_back(parameters);
  }
  return result;
}

utils::Shared<dai::EMAlg>::Ptr EMAlgFactory::create(
  const dai::FactorGraph& fg,
  const dai::PropertySet& config,
  const std::vector<utils::real>& pseudo_counts
)
{
  // collect the configuration
  std::string inferenceAlg("JTREE");
  dai::PropertySet inferenceProps;
  std::string estimationAlg("CondProbEstimation");
  dai::PropertySet estimationProps;
  size_t defaultPseudoCount(1);
  dai::PropertySet terminationProps;

  if(config.hasKey("inference")) inferenceAlg = config.getAs<std::string>("inference");
  if(config.hasKey("inference_properties")) inferenceProps = config.getAs<dai::PropertySet>("inference_properties");
  if(config.hasKey("estimation")) estimationAlg = config.getAs<std::string>("estimation");
  if(config.hasKey("estimation_properties")) estimationProps = config.getAs<dai::PropertySet>("estimation_properties");
  if(config.hasKey("default_pseudo_count")) defaultPseudoCount = config.getAs<size_t>("default_pseudo_count");
  if(config.hasKey("termination_properties")) terminationProps = config.getAs<dai::PropertySet>(
      "termination_properties");

  std::vector<real> pseudoCounts(pseudo_counts);
  pseudoCounts.reserve(fg.factors().size());
  while(pseudoCounts.size() < fg.factors().size()) {
    pseudoCounts.push_back(defaultPseudoCount);
  }

  dai::InfAlg* inference = dai::newInfAlg(inferenceAlg,fg,inferenceProps);
  inference->init();

  std::vector<dai::SharedParameters> sharedParameters = createSharedParameters(fg,
                                                                               estimationAlg,
                                                                               estimationProps,
                                                                               pseudoCounts
                                                        );
  std::vector<dai::MaximizationStep> maximizationSteps;
  maximizationSteps.push_back(dai::MaximizationStep(sharedParameters));

  auto result = Shared<dai::EMAlg>::Ptr(new dai::EMAlg(*inference,maximizationSteps,terminationProps));

  delete inference;
  return result;
}

dai::PropertySet EMAlgFactory::defaultConfig(){
  dai::PropertySet result,infProps,estProps,termProps;
  infProps.set("verbose",(size_t)0);
  infProps.set("updates",(std::string)"HUGIN");
  termProps.set("max_iters",(size_t)10);

  result.set("inference",(std::string)"JTREE");
  result.set("inference_properties", infProps);
  result.set("estimation",(std::string)"CondProbEstimation");
  result.set("estimation_properties", estProps);
  result.set("default_pseudo_count", (size_t)1);
  result.set("termination_properties", termProps);
  return result;
}
