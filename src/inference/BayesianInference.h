/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/inference/BayesianInference.h                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <dai/alldai.h>

#include <inference/BayesianFactorGraph.h>
#include <network/AspectStateEvidence.h>
#include <inference/InferenceResult.h>

namespace sitinf {
namespace inference {

  /**
   * This class uses different inference algorithms of libdai to calculate
   * the state of a BayesianFactorGraph under NetworkState.
   */
  class BayesianInference {
  public:

    /**
     * @brief The InferenceAlgorithm enum defines available inference algorithms.
     */
    enum InferenceAlgorithm{
      /**
        * Exact inference using a junction tree.
        */
      JunctionTree = 0,

      /**
        * Belief propagation using sum product.
        */
      BeliefPropagationSumprod = 1,
      /**
        * Belief propagation using max product.
        */
      BeliefPropagationMaxprod = 2,
      /**
        * Inference using decimation and max product.
        */
      Decimation = 3
    };

    /**
     * @brief Ptr typedef for shared pointer type
     */
    typedef boost::shared_ptr<BayesianInference> Ptr;

    /**
     * @brief BayesianInference constructor
     * @param algorithm the algorithm to be used for inference
     * @param maximum_states the maximum states a junction tree may create
     * @param maximum_iterations the maximum iteration to be calculated
     * @param tolerance the tolerance used in the algorithm
     * @param verbosity a verbosity > 0 makes the algorithm print information.
     */
    BayesianInference(InferenceAlgorithm algorithm=JunctionTree,
                      size_t maximum_states=100000,
                      size_t maximum_iterations=10000,
                      utils::real tolerance=1e-9,
                      size_t verbosity=0);

    /**
     * @brief A virtual destructor to be reimplemented in subclasses for nice cleanup.
     */
    virtual ~BayesianInference();

    /**
     * @brief infer uses the set inference algorithm to calculate the state of the BayesianFactorGraph
     * under evidence and returns the result.
     * @param bayesianFactorGraph the FactorGraph to be used
     * @param evidence the evidence for the factor graph
     * @return a result object for this calculation
     */
    InferenceResult::Ptr infer(BayesianFactorGraph* bayesianFactorGraph,
                               network::AspectStateEvidence* evidence);

  private:
    /**
     * @brief m_Algorithm the algorithm type.
     */
    InferenceAlgorithm m_Algorithm;
    /**
     * @brief m_MaxStates the maximum amount of states that can be used by JunctionTree.
     */
    size_t m_MaxStates;
    /**
     * @brief m_MaxIterations the maximum amount of iterations to be calculated.
     */
    size_t m_MaxIterations;
    /**
     * @brief m_Tolerance the value tolerance.
     */
    dai::Real m_Tolerance;
    /**
     * @brief m_Verbosity a verbosity > 0 will print information to console output.
     */
    size_t m_Verbosity;
  };

} // inference
} // namespace sitinf
