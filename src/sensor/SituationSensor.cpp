/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/SituationSensor.cpp                         **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/SituationSensor.h>
#include <utils/Exception.h>
#include <io/AnyTypeParticipantConfig.h>
#include <rsb/Factory.h>
#include <io/RsbFilters.h>
#include <utils/Macros.h>

#define _LOGGER "sensor.SituationSensor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::sensor;
using namespace sitinf::utils;


SituationSensor::SituationSensor(const std::string& name,
                                 const std::vector<std::string>& states,
                                 const std::string& aspect,
                                 const std::map<std::string,std::string> state_map,
                                 const std::string& default_state)
  : m_States(states), m_Name(name), m_CurrentState(default_state), m_Aspect(aspect), m_StateMap(state_map), m_DefaultState(default_state)
{}

SituationSensor::~SituationSensor(){}

const std::vector<std::string>& SituationSensor::states() const{
  return m_States;
}

std::string SituationSensor::name() const{
  return m_Name;
}

std::string SituationSensor::currentState() const{
  RecursiveLocker l(m_Lock);
  return m_CurrentState;
}

void SituationSensor::update(const Subject<boost::shared_ptr<rst::classification::ClassificationResultMap> >* subject,
                          boost::shared_ptr<rst::classification::ClassificationResultMap> data)
{
  SASSERT_RETURN(data.get());
  SLOG_TRACE << "Received Situation.";
  for (int i = 0; i < data->aspects_size(); ++i){
    if(data->aspects(i).name() == m_Aspect){
      std::string value = data->aspects(i).result().decided_class();
      if(m_StateMap.find(value) != m_StateMap.end()){
        updateCurrentState(m_StateMap[value]);
      } else {
        updateCurrentState(m_DefaultState);
      }
    }
  }

}

void SituationSensor::updateCurrentState(const std::string& value){
  if(value != m_CurrentState){
    RecursiveLocker l(m_Lock);
    if(value != m_CurrentState){
      SLOG_TRACE << "Update SituationSensor " << m_Name << " state to " << value;
      m_CurrentState = value;
      notify(m_CurrentState);
    }
  }
}
