/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/MutualGazeSensor.h                          **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Subject.h>
#include <sensor/Sensor.h>
#include <sensor/FacingSensor.h>

#include <rsb/Listener.h>
#include <rst/vision/FaceWithGazeCollection.pb.h>

namespace sitinf {
  namespace sensor {

    /**
     * @brief The MutualGazeSensor tells if a person is looking directly at an agent based on the
     * agents gaze recognition.
     */
    class MutualGazeSensor : public SimpleSensor {
    public:

      typedef utils::sint PersonIdType;
      typedef rst::vision::FaceWithGazeCollection PersonsData;
      typedef boost::shared_ptr<PersonsData> PersonsDataPtr;

      MutualGazeSensor(const std::string& name, const std::string& scope, GazesSelectionStrategy::Ptr person,
                       double min_horizontal_gaze, double max_horizontal_gaze, double min_vertical_gaze,
                       double max_vertical_gaze);

      virtual ~MutualGazeSensor();

      void handle(PersonsDataPtr data);

      static std::string yes(){ return "Yes"; }
      static std::string no(){ return "No"; }

    private:
      const GazesSelectionStrategy::Ptr m_Person;
      const double m_MinHorizontalGaze;
      const double m_MaxHorizontalGaze;
      const double m_MinVerticalGaze;
      const double m_MaxVerticalGaze;
      rsb::ListenerPtr m_Listener;
      rsb::HandlerPtr m_Handler;
    };

  } // namespace sensor
} // namespace sitinf
