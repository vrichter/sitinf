/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/Sensor.cpp                                  **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/Sensor.h>
#include <utils/Exception.h>
#include <sensor/SensorFactory.h>
#include <utils/YamlUtils.h>
#include <utils/Macros.h>

#define _LOGGER "sensor.Sensor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::sensor;

std::string Sensor::toString() const{
  std::stringstream ret;
  ret << "Sensor ( " << name() << " ";
  for (auto state : states()){
    ret << " | " << state;
  }
  ret << " )";
  return ret.str();
}


SimpleSensor::SimpleSensor(const std::string& name, const std::vector<std::string>& states,
                           const std::string& current_state)
  : m_Name(name), m_States(states)
{
  m_CurrentState.store(m_States.size());
  for (uint i = 0; i <  m_States.size(); ++i){
    if(m_States[i] == current_state){
      m_CurrentState.store(i);
    }
  }
  if(m_CurrentState.load() == m_States.size() && current_state != Sensor::unobserved()){
    throw utils::NotFoundException("The state " + current_state + " is unknown to sensor: " + m_Name);
  }
}

SimpleSensor::~SimpleSensor(){}

const std::vector<std::string>& SimpleSensor::states() const {
  return m_States;
}

std::string SimpleSensor::name() const{
  return m_Name;
}

std::string SimpleSensor::currentState() const{
  auto current = m_CurrentState.load();
  return (current < m_States.size()) ? m_States[current] : Sensor::unobserved();
}

/**
 * @brief changeCurrnetState changes the current state to state if the passed one is a valid state.
 * @param state the new state
 * @return false if the passed state is unknown and the sensor is not updated.
 */
bool SimpleSensor::changeCurrentState(const std::string& state){
  // edge case set unobserved
  if(state == Sensor::unobserved()){
    m_CurrentState.store(m_States.size());
    notify(Sensor::unobserved());
    return true;
  }
  // set to corresponding state
  for (uint i = 0; i <  m_States.size(); ++i){
    if(m_States[i] == state){
      m_CurrentState.store(i);
      notify(m_States[i]);
      return true;
    }
  }
  // state not found
  return false;
}

static std::vector<Sensor::Ptr> createFromConfig(const YAML::Node sensor, const SensorFactory& factory){
  YAML::Node config = sensor["config"];
//  name: SimpleSensor
//  states: [ one, two, three ]

  auto name = utils::yaml::read<std::string>(config,"name");
  auto states = utils::yaml::read<std::vector<std::string>>(config,"states");

  std::vector<Sensor::Ptr> result;
  result.push_back(Sensor::Ptr(new SimpleSensor(name,states,Sensor::unobserved())));
  return result;
}

// register at factory
static SensorFactory::Registrar registerDayTimeSensor("SimpleSensor", createFromConfig);


ChangeFilterSensor::ChangeFilterSensor(Sensor::Ptr other)
  : m_Sensor(other)
{
  other->addObserver(this);
  m_LastKnownState = other->currentState();
}

ChangeFilterSensor::~ChangeFilterSensor() {
  m_Sensor->removeObserver(this);
}

const std::vector<std::string>& ChangeFilterSensor::states() const {
  return m_Sensor->states();
}

std::string ChangeFilterSensor::name() const {
 return m_Sensor->name();
}

std::string ChangeFilterSensor::currentState() const {
  return m_Sensor->currentState();
}

void ChangeFilterSensor::update(const Sensor::SubjectType* subject, const std::string& data) {
  if(data != m_LastKnownState){
    m_LastKnownState = data;
    notify(m_LastKnownState);
  }
}
