/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/DelaySensor.h                               **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <sensor/Sensor.h>
#include <utils/Types.h>

namespace sitinf {
  namespace sensor {

    class DelaySensor : public Sensor, public utils::Observer<const std::string&> {

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<DelaySensor> Ptr;

      DelaySensor(Sensor::Ptr sensor, uint delay_millis, std::string delay_state);

      virtual ~DelaySensor();

      virtual const std::vector<std::string>& states() const;

      virtual std::string name() const;

      virtual std::string currentState() const;

      virtual void update(const Subject* subject, const std::string& data);

      static Sensor::Ptr delay(Sensor::Ptr sensor, uint delay_minis, const std::string& delay_state){
        return DelaySensor::Ptr(new DelaySensor(sensor,delay_minis,delay_state));
      }

      static Sensor::Ptr delay(Sensor::Ptr sensor, const std::vector<std::pair<uint,std::string>>& delay_state){
        Sensor::Ptr result = sensor;
        for (auto pair : delay_state){
          result = Sensor::Ptr(new DelaySensor(result,pair.first,pair.second));
        }
        return result;
      }

    private:
      mutable utils::RecursiveMutex m_Lock;
      Sensor::Ptr m_Sensor;
      std::string m_CurrentState;
      std::string m_RequestedState;
      utils::milliseconds m_Delay;
      std::string m_DelayState;
      utils::system_time m_LastUpdate;
      utils::Thread m_Thread;
      boost::condition_variable m_Condition;
      void waitForTimeout();

    };

  } // namespace sensor
} // namespace sitinf
