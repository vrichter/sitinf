/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/MouthMovementSensor.cpp                     **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/MouthMovementSensor.h>
#include <utils/Exception.h>
#include <rsb/Factory.h>
#include <io/RsbFilters.h>
#include <utils/Macros.h>
#include <sensor/SensorFactory.h>
#include <yaml-cpp/yaml.h>
#include <utils/YamlUtils.h>
#include <sensor/TimeoutSensor.h>
#include <io/RsbSource.h>
#include <sensor/DelaySensor.h>

#define _LOGGER "sensor.MouthMovementSensor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using sitinf::sensor::MouthMovementSensor;
using sitinf::sensor::SimpleSensor;
using sitinf::sensor::Sensor;
using sitinf::sensor::SensorFactory;

MouthMovementSensor::MouthMovementSensor(const std::string& name,
                                         const std::string& gazes_scope,
                                         const std::string& landmarks_scope,
                                         GazesSelectionStrategy::Ptr person,
                                         boost::posix_time::time_duration max_age,
                                         double speaking_threshold) :
  SimpleSensor(name, { moving(), stable() }, Sensor::unobserved()),
  m_Person(person),
  m_Distances(max_age),
  m_SpeakingThreshold(speaking_threshold),
  m_Listener({gazes_scope,landmarks_scope})
{
  m_Distances.addObserver(this);
  m_Listener.addObserver(this);
}

MouthMovementSensor::~MouthMovementSensor(){
  m_Distances.removeObserver(this);
  m_Listener.removeObserver(this);
}

namespace {

  double distance(rst::math::Vec2DInt a, rst::math::Vec2DInt b){
    return std::sqrt(std::pow(a.x() - b.x(), 2) + std::pow(a.y() - b.y(),2));
  }

  double normalized_inner_lip_distance(const rst::vision::FaceLandmarks* face){
    assert(face->left_eye_size() >= 0);
    assert(face->right_eye_size() >= 3);
    assert(face->inner_lips_size() >= 6);
    // use distance between eyes as face-size reference
    double mag = distance(face->left_eye(0),face->right_eye(3));
    // calculate centered inner mouth points distance
    double dist = distance(face->inner_lips(2), face->inner_lips(6));
    // scale distance with face size
    return dist / mag;
  }

} // anonymous namespace

void MouthMovementSensor::update(const Subject<const std::vector<double>&>* subject, const std::vector<double>& data) {
  assert(subject == &m_Distances);
  if(data.size() < 2) {
    changeCurrentState(Sensor::unobserved());
  } else {
    if(utils::variance(data) > m_SpeakingThreshold){
      changeCurrentState("Moving");
    } else {
      changeCurrentState("Stable");
    }
  }
}

namespace {
  template<typename T>
  boost::shared_ptr<T> check_and_get(rsb::EventPtr event){
    if(event->getType() != rsc::runtime::typeName<T>()){
      SLOG_WARNING << "Received unexpected data type: '" << event->getType()
                   << "'. Expected: '" << rsc::runtime::typeName<T>();
      return boost::shared_ptr<T>();
    } else {
      return boost::static_pointer_cast<T>(event->getData());
    }
  }
}

void MouthMovementSensor::update(
    const Subject<std::vector<rsb::EventPtr>>* subject,
    std::vector<rsb::EventPtr> data)
{
  boost::shared_ptr<rst::vision::FaceWithGazeCollection> faces = check_and_get<rst::vision::FaceWithGazeCollection>(data.at(0));
  boost::shared_ptr<rst::vision::FaceLandmarksCollection> landmarks = check_and_get<rst::vision::FaceLandmarksCollection>(data.at(1));
  assert(faces->element_size() == landmarks->element_size());
  auto person = m_Person->select(faces);
  const rst::vision::FaceLandmarks* selected = nullptr;
  for(int i = 0; i < faces->element_size(); ++i){
    if(person == &faces->element(i)){
      selected = &landmarks->element(i);
      break;
    }
  }
  if(!selected || selected->inner_lips_size() <= 2) return;
  m_Distances.insert(normalized_inner_lip_distance(selected));
}

static std::vector<Sensor::Ptr> createFromConfig(const YAML::Node sensor, const SensorFactory& factory){
  sitinf::io::register_rst<MouthMovementSensor::PersonsData>();
  YAML::Node config = sensor["config"];
  //  persons_prefix: Person
  //  scope_gazes: /home/wardrobe/gaze/faces
  //  scope_landmarks: /home/wardrobe/gaze/landmarks
  //  persons: 5
  //  speaking_max_age_ms: 200
  //  speaking_threshold: 200
  //  selection: original

  namespace uyaml =  sitinf::utils::yaml;
  auto persons_prefix = uyaml::read<std::string>(config,"persons_prefix");
  auto scope_gazes = uyaml::read<std::string>(config,"scope_gazes");
  auto scope_landmarks = uyaml::read<std::string>(config,"scope_landmarks");
  auto persons = uyaml::read<uint>(config,"persons");
  auto speaking_max_age_ms = uyaml::read<double>(config,"speaking_max_age_ms");
  auto speaking_threshold = uyaml::read<double>(config,"speaking_threshold");
  auto selection = uyaml::read<std::string>(config,"selection");

  std::vector<Sensor::Ptr> result;
  for(uint i = 0; i < persons; ++i) {
    auto strategy = sitinf::sensor::GazesSelectionStrategy::create(selection,i);
    // person is speaking
    result.push_back(Sensor::Ptr(new MouthMovementSensor(persons_prefix + std::to_string(i) + "_Mouth",
                                                         scope_gazes,
                                                         scope_landmarks,
                                                         strategy,
                                                         boost::posix_time::milliseconds(speaking_max_age_ms),
                                                         speaking_threshold)));
    result.back() = sitinf::sensor::DelaySensor::delay(result.back(),{
                                         {500,Sensor::unobserved()},
                                         {200,MouthMovementSensor::moving()},
                                         {800,MouthMovementSensor::stable()}
                                       });
  }
  return result;
}

// register at factory
static SensorFactory::Registrar registerDayTimeSensor("MouthMovementSensor", createFromConfig);

