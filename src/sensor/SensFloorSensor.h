/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/SensFloorSensor.h                           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <sensor/Sensor.h>
#include <rsb/Listener.h>

namespace sitinf {
  namespace sensor {
    /**
     * @brief The SensFloorSensor watches the states of a complete sens floor via rsb.
     *
     * It is activated if at least one element is activted and deactivated otherwise.
     *
     */
    class SensFloorSensor : public SimpleSensor {
    public:

      typedef std::map<int,std::map<int,bool>> ActivationMap;

      SensFloorSensor(const std::string& name, const std::string& scope);

      virtual ~SensFloorSensor();

      static const char* activeState(){ return "Active"; }
      static const char* inactiveState(){ return "Inactive"; }

    private:

      rsb::ListenerPtr m_Listener;
      rsb::HandlerPtr m_Handler;
      ActivationMap m_Activations;


      void handle(rsb::EventPtr event);
    };

  } // namespace sensor
} // namespace sitinf
