/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/DayTimeSensor.cpp                           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <algorithm>
#include <future>

#include <boost/date_time/posix_time/posix_time.hpp>

#include <sensor/SensorFactory.h>
#include <sensor/DayTimeSensor.h>
#include <utils/Exception.h>
#include <utils/Macros.h>

#define _LOGGER "sensor.DayTimeSensor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::sensor;
using namespace sitinf::utils;

namespace pt = ::boost::posix_time;

DayTimeSensor::DayTime::DayTime() : DayTime("unknown", boost::posix_time::time_duration(0,0,0)) {}

DayTimeSensor::DayTime::DayTime(std::string name, boost::posix_time::time_duration day_progress)
  : m_Name(name), m_End(day_progress)
{
  SASSERT_THROW(m_Name != "", IllegalArgumentException, "DayTime name must be a valid, nonempty string.");
  SASSERT_THROW(m_End  < pt::time_duration(pt::hours(24)),
                IllegalArgumentException, "DayTime progress must be less than a day. Got '" << m_Name << "' = " << m_End);
}


DayTimeSensor::DayTimeSensor(const std::string& name, const std::vector<DayTime>& times)
 : m_Name(name), m_DayTimes(times), m_Exit(false)
{
  SASSERT_THROW(name != "", IllegalArgumentException, "Sensor name must be a valid, nonempty string.");
  SASSERT_THROW(!times.empty(), IllegalArgumentException, "DayTimeSensors times cannot be empty:.");
  // sort elements in ascending order
  std::sort(m_DayTimes.begin(), m_DayTimes.end());

  for(uint i = 1; i < m_DayTimes.size(); ++i){
    SASSERT_THROW(m_DayTimes[i-1].end() != m_DayTimes[i].end(),IllegalArgumentException,
        "Two DayTimes in a DayTimeSensor cannot have equal end time. Please check '"
        << m_DayTimes[i-1].name() << "' and '" << m_DayTimes[i].name());
  }

  // use a set to filter double DayTimes for state list
  std::set<std::string> single;
  for(auto dt : m_DayTimes){
    single.insert(dt.name());
  }
  m_States.reserve(single.size());
  m_States.insert(m_States.end(),single.begin(), single.end());

  // set current time state == smallest DayTime which is bigger than current.
  pt::time_duration currentTime = currentDayTime();
  /*
   * The current daytime.
   * Edge case: When the current daytime is bigger than all passed the right daytime is the first one.
   */
  m_CurrentState = m_DayTimes.front().name();
  uint current = 0;
  for (uint i = 0; i < m_DayTimes.size(); ++i){
    if(currentTime < m_DayTimes[i].end()){
      m_CurrentState = m_DayTimes[i].name();
      current = i;
      break;
    }
  }
  // spawn a new thread that waits for timeout.
  m_Thread = Thread(&DayTimeSensor::updateState,this,current);
}

DayTimeSensor::~DayTimeSensor(){
  m_ConditionMutex.lock();
  m_Exit = true;
  m_ConditionMutex.unlock();
  m_Condition.notify_all();
  m_Thread.join();
  RecursiveLocker l(m_Lock);
}

const std::vector<std::string>& DayTimeSensor::states() const{
  return m_States;
}

std::string DayTimeSensor::name() const {
  return m_Name;
}

std::string DayTimeSensor::currentState() const{
  RecursiveLocker l(m_Lock);
  return m_CurrentState;
}

void DayTimeSensor::updateState(uint current){
  // the wait-mutex
  Mutex lock;
  while(true){
    // setup the until end time of the current daytiime
    pt::ptime::date_type date = boost::gregorian::day_clock::local_day();
    if(currentDayTime() > m_DayTimes[current].end()){ // tomorrow
      date += boost::gregorian::date_duration(1);
    }
    pt::ptime local(date,m_DayTimes[current].end());
    boost::system_time until = local - (pt::microsec_clock::local_time() - boost::get_system_time());
    // lock the mutex
    Locker locker(lock);
    if(m_Exit) return; // destructor is called exit.
    // calling wait releases the lock and blocks.
    bool notified = m_Condition.timed_wait(locker,until);
    if(!notified){
      if(currentDayTime() > m_DayTimes[current].end()){
        // the time is realy after timeout. update current and state
        std::string oldState = m_CurrentState;
        current = (current+1 == m_DayTimes.size()) ? 0 : current+1;
        m_Lock.lock();
        m_CurrentState = m_DayTimes[current].name();
        m_Lock.unlock();
        if(oldState != m_CurrentState){
          notify(m_CurrentState);
        }
      }
    }
  }
  // this point is not reachable. The thread will by stopped by interrupt.
}

pt::time_duration DayTimeSensor::currentDayTime(){
  return pt::microsec_clock::local_time().time_of_day();
}

namespace YAML {
  template<>
  struct convert<DayTimeSensor::DayTime> {
    static Node encode(const DayTimeSensor::DayTime& rhs) {
      Node node;
      node["name"] = rhs.name();
      std::stringstream endTime;
      endTime << rhs.end().hours() << ":"
              << rhs.end().minutes() << ":"
              << rhs.end().seconds() << "."
              << rhs.end().fractional_seconds();
      node["end"] = endTime.str();
      return node;
    }

    static bool decode(const Node& node, DayTimeSensor::DayTime& rhs) {
      SASSERT_THROW(node["name"],ParsingException,"DayTimeSensor::DayTime needs a name element.");
      SASSERT_THROW(node["end"],ParsingException,"DayTimeSensor::DayTime needs an end element.");
      std::string name = node["name"].as<std::string>();
      std::string time = node["end"].as<std::string>();
      boost::posix_time::time_duration end = boost::posix_time::duration_from_string(time);
      rhs = DayTimeSensor::DayTime(name,end);
      return true;
    }

  };
}

static std::vector<Sensor::Ptr> createFromConfig(const YAML::Node sensor, const SensorFactory& factory){
  YAML::Node config = sensor["config"];
//  name: Time
//  times:
//    - name: Night
//      end: 06:00:00
//    - name: Morning
//      end: 12:00:00
//    - name: Afternoon
//      end: 18:00:00
//    - name: Evening
//      end: 00:00:00
  std::vector<DayTimeSensor::DayTime> dayTimes = config["times"].as<std::vector<DayTimeSensor::DayTime>>();

  std::vector<std::string> names;
  if (sensor["manifestations"]) {
    sensor["manifestations"].as<std::vector<std::string>>();
  }
  if(config["name"]){
    names.push_back(config["name"].as<std::string>());
  }
  SASSERT_THROW(names.size(),ParsingException,"No name specified.");

  std::vector<Sensor::Ptr> result;
  for(std::string name : names) {
    result.push_back(Sensor::Ptr(new DayTimeSensor(name,dayTimes)));
    SLOG_INFO << "Created DayTimeSensor '" << result.back()->name() << "'.";
  }
  return result;
}

// register at factory
static SensorFactory::Registrar registerDayTimeSensor("DayTimeSensor", createFromConfig);
