/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/TypeSensor.cpp                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/TypeSensor.h>
#include <utils/Exception.h>
#include <io/AnyTypeParticipantConfig.h>
#include <rsb/Factory.h>
#include <io/RsbFilters.h>
#include <utils/Macros.h>
#include <sensor/SensorFactory.h>
#include <yaml-cpp/yaml.h>
#include <boost/regex.hpp>
#include <boost/tokenizer.hpp>

#define _LOGGER "sensor.TypeSensor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

static const bool USE_SUBSCOPES = true;

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::sensor;
using namespace sitinf::utils;

TypeSensor::TypeSensor(const std::vector<std::string>& states, const std::string& name,
                       const std::string& scope, const google::protobuf::Descriptor* type,
                       const std::vector<QueryState>& queries, bool use_subscopes)
  : m_States(states), m_Name(name), m_CurrentState(Sensor::unobserved()), m_Desciptor(type)
{
  // create queries
  for(const QueryState& i : queries){
    try{
      pquery::Query::Ptr query = m_QueryContext.parse(i.query);
      m_Queries.push_back(std::pair<pquery::Query::Ptr,std::string>(query,i.state));
    } catch (const pquery::PQException& e){
      STHROW(IOException,"Could not parse PQuery from string: '" << i.query << "'.\nGot: " << e.what());
    }
  }
  // create rsb listener, set filters, register as handler.
  m_Listener = rsb::getFactory().createListener(scope,AnyTypeParticipantConfig());
  m_Listener->addFilter(rsb::filter::FilterPtr(new ScopeFilter(scope,use_subscopes)));
  m_Listener->addFilter(rsb::filter::FilterPtr(new SchemaAndByteArrayTypeFilter(type->full_name())));
  m_Listener->addHandler(rsb::HandlerPtr(new rsb::EventFunctionHandler(
                                           boost::bind(&TypeSensor::handle,this,_1)
                                           )));
}

TypeSensor::~TypeSensor(){
  Locker l(m_Lock);
  m_Listener.reset();
}

const std::vector<std::string>& TypeSensor::states() const{
  return m_States;
}

std::string TypeSensor::name() const {
  return m_Name;
}

std::string TypeSensor::currentState() const{
  Locker l(m_Lock);
  return m_CurrentState;
}

void TypeSensor::handle(rsb::EventPtr event){
  // get event data. cast to AnnotatedData::Ptr
  boost::shared_ptr<rsb::AnnotatedData> data = boost::static_pointer_cast<rsb::AnnotatedData>(event->getData());
  // get string message from AnnotatedData
  boost::shared_ptr<std::string> message = boost::static_pointer_cast<std::string>(data->second);
  // create query message
  pquery::Item::Ptr qmessage = m_QueryContext.getMessage(m_Desciptor, message.get());
  // query the message with every query available.
  for(auto query : m_Queries){
    pquery::Result result = query.first->execute(qmessage);
    if(result.next().get()){
      // result is not empty
      m_Lock.lock();
      m_CurrentState = query.second;
      m_Lock.unlock();
      notify(m_CurrentState);
      break;
    }
  }
}

#define YAML_PARSE(var,name,node,type)\
{\
  try{\
    var = node[name].as<type>();\
  } catch (const std::exception& e){\
    STHROW(ParsingException, "Malformed element '" << name << "': " << YAML::Dump(node[name]) << ". Expected: " << #type);\
  }\
}

#define YAML_PARSE_IF(var,name,node,type)\
{\
  try{\
    if(node[name]) var = node[name].as<type>();\
  } catch (const std::exception& e){\
    STHROW(ParsingException, "Malformed element '" << name << "': " << YAML::Dump(node[name]) << ". Expected: " << #type);\
  }\
}

// Sensor creation from yaml config --------------------------------------------------------
  class Manifestation {
  public:

    enum Subscopes {
      yes,
      no,
      unknown
    };

    std::string name = "";
    std::string scope = "";
    Subscopes subscopes = unknown;
  };

  namespace YAML{
  template<>
  struct convert<Manifestation> {
    static bool decode(const Node& node, Manifestation& rhs) {
      SASSERT_THROW(node["scope"],ParsingException,"TypeSensor.manifestations need scope elements.");
      YAML_PARSE_IF(rhs.name,"name",node,std::string);
      YAML_PARSE(rhs.scope,"scope",node,std::string);
      if(node["subscopes"]){
        bool val;
        YAML_PARSE(val,"subscopes",node,bool);
        rhs.subscopes = (val) ? Manifestation::yes : Manifestation::no;
      }
      return true;
    }
  };
  }

  namespace YAML{
  template<>
  struct convert<TypeSensor::QueryState> {
    static bool decode(const Node& node, TypeSensor::QueryState& rhs) {
      SASSERT_THROW(node["xpath"],ParsingException,"TypeSensor.config.results need xpath elements.");
      SASSERT_THROW(node["result"],ParsingException,"TypeSensor.config.results need result elements.");
      YAML_PARSE(rhs.query,"xpath",node,std::string);
      YAML_PARSE(rhs.state,"result",node,std::string);
      return true;
    }
  };
  }

std::string capitalize(const std::string& string){
  if(string.empty()) return "";
  std::stringstream capitalized;
  capitalized << (char) std::toupper(string[0]);
  if(string.size() > 1){
    capitalized << string.substr(1);
  }
 return capitalized.str();
}

std::string createSensorName(const std::string& prefix, const std::string& filter, const std::string& scope){
  std::string filteredScope = boost::regex_replace(scope, boost::regex(filter),"");
  boost::char_separator<char> separator("/");
  boost::tokenizer<boost::char_separator<char>> tokens(filteredScope, separator);
  std::stringstream name;
  name << capitalize(prefix);
  for(auto token : tokens){
    if(!token.empty()){
      name << "_";
      name << capitalize(token);
    }
  }
  std::string namestring = name.str();
  if(namestring[0] == '_') namestring = namestring.substr(1);
  return namestring;
}

static std::vector<Sensor::Ptr> createFromConfig(const YAML::Node sensor, const SensorFactory& factory){
// Example configuration with name:
//  config:
//    type: rst.vision.Faces
//    results:
//      - xpath: /message[count(faces)>0]
//        result: Some
//  manifestations:
//    - name: Attention_Flobi_Wardrobe
//      scope: /home/wardrobe/faces
//    - name: Attention_Flobi_Kitchen
//      scope: /home/kitchen/faces
//
// Example configuration with name generation from scope.
//  sensor_type: TypeSensor
//  config:
//    type: rst.domotic.unit.MotionSensor
//    name_from_scope:
//      prefix: Movement
//      filter: "(home|motionsensor|status)"
//    results:
//      - xpath: /message/motion_state[state='MOVEMENT']
//        result: Some
//      - xpath: /message/motion_state[state='NO_MOVEMENT']
//        result: None
//      - xpath: /message/motion_state[state='UNKNOWN']
//        result: __unobserved__
//    manifestations:
//      - scope: /home/wardrobe/motionsensor/entrance/status

// read data --------------------------------------------------------
  YAML::Node config = sensor["config"];
  std::string protoType = config["type"].as<std::string>();
  auto protoDescriptor = factory.protoFilesImporter().findDescriptor(protoType);
  SASSERT_THROW(protoDescriptor,NotFoundException,
                "Cannot find descriptor for requested Prototype '" << protoType << "'");
  std::vector<TypeSensor::QueryState> queryStates = config["results"].as<std::vector<TypeSensor::QueryState>>();
  std::vector<std::string> states = (config["states"]) ?
        config["states"].as<std::vector<std::string>>() : std::vector<std::string>();
  bool useSubscopes = (config["subscopes"]) ? config["subscopes"].as<bool>() : USE_SUBSCOPES;
  if(states.empty()){ // when states not defined fill them from results
    states.reserve(queryStates.size());
    for (auto q : queryStates){
      if(q.state != Sensor::unobserved()){
        states.push_back(q.state);
      }
    }
    states.shrink_to_fit();
  }

// simple configuration without manifestations -----------------------
  if(!sensor["manifestations"]){
    STRY_RETHROW_MESSAGE(
    std::string name = config["name"].as<std::string>();
    std::string scope = config["scope"].as<std::string>();
    std::vector<Sensor::Ptr> result(1);
    result.push_back(Sensor::Ptr(new TypeSensor(states,name,scope,protoDescriptor,queryStates,useSubscopes)));
    SLOG_INFO << "Created TypeSensor " << name << " on  scope " << scope;
    return result;,
    "TypeSensor without manifestations needs name and scope.");
  }

// manifestations defined. create multiple sensors -------------------
  std::vector<Manifestation> parsed_manifestations = sensor["manifestations"].as<std::vector<Manifestation>>();
  std::string name_prefix = "";
  std::string name_filter = "";
  if(config["name_from_scope"]){
    name_prefix = (config["name_from_scope"]["prefix"]) ? config["name_from_scope"]["prefix"].as<std::string>() : "";
    name_filter = (config["name_from_scope"]["filter"]) ? config["name_from_scope"]["filter"].as<std::string>() : "";
  }
  std::vector<Sensor::Ptr> result;
  result.reserve(parsed_manifestations.size());
  for(Manifestation m : parsed_manifestations){
    bool subscopes = (m.subscopes == Manifestation::unknown) ? useSubscopes : (m.subscopes == Manifestation::yes);
    if(m.name.empty()) m.name = createSensorName(name_prefix,name_filter,m.scope);
    result.push_back(Sensor::Ptr(new TypeSensor(states,
                                                m.name,
                                                m.scope,protoDescriptor,queryStates,subscopes)));
    SLOG_INFO << "Created TypeSensor " << m.name << " on scope " << m.scope;
  }
  return result;
}

// register at factory
static SensorFactory::Registrar registerDayTimeSensor("TypeSensor", createFromConfig);
