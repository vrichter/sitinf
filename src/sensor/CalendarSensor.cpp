/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/CalendarSensor.cpp                          **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <algorithm>

#include <io/RsbFilters.h>
#include <sensor/CalendarSensor.h>
#include <sensor/SensorFactory.h>
#include <utils/Exception.h>

#include <rsb/Factory.h>
#include <rsb/converter/ProtocolBufferConverter.h>
#include <rsb/converter/Repository.h>

#include <rst/calendar/CalendarUpdate.pb.h>
#include <rst/calendar/CalendarQuery.pb.h>
#include <rst/calendar/CalendarQueryResult.pb.h>
#include <rst/calendar/CalendarComponents.pb.h>

#include <boost/regex.hpp>
#include <boost/tokenizer.hpp>
#include <boost/date_time/local_time/local_time.hpp>

#include <yaml-cpp/yaml.h>

#include <utils/Macros.h>

#define _LOGGER "sensor.CalendarSensor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

static const char* CALENDAR_EVENT_METHOD_NAME = "calendarEvents";
static const char* EVENT_NOT_FOUND_STATE = "None";
static const uint CALENDAR_EVENT_MAX_WAIT_TIME = 25; // seconds
static const char* CALENDAR_EVENT_REQUEST_TYPE = "rst::calendar::CalendarQuery";
static const char* CALENDAR_EVENT_RESULT_TYPE = "rst::calendar::CalendarQueryResult";
static const char* CALENDAR_SENSOR_STATE_NOW = "Now";

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::sensor;
using namespace sitinf::utils;

template<typename Type>
void registerProtocolBufferConverters(){
  // try to register converter if not already done
  try {
    boost::shared_ptr<rsb::converter::ProtocolBufferConverter<Type> > converter(
      new rsb::converter::ProtocolBufferConverter<Type>());
    rsb::converter::converterRepository<std::string>()->registerConverter(converter);
  } catch (const std::exception& e) {
    // already available do nothing
  }
}

template<typename First, typename Second, typename... Rest>
void registerProtocolBufferConverters(){
  // register list recursive
  registerProtocolBufferConverters<First>();
  registerProtocolBufferConverters<Second, Rest...>();
}

CalendarSensor::CalendarSensor(const std::string& name, const std::string& match_events_regex,
                               const std::string& event_scope, const std::vector<std::string>& update_scope,
                               std::vector<TimePeriod> time_periods)
  : m_Name(name), m_Regex(match_events_regex), m_Thread([this] {this->resetState();}),
    m_CurrentState(Sensor::unobserved()), m_TimePeriods(time_periods)
{
  // time periods must be sorted ascending by time
  std::sort(m_TimePeriods.begin(),m_TimePeriods.end(),
            [](const TimePeriod& i, const TimePeriod& j) -> bool { return i.time_millis < j.time_millis; });
  std::cout << std::endl;

  m_States.reserve(m_TimePeriods.size()+1);
  m_States.push_back(CALENDAR_SENSOR_STATE_NOW);
  m_States.push_back(EVENT_NOT_FOUND_STATE);
  for(auto period : m_TimePeriods){
    m_States.push_back(period.name);
  }

  // set up communication and register handlers
  registerProtocolBufferConverters<
      rst::calendar::CalendarComponents,
      rst::calendar::CalendarUpdate,
      rst::calendar::CalendarQuery,
      rst::calendar::CalendarQueryResult>();
  m_UpdateListener.reserve(update_scope.size());
  for(auto reset : update_scope){
    m_UpdateListener.push_back(rsb::getFactory().createListener(reset));
    m_UpdateListener.back() -> addHandler(rsb::HandlerPtr(new rsb::EventFunctionHandler(
                                                       boost::bind(&CalendarSensor::resetState,this)
                                                       )));
  }
  m_CalendarServer = rsb::getFactory().createRemoteServer(event_scope);
  m_Thread.call_in(0); // asynchronous update.
}

CalendarSensor::~CalendarSensor(){

}

const std::vector<std::string>& CalendarSensor::states() const{
  return m_States;
}

std::string CalendarSensor::name() const{
  return m_Name;
}

std::string CalendarSensor::currentState() const{
  return m_CurrentState;
}

void CalendarSensor::setState(const std::string& state){
  SLOG_TRACE << m_Name << " -> " << state;
  Locker l(m_Lock);
  m_CurrentState = state;
  notify(m_CurrentState);
}

void CalendarSensor::handle(rsb::EventPtr event){
  resetState();
}

// do not lead data.
rst::calendar::DateTime* createNewDateTime(uint64 millis_after_now=0){
  auto time = rst::calendar::DateTime::default_instance().New();
  time->set_date_time_type(rst::calendar::DateTime::UTC);
  time->set_timezone_id("UTC");
  time->set_milliseconds_since_epoch(utils::currentTimeMillis() + millis_after_now);
  return time;
}

uint64 getUtcMillis(const rst::calendar::DateTime& time){
  return time.milliseconds_since_epoch(); // always is utc
}

static rsb::EventPtr createQuery(uint64 time){
  typedef rst::calendar::CalendarQuery Query;
  typedef boost::shared_ptr<Query> QueryPtr;

  QueryPtr query = QueryPtr(Query::default_instance().New());
  query->set_allocated_start_time(createNewDateTime(0));
  query->set_allocated_end_time(createNewDateTime(time));
  query->set_search_todos(false);
  query->set_search_events(true);
  query->set_calculate_time_periods(true);

  rsb::EventPtr event = rsb::EventPtr(new rsb::Event());
  event->setData(query);
  event->setType(CALENDAR_EVENT_REQUEST_TYPE);
  return event;
}

static bool matchingEvent(const rst::calendar::Event& event, const std::string& regex){
    boost::smatch match;
    return boost::regex_match(event.entry().summary(),match,boost::regex(regex));
}

static void collectMatchingEvents(rsb::EventPtr data,
                                  std::vector<uint64>& active_ends,
                                  std::vector<uint64>& future_starts,
                                  const std::string& regex)
{
  if(!data->getData()){
    SLOG_WARNING << "No data received from CalendarRequest.";
    return;
  }
  if(data->getType() != CALENDAR_EVENT_RESULT_TYPE){
    SLOG_WARNING << "Wrong data type received from CalendarRequest. "
                 << "Expected: " << CALENDAR_EVENT_RESULT_TYPE << " - "
                 << "Received: " << data->getType();
    return;
  }
  auto result = boost::static_pointer_cast<rst::calendar::CalendarQueryResult>(data->getData());
  uint64 now = currentTimeMillis();
  for(auto eventWithTimes : result->event()){
    if(matchingEvent(eventWithTimes.event(),regex)){
      for(auto period : eventWithTimes.time()){
        uint64 start = getUtcMillis(period.start());
        if(start > now){
          future_starts.push_back(start);
        } else {
          active_ends.push_back(getUtcMillis(period.end()));
        }
      }
    }
  }
  std::sort(active_ends.begin(),active_ends.end());
  std::sort(future_starts.begin(),future_starts.end());
}

void CalendarSensor::resetState(){
  try {
    for(uint i = 0; i <  m_TimePeriods.size(); ++i){
      const TimePeriod& time = m_TimePeriods[i];
      auto request = createQuery(time.time_millis);
      request->setMethod(CALENDAR_EVENT_METHOD_NAME);
      rsb::EventPtr result = m_CalendarServer->call(CALENDAR_EVENT_METHOD_NAME,request,CALENDAR_EVENT_MAX_WAIT_TIME);
      std::vector<uint64> active,future; collectMatchingEvents(result,active,future,m_Regex);
      if(!active.empty()){ // a matching event is currently active set state to now and wait for end.
        SLOG_TRACE << m_Name << " Currently active event. Sleep until " << active.front();
        setState(CALENDAR_SENSOR_STATE_NOW);
        m_Thread.call_at(active.front());
        return;
      } else if (!future.empty()){
        setState(time.name);
        uint64 nextBitggest = ( i > 0 ) ? m_TimePeriods.at(i-1).time_millis : 0; // so the next state can be set
        SLOG_TRACE << m_Name << " Future event available. Sleep until " << future.front() - nextBitggest;
        m_Thread.call_at(future.front() - nextBitggest);
        return;
      }
    }
    // what is the correct time to wait when no next event is found? Waiting the longest time is wrong because
    // an event could occure right after that and whoud - in this case - be ignored.
    // Heuristic: Check if an event occurs in 2*the longest time. If not wait the longest else wait until
    // the event minus the longest.
    rsb::EventPtr result = m_CalendarServer->call(CALENDAR_EVENT_METHOD_NAME,
                                                  createQuery(m_TimePeriods.back().time_millis*2),
                                                  CALENDAR_EVENT_MAX_WAIT_TIME);
    std::vector<uint64> active,future; collectMatchingEvents(result,active,future,m_Regex);
    if(future.empty()){
      m_Thread.call_in(m_TimePeriods.back().time_millis);
    } else {
      m_Thread.call_at(future.front() - m_TimePeriods.back().time_millis);
    }
    setState(EVENT_NOT_FOUND_STATE);
  } catch (const std::exception& e){
     SLOG_WARNING << "Exception on calendar events request. "
                  << "scope = " << m_CalendarServer->getScope()->toString()
                  << "function = " << CALENDAR_EVENT_METHOD_NAME << "."
                  << "Error: " << e.what();
     setState(Sensor::unobserved());
  }
}

// Sensor creation from yaml config --------------------------------------------------------

#define PARSE_REQUIRED(node,field,variable,type)\
{\
  if(!node[field]){\
    STHROW(ParsingException,"Required field '" << field << "' does not exist.");\
  }\
  try{\
    variable = node[field].as<type>();\
  } catch (const std::exception& e){\
    STHROW(ParsingException,"Could not parse required field '" << field\
           << "' as " << #type << ". Error: " << e.what());\
  }\
}

namespace YAML{
template<>
struct convert<CalendarSensor::TimePeriod> {
  static bool decode(const Node& node, CalendarSensor::TimePeriod& rhs) {
    PARSE_REQUIRED(node,"name",rhs.name,std::string);
    PARSE_REQUIRED(node,"time_frame_millis",rhs.time_millis,utils::uint64);
    return true;
  }
};
}

struct CalendarSensorConfig {
  std::string name = "";
  std::string regex = "";
};

namespace YAML{
template<>
struct convert<CalendarSensorConfig> {
  static bool decode(const Node& node, CalendarSensorConfig& rhs) {
    PARSE_REQUIRED(node,"name",rhs.name,std::string);
    PARSE_REQUIRED(node,"regex",rhs.regex,std::string);
    return true;
  }
};
}

static std::vector<Sensor::Ptr> createFromConfig(const YAML::Node sensor, const SensorFactory& factory){
//  - sensor_type: CalendarSensor
//  # The CalendarSensor is somewhat different. it has a fixed number of states but a rather high number of
//  # sensors by creating a sensor for every event_sensor * calendar_names element
//    config:
//      prefix: Calendar_Event_ # event_sensor.name and calendar_name will be attached
//      scope_prefix: /apartment/calendar/ # the root scope of all caledar information
//      scope_postfix_events: /calendarEvents # where to get calendar events
//      scope_postfix_updates: [ /update, /content ] # where updates are published
//      states: # Now is always used for currently running states.
//        - name: Shortly
//          time_frame_millis: 3600000 # one hour
//        - name: Today
//          time_frame_millis: 86400000 # 24 hours
//        - name: This_Week
//          time_frame_millis: 604800000 # 7 days
//        - name: This_Month
//          time_frame_millis: 2592000000 # 30 days
//    manifestations:
//    # for every calendar name all event sensors are created.
//      calendar_names:
//        - Personal_Calendar
//        - CITEC-1_112
//        - LSP-CSRA_Raumblockung
//        - LSP-CSRA_Termine
//      event_sensors:
//      # event sensors match a calendar event description with a regex.
//        - name: Demo
//          regex: ".*demo.*"
//        - name: Meeting
//          regex: ".*meeting.*"
//        - name: Work
//          regex: ".*work.*"
//        - name: Party
//          regex: ".*party.*"
//        - name: Movie
//          regex: ".*movie.*"
//        - name: Study
//          regex: ".*study.*"
//        - name: Integration_Group
//          regex: ".*integration.group.*"

// read data --------------------------------------------------------
  std::string sensor_name_prefix, scope_prefix, scope_events;
  std::vector<std::string> scope_updates;
  std::vector<CalendarSensor::TimePeriod> periods;
  std::vector<std::string> calendars;
  std::vector<CalendarSensorConfig> configs;

  YAML::Node config = sensor["config"];
  PARSE_REQUIRED(config,"prefix",sensor_name_prefix,std::string);
  PARSE_REQUIRED(config,"scope_prefix",scope_prefix,std::string);
  PARSE_REQUIRED(config,"scope_postfix_events",scope_events,std::string);
  PARSE_REQUIRED(config,"scope_postfix_updates",scope_updates,std::vector<std::string>);
  PARSE_REQUIRED(config,"states",periods,std::vector<CalendarSensor::TimePeriod>);
  YAML::Node manifestations = sensor["manifestations"];
  PARSE_REQUIRED(manifestations,"calendar_names",calendars,std::vector<std::string>);
  PARSE_REQUIRED(manifestations,"event_sensors",configs,std::vector<CalendarSensorConfig>);

// create sensors
  std::vector<Sensor::Ptr> result;
  for(auto calendar : calendars){
    std::string eventScope = scope_prefix + calendar + scope_events;
    std::vector<std::string> updateScopes;
    std::stringstream scopes;
    scopes << scope_prefix << calendar << "[ " << scope_events;
    for(auto update : scope_updates){
      scopes << ", " << update;
      updateScopes.push_back(scope_prefix + calendar + update);
    }
    scopes << " ]";
    for(CalendarSensorConfig c : configs){
      std::string name = sensor_name_prefix + c.name + std::string("_") + calendar;
      result.push_back(Sensor::Ptr(new CalendarSensor(name, c.regex, eventScope, updateScopes ,periods)));
      SLOG_INFO << "Created CalendarSensor '" << name << "' on scope '" << scopes.str() << "'";
    }
  }
  return result;
}

// register at factory
static SensorFactory::Registrar registerDayTimeSensor("CalendarSensor", createFromConfig);
