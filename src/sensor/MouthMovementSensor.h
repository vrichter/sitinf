/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/MouthMovementSensor.h                       **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <sensor/Sensor.h>
#include <utils/Subject.h>
#include <utils/TimedVector.h>
#include <sensor/FacingSensor.h>

#include <io/RsbMultiEventListener.h>
#include <rst/vision/FaceLandmarksCollection.pb.h>

namespace sitinf {
  namespace sensor {


    /**
     * @brief The PersonSensor is a Sensor that represents data of a single person.
     */
    class MouthMovementSensor :
        public SimpleSensor,
        private utils::TimedVector<double>::ObserverType,
        private io::RsbMultiEventListener<>::ObserverType
    {
    public:

      typedef rst::vision::FaceLandmarks PersonData;
      typedef rst::vision::FaceLandmarksCollection PersonsData;
      typedef boost::shared_ptr<PersonsData> PersonsDataPtr;

      MouthMovementSensor(const std::string& name, const std::string& gaze_scope, const std::string& landmark_scope,
                          GazesSelectionStrategy::Ptr person, boost::posix_time::time_duration max_age,
                          double speaking_threshold);

      virtual ~MouthMovementSensor();

      static std::string moving(){ return "Moving"; }
      static std::string stable(){ return "Stable"; }

    private:
      const GazesSelectionStrategy::Ptr m_Person;
      utils::TimedVector<double> m_Distances;
      const double m_SpeakingThreshold;
      io::RsbMultiEventListener<> m_Listener;

      virtual void update(const Subject<const std::vector<double>&>* subject, const std::vector<double>& data) override;
      virtual void update(const Subject<std::vector<rsb::EventPtr>>* subject, std::vector<rsb::EventPtr> data) override;

    };

  } // namespace sensor
} // namespace sitinf
