/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/SensorFactory.cpp                           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/SensorFactory.h>
#include <utils/Exception.h>

#include <io/File.h>

#include <sensor/TypeSensor.h>
#include <sensor/TimeoutSensor.h>
#include <sensor/DayTimeSensor.h>
#include <utils/Macros.h>

#define _LOGGER "sensor.SensorFactory"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::sensor;
using namespace sitinf::utils;

bool USE_SUBSCOPES = true;

namespace YAML {
  template<>
  struct convert<std::vector<YAML::Node>> {
    static Node encode(const std::vector<YAML::Node>& rhs) {
      Node node;
      for(auto n : rhs){
        node.push_back(n);
      }
      return node;
    }

    static bool decode(const Node& node, std::vector<YAML::Node>& rhs) {
      if(node.IsScalar()){
          rhs.push_back(node);
      } else if(node.IsSequence()){
        for (auto it : node){
          rhs.push_back(it);
        }
      } else {
        STHROW(ParsingException,"Cannot parse node into vector of nodes because it is neither scalar nor list.");
      }
      return true;
    }

  };
}

std::vector<Sensor::Ptr> SensorFactory::createSensors(const std::string& filename){
  io::ReadFile test(filename);
  return createSensors(YAML::LoadFile(filename));
}

std::vector<Sensor::Ptr> SensorFactory::createSensors(const YAML::Node sensor_config){
  SASSERT_THROW(sensor_config["sensors"],ParsingException,"A sensor configuration needs a 'sensors' field.");
  std::vector<Sensor::Ptr> result;
  int configCounter = 0;
  std::vector<YAML::Node> sensors = sensor_config["sensors"].as<std::vector<YAML::Node>>();
  for(YAML::Node sensor : sensors) {
    SASSERT_THROW(sensor["sensor_type"],
        ParsingException,"Sensor configuration #" << configCounter << " has no 'sensor_type' element.");
    std::string type = sensor["sensor_type"].as<std::string>();
    SASSERT_THROW(registry().hasFunction(type),
        ParsingException,"Sensor configuration #" << configCounter << " sensor_type := '" << type << "' unknown.");
    Function func = registry().getFunction(type);
    STRY_RETHROW_MESSAGE(
      std::vector<Sensor::Ptr> created = func(sensor,*this);
      result.reserve(result.size() + created.size());
      result.insert(result.end(),created.begin(),created.end());
      ,"Could not parse sensor #" << configCounter << " into '" << type << "'. Data:\n" << YAML::Dump(sensor)
      )
    ++configCounter;
  }
  return result;
}

ProtoFilesImporter& SensorFactory::protoFilesImporter() const{
  return *m_Proto;
}
