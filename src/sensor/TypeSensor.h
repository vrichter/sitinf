/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/TypeSensor.h                                **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <sensor/Sensor.h>
#include <rsb/Listener.h>
#include <pquery/pquery.hpp>
#include <google/protobuf/descriptor.h>

namespace sitinf {
  namespace sensor {
    /**
     * @brief The TypeSensor class represents a Sensor that uses messages of a single
     * Protobuf type received via a single rsb scope to update its current state.
     */
    class TypeSensor : public Sensor {

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<TypeSensor> Ptr;

      /**
       * @brief The QueryState struct maps message queries to the corresponding states.
       */
      struct QueryState{
        /**
         * @brief query the query
         */
        std::string query;
        /**
         * @brief state the state
         */
        std::string state;

        /**
         * @brief QueryState constructor setting the inner fields
         * @param query_string the query string
         * @param state_string the state string
         */
        QueryState(const std::string& query_string, const std::string& state_string)
          : query(query_string), state(state_string) {}

        /**
         * @brief QueryState default constructor setting fields to empty strings.
         */
         QueryState() : QueryState("",""){

        }
      };

      /**
       * @brief TypeSensor listens for messages of type on scope. sets current state according to query outputs.
       *
       * On every message of type on scope the
       *
       * @param states the possible states of this sensor
       * @param name the name of this sensor/aspect
       * @param scope the scope this sensor listens to
       * @param type the descriptor of the message type this sensor interprets
       * @param queries maps states to queries.
       * @param use_subscopes whether to interpret messages from subscopes of scope.
       */
      TypeSensor(const std::vector<std::string>& states,
                 const std::string& name,
                 const std::string& scope,
                 const google::protobuf::Descriptor* type,
                 const std::vector<QueryState>& queries,
                 bool use_subscopes=false);

      /**
       * @brief ~TypeSensor virtual destructor for nice cleanup.
       */
      virtual ~TypeSensor();

      /**
       * @brief states access to the possible states of this sensor
       * @return a vector containing all states
       */
      virtual const std::vector<std::string>& states() const;

      /**
       * @brief name acess to the name of this sensor/aspect.
       * @return the name of this aspect.
       */
      virtual std::string name() const;

      /**
       * @brief currentState access to the current state of this sensor
       * @return the current state of this sensor. may be __unobserved__
       */
      virtual std::string currentState() const;

    private:
      /**
       * @brief m_Lock used for synchronization.
       */
      mutable utils::Mutex m_Lock;
      /**
       * @brief m_States the vector of possible states of this TypeSensor.
       *
       * This will not change after construction.
       */
      std::vector<std::string> m_States;
      /**
       * @brief m_Name the name of this sensor/aspect.
       */
      std::string m_Name;
      /**
       * @brief m_CurrentState the current state of this sensor may be an element of m_States or '__unobserved__'
       */
      std::string m_CurrentState;
      /**
       * @brief m_Desciptor a pointer to the message descriptor this sensor uses.
       */
      const google::protobuf::Descriptor* m_Desciptor;
      /**
       * @brief m_QueryContext the pquery context.
       */
      pquery::PQuery m_QueryContext;
      /**
       * @brief m_Queries holds the queries and corresponding states.
       */
      std::vector<std::pair<pquery::Query::Ptr,std::string> > m_Queries;
      /**
       * @brief m_Listener provides message data from rsb.
       */
      rsb::ListenerPtr m_Listener;
      /**
       * @brief handle handler function receives events from m_Listener, executes queries and calls notify.
       *
       * This handle expects the event being from the right scope and type.
       *
       * @param event the event from the rsb listener.
       */
      void handle(rsb::EventPtr event);
    };

  } // namespace sensor
} // namespace sitinf
