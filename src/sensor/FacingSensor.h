/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/FacingSensor.h                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Subject.h>
#include <sensor/Sensor.h>

#include <rst/vision/Faces.pb.h>
#include <rst/vision/FaceWithGazeCollection.pb.h>
#include <rst/vision/FaceLandmarksCollection.pb.h>

namespace sitinf {
  namespace sensor {

    template<typename PersonDataType,
             typename PersonsDataType>
    class PersonSelectionStrategy : public utils::NonCopyable {
    public:
      typedef PersonDataType PersonData;
      typedef PersonsDataType PersonsData;
      typedef boost::shared_ptr<PersonsData> PersonsDataPtr;
      typedef PersonSelectionStrategy<PersonDataType,PersonsDataType> StrategyType;
      typedef std::shared_ptr<StrategyType> Ptr;

      static Ptr create(const std::string& name, const sitinf::utils::sint& id);

      virtual ~PersonSelectionStrategy<PersonDataType,PersonsDataType>() = default;

      virtual const PersonData* select(const PersonsDataPtr data) const = 0;
    };

    typedef PersonSelectionStrategy<rst::vision::Face,rst::vision::Faces> FacesSelectionStrategy;
    typedef PersonSelectionStrategy<rst::vision::FaceWithGaze,rst::vision::FaceWithGazeCollection>
    GazesSelectionStrategy;


    template<typename PersonDataType, typename PersonsDataType>
    class FacingSensor; // forward declaration. implementation in source file


  } // namespace sensor
} // namespace sitinf
