/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/StringSensor.cpp                            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/StringSensor.h>
#include <utils/Exception.h>
#include <rsb/Factory.h>
#include <sensor/SensorFactory.h>
#include <yaml-cpp/yaml.h>
#include <utils/YamlUtils.h>
#include <utils/Macros.h>
#include <utils/Macros.h>
#include <boost/regex.h>

#define _LOGGER "sensor.StringSensor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

static const bool USE_SUBSCOPES = true;

using sitinf::sensor::StringSensor;
using sitinf::sensor::Sensor;

namespace {
  std::vector<std::string> collectStates(std::vector<StringSensor::StateMatch> state_matches){
    std::vector<std::string> result;
    result.reserve(state_matches.size());
    for (auto match : state_matches){
      result.push_back(match.state);
    }
    return result;
  }
}

StringSensor::StringSensor(const std::string& name, const std::vector<StateMatch> state_matches, const std::string& scope)
  : SimpleSensor(name, collectStates(state_matches),Sensor::unobserved()), m_StateMatches(state_matches)
{
  m_Listener = rsb::getFactory().createListener(scope);
  m_Handler = rsb::HandlerPtr(
        new rsb::EventFunctionHandler([this] (rsb::EventPtr event) -> void {this -> handle(event);}));
  m_Listener->addHandler(m_Handler);
}

void StringSensor::handle(rsb::EventPtr event){
  if(event->getType() == "std::string"){
    boost::shared_ptr<std::string> message = boost::static_pointer_cast<std::string>(event->getData());
    for(auto state_match : m_StateMatches){
      boost::smatch match;
      if(boost::regex_match(*message,match,boost::regex(state_match.pattern))){
        this->changeCurrentState(state_match.state);
        return;
      }
    }
    SLOG_TRACE << "Received string '" << *message << "' does not match anything.";
  } else {
    SLOG_TRACE << "Received non-string message Got '" << event->getType() << "'.";
  }
}

StringSensor::~StringSensor(){
  m_Listener->removeHandler(m_Handler);
}

namespace YAML {
  template<>
  struct convert<StringSensor::StateMatch> {
    static Node encode(const StringSensor::StateMatch& rhs) {
      Node node;
      node["state"] = rhs.state;
      node["pattern"] = rhs.pattern;
      return node;
    }

    static bool decode(const Node& node, StringSensor::StateMatch& rhs) {
      rhs.state = sitinf::utils::yaml::read<std::string>(node,"state");
      rhs.pattern = sitinf::utils::yaml::read<std::string>(node,"pattern");
      return true;
    }

  };
}

static std::vector<Sensor::Ptr> createFromConfig(const YAML::Node sensor, const sitinf::sensor::SensorFactory& factory){
  YAML::Node config = sensor["config"];
  //  name: "Flobi_Speaking_Wardrobe"
  //  states:
  //    - state: "Yes"
  //    - pattern: "speaking_start"
  //    - state: "No"
  //    - pattern: "speaking_end"
  //  scope: /home/wardrobe/flobi/speakingstate/

  auto name = sitinf::utils::yaml::read<std::string>(config,"name");
  auto scope = sitinf::utils::yaml::read<std::string>(config,"scope");
  auto states = sitinf::utils::yaml::read<std::vector<StringSensor::StateMatch>>(config,"states");

  std::vector<Sensor::Ptr> result;
  result.push_back(Sensor::Ptr(new StringSensor(name,states,scope)));

  return result;
}

// register at factory
static sitinf::sensor::SensorFactory::Registrar registerDayTimeSensor("StringSensor", createFromConfig);

