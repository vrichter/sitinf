/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/DelaySensor.cpp                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/DelaySensor.h>
#include <utils/Exception.h>
#include <io/AnyTypeParticipantConfig.h>
#include <rsb/Factory.h>
#include <io/RsbFilters.h>
#include <utils/Macros.h>

#define _LOGGER "sensor.DelaySensor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::sensor;
using namespace sitinf::utils;

DelaySensor::DelaySensor(Sensor::Ptr sensor, uint delay_millis, std::string delay_state)
  : m_Sensor(sensor), m_Delay(delay_millis), m_DelayState(delay_state)
{
  m_CurrentState = m_Sensor->currentState();
  m_RequestedState = m_CurrentState;
  // spawn a new thread that waits for timeout.
  m_Thread = Thread(&DelaySensor::waitForTimeout,this); // will directly go into long wait.
  // register to sensor
  sensor->addObserver(this);
}

DelaySensor::~DelaySensor(){
  m_Sensor->removeObserver(this);
  m_Thread.interrupt();
  m_Thread.join();
  RecursiveLocker l(m_Lock);
}

const std::vector<std::string>& DelaySensor::states() const {
  return m_Sensor->states();
}

std::string DelaySensor::name() const {
  return m_Sensor->name();
}

std::string DelaySensor::currentState() const{
  RecursiveLocker l(m_Lock);
  return m_CurrentState;
}

void DelaySensor::update(const Subject* subject, const std::string& data){
  // subject must be the sensor
  if(subject != m_Sensor.get()){
    SLOG_TRACE << "Subject is not a known Sensor";
    return;
  }
  RecursiveLocker l(m_Lock);
  // update current state if state is not delayed
  if(data != m_DelayState){
    m_CurrentState = data;
    m_RequestedState = data;
    m_LastUpdate = boost::system_time(boost::posix_time::pos_infin); // not delayed state -> no timeout
    notify(m_CurrentState);
  } else {
    if(m_RequestedState != m_DelayState){
      m_RequestedState = m_DelayState;
      m_LastUpdate = boost::get_system_time();
      // notify thread
      m_Condition.notify_all();
    } else if (m_DelayState == m_CurrentState){
      notify(m_CurrentState); // propagate update of already set delaystate
    } // else: no changes on timeout the thread will set to the delayed state
  }
}

 void DelaySensor::waitForTimeout(){
  // the wait-mutex
  Mutex lock;
  // set time for timeout
  boost::system_time until = m_LastUpdate + m_Delay;
  while(true){
    // lock the mutex
    Locker locker(lock);
    // calling wait releases the lock and blocks.
    bool notified = m_Condition.timed_wait(locker,until);
    RecursiveLocker memberLock(m_Lock);
    if(notified){
      // notified by another thread.
      until = m_LastUpdate + m_Delay;
    } else {
      // need to lock for the loop
      if(boost::get_system_time() > m_LastUpdate + m_Delay){
        // the time is realy after timeout. notify
        m_CurrentState = m_DelayState;
        notify(m_CurrentState);
      }
      if(m_CurrentState == m_DelayState){
        // the current state is already the delay. wait until mtv plays music again
        until = boost::system_time(boost::posix_time::pos_infin);
      } else {
        // the current state is not the delayed so we wait until timeout.
        until = m_LastUpdate + m_Delay;
      }
    }
  }
  // this point is not reachable. The thread will be stopped by interrupt.
 }
