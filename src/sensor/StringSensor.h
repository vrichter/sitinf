/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/StringSensor.h                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <sensor/Sensor.h>
#include <rsb/Listener.h>

namespace sitinf {
  namespace sensor {
    /**
     * @brief The StringSensor matches strings from rsb to states.
     */
    class StringSensor : public SimpleSensor {
    public:

      struct StateMatch {
        std::string state;
        std::string pattern;
      };

      StringSensor(const std::string& name, const std::vector<StateMatch> state_matches, const std::string& scope);

      virtual ~StringSensor();

    private:
      rsb::ListenerPtr m_Listener;
      rsb::HandlerPtr m_Handler;
      std::vector<StateMatch> m_StateMatches;

      void handle(rsb::EventPtr event);
    };

  } // namespace sensor
} // namespace sitinf
