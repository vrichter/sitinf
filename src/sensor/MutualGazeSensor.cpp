/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/MutualGazeSensor.cpp                        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/DelaySensor.h>
#include <sensor/MutualGazeSensor.h>
#include <sensor/SensorFactory.h>
#include <sensor/TimeoutSensor.h>
#include <utils/Exception.h>
#include <utils/YamlUtils.h>

#include <io/RsbFilters.h>
#include <io/RsbSource.h>

#include <rsb/Factory.h>
#include <yaml-cpp/yaml.h>

#include <utils/Macros.h>
#define _LOGGER "sensor.MutualGazeSensor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using sitinf::sensor::MutualGazeSensor;
using sitinf::sensor::SimpleSensor;
using sitinf::sensor::Sensor;
using sitinf::sensor::SensorFactory;

MutualGazeSensor::MutualGazeSensor(const std::string& name, const std::string& scope,
                                   sitinf::sensor::GazesSelectionStrategy::Ptr person,
                                   double min_horizontal_gaze, double max_horizontal_gaze, double min_vertical_gaze,
                                   double max_vertical_gaze) :
  SimpleSensor(name, { yes(), no() }, Sensor::unobserved()),
  m_Person(person), m_MinHorizontalGaze(min_horizontal_gaze), m_MaxHorizontalGaze(max_horizontal_gaze),
  m_MinVerticalGaze(min_vertical_gaze), m_MaxVerticalGaze(max_vertical_gaze)
{
  m_Listener = rsb::getFactory().createListener(scope);
  m_Handler = rsb::HandlerPtr(new rsb::DataFunctionHandler<PersonsData>(
                                boost::bind(&MutualGazeSensor::handle,this,_1)
                                ));
  m_Listener->addHandler(m_Handler,true);
}

MutualGazeSensor::~MutualGazeSensor(){
  m_Listener->removeHandler(m_Handler);
}

void MutualGazeSensor::handle(PersonsDataPtr data) {
  if(!m_Person->select(data)){
    changeCurrentState(Sensor::unobserved());
    return;
  }
  auto person = *m_Person->select(data);
  if(!(person.has_horizontal_gaze_estimation() && person.has_vertical_gaze_estimation())){
    changeCurrentState(Sensor::unobserved());
    return;
  }
  if(person.horizontal_gaze_estimation() >= m_MinHorizontalGaze &&
     person.horizontal_gaze_estimation() <= m_MaxHorizontalGaze &&
     person.vertical_gaze_estimation() >= m_MinVerticalGaze &&
     person.vertical_gaze_estimation() <= m_MaxVerticalGaze)
  {
    changeCurrentState("Yes");
  } else {
    changeCurrentState("No");
  }
}

static std::vector<Sensor::Ptr> createFromConfig(const YAML::Node sensor, const SensorFactory& factory){
  sitinf::io::register_rst<MutualGazeSensor::PersonsData>();
  YAML::Node config = sensor["config"];
  //  agent: Flobi_Wardrobe
  //  persons_prefix: Person
  //  scope: /home/wardrobe/faces
  //  persons: 5
  //  min_horizontal: -10
  //  max_horizontal: 10
  //  min_vertical: -10
  //  max_vertical: 10
  //  selection: original

  namespace uyaml = sitinf::utils::yaml;
  auto agent = uyaml::read<std::string>(config,"agent");
  auto persons_prefix = uyaml::read<std::string>(config,"persons_prefix");
  auto scope = uyaml::read<std::string>(config,"scope");
  auto persons = uyaml::read<uint>(config,"persons");
  auto min_horizontal = uyaml::read<double>(config,"min_horizontal");
  auto max_horizontal = uyaml::read<double>(config,"max_horizontal");
  auto min_vertical = uyaml::read<double>(config,"min_vertical");
  auto max_vertical = uyaml::read<double>(config,"max_vertical");
  auto selection = uyaml::read<std::string>(config,"selection");

  using sitinf::sensor::TimeoutSensor;
  using sitinf::sensor::DelaySensor;
  std::vector<Sensor::Ptr> result;
  for(uint i = 0; i < persons; ++i) {
    auto strategy = sitinf::sensor::GazesSelectionStrategy::create(selection,i);
    result.push_back(Sensor::Ptr(
                       new MutualGazeSensor("Viewing_" + persons_prefix + std::to_string(i) + "_" + agent,
                                            scope, strategy, min_horizontal, max_horizontal, min_vertical, max_vertical)));
  }
  return result;
}

// register at factory
static SensorFactory::Registrar registerDayTimeSensor("MutualGazeSensor", createFromConfig);

