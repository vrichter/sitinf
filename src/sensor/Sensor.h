/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/Sensor.h                                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Types.h>
#include <utils/Subject.h>
#include <atomic>
#include <utils/Exception.h>
#include <utils/NonCopyable.h>

namespace sitinf {
  namespace sensor {
    /**
     * @brief The Sensor class represents an aspect of the environment.
     *
     * The aspect should be observable in some way and the amout of its states must be finite and known.
     */
    class Sensor : public utils::Subject<const std::string&>, public utils::NonCopyable {

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<Sensor> Ptr;

      /**
       * @brief ~Sensor virtual destructor for nice cleanup.
       */
      virtual ~Sensor(){}

      /**
       * @brief states access to the possible states of this sensor
       * @return a vector containing all states
       */
      virtual const std::vector<std::string>& states() const = 0;

      /**
       * @brief name acess to the name of this sensor/aspect.
       * @return the name of this aspect.
       */
      virtual std::string name() const = 0;

      /**
       * @brief currentState access to the current state of this sensor
       * @return the current state of this sensor. may be __unobserved__
       */
      virtual std::string currentState() const = 0;

      /**
       * @brief toString creates a string representation of the Sensor.
       * @return a string describing this sensor
       */
      virtual std::string toString() const;

      /**
       * @brief unubserved default identifier of 'state not observed'
       * @return unobserved.
       */
      static inline std::string unobserved(){ return "__unobserved__"; }

    };

    class SimpleSensor : public Sensor {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<SimpleSensor> Ptr;


      SimpleSensor(const std::string& name, const std::vector<std::string>& states, const std::string& current_state);

      virtual ~SimpleSensor();

      virtual const std::vector<std::string>& states() const final;

      virtual std::string name() const final;

      virtual std::string currentState() const final;

      /**
       * @brief changeCurrnetState changes the current state to state if the passed one is a valid state.
       * @param state the new state
       * @return false if the passed state is unknown and the sensor is not updated.
       */
      bool changeCurrentState(const std::string& state);

    private:
      std::string m_Name;
      std::vector<std::string> m_States;
      std::atomic_uint m_CurrentState;
    };

    class ChangeFilterSensor : public Sensor, public Sensor::ObserverType {
    public:

      ChangeFilterSensor(Sensor::Ptr other);

      virtual ~ChangeFilterSensor();

      virtual const std::vector<std::string>& states() const final;

      virtual std::string name() const final;

      virtual std::string currentState() const final;

      virtual void update(const Sensor::SubjectType* subject, const std::string& data) override;

    private:
      const Sensor::Ptr m_Sensor;
      std::string m_LastKnownState;
    };

  } // namespace sensor
} // namespace sitinf
