/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/SensorFactory.h                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <sensor/Sensor.h>
#include <utils/FunctionRegistry.h>
#include <io/ProtoFilesImporter.h>
#include <network/BayesianNetwork.h>
#include <rsb/Listener.h>
#include <yaml-cpp/yaml.h>
#include <functional>

namespace sitinf {
  namespace sensor {

    /**
     * @brief The SensorFactory class creates discrete sensor instances.
     */
    class SensorFactory : public utils::Factory<
        std::vector<sensor::Sensor::Ptr>,
        std::string,
        std::function<std::vector<sensor::Sensor::Ptr>(YAML::Node sensor, SensorFactory& factory)>
    >{

    public:

      /**
       * @brief SensorFactory constructor sets up factory.
       * @param proto the ProtoFilesImporter to use to get Type Descriptors.
       */
      SensorFactory(io::ProtoFilesImporter::Ptr proto) : m_Proto(proto){}

      /**
       * @brief createSensors creates sensors from a Config description.
       *
       * The passed yaml-node must have a 'sensors' element containing a vector of
       * sensor configurations.
       *
       * @param sensor_config a sensor configuration in form of a yaml node
       * @return a vector of SensorPointers
       */
      std::vector<Sensor::Ptr> createSensors(const YAML::Node sensor_config);

      /**
       * @brief createSensors creates sensors from a yaml description in a file.
       * @param filename the file must contain a valid sensor configuration.
       * @return a vector of sensor pointers.
       */
      std::vector<Sensor::Ptr> createSensors(const std::string& filename);

      /**
       * @brief protoFilesImporter accessor to the internal ProtoFilesImporter.
       * @return a reference to this factories ProtoFilesImporter
       */
      io::ProtoFilesImporter& protoFilesImporter() const;

    private:
      /**
       * @brief m_Proto a ProtoFilesImporter used to get Protorbuf Descriptors if needed.
       */
      io::ProtoFilesImporter::Ptr m_Proto;

    };

  } // namespace sensor
} // namespace sitinf
