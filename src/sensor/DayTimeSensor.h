/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/DayTimeSensor.h                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <sensor/Sensor.h>
#include <utils/Types.h>

namespace sitinf {
  namespace sensor {
    /**
     * @brief The DayTimeSensor class represents a Sensor decorator that extends a Sensor with a time and
     * possibly a new state.
     *
     * To achieve this the DayTimeSensor registeres to its decorated Sensor and mirrores its output. If the
     * inner Sensor does not provide a state change for a specified time, the DayTimeSensor emits its time-state.
     */
    class DayTimeSensor : public Sensor {

    public:

      /**
       * @brief The DayTime class holds a time duration as the end of the DayTime and its name:
       *
       * Example: DayTime(Morning,12h) means that the part of the day called morning ends at 12 o'clock.
       */
      class DayTime {

      public:
        /**
         * @brief Ptr typedef for shared pointer type
         */
        typedef std::shared_ptr<DayTimeSensor> Ptr;

        /**
         * @brief DayTime default constructor. initializes to "unknown" 00:00:00.
         */
        DayTime();

        /**
         * @brief DayTime creates a DayTime instance.
         * @param name the name of the DayTime
         * @param day_progress when does it end. Must be < 24h
         */
        DayTime(std::string name, boost::posix_time::time_duration day_progress);

        /**
         * @brief name The name of this DayTime.
         * @return A string representing the name. Example: "Morning"
         */
        const std::string name() const{
          return m_Name;
        }

        /**
         * @brief end The end of this DayTime
         * @return A duration < 24h
         */
        const boost::posix_time::time_duration& end() const{
          return m_End;
        }

        inline bool operator< (const DayTime& rhs) const { return this->end() < rhs.end(); }
        inline bool operator> (const DayTime& rhs) const { return rhs < *this;    }
        inline bool operator<=(const DayTime& rhs) const { return !(*this > rhs); }
        inline bool operator>=(const DayTime& rhs) const { return !(*this < rhs); }

      private:
        /**
         * @brief m_Name internal name field.
         */
        std::string m_Name;

        /**
         * @brief m_End internal end duration field.
         */
        boost::posix_time::time_duration m_End;
      };

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<DayTimeSensor> Ptr;

      /**
       * @brief DayTimeSensor emits states for diffetent times of the day based on the machines locale settings.
       *
       * This sensor will have a many states as DayTimes with distinct names are passed.
       *
       * @param times relevant times of the day. Must not be empty.
       * @param name the name of this sensor. Must not be empty.
       */
      DayTimeSensor(const std::string& name, const std::vector<DayTime>& times);

      /**
       * @brief ~DayTimeSensor virtual destructor for nice cleanup.
       */
      virtual ~DayTimeSensor();

      /**
       * @brief operator = prevent copying instances.
       * @param other
       * @return
       */
      DayTimeSensor& operator=(const DayTimeSensor& other) = delete;

    public:

      /**
       * @brief states access to the possible states of this sensor
       * @return a vector containing all states
       */
      virtual const std::vector<std::string>& states() const;

      /**
       * @brief name acess to the name of this sensor/aspect.
       * @return the name of this aspect.
       */
      virtual std::string name() const;

      /**
       * @brief currentState access to the current state of this sensor
       * @return the current state of this sensor. may be __unobserved__
       */
      virtual std::string currentState() const;


      /**
       * @brief currentDayTime returns the current day time
       * @return The progress of the current day as a time duration.
       */
      static boost::posix_time::time_duration currentDayTime();

    private:
      /**
       * @brief m_Lock used for synchronization.
       */
      mutable utils::RecursiveMutex m_Lock;

      std::string m_Name;
      std::vector<std::string> m_States;
      std::vector<DayTime> m_DayTimes;
      std::string m_CurrentState;

      /**
       * @brief m_Thread a locally created thread waiting for the timeout.
       */
      utils::Thread m_Thread;
      /**
       * @brief m_ConditionMutex mutex lock for the cundition.
       */
      utils::Mutex m_ConditionMutex;
      /**
       * @brief m_Condition is used to wait for different daytimes/destruction.
       */
      boost::condition_variable m_Condition;
      /**
       * @brief m_Exit exit flag used to stop thread.
       */
      bool m_Exit;

      /**
       * @brief updateState updates the current state in a loop.
       *
       * Waits until the end of the current DayTime, updates the stete to
       * the next and waits for the following in a loop.
       *
       * @param current the index of the current DayTime in the internal list.
       */
      void updateState(uint current);
    };

  } // namespace sensor
} // namespace sitinf
