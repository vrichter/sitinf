/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/FacingSensor.cpp                            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/DelaySensor.h>
#include <sensor/FacingSensor.h>
#include <sensor/SensorFactory.h>
#include <sensor/TimeoutSensor.h>
#include <utils/Exception.h>
#include <utils/YamlUtils.h>

#include <io/RsbFilters.h>
#include <io/RsbSource.h>

#include <rsb/Factory.h>
#include <rsb/Listener.h>
#include <rst/vision/Faces.pb.h>
#include <rst/vision/FaceWithGazeCollection.pb.h>


#include <yaml-cpp/yaml.h>

#include <utils/Macros.h>
#define _LOGGER "sensor.FacingSensor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using sitinf::sensor::FacingSensor;
using sitinf::sensor::SimpleSensor;
using sitinf::sensor::Sensor;
using sitinf::sensor::SensorFactory;

using sitinf::sensor::PersonSelectionStrategy;
using rst::vision::Face;
using rst::vision::Faces;
using rst::vision::FaceWithGaze;
using rst::vision::FaceWithGazeCollection;
using rst::vision::FaceLandmarks;
using rst::vision::FaceLandmarksCollection;

double dist(double x1, double y1, double x2, double y2){
  return (std::sqrt(std::pow(x2-x1,2) + std::pow(y2-y1,2)));
}

template<typename PersonDataType>
double centerDist(const PersonDataType* person);

template<>
double centerDist<Face>(const Face* face){
  return dist(face->region().image_width()/2,
              face->region().image_height()/2,
              face->region().top_left().x() + face->region().width()/2,
              face->region().top_left().y() + face->region().height()/2
              );
}

template<>
double centerDist<FaceWithGaze>(const FaceWithGaze* face){
  return centerDist<Face>(&face->region());
}

template<>
double centerDist<FaceLandmarks>(const FaceLandmarks* face){
  return 0;
}

template<typename PersonDataType>
bool compareCenterDist(const PersonDataType* first,const PersonDataType* second){
  return centerDist<PersonDataType>(first) < centerDist<PersonDataType>(second);
}

template<typename PersonDataType, typename PersonsDataType>
std::vector<const PersonDataType*> collect(PersonsDataType* data){
  std::vector<const PersonDataType*> result; result.reserve(data->element_size());
  for(const PersonDataType& face : data->element()){
    result.push_back(&face);
  }
  return result;
}

template<>
std::vector<const Face*> collect(Faces* data){
  std::vector<const Face*> result; result.reserve(data->faces_size());
  for(auto face : data->faces()){
    result.push_back(&face);
  }
  return result;
}

template<typename PersonDataType, typename PersonsDataType>
class CenteredOrder : public PersonSelectionStrategy<PersonDataType,PersonsDataType> {
  const sitinf::utils::sint m_Id;
public:
  CenteredOrder(sitinf::utils::sint id) : m_Id(id) {}

  virtual const PersonDataType* select(boost::shared_ptr<PersonsDataType> data) const override
  {
    auto sorted = collect<PersonDataType,PersonsDataType>(data.get());
    std::sort(sorted.begin(), sorted.end(),
              compareCenterDist<PersonDataType>);
    return ((int)sorted.size() > m_Id) ? sorted[m_Id] : nullptr;
  }
};

template<typename PersonDataType, typename PersonsDataType>
class OriginalOrder : public PersonSelectionStrategy<PersonDataType,PersonsDataType> {
  const sitinf::utils::sint m_Id;
public:
  OriginalOrder(sitinf::utils::sint id) : m_Id(id) {}

  virtual const PersonDataType* select(boost::shared_ptr<PersonsDataType> data) const override
  {
    auto persons = collect<PersonDataType,PersonsDataType>(data.get());
    return ((int)persons.size() > m_Id) ? persons[m_Id] : nullptr;
  }
};

namespace sitinf {
namespace sensor {
  template<typename PersonDataType, typename PersonsDataType>
  typename PersonSelectionStrategy<PersonDataType,PersonsDataType>::Ptr
  PersonSelectionStrategy<PersonDataType,PersonsDataType>::create(
      const std::string &name, const sitinf::utils::sint& id)
  {
    if(name == "original"){
      return std::make_shared<OriginalOrder<PersonDataType,PersonsDataType>>(id);
    } else if(name == "sort_center"){
      return std::make_shared<CenteredOrder<PersonDataType,PersonsDataType>>(id);
    }
    STHROW(sitinf::utils::ParsingException, "Unknown PersonSelectionStrategy '" << name << "'");
  }

  /**
   * @brief The FacingSensor tells if a person is facing an agent based on the agents face detection.
   */
  template<typename PersonDataType,
           typename PersonsDataType>
  class FacingSensor : public SimpleSensor {
  public:

    typedef PersonsDataType PersonsData;
    typedef boost::shared_ptr<PersonsData> PersonsDataPtr;
    typedef PersonSelectionStrategy<PersonDataType,PersonsDataType> SelectionStrategyType;

    FacingSensor(const std::string& name, const std::string& scope, typename SelectionStrategyType::Ptr person)
      : SimpleSensor(name, { yes(), no() }, Sensor::unobserved()), m_Person(person)
    {
      sitinf::io::register_rst<PersonsDataType>();
      m_Listener = rsb::getFactory().createListener(scope);
      m_Handler = rsb::HandlerPtr(new rsb::DataFunctionHandler<PersonsData>(
                                    boost::bind(&FacingSensor::handle,this,_1)
                                  ));
      m_Listener->addHandler(m_Handler,true);
    }

    virtual ~FacingSensor(){
      m_Listener->removeHandler(m_Handler);
    }

    void handle(PersonsDataPtr data){
      if(!m_Person->select(data)){
        changeCurrentState(no());
      } else {
        changeCurrentState(yes());
      }
    }

    static std::string yes(){ return "Yes"; }
    static std::string no(){ return "No"; }

  private:
    const typename SelectionStrategyType::Ptr m_Person;
    rsb::ListenerPtr m_Listener;
    rsb::HandlerPtr m_Handler;
  };
}
}

template class PersonSelectionStrategy<Face,Faces>;
template class PersonSelectionStrategy<FaceWithGaze,FaceWithGazeCollection>;
template class PersonSelectionStrategy<FaceLandmarks,FaceLandmarksCollection>;

template<typename PersonDataType,typename PersonsDataType>
static std::vector<Sensor::Ptr> createFromConfig(const YAML::Node sensor, const SensorFactory& factory){
  typedef FacingSensor<PersonDataType,PersonsDataType> FacingSensorType;
  sitinf::io::register_rst<typename FacingSensorType::PersonsData>();
  YAML::Node config = sensor["config"];
  //  agent: Flobi_Wardrobe
  //  persons_prefix: Person
  //  scope: /home/wardrobe/faces
  //  persons: 5
  //  selection: original

  namespace uyaml = sitinf::utils::yaml;
  auto agent = uyaml::read<std::string>(config,"agent");
  auto persons_prefix = uyaml::read<std::string>(config,"persons_prefix");
  auto scope = uyaml::read<std::string>(config,"scope");
  auto persons = uyaml::read<uint>(config,"persons");
  auto selection = uyaml::read<std::string>(config,"selection");

  using sitinf::sensor::TimeoutSensor;
  using sitinf::sensor::DelaySensor;
  std::vector<Sensor::Ptr> result;
  for(uint i = 0; i < persons; ++i) {
  auto strategy = FacingSensorType::SelectionStrategyType::create(selection,i);
    result.push_back(Sensor::Ptr(
                       new FacingSensorType("Facing_" + agent + "_" + persons_prefix + std::to_string(i),scope, strategy))
                     );
    result.back() = Sensor::Ptr(new TimeoutSensor(result.back(),500,"No"));
    result.back() = Sensor::Ptr(new TimeoutSensor(result.back(),2000,Sensor::unobserved()));

    // person faces agent
    result.push_back(Sensor::Ptr(
                       new FacingSensorType("Facing_" + persons_prefix + std::to_string(i) + "_" + agent,scope,strategy))
                     );
    result.back() = Sensor::Ptr(new TimeoutSensor(result.back(),500,"No"));
    result.back() = Sensor::Ptr(new TimeoutSensor(result.back(),2000,Sensor::unobserved()));

    // person exists
    result.push_back(Sensor::Ptr(
                       new FacingSensorType("Exists_" + persons_prefix + std::to_string(i),scope,strategy))
                     );
    result.back() = Sensor::Ptr(new DelaySensor(result.back(),500,"No"));
    result.back() = Sensor::Ptr(new TimeoutSensor(result.back(),2000,Sensor::unobserved()));
  }
  return result;
}

// register at factory
static SensorFactory::Registrar registerSensor1(
    "FacesFacingSensor", createFromConfig<Face,Faces>);
static SensorFactory::Registrar registerSensor2(
    "GazesFacingSensor", createFromConfig<FaceWithGaze,FaceWithGazeCollection>);

