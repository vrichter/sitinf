/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/SituationSensor.h                           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <sensor/Sensor.h>
#include <rsb/Listener.h>
#include <rst/classification/ClassificationResultMap.pb.h>

namespace sitinf {
  namespace sensor {
    /**
     * @brief The SituationSensor class represents a Sensor that uses Situation messages
     * to update its crrent state.
     */
    class SituationSensor :
        public Sensor,
        public utils::Observer<boost::shared_ptr<rst::classification::ClassificationResultMap> >
    {

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<SituationSensor> Ptr;

      /**
       * @brief SituationSensor listens to the passed Situation Source and sets current state according
       * to its outputs.
       *
       * @param name the name of this sensor.
       * @param states all possible states of this sensor.
       * @param aspect the aspect that is relevant to this sensor.
       * @param state_map mapping of aspect states to this sensors states.
       * @param default_state the sensors default state is used when the observed aspect is in
       *        a state which is not represented in the state_map.
       */
      SituationSensor(const std::string& name,
                      const std::vector<std::string>& states,
                      const std::string& aspect,
                      const std::map<std::string,std::string> state_map,
                      const std::string& default_state=unobserved());

      /**
       * @brief ~SituationSensor virtual destructor for nice cleanup.
       */
      virtual ~SituationSensor();

      /**
       * @brief states access to the possible states of this sensor
       * @return a vector containing all states
       */
      virtual const std::vector<std::string>& states() const;

      /**
       * @brief name acess to the name of this sensor/aspect.
       * @return the name of this aspect.
       */
      virtual std::string name() const;

      /**
       * @brief currentState access to the current state of this sensor
       * @return the current state of this sensor. may be __unobserved__
       */
      virtual std::string currentState() const;

      /**
       * @brief update observer implementation. updates the internal state according to data.
       * @param subject the subject that provides information.
       * @param data a situation classification
       */
      virtual void update(const Subject<boost::shared_ptr<rst::classification::ClassificationResultMap> >* subject,
                          boost::shared_ptr<rst::classification::ClassificationResultMap> data);

    private:
      /**
       * @brief m_Lock used for synchronization.
       */
      mutable utils::RecursiveMutex m_Lock;
      /**
       * @brief m_States the vector of possible states of this SituationSensor.
       *
       * This will not change after construction.
       */
      std::vector<std::string> m_States;
      /**
       * @brief m_Name the name of this sensor/aspect.
       */
      std::string m_Name;
      /**
       * @brief m_CurrentState the current state of this sensor may be an element of m_States or '__unobserved__'
       */
      std::string m_CurrentState;

      /**
       * @brief m_Aspect the relevant aspect for this sensor.
       */
      std::string m_Aspect;

      /**
       * @brief m_StateMap a mapping of aspect states to sensor states.
       */
      std::map<std::string,std::string> m_StateMap;

      /**
       * @brief m_DefaultState the default state of this sensor.
       */
      std::string m_DefaultState;

      /**
       * @brief updateCurrentState internal update function sets the current state and notifies observers if changed.
       * @param value the new current statte.
       */
      void updateCurrentState(const std::string& value);
    };

  } // namespace sensor
} // namespace sitinf
