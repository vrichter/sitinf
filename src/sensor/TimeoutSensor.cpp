/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/TimeoutSensor.cpp                           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/TimeoutSensor.h>
#include <utils/Exception.h>
#include <io/AnyTypeParticipantConfig.h>
#include <rsb/Factory.h>
#include <io/RsbFilters.h>
#include <utils/Macros.h>
#include <sensor/SensorFactory.h>
#include <yaml-cpp/yaml.h>

#define _LOGGER "sensor.TimeoutSensor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::sensor;
using namespace sitinf::utils;

TimeoutSensor::TimeoutSensor(Sensor::Ptr sensor, uint timeout_millis, std::string timeout_state, TimeoutResetStrategy reset)
  : m_Sensor(sensor), m_States(m_Sensor->states()), m_CurrentState(timeout_state), m_TimeoutState(timeout_state),
    m_Timeout(timeout_millis), m_LastUpdate(boost::get_system_time() + boost::posix_time::minutes(60)),
    m_ResetStrategy(reset)
{
  if(m_TimeoutState != Sensor::unobserved()){
    // add timeout state if not already present
    if(std::find(m_States.begin(), m_States.end(), m_TimeoutState) == m_States.end()){
      m_States.push_back(m_TimeoutState);
    }
  }
  // spawn a new thread that waits for timeout.
  m_Thread = Thread(&TimeoutSensor::waitForTimeout,this); // will directly go into long wait.
  // register to sensor
  sensor->addObserver(this);
}
TimeoutSensor::~TimeoutSensor(){
  RecursiveLocker l(m_Lock);
  m_Sensor->removeObserver(this);
  m_Thread.interrupt();
  m_Thread.join();
}

const std::vector<std::string>& TimeoutSensor::states() const{
  return m_States;
}

std::string TimeoutSensor::name() const {
  return m_Sensor->name();
}

std::string TimeoutSensor::currentState() const{
  RecursiveLocker l(m_Lock);
  return m_CurrentState;
}

void TimeoutSensor::update(const Subject* subject, const std::string& data){
  // subject must be the sensor
  if(subject != m_Sensor.get()){
    SLOG_TRACE << "Subject is not a known Sensor";
    return;
  }
  RecursiveLocker l(m_Lock);
  if(m_ResetStrategy == on_update || (m_CurrentState != data)){
    // notify thread
    m_LastUpdate = boost::get_system_time();
    m_Condition.notify_all();
  }
  // update current state
  m_CurrentState = data;
  notify(m_CurrentState);
}

void TimeoutSensor::waitForTimeout(){
  // the wait-mutex
  Mutex lock;
  // set time for timeout
  boost::system_time until = m_LastUpdate + m_Timeout;
  while(true){
    // lock the mutex
    Locker locker(lock);
    // calling wait releases the lock and blocks.
    bool notified = m_Condition.timed_wait(locker,until);
    RecursiveLocker memberLock(m_Lock);
    if(notified){
      // notified by another thread.
      until = m_LastUpdate + m_Timeout;
    } else {
      // need to lock for the loop
      if(boost::get_system_time() > m_LastUpdate + m_Timeout){
        // the time is realy after timeout. notify
        m_CurrentState = m_TimeoutState;
        notify(m_CurrentState);
      }
      if(m_CurrentState == m_TimeoutState){
        // the current state is timeout. we can sleep until notify (or al least for a long time)
        until = boost::get_system_time() + boost::posix_time::minutes(60);
      } else {
        // the current state is not timeout so we wait until timeout.
        until = m_LastUpdate + m_Timeout;
      }
    }
  }
  // this point is not reachable. The thread will be stopped by interrupt.
}

  namespace YAML{
  template<>
  struct convert<TimeoutSensor::TimeoutResetStrategy> {
    static bool decode(const Node& node, TimeoutSensor::TimeoutResetStrategy& rhs) {
      std::string reset = node.as<std::string>();
      if(reset == "on_update"){
        rhs = TimeoutSensor::TimeoutResetStrategy::on_update;
      } else if (reset == "on_change"){
        rhs = TimeoutSensor::TimeoutResetStrategy::on_change;
      } else {
        STHROW(ParsingException,"Unknown TimeOutSensor.reset value '" << reset << "' expected on_change or on_update.");
      }
      return true;
    }
  };
  }


static std::vector<Sensor::Ptr> createFromConfig(const YAML::Node sensor, SensorFactory& factory){
  YAML::Node config = sensor["config"];
  SASSERT_THROW(config["sensors"],ParsingException,"TimeOutSensor requires a sensor field.");
  SASSERT_THROW(config["timeout_millis"],ParsingException,"TimeOutSensor requires a timeout_millis field.");
  SASSERT_THROW(config["result"],ParsingException,"TimeOutSensor requires a result field.");
  SASSERT_THROW(config["reset"],ParsingException,"TimeOutSensor requires a reset field.");
  std::string timeoutState = config["result"].as<std::string>();
  TimeoutSensor::TimeoutResetStrategy reset = config["reset"].as<TimeoutSensor::TimeoutResetStrategy>();
  uint timeout = config["timeout_millis"].as<uint>();
  std::vector<Sensor::Ptr> sensors = factory.createSensors(config);
  std::vector<Sensor::Ptr> result;
  for(auto inner : sensors){
    result.push_back(Sensor::Ptr(new TimeoutSensor(inner,timeout,timeoutState,reset)));
    SLOG_INFO << "Created TimeOutSensor (" << timeout << "," << timeoutState << ","
              << config["reset"].as<std::string>() << ") on sensor " << inner->name();
  }
  return result;
}

// register at factory
static SensorFactory::Registrar registerDayTimeSensor("TimeOutSensor", createFromConfig);
