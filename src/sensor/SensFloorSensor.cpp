/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/SensFloorSensor.cpp                         **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <sensor/SensFloorSensor.h>
#include <utils/Exception.h>
#include <rsb/Factory.h>
#include <sensor/SensorFactory.h>
#include <yaml-cpp/yaml.h>
#include <utils/YamlUtils.h>
#include <utils/Macros.h>
#include <utils/Macros.h>
#include <io/RsbSource.h>
#include <boost/regex.h>
#include <openbase/type/device/sensfloor/FloorModuleState.pb.h>

#define _LOGGER "sensor.SensFloorSensor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

static const bool USE_SUBSCOPES = true;

using sitinf::sensor::SensFloorSensor;
using sitinf::sensor::Sensor;
using openbase::type::device::sensfloor::FloorModuleState;
typedef boost::shared_ptr<FloorModuleState> FloorModuleStatePtr;


namespace {
  bool checkActivation(FloorModuleStatePtr fms){
    for(auto segment : fms->segment()){
      if(segment.activation_segment()){
        return true;
      }
    }
    return false;
  }

  bool checkActivation(const SensFloorSensor::ActivationMap& map){
    for (auto row : map){
      for (auto column : row.second){
        if (column.second){
          return true;
        }
      }
    }
    return false;
  }
}

SensFloorSensor::SensFloorSensor(const std::string& name, const std::string& scope)
  : SimpleSensor(name, {activeState(), inactiveState()}, Sensor::unobserved())
{
  sitinf::io::register_rst<FloorModuleState>();
  m_Listener = rsb::getFactory().createListener(scope);
  m_Handler = rsb::HandlerPtr(
        new rsb::EventFunctionHandler([this] (rsb::EventPtr event) -> void {this -> handle(event);}));
  m_Listener->addHandler(m_Handler);
}

void SensFloorSensor::handle(rsb::EventPtr event){
  if(event->getType() != rsc::runtime::typeName<FloorModuleState>()) {
    SLOG_WARNING << "Received unexpected data type: '" << event->getType()
                 << "'. Expected: '" << rsc::runtime::typeName<FloorModuleState>();
  } else {
      FloorModuleStatePtr message = boost::static_pointer_cast<FloorModuleState>(event->getData());
      bool active = checkActivation(message);
      m_Activations[message->position_row()][message->position_col()] = active;
      SLOG_TRACE << "Received activation = " << active << " on "
                 << message->position_row() << "x" << message->position_col();
      if (active || checkActivation(m_Activations)) {
        SLOG_TRACE << "Setting " << this->name() << " to active (prev: " << this->currentState() << ").";
        SimpleSensor::changeCurrentState(activeState());
      } else {
        SLOG_TRACE << "Setting " << this->name() << " to inactive (prev: " << this->currentState() << ").";
        SimpleSensor::changeCurrentState(inactiveState());
      }
  }
}

SensFloorSensor::~SensFloorSensor(){
  m_Listener->removeHandler(m_Handler);
}

static std::vector<Sensor::Ptr> createFromConfig(const YAML::Node sensor, const sitinf::sensor::SensorFactory& factory){
  YAML::Node config = sensor["config"];
  //  name: "FloorKitchen"
  //  scope: /home/kitchen/device/

  auto name = sitinf::utils::yaml::read<std::string>(config,"name");
  auto scope = sitinf::utils::yaml::read<std::string>(config,"scope");

  std::vector<Sensor::Ptr> result;
  result.push_back(Sensor::Ptr(new SensFloorSensor(name,scope)));

  return result;
}

// register at factory
static sitinf::sensor::SensorFactory::Registrar registerSensFloorSensor("SensFloorSensor", createFromConfig);

