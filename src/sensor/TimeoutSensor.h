/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/TimeoutSensor.h                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <sensor/Sensor.h>
#include <utils/Types.h>

namespace sitinf {
  namespace sensor {

    /**
     * @brief The TimeoutSensor class represents a Sensor decorator that extends a Sensor with a timeout and
     * possibly a new state.
     *
     * To achieve this the TimeoutSensor registeres to its decorated Sensor and mirrores its output. If the
     * inner Sensor does not provide a state change for a specified time, the TimeoutSensor emits its timeout-state.
     */
    class TimeoutSensor : public Sensor, public utils::Observer<const std::string&> {

    public:

      /**
       * @brief The TimeoutResetStrategy enum
       */
      enum TimeoutResetStrategy{
        on_update, // reset the timeout counter on every update
        on_change // reset the timeout counter only on state changes.
      };

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<TimeoutSensor> Ptr;

      /**
       * @brief TimeoutSensor registers to its inner Sensor and mirrors its output. Emits timeout_state on timeout.
       *
       * @param sensor the sensor to register to
       * @param timeout_millis the time in milliseconds to wait before timeout
       * @param timeout_state the state to emit on timeout
       * @param reset when to reset the timeout counter
       */
      TimeoutSensor(Sensor::Ptr sensor, uint timeout_millis, std::string timeout_state, TimeoutResetStrategy reset=on_update);

      /**
       * @brief ~TimeoutSensor virtual destructor for nice cleanup.
       */
      virtual ~TimeoutSensor();

      /**
       * @brief states access to the possible states of this sensor
       * @return a vector containing all states
       */
      virtual const std::vector<std::string>& states() const;

      /**
       * @brief name acess to the name of this sensor/aspect.
       * @return the name of this aspect.
       */
      virtual std::string name() const;

      /**
       * @brief currentState access to the current state of this sensor
       * @return the current state of this sensor. may be __unobserved__
       */
      virtual std::string currentState() const;

      /**
       * @brief update function of the Observer interface.
       *
       * Gets new data from sensor updates m_LastUpdate and notifies
       * m_Condition.
       */
      virtual void update(const Subject* subject, const std::string& data);

    private:
      /**
       * @brief m_Lock used for synchronization.
       */
      mutable utils::RecursiveMutex m_Lock;
      /**
       * @brief m_Sensor the decorated sensor.
       */
      Sensor::Ptr m_Sensor;
      /**
       * @brief m_States the vector of possible states of this TimeoutSensor.
       *
       * This consists of the states of the inner Sensor and the TimeoutState (if not already existent)
       */
      std::vector<std::string> m_States;
      /**
       * @brief m_CurrentState the current state of this sensor may be an element of m_States or '__unobserved__'
       */
      std::string m_CurrentState;
      /**
       * @brief m_TimeoutState the state to emit on timeout.
       */
      std::string m_TimeoutState;
      /**
       * @brief m_Timeout the amount of milliseconds after which a timeout signal is raised.
       */
      utils::milliseconds m_Timeout;
      /**
       * @brief m_LastUpdate tells when the last update happened.
       */
      utils::system_time m_LastUpdate;
      /**
       * @brief m_Thread a locally created thread waiting for the timeout.
       */
      utils::Thread m_Thread;
      /**
       * @brief m_Condition is used so the thread does not need to poll but can sleep until timeout or signal.
       */
      boost::condition_variable m_Condition;
      /**
       * @brief m_ResetStrategy when to reset countdown.
       */
      const TimeoutResetStrategy m_ResetStrategy;

      /**
       * @brief waitForTimeout is used by the internally spawned thread.
       *
       * In this function the thread goes to sleep until m_LastUpdate + m_Timeout passes by.
       * This happens in a loop until m_LastUpdate + m_Timeout is really passed. If that happens
       * a notification is emitted with the m_TimeoutState as m_CurrentState.
       */
      void waitForTimeout();
    };

  } // namespace sensor
} // namespace sitinf
