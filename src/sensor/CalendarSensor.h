/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/sensor/CalendarSensor.h                            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <sensor/Sensor.h>
#include <rsb/Listener.h>
#include <rsb/patterns/RemoteServer.h>
#include <utils/Types.h>
#include <utils/NotifyThread.h>

namespace sitinf {
  namespace sensor {

    /**
     * @brief The CalendarSensor class represents a Sensor that shows occurances of a single
     * calendar event type for different periods of time.
     */
    class CalendarSensor : public Sensor {

    public:

      struct TimePeriod {
        std::string name = "";
        utils::uint64 time_millis = 0;
      };

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<CalendarSensor> Ptr;

      CalendarSensor(const std::string& name, const std::string& match_events_regex, const std::string& event_scope,
                     const std::vector<std::string>& update_scope, std::vector<TimePeriod> time_periods);

      /**
       * @brief ~CalendarSensor virtual destructor for nice cleanup.
       */
      virtual ~CalendarSensor();

      /**
       * @brief states access to the possible states of this sensor
       * @return a vector containing all states
       */
      virtual const std::vector<std::string>& states() const;

      /**
       * @brief name acess to the name of this sensor/aspect.
       * @return the name of this aspect.
       */
      virtual std::string name() const;

      /**
       * @brief currentState access to the current state of this sensor
       * @return the current state of this sensor. may be __unobserved__
       */
      virtual std::string currentState() const;

    private:
      /**
       * @brief m_Lock used for synchronization.
       */
      mutable utils::Mutex m_Lock;
      /**
       * @brief m_States the vector of possible states of this CalendarSensor.
       *
       * This will not change after construction.
       */
      std::vector<std::string> m_States;
      /**
       * @brief m_Name the name of this sensor/aspect.
       */
      const std::string m_Name;
      /**
       * @brief m_Regex the regular expression that matches event descriptions to this sensors.
       */
      const std::string m_Regex;
      /**
       * @brief m_Thread  the thread that resets this CalendarSensor after a specific time.
       */
      utils::NotifyThread m_Thread;
      /**
       * @brief m_CurrentState the current state of this sensor may be an element of m_States or '__unobserved__'
       */
      std::string m_CurrentState;

      /**
       * @brief m_TimePeriods a sorted list of the TimePeriods of this CalendarSensor.w
       */
      std::vector<TimePeriod> m_TimePeriods;
      /**
       * @brief m_UpdateListener is used to monitor remote calendar changes.
       */
      std::vector<rsb::ListenerPtr> m_UpdateListener;
      /**
       * @brief m_Calendar is used to get calendar events.
       */
      rsb::patterns::RemoteServerPtr m_CalendarServer;
      /**
       * @brief handle handler function receives events from m_Listener, executes queries and calls notify.
       *
       * This handle expects the event being from the right scope and type.
       *
       * @param event the event from the rsb listener.
       */
      void handle(rsb::EventPtr event);

      void resetState();

      void setState(const std::string& state);
    };

  } // namespace sensor
} // namespace sitinf
