/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/actor/ScreenActor.cpp                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <actor/ScreenActor.h>
#include <utils/Exception.h>
#include <rsb/Factory.h>
#include <sensor/Sensor.h>
#include <utils/Exception.h>
#include <utils/Macros.h>

#include <rsb/converter/ProtocolBufferConverter.h>
#include <rsb/converter/Repository.h>
#include <openbase/type/domotic/state/StandbyState.pb.h>

#include <future>

#define _LOGGER "actor.ScreenActor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::actor;
using namespace sitinf::utils;

using namespace openbase::type::domotic::state;

ScreenActor::ScreenActor(const std::string& scope){
  try {
    SLOG_TRACE << "scope:" << scope;
    boost::shared_ptr<rsb::converter::ProtocolBufferConverter<StandbyState> > converter(
      new rsb::converter::ProtocolBufferConverter<StandbyState>());
    rsb::converter::converterRepository<std::string>()->registerConverter(converter);
  } catch (const std::exception& e) {
    // already available do nothing
  }
  m_ScreenServer = rsb::getFactory().createRemoteServer(scope);
}

ScreenActor::~ScreenActor(){}

void ScreenActor::turn_standby_on(){
  boost::shared_ptr<StandbyState> state = boost::shared_ptr<StandbyState>(StandbyState::default_instance().New());
  state->set_value(StandbyState_State_RUNNING);
  SLOG_TRACE << "Turning on remote Screen.";
  std::thread thread([this,state] {
  STRY_PRINT(m_ScreenServer->call<std::string>("setStandby",state),
             "Error turning off screen on " << m_ScreenServer->getScope()->toString());
  });
  thread.detach();
}


void ScreenActor::turn_standby_off(){
  boost::shared_ptr<StandbyState> state = boost::shared_ptr<StandbyState>(StandbyState::default_instance().New());
  state->set_value(StandbyState_State_STANDBY);
  SLOG_TRACE << "Turning off remote Screen.";
  std::thread thread([this,state] {
  STRY_PRINT(m_ScreenServer->call<std::string>("setStandby",state),
             "Error turning off screen on " << m_ScreenServer->getScope()->toString());
  });
  thread.detach();
}

ScreenSaver::ScreenSaver(const std::string& scope)
  : ScreenActor(scope)
{}

void ScreenSaver::update(const utils::Subject<const std::string&>* subject, const std::string& data){
    const sensor::Sensor* sensor = dynamic_cast<const sensor::Sensor*>(subject);
    std::string text = (sensor) ? sensor->name() + std::string(" is ") + data : data;
    if(data == "Idle"){
      SLOG_TRACE << text << ". Turning off.";
      this->turn_standby_off();
    } else {
      SLOG_TRACE << text << ". turning on.";
      this->turn_standby_on();
    }
}
