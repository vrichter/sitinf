/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/actor/SpeakerActor.cpp                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <actor/SpeakerActor.h>
#include <utils/Exception.h>
#include <rsb/Factory.h>
#include <sensor/Sensor.h>
#include <utils/Exception.h>
#include <utils/Macros.h>

#include <rsb/converter/ProtocolBufferConverter.h>
#include <rsb/converter/Repository.h>

#define _LOGGER "actor.SpeakerActor"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::actor;
using namespace sitinf::utils;


SpeakerKeepAlive::SpeakerKeepAlive(const std::string& scope, const std::string& wake_up_state,
                                   uint repeat_after_millisecods)
  : SpeakerActor(), m_WakeUpState(wake_up_state), m_RepeatMillis(repeat_after_millisecods),
    m_Notify([this] {this->wake_up_call();})
{
  m_Informer = rsb::getFactory().createInformer<std::string>(scope);
}

void SpeakerKeepAlive::update(const utils::Subject<const std::string&>* subject, const std::string& data){
    const sensor::Sensor* sensor = dynamic_cast<const sensor::Sensor*>(subject);
    std::string text = (sensor) ? sensor->name() + std::string(" is ") + data : data;
    if(data != m_WakeUpState){
      SLOG_TRACE << text << ". Turning speaker wake-up off.";
      m_Notify.stop();
    } else {
      SLOG_TRACE << text << ". Turning speaker wake-up on.";
      wake_up_call();
    }
}

void SpeakerKeepAlive::wake_up_call(){
  SLOG_TRACE << "Wake up speakers!";
  static auto data = boost::shared_ptr<std::string>(new std::string("Wake up!"));
  m_Informer->publish(data);
  if(m_RepeatMillis){
    m_Notify.call_in(m_RepeatMillis);
  }
}
