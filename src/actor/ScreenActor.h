/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/actor/ScreenActor.h                                **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Subject.h>
#include <rsb/Factory.h>
#include <rsb/patterns/RemoteServer.h>

namespace sitinf {
  namespace actor {

    /**
     * @brief The ScreenActor class can control a screen unit via rsb.
     */
    class ScreenActor {

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<ScreenActor> Ptr;


      ScreenActor(const std::string& scope);

      /**
       * @brief ~ScreenActor virtual destructor for nice cleanup.
       */
      virtual ~ScreenActor();

      virtual void turn_standby_on();
      virtual void turn_standby_off();


    private:
      /**
       * @brief m_Lock used for synchronization.
       */
      mutable utils::RecursiveMutex m_Lock;

      rsb::patterns::RemoteServerPtr m_ScreenServer;
    };

    class ScreenSaver : public ScreenActor, public utils::Observer<const std::string&> {

    public:
      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<ScreenSaver> Ptr;

      ScreenSaver(const std::string& scope);

      virtual void update(const utils::Subject<const std::string&>* subject, const std::string& data) override;

    };

  } // namespace actor
} // namespace sitinf
