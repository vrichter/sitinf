/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/actor/SpeakerActor.h                               **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Subject.h>
#include <utils/NotifyThread.h>
#include <rsb/Factory.h>
#include <rsb/Informer.h>

namespace sitinf {
  namespace actor {

    /**
     * @brief The SpeakerActor class can control a speaker via rsb.
     */
    class SpeakerActor {

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<SpeakerActor> Ptr;

    };

    /**
     * @brief The SpeakerKeepAlive class sends wake-up signals to speakers.
     */
    class SpeakerKeepAlive : public SpeakerActor, public utils::Observer<const std::string&> {

    public:
      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<SpeakerKeepAlive> Ptr;

      SpeakerKeepAlive(const std::string& scope, const std::string& wake_up_state, uint repeat_after_millisecods);

      virtual void update(const utils::Subject<const std::string&>* subject, const std::string& data) override;

    private:

      void wake_up_call();

      utils::Mutex m_Lock;
      std::string m_WakeUpState;
      utils::uint m_RepeatMillis;
      utils::NotifyThread m_Notify;
      rsb::Informer<std::string>::Ptr m_Informer;

    };

  } // namespace actor
} // namespace sitinf
