/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/network/StateEvidenceCollector.cpp                 **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <network/StateEvidenceCollector.h>
#include <utils/Types.h>
#include <utils/Exception.h>
#include <utils/Macros.h>
#include <sensor/Sensor.h>

#define _LOGGER "network.StateEvidenceCollector"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::sensor;
using namespace sitinf::utils;
using namespace sitinf::network;

StateEvidenceCollector::StateEvidenceCollector(const std::vector<sensor::Sensor::Ptr>& sensors){
  for (auto sensor : sensors) {
    Sensor* ptr = sensor.get();
    if(m_SensorMap.find(ptr) != m_SensorMap.end()) {
      SLOG_WARNING << "Sensor '" << sensor->name() << "' provided multiple times. Will be added only once.";
    } else {
      m_SensorMap[ptr] = sensor;
      if(ptr->currentState() != Sensor::unobserved()){
        m_CurrentEvidence.newEvidence(ptr->name(),ptr->currentState());
      }
      sensor->addObserver(this);
    }
  }
}

StateEvidenceCollector::Ptr StateEvidenceCollector::New(const std::vector<sensor::Sensor::Ptr>& sensors){
  return StateEvidenceCollector::Ptr(new StateEvidenceCollector(sensors));
}

StateEvidenceCollector::~StateEvidenceCollector(){
  for(auto sensor : m_SensorMap) {
    sensor.second->removeObserver(this);
  }
}

void StateEvidenceCollector::update(const Subject<const std::string&>* subject, const std::string& data){
  if(m_SensorMap.find((sensor::Sensor*) subject) != m_SensorMap.end()) {
    m_CurrentEvidence.newEvidence(((sensor::Sensor*) subject)->name(),data);
    notify(m_CurrentEvidence);
  } else {
    SLOG_WARNING << "Ignoring update from unknown subject.";
  }
}

const AspectStateEvidence& StateEvidenceCollector::current() {
  return m_CurrentEvidence;
}
