/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/network/BayesianNetwork.cpp                        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <network/BayesianNetwork.h>
#include <sensor/Sensor.h>
#include <utils/Exception.h>
#include <utils/Utils.h>
#include <utils/Macros.h>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/variate_generator.hpp>

#define _LOGGER "network.BayesianNetwork"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::network;

BayesianNetwork::Variable::Variable() : m_Type(nature), m_Name(""), m_Property("") {}

BayesianNetwork::Variable::Type BayesianNetwork::Variable::toType(std::string type){
  if(type == "nature"){
      return nature;
    } else if(type == "decision"){
      return decision;
    } else if(type == "utility"){
      return utility;
    } else{
      STHROW(UnexpectedException,"BayesianNetwork::Variable::Type '" << type << "' does not exist.");
    }
}
BayesianNetwork::Variable::Type BayesianNetwork::Variable::toType(rst::bayesnetwork::BayesVariable_Type type){
  switch(type){
    case rst::bayesnetwork::BayesVariable_Type_DECISION: return decision;
    case rst::bayesnetwork::BayesVariable_Type_NATURE: return nature;
    case rst::bayesnetwork::BayesVariable_Type_UTILITY: return utility;
    default: STHROW(UnexpectedException,"BayesianNetwork::Variable::Type '" << type << "' does not exist.");
  }
}

std::string BayesianNetwork::Variable::toString(Type t){
  switch(t){
    case nature: return "nature";
    case decision: return "decision";
    case utility: return "utility";
    default: STHROW(UnexpectedException,"BayesianNetwork::Variable::Type '" << t << "' does not exist.");
    }
}

int BayesianNetwork::Variable::outcome(std::string name) const{
  if(name == ""){
    return -1;
  }
  for(uint i = 0; i < m_Outcomes.size(); ++i){
    if (m_Outcomes[i] == name){
      return i;
    }
  }
  std::stringstream outcomes;
  outcomes << "|";
  for(auto outcome : m_Outcomes){
    outcomes << " " << outcome << " |";
  }
  outcomes << ")";
  STHROW(NotFoundException,"Outcome with name " << name << " in variable " << m_Name << " not available."
               << "\n\tAvailable outcomes are " << outcomes.str());
}

std::string BayesianNetwork::Variable::outcome(int id) const{
  if(id == -1){
    return "";
  } else if(id >= (int) m_Outcomes.size()){
    STHROW(NotFoundException,"Outcome with id " << id << " in variable " << m_Name << " not available.");
  } else {
    return m_Outcomes.at(id);
  }
}


bool BayesianNetwork::Variable::operator !=(const BayesianNetwork::Variable &other) const{
  if(m_Outcomes.size() != other.m_Outcomes.size()) return true;
  if(m_Type != other.m_Type) return true;
  if(m_Name != other.m_Name) return true;
  if(m_Property != m_Property) return true;
  try{
    for(auto outcome : m_Outcomes){
      other.outcome(outcome);
    }
  } catch (NotFoundException& e) {
    // does not have the corresponding outcome
    return true;
  }
  return false;
}

bool BayesianNetwork::Variable::operator ==(const BayesianNetwork::Variable &other) const{
  return !(operator !=(other));
}

BayesianNetwork::Definition::Definition() : m_Property("") {}

bool BayesianNetwork::Definition::operator !=(const BayesianNetwork::Definition &other) const{
  if(m_Table.size() != other.m_Table.size()) return true;
  if(m_Variables.size() != other.m_Variables.size()) return true;
  for(uint i = 0; i < m_Table.size(); ++i){
    if(m_Table[i] != other.m_Table[i]) return true;
  }
  if(m_Property != other.m_Property) return true;
  for(uint i = 0; i < m_Variables.size(); ++i){
    if(m_Variables[i] != other.m_Variables[i]) return true;
  }
  return false;
}

bool BayesianNetwork::Definition::operator ==(const BayesianNetwork::Definition&other) const{
  return !(operator !=(other));
}

BayesianNetwork::BayesianNetwork(const std::vector<Variable>& variables,
                                 const std::vector<Definition>& definitions,
                                 const std::string name, const std::string comment)
  : m_Name(name), m_Comment(comment), m_Variables(variables), m_Definitions(definitions)
{
  validate();
}


BayesianNetwork::BayesianNetwork(const Message& message)
 : m_Name(""), m_Comment("")
{
  // set name and comment
  m_Name = (message.has_name()) ? message.name() : "unknown";
  for(int i = 0; i < message.property_size(); ++i){
    m_Comment += message.property(i) + std::string(";");
  }

  // set variables and definitions from messages variables
  for(int i = 0; i < message.variable_size(); ++i){
    // variable data
    const rst::bayesnetwork::BayesVariable& messageVariable = message.variable(i);
    Variable variable;
    variable.m_Name = (messageVariable.has_name()) ? messageVariable.name() : "unknown";
    variable.m_Type = (messageVariable.has_type()) ? Variable::toType(messageVariable.type()) : Variable::nature;
    variable.m_Property = "";
    for(int j = 0; j < messageVariable.property_size(); ++j){
      variable.m_Property += messageVariable.property(j) + std::string(";");
    }
    variable.m_Outcomes.reserve(messageVariable.outcomes_size());
    for(int j = 0; j < messageVariable.outcomes_size(); ++j){
      variable.m_Outcomes.push_back(messageVariable.outcomes(j));
    }
    // definition data
    Definition definition;
    definition.m_Property = variable.m_Property;
    definition.m_Variables.reserve(messageVariable.parents_size()+1);
    // m_Variables starts with the variables name followed by the parents
    definition.m_Variables.push_back(variable.m_Name);
    for(int j = 0; j < messageVariable.parents_size(); ++j){
      definition.m_Variables.push_back(messageVariable.parents(j));
    }
    definition.m_Table.reserve(messageVariable.probabilities_size());
    for(int j = 0; j < messageVariable.probabilities_size(); ++j){
      definition.m_Table.push_back(messageVariable.probabilities(j));
    }

    // add both variable and definition
    m_Variables.push_back(variable);
    m_Definitions.push_back(definition);
  }
  // validate
  validate();
}

void BayesianNetwork::validate(){
   // check for size consistency
  if(m_Definitions.size() != m_Variables.size()){
      STHROW(NetworkInconsistentException,"BayesianNetwork: Variable count != Definition count");
    }

  // add variables to map and check for equal names
  for(uint i = 0; i < m_Variables.size(); ++i){
      std::map<std::string,uint>::iterator it = m_VarIdMap.find(m_Variables[i].m_Name);
      if(it != m_VarIdMap.end()){
        STHROW(NetworkInconsistentException,"Two Variables must not have the same name. v_"
                     << it->second
                     << ".name = v_"
                     << i
                     << ".name = "
                     << m_Variables[i].m_Name);
      } else {
          m_VarIdMap[m_Variables[i].m_Name] = i;
        }
    }

  // check for definition consistency
  for(uint i = 0; i < m_Definitions.size(); ++i){
      std::vector<uint> dims = variableDimensions(m_Definitions[i].m_Variables);
      if(utils::prod(dims) != m_Definitions[i].m_Table.size()){
          STHROW(NetworkInconsistentException,"Unexpected CPT size in definition of Variable '"
                                             << variableName(i)
                                             << "'. (" << utils::prod(dims)
                                             << " != " << m_Definitions[i].m_Table.size()
                                             << ")");
        }
    }
}

void BayesianNetwork::rename(std::string name){
  m_Name = name;
}

void BayesianNetwork::changeComment(std::string comment){
  m_Comment = comment;
}

std::string BayesianNetwork::name() const{
  return m_Name;
}

std::string BayesianNetwork::comment() const{
  return m_Comment;
}

uint BayesianNetwork::size() const{
  return m_Definitions.size();
}


const BayesianNetwork::Variable& BayesianNetwork::variable(uint id) const{
  if(id >= m_Variables.size()) STHROW(NotFoundException,"No Variable with id" << id);
  return m_Variables.at(id);
}

const BayesianNetwork::Variable& BayesianNetwork::variable(std::string name) const{
  // return from variable id
  return m_Variables.at(variableId(name));
}

const std::vector<BayesianNetwork::Variable>& BayesianNetwork::variables() const{
  return m_Variables;
}

const BayesianNetwork::Definition& BayesianNetwork::definition(uint id) const{
  if(id >= m_Definitions.size()) STHROW(NotFoundException,"No Definition with id" << id);
  return m_Definitions.at(id);
}

const std::vector<BayesianNetwork::Definition>& BayesianNetwork::definitions() const{
  return m_Definitions;
}

std::string BayesianNetwork::variableName(const uint id) const{
  if(id < m_Variables.size()){
      // the variable exists
      return m_Variables.at(id).m_Name;
    } else {
      STHROW(NotFoundException,"The BayesNetwork does not contain Variable nr '" << id << "'");
    }
}

uint BayesianNetwork::variableId(const std::string& variable_name) const{
  // look for variable in id-map
  std::map<std::string,uint>::const_iterator it = m_VarIdMap.find(variable_name);
  if(it != m_VarIdMap.end()){
      // the variable exists
      return it->second;
    } else {
      STHROW(NotFoundException,"The BayesNetwork does not contain a Variable named '" << variable_name << "'");
    }
}

std::vector<uint> BayesianNetwork::variableIds(const std::vector<std::string>& variable_names) const{
  // create the index-vector which will be returned
  std::vector<uint> ret(variable_names.size());
  for(uint i = 0; i < variable_names.size(); ++i){
      ret[i] = variableId(variable_names[i]);
    }
  return ret;
}

uint BayesianNetwork::variableDimension(const std::string& variable_name) const{
  // get variable id
  uint id = variableId(variable_name);
  return m_Variables.at(id).m_Outcomes.size();
}

std::vector<uint> BayesianNetwork::variableDimensions(const std::vector<std::string>& variable_names) const{
  // create the index-vector which will be returned
  std::vector<uint> ret(variable_names.size());
  for(uint i = 0; i < variable_names.size(); ++i){
      ret[i] = variableDimension(variable_names[i]);
    }
  return ret;
}

boost::shared_ptr<rst::bayesnetwork::BayesNetwork> BayesianNetwork::asRstMessage() const {
  // create the mesage instance
  boost::shared_ptr<rst::bayesnetwork::BayesNetwork> ret(new rst::bayesnetwork::BayesNetwork());

  // set the Networks name and comment
  ret->set_name(name());
  *ret->add_property() = comment();

  // the variables and definitions
  const std::vector<BayesianNetwork::Variable>& variables = this->variables();
  const std::vector<BayesianNetwork::Definition>& definitions = this->definitions();

  for(uint i = 0; i < variables.size(); ++i){
    // fill the message with BayesVariables representing variables and definitions
    const BayesianNetwork::Variable& variable = variables[i];
    const BayesianNetwork::Definition& definition = definitions[i];
    // create representation
    rst::bayesnetwork::BayesVariable& resultVariable = *ret->add_variable();
    // set type
    resultVariable.set_type(rst::bayesnetwork::BayesVariable_Type_NATURE);
    // set properties
    if(!variable.m_Property.empty()) *resultVariable.add_property() = variable.m_Property;
    if(!definition.m_Property.empty()) *resultVariable.add_property() = definition.m_Property;
    // set name
    resultVariable.set_name(variable.m_Name);
    // set outcomes
    for(uint j = 0; j < variable.m_Outcomes.size(); ++j){
      *resultVariable.add_outcomes() = variable.m_Outcomes[j];
    }
    // set parents. first variable is this so it should be ignored.
    for(uint j = 1; j < definition.m_Variables.size(); ++j){
      *resultVariable.add_parents() = definition.m_Variables[j];
    }
    // set cpt
    for(uint j = 0; j < definition.m_Table.size(); ++j){
      resultVariable.add_probabilities(definition.m_Table[j]);
    }
  }
  return ret;
}


bool BayesianNetwork::knows(const std::string& variable_id, const std::string& outcome) const {
  auto variable = m_VarIdMap.find(variable_id);
  if (variable == m_VarIdMap.end()) return false;
  for(auto variableOutcome : m_Variables.at(variable->second).m_Outcomes){
    if(variableOutcome == outcome){
      return true;
    }
  }
  return false;
}

BayesianNetwork::Ptr Converter<BayesianNetwork::Ptr,Shared<rst::bayesnetwork::BayesNetwork>::Ptr>::
convert(Shared<rst::bayesnetwork::BayesNetwork>::Ptr data) {
  return BayesianNetwork::Ptr(new BayesianNetwork(*data));
}

Shared<rst::bayesnetwork::BayesNetwork>::Ptr Converter<Shared<rst::bayesnetwork::BayesNetwork>::Ptr,BayesianNetwork::Ptr>::
convert(BayesianNetwork::Ptr data){
  return data->asRstMessage();
}

BayesianNetwork BayesianNetwork::uniformDistributed(real variance, uint seed) const {
  boost::mt19937 rng(seed);
  boost::uniform_01<boost::mt19937> uniform(rng);

  BayesianNetwork result = *this;
  for (BayesianNetwork::Definition& definition : result.m_Definitions){
    uint dim = result.variable(definition.m_Variables.front()).m_Outcomes.size();
    // set to uniform + uniform noise
    for(uint i = 0; i < definition.m_Table.size(); ++i){
      definition.m_Table.at(i) = 1 + (variance * uniform()) * dim;
    }
    // normalize
    real sum = 0.;
    for(uint i = 0; i < definition.m_Table.size(); ++i){
      sum += definition.m_Table.at(i);
      if(((i+1) % dim) == 0){
        for (uint j = 0; j < dim; ++j){
          definition.m_Table.at(i-j) /= (real) sum;
        }
        sum = 0.;
      }
    }

  }
  return result;
}


void fillApplicableState(const AspectStateEvidence* src, AspectStateEvidence* dst, BayesianNetwork* network){
  for(const auto& aspectState : src->currentState()){
    try {
      const BayesianNetwork::Variable& var = network->variable(aspectState.first);
      if(var.outcome(aspectState.second) == -1){
        STHROW(NotFoundException,"Empty state.");
      }
      dst->newEvidence(var.m_Name,aspectState.second);
    } catch (NotFoundException& ex){
      // ignore. variable or state is not known
      SLOG_TRACE << "Variable: " << aspectState.first << " or state "
                 << aspectState.second << " not known. Error: " << ex.what();
    }
  }
}

AspectStateEvidence BayesianNetwork::applicableEvidence(const AspectStateEvidence& e){
  AspectStateEvidence result;
  fillApplicableState(&e,&result,this);
  return result;
}

WeightedAspectStateEvidence BayesianNetwork::applicableEvidence(const WeightedAspectStateEvidence& e){
  WeightedAspectStateEvidence result(e.weight());
  fillApplicableState(&e,&result,this);
  return result;
}

bool BayesianNetwork::operator !=(const BayesianNetwork &other) const{
  if(m_Variables.size() != other.m_Variables.size()) return true;
  if(m_Definitions.size() != other.m_Definitions.size()) return true;
  if(m_Name != other.m_Name) return true;
  if(m_Comment != other.m_Comment) return true;
  try{
    for(uint i = 0; i < m_Variables.size(); ++i){
      if(m_Variables[i] != other.variable(m_Variables[i].m_Name)) return true;
    }
    for(uint i = 0; i < m_Definitions.size(); ++i){
      if(m_Definitions[i] != other.definition(other.variableId(m_Definitions[i].m_Variables[0]))) return true;
    }
  } catch (NotFoundException& e){
    return true;
  }
  return false;
}

bool BayesianNetwork::operator ==(const BayesianNetwork &other) const{
  return !(operator !=(other));
}
