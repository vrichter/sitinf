/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/network/BayesianNetworkStateEvidence.h             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <network/BayesianNetwork.h>
#include <network/AspectStateEvidence.h>
#include <utils/Types.h>
#include <sensor/Sensor.h>

namespace sitinf {
namespace network {

  /**
   * This class holds the state evidence of a network plus a BayesianNetwork for validation.
   * It registers directly to Sensors to get state updates.
   */
  class BayesianNetworkStateEvidence : public AspectStateEvidence, public utils::Observer<const std::string&>, public utils::Subject<const std::map<std::string,std::string>&> {
  public:

    /**
     * @brief Ptr a shared pointer to this
     */
    typedef boost::shared_ptr<BayesianNetworkStateEvidence> Ptr;

    /**
     * @brief BayesianNetworkStateEvidence creates an empty state evidence. Validates the sensors and registers to them.
     * @param network the BayesianNetwork used for Sensor validation.
     * @param sensors the sensors to register to for updates
     */
    BayesianNetworkStateEvidence(const BayesianNetwork::Ptr network, const std::vector<sensor::Sensor::Ptr>& sensors);

    /**
     * @brief update called by Sensors to notify state changes.
     * @param subject which provides new data
     * @param data the new data from subject
     */
    virtual void update(const utils::Subject<const std::string&>* subject, const std::string& data) override;

  private:
    /**
     * @brief m_Network used to validate input data.
     */
    BayesianNetwork::Ptr m_Network;
    /**
     * @brief m_Sensors used to ensure that updates are only accepted from Sensors.
     */
    std::set<const void*> m_Sensors;
  };

} // namespace network
} // namespace sitinf
