/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/network/BayesianNetworkStateEvidence.cpp           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <network/BayesianNetworkStateEvidence.h>
#include <utils/Exception.h>
#include <utils/Macros.h>

#define _LOGGER "network.BayesianNetworkStateEvidence"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::sensor;
using namespace sitinf::utils;
using namespace sitinf::network;

BayesianNetworkStateEvidence::BayesianNetworkStateEvidence(const BayesianNetwork::Ptr network,
                                                           const std::vector<Sensor::Ptr>& sensors)
  : m_Network(network)
{
  // check all sensors
  try {
    for(auto sensor : sensors){
      uint variableId = network->variableId(sensor->name()); // may throw
      for(const std::string& state : sensor->states()){
        SASSERT_THROW(network->variable(variableId).outcome(state) >= 0,
                            Exception,"Sensor '" << sensor->name() << "' provides a state '"
                            << state << "' with negative outcome.");
      }
    }
  } catch (const Exception& e){
    STHROW(ConfigurationException, "Network and sensors are incompatible." << e.what());
  }
  // register to all sensors
  for(auto sensor : sensors) {
    sensor->addObserver(this);
    m_Sensors.insert((const void*) sensor.get());
  }
}

void BayesianNetworkStateEvidence::update(const Subject<const std::string&>* subject, const std::string& data){
  // shis should not happen
  SASSERT_THROW(m_Sensors.find(subject) != m_Sensors.end(),
                      UnexpectedException,
                      "Update called with a subject that is none of the known sensors.");
  // this really should not happen
  const Sensor* sensor = (const Sensor*) subject;
  SASSERT_THROW(m_Network->knows(sensor->name(),sensor->currentState()),
                      UnexpectedException,
                      "Sensor name '" << sensor->name() << "' or state '" << sensor->currentState()
                      << "' not known by network");
  // update state and inform observers
  newEvidence(sensor->name(),data);
  if(observerCount()){
    std::map<std::string,std::string> state = currentState();
    notify(state);
  }
}
