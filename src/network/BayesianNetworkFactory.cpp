/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/network/BayesianNetworkFactory.cpp                 **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <network/BayesianNetworkFactory.h>
#include <utils/Exception.h>
#include <utils/Utils.h>
#include <utils/Macros.h>
#include <utils/Yavl.h>
#include <io/XmlBifIO.h>
#include <io/File.h>
#include <render/GraphLayout.h>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/variate_generator.hpp>

#include <yaml-cpp/yaml.h>

#define _LOGGER "network.BayesianNetworkFactory"
#define _ELPP_STL_LOGGING 1
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::io;
using namespace sitinf::network;

const std::string YAML_GRAMMAR_NETWORK =  "map:\n"
                                          "  name: [string: ]\n"
                                          "  comment: [string: ]\n"
                                          "  nodes:\n"
                                          "    list:\n"
                                          "      map:\n"
                                          "        name: [string: ]\n"
                                          "        outcomes:\n"
                                          "          list: [string: ]\n"
                                          "  connections:\n"
                                          "    list:\n"
                                          "      map:\n"
                                          "        from:\n"
                                          "          list: [string: ]\n"
                                          "        to:\n"
                                          "          list: [string: ]\n";

BayesianNetwork BayesianNetworkFactory::fromFile(const std::string& filename){
  std::vector<std::string> split = utils::split_parse<std::string>(filename,".");
  SASSERT_THROW(split.size() > 1,NotFoundException ,"Cannot find postfix in file name: " << filename);
  ReadFile f(filename);
  if(split.back() == "xml"){
    return fromXml(f);
  } else if (split.back() == "yaml"){
    return fromYaml(f);
  } else {
    // fuck it. just try
    std::vector<std::pair<std::string,std::string> > exceptions;
    try{
      ReadFile xf(filename);
      return fromXml(xf);
    } catch (Exception& e) {
      exceptions.push_back(std::pair<std::string,std::string>("xml",e.what()));
    }
    try{
      ReadFile yf(filename);
      return fromYaml(yf);
    } catch (Exception& e) {
      exceptions.push_back(std::pair<std::string,std::string>("yaml",e.what()));
    }
    std::stringstream errors;
    for (auto e : exceptions){
      errors << e.first << ": " << e.second << "\n";
    }
    STHROW(NotFoundException, "Could not parse network from file. Neither as xml nor as yaml."
           << "\n" << errors.str());
  }
}


BayesianNetwork BayesianNetworkFactory::fromXml(std::istream& data){
  return BayesianNetwork(*XmlBifIO::readStream(data));
}
BayesianNetwork::Definition& BayesianNetworkFactory::equalizeDefinition(
    BayesianNetwork::Definition &definition,
    uint variable_dim,
    utils::real noise_max,
    utils::uint seed)
{
  boost::mt19937 rng(seed);
  boost::uniform_01<boost::mt19937> uniform(rng);

  // set to uniform + uniform noise
  for(uint i = 0; i < definition.m_Table.size(); ++i){
    definition.m_Table.at(i) = 1 + (noise_max * uniform()) * variable_dim;
  }
  // normalize
  real sum = 0.;
  for(uint i = 0; i < definition.m_Table.size(); ++i){
    sum += definition.m_Table.at(i);
    if(((i+1) % variable_dim) == 0){
      for (uint j = 0; j < variable_dim; ++j){
        definition.m_Table.at(i-j) /= (real) sum;
      }
      sum = 0.;
    }
  }
  return definition;
}

BayesianNetwork BayesianNetworkFactory::fromNetwork(const BayesianNetwork& source,
                                                    bool equalize,
                                                    utils::real noise_max,
                                                    utils::uint seed)
{
  std::vector<BayesianNetwork::Variable> resultVariables = source.variables();
  std::vector<BayesianNetwork::Definition> resultDefinitions = source.definitions();
  for (BayesianNetwork::Definition& definition : resultDefinitions){
    uint dim = source.variable(definition.m_Variables.front()).m_Outcomes.size();
    equalizeDefinition(definition,dim,noise_max,++seed);
  }
  return BayesianNetwork(resultVariables,resultDefinitions,source.name(),source.comment());
}

namespace YAML {
  template<>
  struct convert<BayesianNetwork::Variable> {
    static Node encode(const BayesianNetwork::Variable& rhs) {
      Node node;
      node["outcomes"] = rhs.m_Outcomes;
      if(rhs.m_Property != "") {
        node["property"] = rhs.m_Property;
      }
      if(rhs.m_Type != BayesianNetwork::Variable::nature) {
        node["type"] = rhs.toString(rhs.m_Type);
      }
      node["name"] = rhs.m_Name;
      return node;
    }

    static bool decode(const Node& node, BayesianNetwork::Variable& rhs) {
      if(!node.IsMap()) {
        return false;
      }

      SASSERT_RETURN_FALSE(node["name"] && node["name"].IsScalar());
      rhs.m_Name = node["name"].as<std::string>();

      SASSERT_RETURN_FALSE(node["outcomes"] && node["outcomes"].IsSequence());
      rhs.m_Outcomes = node["outcomes"].as<std::vector<std::string> >();
      if(node["type"]) {
        rhs.m_Type = BayesianNetwork::Variable::toType(node["type"].as<std::string>());
      }
      if(node["property"]) {
        rhs.m_Property = node["property"].as<std::string>();
      }
      return true;
    }

  };
}

struct CompatibleParentConfigElement {
  std::vector<std::string> compatible_config;
  std::vector<real> cpt;
};

struct NodeConfig {
  std::string name;
  std::string property;
  std::vector<std::string> outcomes;
  std::vector<std::string> parents;
  std::vector<real> parent_weights;
  std::vector<real> cpt;
  std::vector<CompatibleParentConfigElement> cpc;

  void init(){
    m_OutcomeIdMap.clear();
    for (uint i = 0; i < outcomes.size(); ++i){
      m_OutcomeIdMap[outcomes[i]] = i;
    }
    m_CpcMap.clear();
    for (uint i = 0; i < cpc.size(); ++i){
      for( auto conf : cpc[i].compatible_config ){
        m_CpcMap[conf] = cpc[i].cpt;
      }
    }
  }

  uint outcomeId(const std::string& outcome) const {
    auto it = m_OutcomeIdMap.find(outcome);
    SASSERT_THROW(it != m_OutcomeIdMap.end(), NotFoundException, "Variable '" << name
                  << "' does not have an outcome = '" << outcome << "'")
    return it->second;
  }

  const std::vector<real>& find_cpc(const std::string& parent_config) const {
    auto it = m_CpcMap.find(parent_config);
    SASSERT_THROW(it != m_CpcMap.end(), NotFoundException, "Compatible parental configuration for node '" << name
                  << "' and parent = '" << parent_config << "' is not specified.")
    return it->second;
  }

private:
  std::map<std::string,uint> m_OutcomeIdMap;
  std::map<std::string,std::vector<real> > m_CpcMap;
};

class NetworkConfig {
  std::map<std::string,NodeConfig> m_Nodes;

public:
  NetworkConfig(const std::vector<NodeConfig>& nodes){
    for(auto node : nodes){
      m_Nodes[node.name] = node;
    }
  }

  const std::map<std::string,NodeConfig>& nodes() const {
    return m_Nodes;
  }

  const NodeConfig& node(std::string name) const {
    auto it = m_Nodes.find(name);
    if(it != m_Nodes.end()){
      return it->second;
    }
    STHROW(NotFoundException,"Node with name '" << name << "' not found.");
  }

  uint dim(std::string variable) const {
    return node(variable).outcomes.size();
  }

  uint dim(std::vector<std::string> variables) const {
    uint result = 1;
    for (auto v : variables){
      result *= dim(v);
    }
    return result;
  }

};

class StateConfiguration {
private:
  std::vector<NodeConfig> m_Nodes;
  std::vector<uint> m_State;

public:
  StateConfiguration(const NodeConfig& node, const NetworkConfig& network)
  {
    m_Nodes.reserve(node.parents.size()+1);
    for(auto parent : node.parents){
      m_Nodes.push_back(network.node(parent));
    }
    m_Nodes.push_back(node);
    m_State.reserve(m_Nodes.size());
    m_State.insert(m_State.begin(),m_Nodes.size(),0);
  }

  std::vector<std::string> configuration(){
    SASSERT_THROW(valid(),StateException,"Cannot create configuration of invalid state.");
    std::vector<std::string> result;
    result.resize(m_State.size());
    for(uint i = 0; i < m_State.size()-1; ++i){
      result[i] = m_Nodes[i].name + "=" + m_Nodes[i].outcomes[m_State[i]];
    }
    result.back() = m_Nodes.back().outcomes[m_State.back()];
    return result;
  }

  bool valid(){
    for(uint i = 0; i < m_State.size(); ++i){
      if(m_Nodes[i].outcomes.size() <= m_State[i]){
        return false;
      }
    }
    return true;
  }

  StateConfiguration operator++()
  {
    recursive_increment(m_State.size()-1);
    return *this;
  }

private:
  void recursive_increment(int position){
    ++m_State[position];
    if(position == 0 || m_State[position] < m_Nodes[position].outcomes.size()){
      return;
    } else {
      m_State[position] = 0;
      recursive_increment(position-1);
    }
  }
};

namespace YAML {
  template<>
  struct convert<CompatibleParentConfigElement> {
    static Node encode(const CompatibleParentConfigElement& rhs) {
      Node node;
      node.push_back(rhs.compatible_config);
      node.push_back(rhs.cpt);
      return node;
    }

    static bool decode(const Node& node, CompatibleParentConfigElement& rhs) {
      if(! (node.IsSequence() && node.size() == 2)) return false;
      rhs.compatible_config = node[0].as<std::vector<std::string> >();
      rhs.cpt               = node[1].as<std::vector<real> >();
      return true;
    }

  };
}

namespace YAML {
  template<>
  struct convert<NodeConfig> {
    static Node encode(const NodeConfig& rhs) {
      Node node;
      if(rhs.name != "")               node["name"]     = rhs.name;
      if(rhs.property != "")           node["property"] = rhs.property;
      if(!rhs.outcomes.empty())        node["outcomes"] = rhs.outcomes;
      if(!rhs.parents.empty())         node["parents"]  = rhs.parents;
      if(!rhs.parent_weights.empty())  node["weights"]  = rhs.parent_weights;
      if(!rhs.cpt.empty())             node["cpt"]      = rhs.cpt;
      if(!rhs.cpc.empty())             node["cpc"]      = rhs.cpc;
      return node;
    }

    static bool decode(const Node& node, NodeConfig& rhs) {
      rhs.name = node["name"].as<std::string>();
      rhs.outcomes = node["outcomes"].as<std::vector<std::string> >();
      if(node["property"]) rhs.property       = node["property"].as<std::string>();
      if(node["weights"])  rhs.parent_weights = node["weights"].as<std::vector<real> >();
      if(node["cpt"])      rhs.cpt            = node["cpt"].as<std::vector<real> >();
      if(node["cpc"])      rhs.cpc            = node["cpc"].as<std::vector<CompatibleParentConfigElement> >();
      if(node["parents"]){
        if(node["parents"].IsScalar()){
          rhs.parents.push_back(node["parents"].as<std::string>());
        } else {
          rhs.parents = node["parents"].as<std::vector<std::string> >();
        }
      }
      rhs.init();
      return true;
    }

  };
}

real compatibleConfigurationProbability(const std::string& node_state,
                                        const std::string& relevant_parent_state,
                                        const NodeConfig& node)
{
  const std::vector<real>& cpc = node.find_cpc(relevant_parent_state);
  uint id = node.outcomeId(node_state);
  SASSERT_THROW(cpc.size() > id, UnexpectedException, "outcome id cannot be > cpc table");
  return cpc.at(id);
}

/*
 * Generates a probability of a nodes state as a weighted sum of the parents compatible configurations.
 *
 * read: http://arxiv.org/abs/cs/0411034
 */
real weightedSum(const std::vector<std::string>& configuration, const NodeConfig& node,const NetworkConfig& network)
{
  real p = 0.;
  // parent weights are not always initialized. set to equal when neccessary..
  std::vector<real> parentWeights = node.parent_weights;
  if(parentWeights.size() != node.parents.size()){
    parentWeights.clear();
    parentWeights.insert(parentWeights.begin(),node.parents.size(),1./(real)node.parents.size());
  }
  for(uint i = 0; i < parentWeights.size(); ++i){
    p += parentWeights[p] * compatibleConfigurationProbability(configuration.back(),configuration[p],node);
  }
  return p;
}

std::vector<real> generateTableFromCpc(const NodeConfig& node, const NetworkConfig& config, real noise, uint seed){
  if(node.name == "Attention_Flobi_Wardrobe"){
    SLOG_TRACE << "Wardrobe";
  }
  StateConfiguration state(node,config);
  std::vector<real> cpt;
  while(state.valid()){
    cpt.push_back(weightedSum(state.configuration(),node,config));
    ++state;
  }
  SLOG_TRACE << "generated cpt from cpc: " << node.name << ": " << cpt;
  return cpt;
}

void normalizeDefinition(BayesianNetwork::Variable& variable, BayesianNetwork::Definition& definition){
  uint cols = variable.m_Outcomes.size();
  uint rows = definition.m_Table.size()/cols;
  for (uint row = 0; row < rows; ++row) {
    real sum = 0.;
    for (uint elem = 0; elem < cols; ++elem) {
        sum += definition.m_Table.at(row*cols + elem);
    }
    if (sum != 1.){
      SLOG_INFO << "Need to normalize probability distribution of variable: "
                << variable.m_Name << " row " << row << ": sum := " << sum << " != 1.";
      for (uint elem = 0; elem < cols; ++elem) {
        definition.m_Table.at(row*cols+elem) /= sum;
      }
    }
  }
}

std::vector<std::pair<BayesianNetwork::Variable,BayesianNetwork::Definition> >
createNodesFromConfig(const NetworkConfig& config, real noise, uint seed)
{
  typedef std::pair<BayesianNetwork::Variable,BayesianNetwork::Definition> Node;
  std::vector<Node> result;
  for (auto nodeElem : config.nodes()){
    const NodeConfig& node = nodeElem.second;
    BayesianNetwork::Variable variable;
    variable.m_Name = node.name;
    variable.m_Property = node.property;
    variable.m_Type = BayesianNetwork::Variable::nature;
    variable.m_Outcomes = node.outcomes;

    BayesianNetwork::Definition definition;
    definition.m_Variables.push_back(node.name);
    definition.m_Variables.insert(definition.m_Variables.end(),node.parents.begin(),node.parents.end());
    if(!node.cpt.empty()){
      definition.m_Table = node.cpt;
    } else if (!node.cpc.empty()) {
      definition.m_Table = generateTableFromCpc(node, config, noise, seed++);
    } else {
      definition.m_Table.resize(config.dim(definition.m_Variables));
      definition = BayesianNetworkFactory::equalizeDefinition(definition,config.dim(node.name),noise,seed++);
    }
    normalizeDefinition(variable,definition);
    result.push_back(Node(variable,definition));
  }
  return result;
}

BayesianNetwork BayesianNetworkFactory::fromYaml(std::istream& data){
  YAML::Node metaNetwork = YAML::Load(data);

  real noise = (metaNetwork["noise"]) ? metaNetwork["noise"].as<real>() : 1e-3;
  uint seed = (metaNetwork["seed"]) ? metaNetwork["seed"].as<uint>() : utils::random_uint();

  std::string name = "BayesianNetwork";
  if(metaNetwork["name"]){
    name = metaNetwork["name"].as<std::string>();
  }

  std::string comment = "Generated from YAML";
  if(metaNetwork["comment"]){
    comment = metaNetwork["comment"].as<std::string>();
  }

  std::vector<BayesianNetwork::Variable> variables;
  std::vector<BayesianNetwork::Definition> definitions;

  NetworkConfig config(metaNetwork["nodes"].as<std::vector<NodeConfig> >());

  auto nodes = createNodesFromConfig(config,noise,seed);

  variables.reserve(nodes.size());
  definitions.reserve(nodes.size());
  for(auto node : nodes){
    variables.push_back(node.first);
    definitions.push_back(node.second);
  }

  return layout(BayesianNetwork(variables,definitions,name,comment));
}


void BayesianNetworkFactory::toXml(std::ostream& sink, const BayesianNetwork& network){
  io::XmlBifIO::writeStream(sink,network);
}

void BayesianNetworkFactory::toYaml(std::ostream& sink, const BayesianNetwork& network){
  YAML::Node node;
  node["name"] = network.name();
  node["comment"] = network.comment();
  for(const BayesianNetwork::Variable& var : network.variables()){
    NodeConfig config;
    config.name = var.m_Name;
    config.outcomes = var.m_Outcomes;
    config.property = var.m_Property;
    const BayesianNetwork::Definition& def = network.definition(network.variableId(var.m_Name));
    config.parents.reserve(def.m_Variables.size()-1);
    config.parents.insert(config.parents.begin(),def.m_Variables.begin()+1,def.m_Variables.end());
    config.cpt = def.m_Table;
    node["nodes"].push_back(config);
  }
  sink << "---\n";
  sink << node;
  sink << "\n...";
}

void BayesianNetworkFactory::toJson(std::ostream& sink, const BayesianNetwork& network){
  sink << network.asRstMessage()->DebugString();
}

uint max_length(const std::vector<std::string>& strings){
  uint result = 0;
  for(auto s : strings){
    result = (s.size() > result) ? s.size() : result;
  }
  return result;
}

#define write(t,n,x) \
{ \
  for(unsigned int i = 0; i < n; ++i){ \
    t << "    ";  \
  } \
  t << x << "\n"; \
} \

void BayesianNetworkFactory::toDot(std::ostream& sink, const BayesianNetwork& network){
  const std::vector<BayesianNetwork::Variable>& variables = network.variables();
  const std::vector<BayesianNetwork::Definition>& definitions = network.definitions();

  // network base info
  std::string name = network.name();
  if(name == "") name = "Network";
  write(sink,0,"digraph " << name << " {");
  write(sink,1,"overlap = false;");
  sink << "\n";

  // node info
  for(uint i = 0; i < variables.size(); ++i){
    const BayesianNetwork::Variable& var = variables.at(i);
    write(sink,1,var.m_Name << " [shape=record,label=\""<< var.m_Name
          <<"|" << utils::concat(var.m_Outcomes,'|','{','}') << "\"];");
  }
  // print connections
  sink << "\n";
  for(uint i = 0; i < definitions.size(); ++i){
    const BayesianNetwork::Definition& def = definitions.at(i);
    for(uint j = 1; j < def.m_Variables.size(); ++j){
      write(sink,1, def.m_Variables.at(j) << " -> " << def.m_Variables.front());
    }
  }
  write(sink,0,"}");
}

#undef write

BayesianNetwork BayesianNetworkFactory::layout(const BayesianNetwork& source){
   render::GraphLayout graphLayout(source.asRstMessage().get());
   std::vector<BayesianNetwork::Variable> variables = source.variables();
   for(BayesianNetwork::Variable& variable : variables){
     auto position = graphLayout.positions().find(variable.m_Name)->second;
     std::stringstream stream;
     stream << "position = ("<< position.x() << ", " << position.y() << ")";
     variable.m_Property = stream.str();
   }
   return BayesianNetwork(variables,source.definitions(),source.name(),source.comment());
}


const std::string& BayesianNetworkFactory::Networks::sprinkler_yaml(){
  static std::string sprinkler = "---"
      "\nname: Sprinkler"
      "\ncomment: yaml network for Sprinkler network"
      "\nnoise: 0."
      "\nseed: 0"
      "\nnodes:"
      "\n  - name: Cloudy"
      "\n    outcomes: [ False,True ]"
      "\n    cpt: [ 0.5, 0.5 ]"
      "\n"
      "\n  - name: Rain"
      "\n    outcomes: [ False,True ]"
      "\n    parents: Cloudy"
      "\n    cpt: [ 0.8, 0.2, 0.2, 0.8 ]"
      "\n"
      "\n  - name: Sprinkler"
      "\n    outcomes: [ False,True ]"
      "\n    parents: Cloudy"
      "\n    cpt: [ 0.5, 0.5, 0.9, 0.1 ]"
      "\n"
      "\n  - name: WetGrass"
      "\n    outcomes: [ False,True ]"
      "\n    parents: [ Sprinkler, Rain ]"
      "\n    cpt: [ 1.0, 0.0, 0.1, 0.9, 0.1, 0.9, 0.01, 0.99 ]"
      "\n...\n";
  return sprinkler;
}

const std::string& BayesianNetworkFactory::Networks::coin_yaml(){
  static std::string coin = "---"
      "\nname: CoinFlip"
      "\ncomment: yaml network for Coinflip network"
      "\nnoise: 0."
      "\nseed: 0"
      "\nnodes:"
      "\n  - name: Coin"
      "\n    outcomes: [ Heads, Tails ]"
      "\n    cpt: [ 0.5, 0.5 ]"
      "\n"
      "\n...\n";
  return coin;
}
