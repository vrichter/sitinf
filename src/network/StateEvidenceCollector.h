/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/network/StateEvidenceCollector.h                   **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <io/Source.h>
#include <utils/Types.h>
#include <sensor/Sensor.h>
#include <network/AspectStateEvidence.h>

namespace sitinf {
namespace network {

  class StateEvidenceCollector :
      public utils::Observer<const std::string&>,
      public io::Source<const AspectStateEvidence&>
  {
  public:

    /**
     * @brief Ptr typedef for shared pointer type
     */
    typedef boost::shared_ptr<StateEvidenceCollector> Ptr;

    StateEvidenceCollector(const std::vector<sensor::Sensor::Ptr>& sensors);

    static StateEvidenceCollector::Ptr New(const std::vector<sensor::Sensor::Ptr>& sensors);

    ~StateEvidenceCollector();

    virtual void update(const Subject<const std::string&>* subject, const std::string& data) override;

    virtual const AspectStateEvidence& current() override;

  private:
    utils::Mutex m_Lock;
    AspectStateEvidence m_CurrentEvidence;
    std::map<sensor::Sensor*,sensor::Sensor::Ptr> m_SensorMap;

  };

} // namespace network
} // namespace sitinf
