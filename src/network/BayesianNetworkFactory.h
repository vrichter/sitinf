﻿/*******************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/network/BayesianNetworkFactory.h                   **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <network/BayesianNetwork.h>
#include <utils/Types.h>

namespace sitinf {
namespace network {

  class BayesianNetworkFactory{
  public:

    static BayesianNetwork fromFile(const std::string& filename);
    static BayesianNetwork fromXml(std::istream& data);
    static BayesianNetwork fromYaml(std::istream& data);
    static BayesianNetwork fromNetwork(const BayesianNetwork& source,
                                bool equalize=false,
                                utils::real noise_max=1e-3,
                                utils::uint seed=utils::random_uint()
        );
    static BayesianNetwork layout(const BayesianNetwork& source);

    static BayesianNetwork::Definition& equalizeDefinition(BayesianNetwork::Definition& definition,
                                                    uint variable_dim,
                                                    utils::real noise_max=1e-3,
                                                    utils::uint seed=utils::random_uint()
                                                    );

    static void toXml(std::ostream& sink, const BayesianNetwork& network);
    static void toYaml(std::ostream& sink, const BayesianNetwork& network);
    static void toJson(std::ostream& sink, const BayesianNetwork& network);
    static void toDot(std::ostream& sink, const BayesianNetwork& network);

    class Networks {
    public:
      static const std::string& sprinkler_yaml();
      static const std::string& coin_yaml();

    };

  };

} // namespace network
} // namespace sitinf
