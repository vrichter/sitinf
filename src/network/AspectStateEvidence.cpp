/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/network/AspectStateEvidence.cpp                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <network/AspectStateEvidence.h>
#include <utils/Exception.h>
#include <utils/Macros.h>
#include <sensor/Sensor.h>

#define _LOGGER "network.AspectStateEvidence"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;
using namespace sitinf::network;

AspectStateEvidence::AspectStateEvidence(){}

AspectStateEvidence::~AspectStateEvidence(){}

AspectStateEvidence::AspectStateEvidence(const AspectStateEvidence& other)
  : m_Evidence(other.m_Evidence)
{}

void AspectStateEvidence::newEvidence(const std::string& variable, const std::string& state){
  Locker l(m_Lock);
  // save new state.
  auto it = m_Evidence.find(variable);
  if(it != m_Evidence.end()){
    if(it -> second == sensor::Sensor::unobserved()){
      SLOG_TRACE << variable << " -> " << state;
      m_Evidence.erase(it);
    } else if(it -> second != state){
      SLOG_TRACE << variable << " -> " << state;
      m_Evidence[variable] = state;
    }
  } else {
    if(state != sensor::Sensor::unobserved()){
      SLOG_TRACE << variable << " -> " << state;
      m_Evidence.insert(it,std::pair<std::string,std::string>(variable,state));
    }
  }
}

std::map<std::string,std::string> AspectStateEvidence::currentState() const{
  Locker l(m_Lock);
  return m_Evidence;
}

bool AspectStateEvidence::equals(const AspectStateEvidence* network_state) const {
  m_Lock.lock();
  // copy evidence so setting evidence afterwards is possible
  // but does not change outcome of this call
  std::map<std::string,std::string> a = m_Evidence;
  std::map<std::string,std::string> b = network_state->currentState();
  m_Lock.unlock();
  if(a.size() != b.size()) return false;
  for(auto it : a){
      if(b.find(it.first) == b.end() // evidence for variable not set
         || b[it.first] != it.second // evidence for variable set to other value
         ){
        return false;
      }
  }
  // they are equal
  return true;
}

std::string AspectStateEvidence::toString() const{
  m_Lock.lock();
  // copy evidence so setting evidence afterwards is possible
  // but does not change outcome of this call
  std::map<std::string,std::string> copy = m_Evidence;
  m_Lock.unlock();
  std::stringstream ret;
  ret << " Evidence {";
  for(auto it : copy){
    // add evidence to output stream
    ret << "{" << it.first << "," << it.second << "}";
  }
  ret << "}";
  return ret.str();
}


void AspectStateEvidence::override(const AspectStateEvidence* other){
  // add ither / overwrite the current states of this
  for(auto it : other->currentState()){
    newEvidence(it.first,it.second);
  }
}


void AspectStateEvidence::override(const rst::classification::ClassificationResultMap* situation){
  if(situation){
    for(int i = 0; i < situation->aspects_size(); ++i){
      auto aspect = situation->aspects(i);
      auto state = aspect.result();
      newEvidence(aspect.name(),state.decided_class());
    }
  }
}


void AspectStateEvidence::override(const Shared<rst::classification::ClassificationResultMap>::Ptr situation){
  override(situation.get());
}

AspectStateEvidence& AspectStateEvidence::operator=(const AspectStateEvidence& other){
  Locker l(m_Lock);
  m_Evidence = other.currentState();
  return *this;
}

inline void fillEvidence(rst::bayesnetwork::BayesNetworkEvidence* evidence,const AspectStateEvidence& data){
  for(auto it : data.currentState()){
    if(it.second != sensor::Sensor::unobserved()){
      rst::bayesnetwork::BayesNetworkEvidence_VariableState* observation = evidence->add_observations();
      observation->set_variable(it.first);
      observation->set_state(it.second);
    }
  }
}

rst::bayesnetwork::BayesNetworkEvidence
utils::Converter<rst::bayesnetwork::BayesNetworkEvidence,const AspectStateEvidence&>::convert(const AspectStateEvidence& data){
  rst::bayesnetwork::BayesNetworkEvidence evidence;
  fillEvidence(&evidence,data);
  return evidence;
}

Shared<rst::bayesnetwork::BayesNetworkEvidence>::Ptr
utils::Converter<Shared<rst::bayesnetwork::BayesNetworkEvidence>::Ptr,const AspectStateEvidence&>::convert(const AspectStateEvidence& data){
  Shared<rst::bayesnetwork::BayesNetworkEvidence>::Ptr evidence(new rst::bayesnetwork::BayesNetworkEvidence());
  fillEvidence(evidence.get(),data);
  return evidence;
}

bool AspectStateEvidence::operator <(const AspectStateEvidence& other) const{
  const auto& state1 = currentState();
  const auto& state2 = other.currentState();
  if(state1.size() < state2.size()) return true;
  if(state1.size() > state2.size()) return false;

  auto stateIt1 = state1.begin();
  auto stateIt2 = state2.begin();
  for(;stateIt1 != state1.end();++stateIt1,++stateIt2){
    if(stateIt1->first != stateIt2->first){ // compare variable
      return stateIt1->first < stateIt2->first;
    } else if (stateIt1->second != stateIt2->second){ // compare state
      return stateIt1->second < stateIt2->second;
    }
  }
  return false; // equal
}


WeightedAspectStateEvidence::WeightedAspectStateEvidence(utils::real weight)
{
  this->weight(weight);
}

utils::real WeightedAspectStateEvidence::weight() const {
  return m_Weight;
}

void WeightedAspectStateEvidence::weight(utils::real now){
  SASSERT_THROW(now > 0., InconsistentParameterException, "Cannot set weight of WeightedAspectStateEvidency to <= 0");
  m_Weight = now;
}
