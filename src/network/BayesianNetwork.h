﻿/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/network/BayesianNetwork.h                          **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <dai/alldai.h>

#include <utils/Types.h>

#include <rst/bayesnetwork/BayesNetwork.pb.h>
#include <utils/Converter.h>
#include <network/AspectStateEvidence.h>
#include <utils/Utils.h>

namespace sitinf {
namespace network {

  /**
   * This class holds all the information of a discrete, directed Bayesian Network description.
   *
   * A discrete, directed BayesianNetwork consists of a set of Variable and Definition. The Variable
   * hold the name and the possible outcomes of a node in the graph. The Definition on the other hand
   * define the corresponding 'conditional probality table' (CPT).
   */
  class BayesianNetwork{
  public:

    /**
     * This struct holds all the information of a Variable node of a BayesianNetwork description.
     *
     * The Variable tells the name, type and the possible outcomes of a node in a BayesianNetwork.
     * In addition an arbitrary property string can be stored for further information.
     */
    struct Variable{

      /**
       * A node in a discrete, directed graph can be of the type
       */
      enum Type{
        nature,
        decision,
        utility
      };

      /**
       * @brief toType can be used to get Type from string
       * @param s the string representation of Type
       * @return a type. Throws an exception when type is unknown.
       */
      static Type toType(std::string s);

      /**
       * @brief toType can be used to get the corresponding type to a rst::bayesnetwork::BayesVariable_Type
       * @param t the BayesVariableType
       * @return a type. Throws an exception when type is unknown.
       */
      static Type toType(rst::bayesnetwork::BayesVariable_Type t);
      static std::string toString(Type t);

      /**
       * Default constructor initializes members to default values
       */
      Variable();

      /**
       * The type of the variable
       */
      Type m_Type;

      /**
       * The name of the variable
       */
      std::string m_Name;

      /**
       * The values this variable can attain
       */
      std::vector<std::string> m_Outcomes;

      /**
       * A string describing  further properties of the variable
       */
      std::string m_Property;

      /**
       * @brief outcome returns the outcome id of the passed outcome.
       * @param name the name of the outcome
       * @return the corresponding id (position in m_Outcomes)
       *         when name is "" getOutcome returns -1 for unobserved.
       */
      int outcome(std::string name="") const;

      /**
       * @brief outcome returns the outcome name to the passed id.
       * @param id the position in m_Outcomes
       * @return the corresponding name
       *         when id is -1 getOutcome returns "" for unobserved.
       */
      std::string outcome(int id=-1) const;


      bool operator !=(const Variable &other) const;
      bool operator ==(const Variable &other) const;
    };

    /**
     * This struct holds all the information of a Definition node of a BayesianNetwork description.
     *
     * The Description defines the 'conditional probality table' (CPT) of a node in a BayesianNetwork
     * by holding the involved variable-names and the values of the CPT.
     *
     * In addition an arbitrary property string can be stored for futher information.
     */
    struct Definition{

      /**
       * Default constructor initializes members to default values
       */
      Definition();


      /**
       * The first variable \f$v_0\f$ always corresponds to the referenced node.
       * The other variables reference the parent nodes, so the CPT formats to: \f[P(v_0 | v_1 \dots \ v_n)\f]
       */
      std::vector<std::string> m_Variables;

      /**
       * The CPT table holds the probability values for the variable outcomes in the order \f$ v_1, \dots, v_n, v_0 \f$
       * with the value permutation from right to left.
       *
       * \verbatim
         An example with three binary variables would look the following way:

         |v1 |v2 |v0 | position in table |
         ---------------------------------
         | 0 | 0 | 0 |         0         |
         | 0 | 0 | 1 |         1         |
         | 0 | 1 | 0 |         2         |
         | 0 | 1 | 1 |         3         |
         | 1 | 0 | 0 |         4         |
         | 1 | 0 | 1 |         5         |
         | 1 | 1 | 0 |         6         |
         | 1 | 1 | 1 |         7         |
         \endverbatim
       *
       */
      std::vector<double> m_Table;

      /**
       * A string describing further properties of the Definition
       */
      std::string m_Property;

      bool operator !=(const Definition &other) const;
      bool operator ==(const Definition &other) const;
    };

  public:

    /**
     * @brief Ptr a typedef to a shared instance of this.
     */
    typedef boost::shared_ptr<BayesianNetwork> Ptr;

    /**
     * @brief Ptr a typedef to a shared const instance of this.
     */
    typedef boost::shared_ptr<const BayesianNetwork> CPtr;

    /**
     * @brief Message an rst message representing a BayesianNetwork
     */
    typedef rst::bayesnetwork::BayesNetwork Message;

    /**
     * @brief MessagePtr a shered pointer to a Message
     */
    typedef boost::shared_ptr<Message> MessagePtr;

    /**
     * @brief BayesianNetwork construcs a BayesianNetwork instance frome the passed data.
     * @param variables a vector of the networks variables
     * @param definitions a vector of variable definitions of the network
     * @param name the name of the network
     * @param comment a comment to this network
     */
    BayesianNetwork(const std::vector<Variable>& variables=std::vector<Variable>(),
                    const std::vector<Definition>& definitions=std::vector<Definition>(),
                    const std::string name="", const std::string comment="");

    /**
     * @brief BayesianNetwork constructs the BayesianNetwork from an rst message type
     * @param message
     */
    BayesianNetwork(const Message& message);

    /**
     * @brief rename renames the BayesNetwork
     * @param name the networks now name
     */
    void rename(std::string name);

    /**
     * @brief changeComment changes the BayesNetworks comment.
     * @param comment the new comment value.
     */
    void changeComment(std::string comment);

    /**
     * @brief name
     * @return the BayesianNetworks name
     */
    std::string name() const;

    /**
     * @brief comment
     * @return the BayesianNetworks comment
     */
    std::string comment() const;

    /**
     * @brief size
     * @return the size of the network
     */
    uint size() const;

    /**
     * @brief variable access to a variable via id
     * @param id of the variable
     * @return the corresponding variable
     */
    const Variable& variable(utils::uint id) const;

    /**
     * @brief variable access to a variable via its name
     * @param name the name of the variable
     * @return the corresponding variable
     */
    const Variable& variable(std::string name) const;

    /**
     * @brief variables access to all variables
     * @return a vector holding all variables
     */
    const std::vector<Variable>& variables() const;

    /**
     * @brief definition access to a definition via id
     * @param id of the definition
     * @return the corresponding definition
     */
    const Definition& definition(uint id) const;

    /**
     * @brief definitions access to all definitions
     * @return a vector holding all definitions
     */
    const std::vector<Definition>& definitions() const;

    /**
     * @brief variableName finds the name of a variable via id
     * @param id of the variable
     * @return the name of the vriable with id 
     */
    std::string variableName(const utils::uint id) const;

    /**
     * @brief variableId finds the id of a variable via name
     * @param variable_name the name of the variable
     * @return the id of the variable with variable_name
     */
    uint variableId(const std::string& variable_name) const;

    /**
     * @brief variableIds the ids of all variables by name
     * @param variable_names a vector of variable names
     * @return a vector of variable ids
     */
    std::vector<uint> variableIds(const std::vector<std::string>& variable_names) const;

    /**
     * @brief variableDimension the dimension of a variable
     * @param variable_name the name of the variable
     * @return how many outcomes the variable has.
     */
    uint variableDimension(const std::string& variable_name) const;

    std::vector<uint> variableDimensions(const std::vector<std::string>& variable_names) const;

    /**
     * @brief asRstMessage creates an rst::bayesnetwork::BayesNetwork representation of
     * this
     * @return a shared pointer to the rst::bayesnetwork::BayesNetwork representation of this
     */
    MessagePtr asRstMessage() const;

    /**
     * @brief validate checks if this network is consistent.
     * @throw throws NetworkInconsistentException if not.
     */
    void validate();

    /**
     * @brief knows tells whether this network knows a vairable with a special outcome.
     * @param variable_id the variable to check
     * @param outcome the variables outcome to check
     * @return true if this network contains the variable and is has the outcome
     */
    bool knows(const std::string& variable_id, const std::string& outcome) const;

    /**
     * @brief uniformDistributed create a uniform distributed version of this BayesianNetwork.
     *
     * When noise_max is set to zero the resulting BayesianNetwork is uniformly distributed. Otherwise the resulting
     * BayesianNetwork probabilities are first set to a uniform distribution with additional noise between 0. and
     * noise_max (again uniformly distributed) and in the end normalized to provide a correct distribution.
     *
     * @param noise_max the maximum noise to apply to each probability.
     * @param seed the seed to the random number generator may be set to get reproducable noise.
     * @return a copy of this with uniform distributed definitions with noise.
     */
    BayesianNetwork uniformDistributed(utils::real noise_max=1e-3, utils::uint seed=utils::random_uint()) const;

    /**
     * @brief applicableEvidence creates an AspectStateEvidence which is applicable to this network
     * from the passed one.
     * @param e evidence
     * @return an evidence containing only valid aspects and states. May be empty.
     */
    AspectStateEvidence applicableEvidence(const AspectStateEvidence& e);

    /**
     * @brief applicableEvidence creates an AspectStateEvidence which is applicable to this network
     * from the passed one. Retains the weight.
     * @param e evidence
     * @return an evidence containing only valid aspects and states. May be empty. Weight is copied.
     */
    WeightedAspectStateEvidence applicableEvidence(const WeightedAspectStateEvidence& e);

    bool operator !=(const BayesianNetwork &other) const;
    bool operator ==(const BayesianNetwork &other) const;

  private:
    /**
     * @brief m_Name the name of the network
     */
    std::string m_Name;
    /**
     * @brief m_Comment comment of property information of the network
     */
    std::string m_Comment;
    /**
     * @brief m_Variables this networks variables
     */
    std::vector<Variable> m_Variables;
    /**
     * @brief m_Definitions this networks definitions
     */
    std::vector<Definition> m_Definitions;
    /**
     * @brief m_VarIdMap internally used for id - name matching
     */
    std::map<std::string,utils::uint> m_VarIdMap;

//  public:
//    class ConverterFromRst : public utils::Converter<MessagePtr,BayesianNetwork::Ptr> {
//
//      public:
//
//      /**
//       * @brief Ptr a typedef to a shared instance of this.
//       */
//      typedef boost::shared_ptr<ConverterFromRst> Ptr;
//
//      static ConverterFromRst::Ptr New();
//
//      virtual BayesianNetwork::Ptr convert(const MessagePtr& data) final;
//    };
  };

} // namespace network

  namespace  utils {
  template<>
  class Converter<network::BayesianNetwork::Ptr,Shared<rst::bayesnetwork::BayesNetwork>::Ptr> {
      public:

      /**
       * @brief Ptr a typedef to a shared instance of this.
       */
      typedef boost::shared_ptr<Converter<Shared<rst::bayesnetwork::BayesNetwork>::Ptr,network::BayesianNetwork::Ptr> > Ptr;

      virtual network::BayesianNetwork::Ptr convert(Shared<rst::bayesnetwork::BayesNetwork>::Ptr data) final;
    };

  template<>
  class Converter<Shared<rst::bayesnetwork::BayesNetwork>::Ptr,network::BayesianNetwork::Ptr> {
      public:

      /**
       * @brief Ptr a typedef to a shared instance of this.
       */
      typedef boost::shared_ptr<Converter<Shared<rst::bayesnetwork::BayesNetwork>::Ptr,network::BayesianNetwork::Ptr> > Ptr;

      virtual Shared<rst::bayesnetwork::BayesNetwork>::Ptr convert(network::BayesianNetwork::Ptr data) final;
    };
  }

} // namespace sitinf
