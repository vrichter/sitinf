/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/network/AspectStateEvidence.h                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Types.h>
#include <utils/Converter.h>
#include <rst/classification/ClassificationResultMap.pb.h>
#include <rst/bayesnetwork/BayesNetworkEvidence.pb.h>


namespace sitinf {
namespace network {

  /**
   * This class holds the state of an amount of sensors.
   */
  class AspectStateEvidence{
  public:

    typedef boost::shared_ptr<AspectStateEvidence> Ptr;

    /**
     * Creates an empty state.
     */
    AspectStateEvidence();

    /**
     * @brief NetworkState copy constructor creates a copy of the passed state
     * @param other a network to be copied
     */
    AspectStateEvidence(const AspectStateEvidence& other);

    /**
     * @brief A virtual destructor to be reimplemented in subclasses for nice cleanup.
     */
    virtual ~AspectStateEvidence();

    /**
     * @brief newEvidence sets evidence that a variable has a special state.
     *
     * @param variable the id of the variable
     * @param state the value of the variable state.
     */
    virtual void newEvidence(const std::string& variable, const std::string& state);

    /**
     * @brief currentState provides a representation of the current states as aspect-state map.
     * @return maps current states to their aspects.
     */
    virtual std::map<std::string,std::string> currentState() const;

    /**
     * creates a string representing the current state
     */
    virtual std::string toString() const;

    /**
     * @brief equals returns whether this NetworkState equals the passed network state.
     * @param network_state an other instance.
     * @return whether this evidence has the same state
     */
    virtual bool equals(const AspectStateEvidence* network_state) const;

    /**
     * @brief override can be used to override this evidence by another.
     * @param other an other obviously more reliable AspectStateEvidence
     */
    virtual void override(const AspectStateEvidence* other);

    /**
     * @brief override can be used to override this evidence from a ClassificationResultMap message.
     * @param situation a pointer to a ClassificationResultMap.
     */
    virtual void override(const rst::classification::ClassificationResultMap* situation);

    /**
     * @brief override can be used to override this evidence from a ClassificationResultMap message.
     * @param situation a shared situation pointer to a ClassificationResultMap.
     */
    virtual void override(const utils::Shared<rst::classification::ClassificationResultMap>::Ptr situation);


    /**
     * @brief operator = mirrors the information of other into this
     * @param other other Evidence
     * @return this
     */
    virtual AspectStateEvidence& operator=(const AspectStateEvidence& other);

    bool operator <(const AspectStateEvidence& other) const;

  private:
    /**
     * @brief m_Lock used to enforce mutual access
     */
    mutable utils::Mutex m_Lock;
    /**
     * @brief m_Evidence this map holds the current variable-state values.
     */
    std::map<std::string,std::string> m_Evidence;
  };

  /**
   * @brief The WeightedAspectStateEvidence class can be used to assign different weights to different
   * Observations.
   */
  class WeightedAspectStateEvidence : public AspectStateEvidence {
  public:

    /**
     * @brief WeightedAspectStateEvidence creates an empty AspectStateEvidence with the passed weight.
     * @param weight the weight of this evidence. Default is 1. Must be > 0.
     */
    WeightedAspectStateEvidence(utils::real weight = 1.);

    /**
     * @brief weight access the weight of this evidence
     * @return the current weight.
     */
    utils::real weight() const;

    /**
     * @brief weight overrides the weight of this evidence with the passed value.
     * @param now must be > 0.
     */
    void weight(utils::real now);

  private:
    /**
     * @brief m_Weight inner weight field.
     */
    utils::real m_Weight;
  };


} // namespace network

  namespace utils{

    template<>
    class Converter<rst::bayesnetwork::BayesNetworkEvidence,const network::AspectStateEvidence&>{
    public:
      virtual rst::bayesnetwork::BayesNetworkEvidence convert(const network::AspectStateEvidence& data);
    };

    template<>
    class Converter<Shared<rst::bayesnetwork::BayesNetworkEvidence>::Ptr,const network::AspectStateEvidence&>{
    public:
      virtual Shared<rst::bayesnetwork::BayesNetworkEvidence>::Ptr convert(const network::AspectStateEvidence& data);
    };

  } // namespace utils
} // namespace sitinf
