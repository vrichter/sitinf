/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/pattern/Expression.cpp                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <pattern/Expression.h>
#include <pattern/ExpressionFactory.h>
#include <yaml-cpp/yaml.h>
#include <boost/regex.hpp>
#include <boost/tokenizer.hpp>

#define _LOGGER "pattern.Expression"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::pattern;
using namespace sitinf::utils;


bool Expression::valid(){
  return m_Valid.load();
}

bool Expression::setValidity(bool validity){
  if(validity != m_Valid.exchange(validity)){
    notify(validity);
    return true;
  } else {
    return false;
  }
}


namespace {

class TrueCounter : public NonCopyable, public Subject<int>, public Observer<bool> {
public:
  TrueCounter(const std::vector<Expression::Ptr>& expressions)
    : m_Expressions(expressions)
  {
    Locker l(m_Lock);
    m_Count.store(0);
    for (auto e : m_Expressions){
      if(e->valid()) m_Count.fetch_add(1);
    }
    for (auto e : m_Expressions){
      e->addObserver(this);
    }
  }

  ~TrueCounter(){
    Locker l(m_Lock);
    for (auto e : m_Expressions){
      e->removeObserver(this);
    }
  }

  virtual void update(const Subject<bool>* subject, bool data){
    if(data){
      m_Count.fetch_add(1);
    } else {
      m_Count.fetch_add(-1);
    }
    notify(m_Count.load());
  }

private:
  Mutex m_Lock;
  std::vector<Expression::Ptr> m_Expressions;
  std::atomic_int m_Count;
};

template <typename ValidAmountComparsionFunction>
class CounterBasedExpression : public Expression, public Observer<int> {

public:

  CounterBasedExpression(const std::vector<Expression::Ptr>& expressions)
    : m_Size(expressions.size()), m_Counter(expressions)
  {
    m_Counter.addObserver(this);
  }

  ~CounterBasedExpression() {
    m_Counter.removeObserver(this);
  }

  virtual void update(const Subject<int>* subject, int data){
    setValidity(ValidAmountComparsionFunction(data,m_Size));
  }

private:
  const int m_Size;
  TrueCounter m_Counter;

};

auto AndExpressionFunction = [] (int true_count, int overall_count) -> bool {
  return (overall_count != 0) && (true_count == overall_count);
};
auto OrExpressionFunction  = [] (int true_count, int overall_count) -> bool {
  return true_count > 0;
};
auto XorExpressionFunction = [] (int true_count, int overall_count) -> bool {
  return true_count == 1;
};

typedef CounterBasedExpression<decltype(AndExpressionFunction)> AndExpression;
typedef CounterBasedExpression<decltype(OrExpressionFunction)> OrExpression;
typedef CounterBasedExpression<decltype(XorExpressionFunction)>XorExpression;



} // unnamed namespace
