/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/pattern/ExpressionFactory.cpp                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <pattern/ExpressionFactory.h>
#include <utils/Exception.h>
#include <io/File.h>
#include <utils/Macros.h>

#define _LOGGER "pattern.ExpressionFactory"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::pattern;
using namespace sitinf::utils;

namespace YAML {
  template<>
  struct convert<std::vector<YAML::Node>> {
    static Node encode(const std::vector<YAML::Node>& rhs) {
      Node node;
      for(auto n : rhs){
        node.push_back(n);
      }
      return node;
    }

    static bool decode(const Node& node, std::vector<YAML::Node>& rhs) {
      if(node.IsScalar()){
          rhs.push_back(node);
      } else if(node.IsSequence()){
        for (auto it : node){
          rhs.push_back(it);
        }
      } else {
        STHROW(ParsingException,"Cannot parse node into vector of nodes because it is neither scalar nor list.");
      }
      return true;
    }

  };
}

std::vector<Expression::Ptr> ExpressionFactory::createExpressions(const std::string& filename){
  io::ReadFile test(filename);
  return createExpressions(YAML::LoadFile(filename));
}

std::vector<Expression::Ptr> ExpressionFactory::createExpressions(const YAML::Node expression_config){
  SASSERT_THROW(expression_config["expressions"],ParsingException,"A expression configuration needs a 'expressions' field.");
  std::vector<Expression::Ptr> result;
  int configCounter = 0;
  std::vector<YAML::Node> expressions = expression_config["expressions"].as<std::vector<YAML::Node>>();
  for(YAML::Node expression : expressions) {
    SASSERT_THROW(expression["expression_type"],
        ParsingException,"Expression configuration #" << configCounter << " has no 'expression_type' element.");
    std::string type = expression["expression_type"].as<std::string>();
    SASSERT_THROW(registry().hasFunction(type),
        ParsingException,"Expression configuration #" << configCounter << " expression_type := '" << type << "' unknown.");
    Function func = registry().getFunction(type);
    STRY_RETHROW_MESSAGE(
      std::vector<Expression::Ptr> created = func(expression,*this);
      result.reserve(result.size() + created.size());
      result.insert(result.end(),created.begin(),created.end());
      ,"Could not parse expression #" << configCounter << " into '" << type << "'. Data:\n" << YAML::Dump(expression)
      )
    ++configCounter;
  }
  return result;
}
