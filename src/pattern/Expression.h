/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/pattern/Expression.h                               **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <atomic>

#include <utils/Types.h>
#include <utils/NonCopyable.h>
#include <utils/Subject.h>

namespace sitinf {
  namespace pattern {

    class Expression : public utils::NonCopyable, public utils::Subject<bool> {

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<Expression> Ptr;

      /**
       * Default constructor.
       */
      Expression() = default;

      /**
       * @brief ~Expression virtual destructor for nice cleanup.
       */
      virtual ~Expression() = default;

      /**
       * @brief valid returns whether this expression is currently valid.
       * @return
       */
      virtual bool valid() final;

    protected:

      /**
       * @brief setValudity sets the validity of this Expression to the passed value.
       * When the value is different from the current Observers are notified.
       * @return true if this->valid() =! validity
       */
      virtual bool setValidity(bool validity) final;

    private:
      std::atomic_bool m_Valid;
    };

}   // namespace pattern
} // namespace sitinf
