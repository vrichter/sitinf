/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/pattern/ExpressionFactory.h                        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <pattern/Expression.h>
#include <utils/FunctionRegistry.h>
#include <yaml-cpp/yaml.h>
#include <functional>

namespace sitinf {
  namespace pattern {

    /**
     * @brief The ExpressionFactory class creates discrete expression instances.
     */
    class ExpressionFactory : public utils::Factory<
        std::vector<pattern::Expression::Ptr>,
        std::string,
        std::function<std::vector<pattern::Expression::Ptr>(YAML::Node expression, ExpressionFactory& factory)>
    >{
    public:

      /**
       * @brief createExpressions creates expressions from a Config description.
       *
       * The passed yaml-node must have a 'expressions' element containing a vector of
       * expression configurations.
       *
       * @param expression_config a expression configuration in form of a yaml node
       * @return a vector of ExpressionPointers
       */
      std::vector<Expression::Ptr> createExpressions(const YAML::Node expression_config);

      /**
       * @brief createExpressions creates expressions from a yaml description in a file.
       * @param filename the file must contain a valid expression configuration.
       * @return a vector of expression pointers.
       */
      std::vector<Expression::Ptr> createExpressions(const std::string& filename);

    };

  } // namespace pattern
} // namespace sitinf
