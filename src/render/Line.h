/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/render/Line.h                                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <render/Position.h>

namespace sitinf {
  namespace render {

    template<class T>
    /**
     * @brief The Line class describes a line by its start and end Positions
     */
    class Line
    {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<Line<T> > Ptr;

      /**
       * @brief Line a line between start and end
       * @param start starting position of the Line
       * @param end ending position of the Line
       */
      Line(const Position<T>& start, const Position<T>& end)
        : m_Start(start), m_End(end) {}

      /**
       * @brief start the start position of this line
       * @return a position
       */
      const Position<T>& start() const { return m_Start; }

      /**
       * @brief end the end position of this line
       * @return a position
       */
      const Position<T>& end() const { return m_End; }

    private:
      /**
       * @brief m_Start holds the start position
       */
      Position<T> m_Start;
      /**
       * @brief m_End holds the end position
       */
      Position<T> m_End;

    };

  } // namespace render
} // namespace sitinf
