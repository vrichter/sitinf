/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/render/Position.h                                  **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>

namespace sitinf {
  namespace render {

    template<class T>
    /**
     * @brief The Position class represents a two dimensional position with x and y coordinates.
     */
    class Position
    {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<Position<T> > Ptr;

      /**
       * @brief Position simple constructor uses passed coordinates
       * @param x the x coordinate of this Position
       * @param y the y coordnate of this Position
       */
      Position(const T& x, const T& y) : m_X(x), m_Y(y) {}

      /**
       * @brief x the x coordinate of this position.
       * @return a coordinate
       */
      const T& x() const { return m_X; }

      /**
       * @brief y the y coordinate of this position
       * @return a coordinate
       */
      const T& y() const { return m_Y; }

    private:
      /**
       * @brief m_X internal x member
       */
      T m_X;
      /**
       * @brief m_Y internal y member
       */
      T m_Y;
    };

    template<class T>
    /**
     * @brief operator == tells whether two positions are equal by checking their coordinates.
     * @param lhs left hand parameter
     * @param rhs right hand parameter
     * @return equal if pointing to the same coordinates.
     */
    bool operator==(Position<T> const& lhs, Position<T> const& rhs){
      return (lhs.x() == rhs.x() && lhs.y() == rhs.y());
    }

    template<class T>
    /**
     * @brief operator != tells whether two Positinos are not equal by checking their coordinates.
     * @param lhs left hand parameter
     * @param rhs right hand parameter
     * @return the inverse of the equals operator.
     */
    bool operator!=(Position<T> const& lhs, Position<T> const& rhs){
      return ! (lhs == rhs);
    }

  } // namespace render
} // namespace sitinf
