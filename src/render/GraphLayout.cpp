/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/render/GraphLayout.cpp                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <render/GraphLayout.h>
#include <graphviz/gvc.h>
#include <utils/Exception.h>
#include <utils/Macros.h>

#define _LOGGER "render.GraphLayout"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

#define LOCAL(x,y) std::unique_ptr<char>x(new char[y.size()+1]); strcpy(x.get(),y.c_str());

using namespace sitinf;
using namespace sitinf::render;
using namespace sitinf::utils;

char RENDERER[] = "dot";
char LAYOUTER[] = "dot"; // circo dot fdp neato nop nop1 nop2 osage patchwork sfdp twopi
char BB[] = "bb";
char LP[] = "lp";
char POS[] = "pos";
char LABEL[] = "label";
char SHAPE[] = "shape";
char NONE[] = "none";
char ASPECT[] = "ratio_";
char ASPECT_DEFAULT[] = "0.5625";
char OVERLAP[] = "overlap";
char OVERLAP_DEFAULT[] = "false";

typedef GVC_t GraphvizContext;
typedef Agraph_t Graph;
typedef Agnode_t Node;
typedef Agedge_t Edge;

BoundingBox<uint> parseBoundingBox(const char* attribute){
  if(!attribute){
    SLOG_TRACE << "Passed attribute is null";
    return BoundingBox<uint>(Position<uint>(0,0),0,0);
  }
  // parse string
  std::string data(attribute);
  std::replace(data.begin(),data.end(),',',' ');

  std::istringstream str(data);
  float x,y,w,h;
  str >> x;
  str >> y;
  str >> w;
  str >> h;
  SLOG_TRACE << "parsed bounding box (" << x << "," << y << ") w = " << w << " h = " << h << " from " << data;
  return BoundingBox<uint>(Position<uint>(x,y),w,h);
}

Position<uint> parsePosition(const char* attribute){
  if(!attribute){
    SLOG_TRACE << "Passed attribute is null";
    return Position<uint>(0,0);
  }
  // parse
  std::string data(attribute);
  std::replace(data.begin(),data.end(),',',' ');

  std::istringstream str(data);
  real x,y;
  str >> x;
  str >> y;
  SLOG_TRACE << "parsed position : (" << x << "," << y << ") from " << data;
  return Position<uint>(x,y);
}


struct GraphLayout::GraphLayoutImpl{
public:


  /**
   * @brief m_Context the graphviz context.
   */
  GraphvizContext* m_Context;

  /**
   * @brief m_Graph the internally wrapped graphviz-graph.
   */
  Graph* m_Graph;

  GraphLayoutImpl(const std::string& name)
    : m_Context(gvContext()), m_Graph(NULL)
  {
    LOCAL(cname,name);
#ifndef WITH_CGRAPH
    m_Graph = agopen(cname,AGDIGRAPHSTRICT);
#else
    m_Graph = agopen(cname.get(),Agdirected,nullptr);
#endif
    agsafeset(m_Graph,ASPECT,ASPECT_DEFAULT,ASPECT_DEFAULT);
    agsafeset(m_Graph,OVERLAP,OVERLAP_DEFAULT,OVERLAP_DEFAULT);
    SLOG_TRACE << "creating Graph: " << agnameof(m_Graph);
  }

  ~GraphLayoutImpl(){
    agclose(m_Graph);
  }

  void addNode(const rst::bayesnetwork::BayesVariable& variable){
    LOCAL(cname,variable.name());
#ifndef WITH_CGRAPH
    Node* node = agnode(m_Graph,cname);
#else
    Node* node = agnode(m_Graph,cname.get(),1);
    SLOG_TRACE << "added Node " << node << " - '" << variable.name() << "'";
#endif

    // create html label forcing a table in place of the node.
    // this helps keeping enough space between the nodes
    std::stringstream label;
    label << variable.name();
    label << "<<table border=\"0\" cellspacing=\"0\">\n\t\t";
    label << "<tr><td port=\"nodeport\" bgcolor=\"white\">" << variable.name() << "</td></tr>\n\t\t";
    for(int i = 0; i < variable.outcomes_size(); ++i){
      label << "<tr><td port=\"port" << i << "\" bgcolor=" << variable.outcomes(i) << "</td></tr>\n\t\t";
    }
    label << "</table>>\n";

    LOCAL(clabel,label.str());
    agsafeset(node,LABEL,clabel.get(),cname.get());
    agsafeset(node,SHAPE,NONE,NONE);
    //DEBUG_LOG("adding Node: " << agnameof(node));
  }

  void addEdge(const std::string tail, const std::string head){
    LOCAL(ctail,tail);
    LOCAL(chead,head);
#ifndef WITH_CGRAPH
    Node* childNode = agnode(m_Graph,ctail);
    Node* parentNode = agnode(m_Graph,chead);
    agedge(m_Graph,childNode,parentNode);
#else
    Node* parentNode = agnode(m_Graph,chead.get(),0);
    Node* childNode = agnode(m_Graph,ctail.get(),0);
    SASSERT_THROW(parentNode,IllegalArgumentException,"Parent node with name '" << head << "' does not exist.");
    SASSERT_THROW(childNode,IllegalArgumentException,"Child node with name '" << tail << "' does not exist.");
    agedge(m_Graph,childNode,parentNode,NONE,1);
#endif
    }

  std::string attribute(void* element, char* attribute){
    const char* tmp = agget(element,attribute);
    return (tmp != NULL) ? tmp : "";
  }

  std::string result(){
    std::stringstream ret;
    return ret.str();
  }

  void layout(const std::string& layout_type){
    LOCAL(clayout_type,layout_type);
    gvLayout(m_Context,m_Graph,clayout_type.get());
    gvRender(m_Context,m_Graph,RENDERER,NULL);
    //print(m_Graph);
    gvFreeLayout(m_Context,m_Graph);
  }

  BoundingBox<uint> boundingBox(std::string name="_root_"){
    if(name == "_root_"){
      return parseBoundingBox(agget(m_Graph,BB));
    } else {
      LOCAL(cname,name);
#ifndef WITH_CGRAPH
      Node* node = agnode(m_Graph,cname);
#else
      Node* node = agnode(m_Graph,cname.get(),0);
#endif
      if(!node){
        return BoundingBox<uint>(Position<uint>(0,0),0,0);
      } else {
        return parseBoundingBox(agget(node,BB));
      }
    }
  }

  Position<uint> position(std::string name="_root_"){
    if(name == "_root_"){
      return parsePosition(agget(m_Graph,POS));
    } else {
      LOCAL(cname,name);
#ifndef WITH_CGRAPH
      Node* node = agnode(m_Graph,cname);
#else
      Node* node = agnode(m_Graph,cname.get(),0);
#endif
      if(!node){
        return Position<uint>(0,0);
      } else {
        return parsePosition(agget(node,POS));
      }
    }
  }

  Line<uint> edge(std::string start, std::string end){
    return Line<uint>(position(start),position(end));
  }

  void print(Graph* elem){
    char* result = NULL;
    uint length = 0;
    gvRenderData(m_Context,elem,RENDERER,&result,&length);
    SLOG_TRACE << "Element:\n" << result;
    delete[] result;
  }

};

GraphLayout::GraphLayout(const rst::bayesnetwork::BayesNetwork* network, const std::string& layout_type)
  : m_BoundingBox(Position<uint>(0,0),0,0)
{
  SASSERT_RETURN(network!=nullptr);
  // create graph
  m_Impl = Shared<GraphLayoutImpl>::Ptr(new GraphLayoutImpl(network->name()));

  // add nodes
  for(int i = 0; i < network->variable_size(); ++i){
    const rst::bayesnetwork::BayesVariable& variable = network->variable(i);
    m_Impl->addNode(variable);
  }

  // add edges
  for(int i = 0; i < network->variable_size(); ++i){
    const rst::bayesnetwork::BayesVariable& variable = network->variable(i);
    for(int j = 0; j < variable.parents_size(); ++j){
      m_Impl->addEdge(variable.name(),variable.parents(j));
    }
  }
  // layout graph
  m_Impl->layout(layout_type);

  // get graph bounding box
  m_BoundingBox = m_Impl->boundingBox();
  // get node positions
  for(int i = 0; i < network->variable_size(); ++i){
    m_Positions.insert(std::pair<std::string,Position<uint> >(
                         network->variable(i).name(),
                         m_Impl->position(network->variable(i).name())
                         )
                       );
  }

  // get edges
  for(int i = 0; i < network->variable_size(); ++i){
    std::string name = network->variable(i).name();
    m_Edges[name] = std::vector<Line<uint> >();
    m_Edges[name].reserve(network->variable(i).parents_size());
    for(int j = 0; j < network->variable(i).parents_size(); ++j){
      m_Edges[name].push_back(m_Impl->edge(name,network->variable(i).parents(j)));
    }
  }
}

const BoundingBox<uint>& GraphLayout::boundingBox() const {
  return m_BoundingBox;
}


const std::map<std::string,Position<uint> >& GraphLayout::positions() const{
  return m_Positions;
}

const std::map<std::string,std::vector<Line<uint> > > GraphLayout::edges() const{
  return m_Edges;
}

const std::vector<std::string>& GraphLayout::layoutTypes(){
  static std::vector<std::string> types = {"dot", "circo", "fdp", "neato", "nop", "nop1",
                                           "nop2", "osage", "patchwork", "sfdp", "twopi"};
  return types;
}
