/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/render/GraphLayout.h                               **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Types.h>
#include <rst/bayesnetwork/BayesNetwork.pb.h>
#include <render/BoundingBox.h>
#include <render/Line.h>

namespace sitinf {
  namespace render {

    /**
     * @brief The GraphLayout class is used to calculate a layout for a rst::bayesnetwork::BayesNetwork
     */
    class GraphLayout
    {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<GraphLayout> Ptr;

      /**
       * @brief GraphLayout creates a GraphLayout instance from a BayesNetwork
       * @param network the network to lay out.
       * @param layout_type the layout type. Ask layoutTypes() for a list of possible parameters.
       */
      GraphLayout(const rst::bayesnetwork::BayesNetwork* network=nullptr, const std::string& layout_type="dot");

      /**
       * Frees all allocated resources
       */
      virtual ~GraphLayout() {}

      /**
       * @brief boundingBox the bounding box of the graph layout
       * @return a BoundingBox
       */
      const BoundingBox<uint>& boundingBox() const ;

      /**
       * @brief positions of nodes in this layout
       * @return map Positions to node-names
       */
      const std::map<std::string,Position<uint> >& positions() const;

      /**
       * @brief edges edges for every node to its parents
       * @return maps vectors of edges parent-edges to node names
       */
      const std::map<std::string,std::vector<Line<uint> > > edges() const;

      /**
       * @brief layoutTypes provides a list of layout types I heard of
       * @return a list of layout types.
       */
      static const std::vector<std::string>& layoutTypes();

    private:
      /**
       * internally used to hide implementation details
       */
      struct GraphLayoutImpl;

      /**
       * @brief m_Impl holds internal data.
       */
      utils::Shared<GraphLayoutImpl>::Ptr m_Impl;

      /**
       * @brief m_BoundingBox internal member for bounding box
       */
      BoundingBox<uint> m_BoundingBox;

      /**
       * @brief m_Positions internal member for positions. maps node positions to node names.
       */
      std::map<std::string,Position<uint> > m_Positions;

      /**
       * @brief m_Edges maps vectors of edges parent-edges to node names.
       */
      std::map<std::string,std::vector<Line<uint> > > m_Edges;
    };

  } // namespace render
} // namespace sitinf
