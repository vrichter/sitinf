/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/render/BoundingBox.h                               **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <render/Position.h>

namespace sitinf {
  namespace render {

    template<class T>
    /**
     * @brief The BoundingBox class represents a bounding box with position, height and width.
     */
    class BoundingBox
    {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<BoundingBox<T> > Ptr;

      /**
       * @brief BoundingBox constructor used passed Position and dimensions.
       * @param position the position of this
       * @param width the width of this
       * @param height the height of this
       */
      BoundingBox(const Position<T>& position, const T& width, const T& height)
        : m_Position(position), m_Width(width), m_Height(height) {}

      /**
       * @brief position the position of this Bounding Box
       * @return a position reference
       */
      const Position<T>& position() const { return m_Position; }

      /**
       * @brief width of this BoundingBox
       * @return a width reference
       */
      const T& width() const { return m_Width; }

      /**
       * @brief height of this BoundingBox
       * @return a height reference
       */
      const T& height() const { return m_Height; }

    private:
      /**
       * @brief m_Position internal Position member
       */
      Position<T> m_Position;
      /**
       * @brief m_Width internal width member
       */
      T m_Width;
      /**
       * @brief m_Height internal height member
       */
      T m_Height;
    };

    template<class T>
    /**
     * @brief operator == equals operator for BoundingBoxes checks equality of data.
     * @param lhs left hand side BoundingBox
     * @param rhs right hand side BoundingBox
     * @return whether the two are at the same position and have the same dimensions.
     */
    bool operator==(BoundingBox<T> const& lhs, BoundingBox<T> const& rhs){
      return (lhs.position() == rhs.position()
              && (lhs.width() == rhs.width())
              && (lhs.height() == rhs.height())
              );
    }

    template<class T>
    /**
     * @brief operator != not equals operator for BoundingBoxes check inequality of data
     * @param lhs left hand side BoundingBox
     * @param rhs right hand side BoundingBox
     * @return inverse of equals operator
     */
    bool operator!=(BoundingBox<T> const& lhs, BoundingBox<T> const& rhs){
      return ! (lhs == rhs);
    }

  } // namespace render
} // namespace sitinf
