/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/WheelArea.h                                     **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <QtDeclarative/QDeclarativeItem>
#include <QtGui/QGraphicsSceneWheelEvent>

namespace sitinf {
  namespace qt {

    /**
 * @brief The WheelArea class used for mouse wheel interaction in a qml gui.
 */
    class WheelArea : public QDeclarativeItem
    {
      Q_OBJECT

    public:
      /**
   * @brief WheelArea creates a WheelArea instance
   * @param parent the QObjects parent object
   */
      explicit WheelArea(QDeclarativeItem *parent = 0) : QDeclarativeItem(parent) {}

    protected:
      /**
   * @brief wheelEvent reacts on QGraphicsSceneWheelEvents and emits horizontalWheel
   * and verticalWheel events.
   * @param event the WheelEvent from the scene
   */
      void wheelEvent(QGraphicsSceneWheelEvent *event) {
        switch(event->orientation())
        {
          case Qt::Horizontal:
            Q_EMIT horizontalWheel(event->delta());
            break;
          case Qt::Vertical:
            Q_EMIT verticalWheel(event->delta());
            break;
          default:
            event->ignore();
            break;
        }
      }

    Q_SIGNALS:
      /**
   * @brief verticalWheel will be emitted when vertical wheel events are registered by this component.
   * @param delta the wheel delta
   */
      void verticalWheel(int delta);

      /**
   * @brief horizontalWheel will be emitted when horizontal wheel events are registered by this component.
   * @param delta the wheel delta
   */
      void horizontalWheel(int delta);
    };

  } // namespace qt
} // namespace sitinf
