/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/EdgeModel.h                                     **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <render/Line.h>
#include <QAbstractListModel>
#include <utils/Types.h>

namespace sitinf {
  namespace qt {

    /**
     * @brief The EdgeModel class Provides connectivity information between nodes in a visualisation.
     */
    class EdgeModel : public QAbstractListModel
    {
      Q_OBJECT
      Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
      Q_PROPERTY(int length READ rowCount NOTIFY countChanged)

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<EdgeModel> Ptr;

      /**
       * @brief The Roles enum used to distinguish the different field types of this model.
       */
      enum Roles {
        /// the start position x value
        StartX = Qt::UserRole + 1,
        /// the start position y value.,
        StartY,
        /// the end position x value
        EndX,
        /// the end position y value
        EndY
      };

      /**
       * @brief EdgeModel creates an empty edge model
       * @param parent the parent of this
       */
      EdgeModel(QObject* parent = 0);

      /**
       * @brief reset resets the edge model. removing all edges and reverting it to initial state.
       */
      void reset();

      /**
       * @brief addEdge adds an edge to this model
       * @param edge A Line<uint> describing the edge.
       */
      void addEdge(const render::Line<uint> edge);

      /**
       * @brief rowCount can be used to check the current amount of elements
       * @param parent is ignored
       * @return the number of internal elements in this model
       */
      int rowCount(const QModelIndex& parent = QModelIndex()) const;

      /**
       * @brief data provides the value of a role at index.
       * @param index should be between 0 and rowCount.
       * @param role should be of type Roles
       * @return the data corresponding to role and index. An empty QVariant if role or index inappropriate.
       */
      QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    Q_SIGNALS:
      /**
       * @brief countChanged a qt signal emitted when the element count changes.
       */
      void countChanged();

    private:
      /**
       * @brief m_Edges the internally held edges data
       */
      std::vector<render::Line<uint> > m_Edges;

    };

  } // namespace qt
} // namespace sitinf
