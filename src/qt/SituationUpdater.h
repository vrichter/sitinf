/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/SituationUpdater.h                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <QObject>
#include <io/RsbSituationReceiver.h>
#include <QAbstractListModel>

namespace sitinf {
  namespace qt {

    /**
     * @brief The SituationUpdater class signals situationChanged every time a situatino changes.
     * This is used to synchronize the application flow with QT-specific threading.
     */
    class SituationUpdater :
        public QObject,
        public utils::Observer<const utils::Shared<rst::classification::ClassificationResultMap>::Ptr >,
        public utils::Subject<const utils::Shared<rst::classification::ClassificationResultMap>::Ptr >
    {
      Q_OBJECT
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<SituationUpdater> Ptr;

      /**
       * @brief SituationUpdater creates a situation receiver and registeres as Observer
       * @param scope the scoipe to listen to for situation changes
       */
      SituationUpdater(std::string scope);

      /**
       * @brief ~SituationUpdate cleans up deregistering from Situation.
       */
      ~SituationUpdater();

      /**
       * @brief update Observer update implementation gets the current Situation from the SituationReceiver
       * and emits the change.
       * @param subject the subject providing new data
       * @param data the new data from subject
       */
      void update(const utils::Subject<const utils::Shared<rst::classification::ClassificationResultMap>::Ptr>* subject,
                  utils::Shared<rst::classification::ClassificationResultMap>::Ptr data);

    Q_SIGNALS:
      /**
         * @brief situationChanged is emited every time the situation changes
         * @param situation the description of the new situation
         */
      void situationChanged(utils::Shared<rst::classification::ClassificationResultMap>::Ptr situation);

    private:
      /**
         * @brief m_SituationReceiver internally used SituationReceiver
         */
      io::RsbSituationReceiver m_SituationReceiver;

    };

  } // namespace qt
} // namespace sitinf
