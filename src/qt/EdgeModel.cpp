/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/EdgeModel.cpp                                   **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <qt/EdgeModel.h>
#include <utils/Exception.h>

using namespace sitinf;
using namespace sitinf::qt;
using namespace sitinf::render;

EdgeModel::EdgeModel(QObject* parent)
  : QAbstractListModel(parent)
{
  QHash<int, QByteArray> roles;
  roles[StartX] = "model_start_x";
  roles[StartY] = "model_start_y";
  roles[EndX] = "model_end_x";
  roles[EndY] = "model_end_y";
  setRoleNames(roles);
  qRegisterMetaType<QModelIndex>("QModelIndex");
}

void EdgeModel::reset(){
  beginResetModel();
  m_Edges.clear();
  endResetModel();
}

void EdgeModel::addEdge(const Line<uint> edge){
  beginInsertRows(QModelIndex(),m_Edges.size(), m_Edges.size());
  m_Edges.push_back(edge);
  endInsertRows();
}

int EdgeModel::rowCount(const QModelIndex& parent) const{
  return m_Edges.size();
}

QVariant EdgeModel::data(const QModelIndex & index, int role) const{
  if (index.row() < 0 || index.row() > rowCount()){
    return QVariant();
  }

  switch (role) {
    case StartX: return QVariant(m_Edges.at(index.row()).start().x());
    case StartY: return QVariant(m_Edges.at(index.row()).start().y());
    case EndX: return QVariant(m_Edges.at(index.row()).end().x());
    case EndY: return QVariant(m_Edges.at(index.row()).end().y());
    default:
      return QVariant();
  }
}
