/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/ListModelMap.h                                  **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <QAbstractListModel>
#include <qt/StateModel.h>

namespace sitinf {
  namespace qt {

    /**
     * @brief The ListModelMap class can be used to provide an amont of ListModels to Qml.
     *
     * Different ListModels can be accessed via their name.
     */
    class ListModelMap : public QObject
    {
      Q_OBJECT

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<ListModelMap> Ptr;

      /**
       * @brief ListModelMap creates an empty ListModelMap.
       */
      ListModelMap();

      /**
       * @brief addModel adds the ListModelMap a new model with a name.
       *
       * Afterwards this model cac be accessed via model(name)
       *
       * @param name the name of the model
       * @param model a pointer to the model
       */
      void addModel(std::string name, QAbstractListModel* model);

      /**
       * @brief model access a ListModel by its name
       * @param sname the name of the required model
       * @return the model or NULL of sname is now known
       */
      Q_INVOKABLE QObject* model(QString sname);

    private:
      /**
       * @brief m_Models internal map storing models
       */
      std::map<std::string,QAbstractListModel*> m_Models;
    };

  } // namespace qt
} // namespace sitinf
