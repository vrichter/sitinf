/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/SituationModel.h                                **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <QAbstractListModel>
#include <QStringListModel>
#include <QStringList>

#include <iostream>

#include <utils/Types.h>
#include <network/BayesianNetwork.h>
#include <inference/BayesianInference.h>
#include <network/AspectStateEvidence.h>

namespace sitinf {
  namespace qt {

    /**
     * @brief The SituationModel class provides a representation of the situation state as data for qml.
     *
     * The current state consists of a list of tripels:
     * model_data_aspect: the name of the aspect
     * model_data_current_state: the name of the currently active state (unobserved is added as
     * the state of not observed aspects)
     * model_data_states: a list of possible states of the aspect
     */
    class SituationModel : public QAbstractListModel
    {
      Q_OBJECT
      Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
      Q_PROPERTY(int length READ rowCount NOTIFY countChanged)
      Q_PROPERTY(QString learn_state READ learnState WRITE setLearnState NOTIFY learnStateChanged)

    protected:

      /**
       * @brief The Aspect class internal aspect representation holding name, current state and possible states.
       */
      class Aspect {
      private:
        /**
         * @brief m_Aspect the aspects name
         */
        QString m_Aspect;
        /**
         * @brief m_CurrentState the aspects current state.
         */
        QString m_CurrentState;
        /**
         * @brief m_States the list of the aspects possible states.
         */
        QStringList m_States;

      public:
        /**
         * @brief Aspect constructor getting all neded information.
         * @param aspect the name of the aspect
         * @param current_state the current state of the aspect
         * @param states all possible states of the aspect
         */
        Aspect(const QString& aspect, const QString& current_state, const QStringList& states)
          : m_Aspect(aspect), m_CurrentState(current_state), m_States(states){}

        /**
         * @brief name the name of this aspect.
         * @return
         */
        const QString& name() const{
          return m_Aspect;
        }

        /**
         * @brief currentState the current state of this aspect
         * @return has to be from states
         */
        const QString& currentState() const{
          return m_CurrentState;
        }

        /**
         * @brief states all possible states of the aspect
         * @return a list of possible states
         */
        const QStringList& states() const{
          return  m_States;
        }

        /**
         * @brief updateCurrentState updates he current states to the passed value
         * @param current should be from states
         */
        void updateCurrentState(const QString& current){
          m_CurrentState = current;
        }
      };

    public:

      /**
       * @brief The SituationRoles enum used to distinguish the different field types of this model.
       */
      enum SituationRoles {
        /// the aspects name
        AspectRole = Qt::UserRole + 1,
        /// the aspects current state
        CurrentStateRole,
        /// the aspects possible states
        StatesRole,
      };

      /**
       * @brief SituationModel creates a new empty instance.
       * @param parent
       */
      SituationModel(QObject *parent = 0);

      /**
       * @brief useNetwork makes this instance use the passed network. forcing it to reset the
       * current data model and signal the change.
       * Every variable in the network gets an extra unobserved state in the model which is used
       * as default state.
       * @param network the BayesianNetwork to represent.
       */
      void useNetwork(const sitinf::network::BayesianNetwork::Ptr network);

      /**
       * @brief result presents the current state of the model as InferenceResult
       * @return the results holds only the observed Aspects setting their max outcome
       * to the current state and its probability to 1.
       */
      const sitinf::inference::InferenceResult::Ptr result();

      /**
       * @brief rowCount can be used to check the current amount of aspects
       * @param parent is ignored
       * @return the number of internal aspects
       */
      int rowCount(const QModelIndex& parent = QModelIndex()) const;

      /**
       * @brief data provides the value of a role at idex.
       * @param index should be between 0  and rowCount.
       * @param role should be of type SituationRoles
       * @return the name, current state or all states of aspect number index.
       * An empty QVariant if role or index inappropriate.
       */
      QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

      /**
       * @brief learnState getter for the inner learn state.
       * @return
       */
      QString learnState();

      /**
        * @brief sets the current state of an aspect.
        * @param aspect_name the name of the aspect to update
        * @param current_state the new current state of the aspect
        *
        */
      Q_INVOKABLE void setAspectState(const QString& aspect_name, const QString& current_state);

      /**
        * @brief sets the learn state of this model
        * @param learnState the learn state to switch to.
        */
      Q_INVOKABLE void setLearnState(const QString& learnState);

      /**
       * @brief registerStateCallback can be used to register a callback funtion.
       * called every time an aspects state is changed.
       * @param callback the callback to be used to notify changes
       */
      void registerStateCallback(boost::function<void()> callback);

    Q_SIGNALS:
      /**
       * @brief countChanged a qt signal emitted when the aspect count changes.
       */
      void countChanged();
      /**
       * @brief stateChanged a qt signal emitted when an aspects state changes.
       */
      void stateChanged();

      /**
       * @brief learnStateChanged a qt signal emitted when the learn state of this SituationModel changes.
       */
      void learnStateChanged();

    private:
      /**
       * @brief m_Aspects the internal list of aspects
       */
      std::vector<Aspect> m_Aspects;
      /**
       * @brief m_Callbacks the internal list of registered callbacks
       */
      std::vector<boost::function<void()> > m_Callbacks;
      /**
       * @brief m_Result the InferenceResult representation of the data model which is always held up to date.
       */
      sitinf::inference::InferenceResult::Ptr m_Result;

      /**
       * @brief m_LearnState the current learn state.
       */
      QString m_LearnState;

      /**
       * @brief getAspect used internally to find an aspect by its name.
       * @param name the aspects name
       * @return a reference to the aspect
       */
      Aspect& getAspect(const QString& name);

      void notify();

    };

  } // namespace qt
} // namespace sitinf
