/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/ListModelMap.cpp                                **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <qt/ListModelMap.h>
#include <utils/Exception.h>

using namespace sitinf;
using namespace sitinf::qt;

ListModelMap::ListModelMap(){

}

void ListModelMap::addModel(std::string name, QAbstractListModel* model){
  m_Models[name] = model;
}


QObject* ListModelMap::model(QString name){
  auto it = m_Models.find(name.toStdString());
  return (QObject*) (it == m_Models.end()) ? NULL : it->second;
}
