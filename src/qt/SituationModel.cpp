/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/SituationModel.cpp                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <qt/SituationModel.h>
#include <utils/Exception.h>
#include <utils/Macros.h>

#define _LOGGER "qt.SituationModel"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

std::string UNOBSERVED = "Unobserved";

using namespace sitinf;
using namespace sitinf::qt;
using namespace sitinf::utils;

SituationModel::SituationModel(QObject *parent)
  : QAbstractListModel(parent), m_LearnState("override")
{
  QHash<int, QByteArray> roles;
  roles[AspectRole] = "model_data_aspect";
  roles[CurrentStateRole] = "model_data_current_state";
  roles[StatesRole] = "model_data_states";
  setRoleNames(roles);
}

void SituationModel::useNetwork(sitinf::network::BayesianNetwork::Ptr network){
  beginRemoveRows(QModelIndex(),0,rowCount()-1);
  m_Aspects.clear();
  endRemoveRows();

  beginInsertRows(QModelIndex(),0,network->variables().size()-1);

  const std::vector<sitinf::network::BayesianNetwork::Variable>& variables = network->variables();
  QStringList states;
  for(uint i = 0; i < variables.size(); ++i){
    const sitinf::network::BayesianNetwork::Variable& variable = variables[i];
    // clear states list
    states.reserve(variable.m_Outcomes.size()+1);
    states.clear();
    // add additional unobserved state
    states.append(QString(UNOBSERVED.c_str()));
    for(std::string outcome : variable.m_Outcomes){
      states.append(QString(outcome.c_str()));
    }
    // create an aspect for this variable
    m_Aspects.push_back(Aspect(
                          QString(variable.m_Name.c_str()),
                          QString(UNOBSERVED.c_str()),
                          states)
                        );
    std::sort(m_Aspects.begin(),m_Aspects.end(),[](const Aspect& a, const Aspect & b) -> bool
    {return a.name() < b.name();});
  }
  m_Result = sitinf::inference::InferenceResult::Ptr(new sitinf::inference::InferenceResult());
  endInsertRows();
}

const sitinf::inference::InferenceResult::Ptr SituationModel::	result()
{
  return m_Result;
}

int SituationModel::rowCount(const QModelIndex& parent) const{
  return m_Aspects.size();
}

QVariant SituationModel::data(const QModelIndex & index, int role) const{
  if (index.row() < 0 || index.row() > (int)m_Aspects.size()){
    return QVariant();
  }

  if (role == AspectRole){
    return QVariant::fromValue(m_Aspects[index.row()].name());
  } else if (role == CurrentStateRole) {
    return QVariant::fromValue(m_Aspects[index.row()].currentState());
  } else if (role == StatesRole) {
    return QVariant::fromValue(m_Aspects[index.row()].states());
  } else {
    return QVariant();
  }
}

QString SituationModel::learnState(){
  return m_LearnState;
}

void SituationModel::setAspectState(const QString& aspect_name, const QString& current_state){
  Aspect& aspect = getAspect(aspect_name);
  if(aspect.currentState() == current_state) return; // ignore not changing sets

  // set state in data model
  aspect.updateCurrentState(current_state);
  // set state in result
  if(current_state.toStdString() == UNOBSERVED){
    // setting to unobserved. remove from result
    m_Result->getOutcomes().erase(aspect.name().toStdString());
  } else {
    // create Outcomes and set as current
    sitinf::inference::InferenceResult::Outcomes outcomes;
    outcomes.m_MaxOutcome = -1;
    for(int i = 0; i < aspect.states().size(); ++i){
      if(aspect.states().at(i).toStdString() == UNOBSERVED){
        // ignore unobserved aspect state because this is
        // not represented directly in the result
      } else if(aspect.states().at(i) == current_state){
        outcomes.m_MaxOutcome = i-1; // 0 should always be unobserved
        outcomes.m_Probabilities.push_back(1.);
      } else {
        outcomes.m_Probabilities.push_back(0.);
      }
    }
    SASSERT_THROW((outcomes.m_MaxOutcome >= 0) && (outcomes.m_MaxOutcome < (uint) aspect.states().size()),
                 Exception,"Unexpected value of max outcome. Maybe it is not set.");
    m_Result->getOutcomes()[aspect.name().toStdString()] = outcomes;
  }
  notify();
}

void SituationModel::notify(){
  for(auto c : m_Callbacks){
    try{
      c();
    } catch (Exception& e){
      STHROW(Exception,"Catched exception from callback function : " << e.what());
    }
  }
}

void SituationModel::setLearnState(const QString& learnState){
  m_LearnState = learnState;
  learnStateChanged();
  notify();
}

void SituationModel::registerStateCallback(boost::function<void()> callback){
  m_Callbacks.push_back(callback);
}

SituationModel::Aspect& SituationModel::getAspect(const QString& name){
  for(Aspect& aspect : m_Aspects){
    if(aspect.name() == name){
      return aspect;
    }
  }
  STHROW(NotFoundException,"Cant find Aspect with name '" << name.toStdString() << "'");
}
