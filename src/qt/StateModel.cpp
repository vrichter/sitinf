/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/StateModel.cpp                                  **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <qt/StateModel.h>
#include <utils/Types.h>
#include <utils/Macros.h>

#define _LOGGER "qt.StateModel"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::qt;

StateModel::StateModel(std::string name, QObject* parent)
  : QAbstractListModel(parent), m_AspectName(name)
{
  QHash<int, QByteArray> roles;
  roles[Name] = "model_name";
  roles[State] = "model_state";
  roles[Probaility] = "model_probability";
  setRoleNames(roles);
  qRegisterMetaType<QModelIndex>("QModelIndex");
}

void StateModel::updateState(const rst::classification::ClassificationResultMap* situation)
{
  // find the right aspect
  for(int i = 0; i < situation->aspects_size(); ++i){
    if(situation->aspects(i).name() == m_AspectName){
      updateState(&situation->aspects(i).result());
      return;
    }
  }
  SLOG_TRACE << "Situation does not contain result for '" << m_AspectName << "'";
}

void StateModel::updateState(std::string name, States state, double confidence){
  auto it = m_IndexMap.find(name);
  if(it == m_IndexMap.end()){
    // unknown result add it
    beginInsertRows(QModelIndex(),m_StateNames.size(), m_StateNames.size());
    m_StateNames.push_back(name);
    m_Probabilities.push_back(confidence);
    m_StateStates.push_back(state);
    m_IndexMap[m_StateNames.back()] = m_StateNames.size()-1;
    endInsertRows();
  } else {
    // update existing result
    m_Probabilities.at(it->second) = confidence;
    dataChanged(index(it->second),index(it->second));
  }
}

void StateModel::updateState(const rst::classification::ClassificationResult* result){
  if(!result){
    SLOG_TRACE << "Passed result is NULL. Ignoring.";
    return;
  }
  // update probabilities from situation
  for(int i = 0; i < result->classes_size(); ++i){
    const rst::classification::ClassificationResult_ClassWithProbability& clazz = result->classes(i);
    updateState(clazz.name(),States::Inactive,clazz.confidence());
  }

  // update stateStates
  for(uint i = 0; i < m_StateNames.size(); ++i){
    if(m_StateStates.at(i) == States::Clamped){
      continue; // clamped always stay clamped
    } else {
      States tmp = m_StateStates.at(i);
      // active when decided class inactive else
      m_StateStates.at(i) = (result->decided_class() == m_StateNames.at(i)) ? States::Active : States::Inactive;
      // call changed if so
      if(tmp != m_StateStates.at(i)){
        dataChanged(index(i),index(i));
      }
    }
  }
}

int StateModel::rowCount(const QModelIndex& parent) const{
  return m_StateNames.size();
}

QVariant StateModel::data(const QModelIndex & index, int role) const{
  if (index.row() < 0 || index.row() > rowCount()){
    return QVariant();
  }
  if (role == Name){
    return QVariant(QString(m_StateNames.at(index.row()).c_str()));
  } else if (role == State) {
    switch  (m_StateStates.at(index.row())){
      case States::Active: return QVariant("Active");
      case States::Inactive: return QVariant("Inactive");
      case States::Clamped: return QVariant("Clamped");
      default: return QVariant();
    }
  } else if (role == Probaility) {
    return QVariant::fromValue(m_Probabilities.at(index.row()));
  } else {
    return QVariant();
  }
}
