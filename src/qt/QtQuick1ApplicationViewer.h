/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/QtQuick1ApplicationViewer.h                     **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <QtDeclarative/QDeclarativeView>

namespace sitinf {
  namespace qt {

    /**
     * @brief The QtQuick1ApplicationViewer class is a default implememtation of QDeclarativeView
     */
    class QtQuick1ApplicationViewer : public QDeclarativeView
    {
      Q_OBJECT

    public:

      /**
       * @brief The ScreenOrientation enum describing the orientation of the screen.
       */
      enum ScreenOrientation {
        ScreenOrientationLockPortrait,
        ScreenOrientationLockLandscape,
        ScreenOrientationAuto
      };

      /**
       * @brief QtQuick1ApplicationViewer constrcts a default instance.
       * @param parent the QObjects parent.
       */
      explicit QtQuick1ApplicationViewer(QWidget *parent = 0);

      /**
       * @brief ~QtQuick1ApplicationViewer virtual destructor for clean destruction.
   */
      virtual ~QtQuick1ApplicationViewer();


      /**
       * @brief create staticly creates an instance of this.
       * @return a new QtQuick1ApplicationViewer instance.
       */
      static QtQuick1ApplicationViewer *create();

      /**
       * @brief setMainQmlFile sets the source qml of this viewer
       * @param file an url to the qml file
       */
      void setMainQmlFile(const QString &file);

      /**
       * @brief addImportPath adds an import path
       * @param path the url of the import path
       */
      void addImportPath(const QString &path);

      /**
       * @brief setOrientation sets the oriantation
       * Note that this will only have an effect on Fremantle.
       * @param orientation
       */
      void setOrientation(ScreenOrientation orientation);

      /**
       * @brief showExpanded maximizes the view if possible
       */
      void showExpanded();
    };

  } // namespace qt
} // namespace sitinf
