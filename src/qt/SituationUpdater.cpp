/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/SituationUpdater.cpp                            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <qt/SituationUpdater.h>

#define _LOGGER "qt.SituationUpdater"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::qt;

SituationUpdater::SituationUpdater(std::string scope)
  : m_SituationReceiver(scope)
{
  qRegisterMetaType<utils::Shared<rst::classification::ClassificationResultMap>::Ptr>(
    "utils::Shared<rst::classification::ClassificationResultMap>::Ptr");
  m_SituationReceiver.addObserver(this);
}

SituationUpdater::~SituationUpdater(){
  m_SituationReceiver.removeObserver(this);
}

void SituationUpdater::update(
    const utils::Subject<const utils::Shared<rst::classification::ClassificationResultMap>::Ptr>* subject,
    utils::Shared<rst::classification::ClassificationResultMap>::Ptr data)
{
  if(subject != &m_SituationReceiver) return;
  io::RsbSituationReceiver::SituationPtr situation = data;
  if(!situation.get()){
    SLOG_TRACE << "Received null object.";
    return;
  }
  SLOG_TRACE << "Received new situation:\n" << situation->DebugString() ;
  notify(situation);
  Q_EMIT situationChanged(situation);
}
