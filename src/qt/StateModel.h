/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/StateModel.h                                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <QAbstractListModel>
#include <QStringList>
#include <network/BayesianNetwork.h>
#include <rst/classification/ClassificationResultMap.pb.h>

namespace sitinf {
  namespace qt {

    /**
     * @brief The StateModel class represents a list of possible states with their corresponding state and probability.
     */
    class StateModel : public QAbstractListModel
    {
      Q_OBJECT
      Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
      Q_PROPERTY(int length READ rowCount NOTIFY countChanged)

    public:
      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<StateModel> Ptr;


      /**
       * @brief The Roles enum used to distinguish the different field types of this model.
       */
      enum Roles {
        /// the states name
        Name = Qt::UserRole + 1,
        /// the state of this state.......,
        State,
        /// the probability of this state
        Probaility
      };

      /**
       * @brief The State enum the different states an aspect state can have
       */
      enum class States {
        /// states curentlx inactivre
        Inactive,
        /// the currently active states
        Active,
        /// the clamped states
        Clamped
      };

      /**
       * @brief StateModel creates a new StateModel.
       * @param name the aspect name of this state model
       * @param parent the parent QObject
       */
      StateModel(std::string name, QObject* parent = 0);

      /**
       * @brief updateState updates the state from a situation object if aspect exists in it.
       * @param situation a situation description
       */
      void updateState(const rst::classification::ClassificationResultMap* situation);

      /**
       * @brief updateState updates the model from a ClassificationResult.
       *
       * Does not check for right Aspect. Adds new states if needed.
       * @param result
       */
      void updateState(const rst::classification::ClassificationResult* result);

      /**
       * @brief updateState updates the model directly from parameters
       * @param name the state name to update
       * @param state its new state
       * @param confidence its confidence as double [0,1]
       */
      void updateState(std::string name, States state, double confidence);

      /**
       * @brief rowCount can be used to check the current amount of elements
       * @param parent is ignored
       * @return the number of internal elements in this model
       */
      int rowCount(const QModelIndex& parent = QModelIndex()) const;

      /**
       * @brief data provides the value of a role at index.
       * @param index should be between 0 and rowCount.
       * @param role should be of type Roles
       * @return the data corresponding to role and index. An empty QVariant if role or index inappropriate.
       */
      QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    Q_SIGNALS:
      /**
       * @brief countChanged a qt signal emitted when the element count changes.
       */
      void countChanged();

    private:
      /**
       * @brief m_AspectName the aspect name of this state model
       */
      std::string m_AspectName;
      /**
       * @brief m_Names the names of the states
       */
      std::vector<std::string> m_StateNames;
      /**
       * @brief m_States the states of the states
       */
      std::vector<States> m_StateStates;
      /**
       * @brief m_Probabilities the probabilities of the states
       */
      std::vector<double> m_Probabilities;
      /**
       * @brief m_IndexMap maps state positions in m_StateNames vector to their names for fast search.
       */
      std::map<std::string,int> m_IndexMap;
    };

  } // namespace qt
} // namespace sitinf
