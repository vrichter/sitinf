/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/AspectModel.h                                   **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <QAbstractListModel>
#include <QStringList>
#include <render/GraphLayout.h>
#include <qt/StateModel.h>
#include <qt/EdgeModel.h>


namespace sitinf {
  namespace qt {

    /**
     * @brief The AspectModel class provides layout and state information of a BayesianNetwork for rendering.
     */
    class AspectModel : public QAbstractListModel
    {
      Q_OBJECT
      Q_PROPERTY(int posX READ posX NOTIFY posXChanged)
      Q_PROPERTY(int posY READ posY NOTIFY posYChanged)
      Q_PROPERTY(int width READ width NOTIFY widthChanged)
      Q_PROPERTY(int height READ height NOTIFY heightChanged)
      Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
      Q_PROPERTY(int length READ rowCount NOTIFY countChanged)

    public:
      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<AspectModel> Ptr;


      /**
     * @brief The Roles enum used to distinguish the different field types of this model.
     */
      enum Roles {
        /// the aspects name
        Name = Qt::UserRole + 1,
        /// the X position of the aspect,
        PosX,
        /// the Y position of the aspect
        PosY
      };

      /**
       * @brief AspectModel constuctor creates default instance.
       * @param parent the parent of this AspectModel
       */
      AspectModel(QObject* parent = 0);

      /**
       * @brief posX the horizontal position of the aspect model
       * @return upper right bound of the bounding box
       */
      int posX();

      /**
       * @brief posY the vertical position of the aspect model
       * @return upper right bound of the bounding box
       */
      int posY();

      /**
       * @brief width the width of the bounding box
       * @return >= 0
       */
      int width();

      /**
       * @brief height the height of the bounding box
       * @return >= 0
       */
      int height();

      /**
       * @brief rowCount can be used to check the current amount of elements
       * @param parent is ignored
       * @return the number of internal elements in this model
       */
      int rowCount(const QModelIndex& parent = QModelIndex()) const;

      /**
       * @brief data provides the value of a role at index.
       * @param index should be between 0 and rowCount.
       * @param role should be of type Roles
       * @return the data corresponding to role and index. An empty QVariant if role or index inappropriate.
       */
      QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

      /**
        * @brief stateModel provides access to state model of this AspectModel.
        * @param sname the name of the StateModel
        * @return a QObject pointer to the corresponding StateModel (child of this) or null if unknown.
        */
      Q_INVOKABLE QObject* stateModel(QString sname);

      /**
        * @brief edgeModel provides access to the EdgeModel of this AspectModel
        * @return a QObject pointer to the internal an EdgeModel
        */
      Q_INVOKABLE QObject* edgeModel();

      /**
        * @brief layoutTypes accessor to the layouting algotithm types this AspectModel can use.
        * @return a list of Layout types
        */
      Q_INVOKABLE QStringList layoutTypes();

    Q_SIGNALS:
      /**
       * @brief countChanged a qt signal emitted when the element count changes.
       */
      void countChanged();

      /**
       * @brief posXChanged emitted every time the xPosition of this AspectModel changes
       */
      void posXChanged();

      /**
       * @brief posYChanged emitted every time the yPosition of this AspectModel changes
       */
      void posYChanged();

      /**
       * @brief posYChanged emitted every time the width of this AspectModel changes
       */
      void widthChanged();

      /**
       * @brief posYChanged emitted every time the height of this AspectModel changes
       */
      void heightChanged();

    public Q_SLOTS:

      /**
       * @brief updateStates updates the internal state model from the passed situation
       * @param situation when situation contains unknown states they will be added
       */
      void updateStates(const utils::Shared<rst::classification::ClassificationResultMap>::Ptr situation);

      /**
       * @brief updateNetwork changes the internal network for the passed. recalculates the layout.
       * @param network
       */
      void updateNetwork(const utils::Shared<rst::bayesnetwork::BayesNetwork>::Ptr network);

      /**
       * @brief useLayoutType changes the used layout type. recalculates the layout.
       * @param layout_type must be element of layoutTypes().
       */
      void useLayoutType(const QString layout_type);

    private:

      /**
       * @brief recreateModel internally used to update layout and add missing StateModels from current network.
       */
      void recreateModel();

      /**
       * @brief m_GraphPosX horizontal position of the upper left corner of surrounding bounding box
       */
      int m_GraphPosX;

      /**
       * @brief m_GraphPosY verticat position of the upper left corner of surrounding bounding box
       */
      int m_GraphPosY;

      /**
       * @brief m_GraphWidth width of the bounding box
       */
      int m_GraphWidth;

      /**
       * @brief m_GraphHeight height of the bounding box
       */
      int m_GraphHeight;

      /**
       * @brief m_Network the currently used Network.
       */
      utils::Shared<rst::bayesnetwork::BayesNetwork>::Ptr m_Network;

      /**
       * @brief m_LayoutTypes a list of different layouting algorithms which can be used.
       */
      QString m_LayoutType;

      /**
       * @brief m_LayoutTypes list of available layout types
       */
      QStringList m_LayoutTypes;

      /**
       * @brief m_Names the aspect names
       */
      QStringList m_Names;

      /**
       * @brief m_PosX the x positions of the aspects
       */
      std::vector<int> m_PosX;

      /**
       * @brief m_PosY the y positions of the aspects
       */
      std::vector<int> m_PosY;

      /**
       * @brief m_EdgeModel the edge model of this AspectModel
       */
      EdgeModel m_EdgeModel;

      /**
       * @brief m_Models holds state models for every aspect
       */
      std::map<std::string,StateModel::Ptr> m_StateModels;

    };

  } // namespace qt
} // namespace sitinf
