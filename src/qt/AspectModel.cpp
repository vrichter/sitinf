/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/AspectModel.cpp                                 **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <qt/AspectModel.h>
#include <QStringList>
#include <utils/Exception.h>
#include <QtDeclarative/qdeclarative.h>
#include <utils/Macros.h>

#define _LOGGER "qt.AspectModel"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::qt;
using namespace sitinf::render;
using namespace sitinf::utils;

AspectModel::AspectModel(QObject* parent)
  : QAbstractListModel(parent), m_EdgeModel(this)
{
  qmlRegisterType<EdgeModel>();
  qmlRegisterType<StateModel>();
  QHash<int, QByteArray> roles;
  roles[Name] = "model_name";
  roles[PosX] = "model_position_x";
  roles[PosY] = "model_position_y";
  setRoleNames(roles);

  // set size from graph
  m_GraphPosX = 0;
  m_GraphPosY = 0;
  m_GraphHeight = 640;
  m_GraphWidth = 480;
  for(auto layout : GraphLayout::layoutTypes()){
    m_LayoutTypes.append(QString(layout.c_str()));
  }
  m_LayoutType = m_LayoutTypes.first();
}

void AspectModel::updateNetwork(const Shared<rst::bayesnetwork::BayesNetwork>::Ptr network){
  m_Network = network;
  recreateModel();
}

void AspectModel::useLayoutType(const QString layout_type){
  m_LayoutType = layout_type;
  if(m_Network.get()) recreateModel();
}

void AspectModel::recreateModel(){
  GraphLayout layout(m_Network.get(),m_LayoutType.toStdString());
  SLOG_TRACE << "updating layout";
  beginResetModel();
  // clear elements
  beginRemoveRows(QModelIndex(),0,rowCount()-1);
  m_Names.clear(); m_PosX.clear(); m_PosY.clear();
  endRemoveRows();

  // add state models where not existing
  for(int i = 0; i < m_Network->variable_size(); ++i){
    std::string name = m_Network->variable(i).name();
    if(m_StateModels.find(name) == m_StateModels.end()){
      // unknown/new aspect add
      SLOG_TRACE << "adding state model " << name;
      m_StateModels[name] = StateModel::Ptr(new StateModel(name,this));
    }
    // either way add states to model
    for(int j = 0; j < m_Network->variable(i).outcomes_size(); ++j){
      m_StateModels[name]->updateState(
            m_Network->variable(i).outcomes(j),
            StateModel::States::Inactive,
            0.
            );
    }
  }

  // set size from graph
  m_GraphPosX = layout.boundingBox().position().x();
  Q_EMIT posXChanged();
  m_GraphPosY = layout.boundingBox().position().y();
  Q_EMIT posYChanged();
  m_GraphHeight = layout.boundingBox().height();
  Q_EMIT heightChanged();
  m_GraphWidth = layout.boundingBox().width();
  Q_EMIT widthChanged();
  //DEBUG_LOG("Graph: x = " << m_GraphPosX << " y = " << m_GraphPosY << " width = " << m_GraphWidth << " height = " << m_GraphHeight);

  // fill with new data from layout
  beginInsertRows(QModelIndex(),0,layout.positions().size()-1);
  for(auto it : layout.positions()){
    m_Names.append(QString(it.first.c_str()));
    m_PosX.push_back(it.second.x());
    m_PosY.push_back(it.second.y());
    //DEBUG_LOG("Node '" << m_Names.back().toStdString() << "' x = " << m_PosX.back() << " y = " << m_PosY.back());
  }
  endInsertRows();

  // fill the new data into the EdgeModel
  m_EdgeModel.reset();
  for(auto node : layout.edges()){
    for(auto edge : node.second){
      m_EdgeModel.addEdge(edge);
    }
  }
  endResetModel();
}

void AspectModel::updateStates(const Shared<rst::classification::ClassificationResultMap>::Ptr situation){
  SASSERT_THROW(situation!=NULL,NullPointerException,"Passed situation is NULL");
  SLOG_TRACE << "updating states ";
  for(int i  = 0; i < situation->aspects_size(); ++i){
    std::string name = situation->aspects(i).name();
    if(m_StateModels.find(name) == m_StateModels.end()){
      // unknown/new aspect add
      SLOG_TRACE << "adding state model " << name;
      m_StateModels[name] = StateModel::Ptr(new StateModel(name));
    }
    //SLOG_TRACE << "updating state model: " << name << " - " << &situation->aspects(i).state();
    // either way update it.
    m_StateModels[name]->updateState(&situation->aspects(i).result());
  }
}

QObject* AspectModel::stateModel(QString name){
  //DEBUG_LOG("getting state " << name .toStdString());
  if(m_StateModels.find(name.toStdString()) == m_StateModels.end()){
    m_StateModels[name.toStdString()] = StateModel::Ptr(new StateModel(name.toStdString()));
  }
  return (QObject*) m_StateModels[name.toStdString()].get();
}

QStringList AspectModel::layoutTypes(){
  return m_LayoutTypes;
}

QObject* AspectModel::edgeModel(){
  return (QObject*) &m_EdgeModel;
}

int AspectModel::posX() {
  return m_GraphPosX;
}

int AspectModel::posY(){
  return m_GraphPosY;
}

int AspectModel::width(){
  return m_GraphWidth;
}

int AspectModel::height(){
  return m_GraphHeight;
}

int AspectModel::rowCount(const QModelIndex& parent) const{
  return m_Names.size();
}

QVariant AspectModel::data(const QModelIndex & index, int role) const{
  if (index.row() < 0 || index.row() > rowCount()){
    return QVariant();
  }
  if (role == Name){
    return QVariant::fromValue(m_Names.at(index.row()));
  } else if (role == PosX) {
    return QVariant::fromValue(m_PosX.at(index.row()));
  } else if (role == PosY) {
    return QVariant::fromValue(m_PosY.at(index.row()));
  } else {
    return QVariant();
  }
}
