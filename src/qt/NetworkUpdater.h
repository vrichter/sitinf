/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/NetworkUpdater.h                                **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <QObject>
#include <io/RsbNetworkReceiver.h>

namespace sitinf {
  namespace qt {

    /**
     * @brief The NetworkUpdater class signals networkChanged every time a remote Network changes.
     * This is used to synchronize the application flow with QT-specific threading.
     */
    class NetworkUpdater :
        public QObject,
        public utils::Observer<utils::Shared<rst::bayesnetwork::BayesNetwork>::Ptr>,
        public utils::Subject<utils::Shared<rst::bayesnetwork::BayesNetwork>::Ptr>
    {
      Q_OBJECT
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<NetworkUpdater> Ptr;

      /**
       * @brief NetworkUpdater creates an instance holding and observing an RsbNetworkReceiver
       * @param scope the scope to listen to for Network information
       * @param parent the parent QObject
       */
      NetworkUpdater(const std::string& scope, QObject* parent = 0);

      /**
       * @brief Cleans up deregistering from Network
       */
      ~NetworkUpdater();

      /**
       * @brief update Observer update implementation. Gets neetwork from Subject and notifies change.
       * @param subject the subject providing data
       * @param data the new data from subject
       */
      void update(const utils::Subject<utils::Shared<rst::bayesnetwork::BayesNetwork>::Ptr>* subject, utils::Shared<rst::bayesnetwork::BayesNetwork>::Ptr data);

    Q_SIGNALS:
      /**
       * @brief networkChanged signal is emitted every time the Network changes
       * @param network the new network description.
       */
      void networkChanged(utils::Shared<rst::bayesnetwork::BayesNetwork>::Ptr network);

    private:
      /**
       * @brief m_NetworkReceiver the observed Network
       */
      io::RsbNetworkReceiver m_NetworkReceiver;

    };

  } // namespace qt
} // namespace sitinf
