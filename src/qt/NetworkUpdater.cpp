/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/qt/NetworkUpdater.cpp                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <qt/NetworkUpdater.h>
#include <QAbstractListModel>

using namespace sitinf;
using namespace sitinf::qt;
using namespace sitinf::utils;

#define _LOGGER "qt.NetworkUpdater"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

NetworkUpdater::NetworkUpdater(const std::string& scope, QObject* parent)
  : QObject(parent), m_NetworkReceiver(scope)
{
  m_NetworkReceiver.addObserver(this);
  qRegisterMetaType<Shared<rst::bayesnetwork::BayesNetwork>::Ptr>("utils::Shared<rst::bayesnetwork::BayesNetwork>::Ptr");
}

NetworkUpdater::~NetworkUpdater(){
  m_NetworkReceiver.removeObserver(this);
}

void NetworkUpdater::update(const Subject<Shared<rst::bayesnetwork::BayesNetwork>::Ptr> *subject,
                            Shared<rst::bayesnetwork::BayesNetwork>::Ptr data){
  if(subject != &m_NetworkReceiver) return;
  Shared<rst::bayesnetwork::BayesNetwork>::Ptr network = data;
  if(!network.get()) {
    SLOG_TRACE << "Received null object.";
    return;
  }
  SLOG_TRACE << "Received new network:\\n" << network->DebugString() ;
  notify(network);
  Q_EMIT networkChanged(network);
}
