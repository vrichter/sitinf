/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/Logging.h                                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

// logging to console should go to cerr
#ifndef ELPP_CUSTOM_COUT
  #define ELPP_CUSTOM_COUT std::cerr
#endif

#define ELPP_NO_DEFAULT_LOG_FILE
#define _ELPP_THREAD_SAFE

#include <utils/easylogging++.h>

// define own logging macros
#define SLOG_TRACE LOG(TRACE)
#define SLOG_DEBUG LOG(DEBUG)
#define SLOG_FATAL LOG(FATAL)
#define SLOG_ERROR LOG(ERROR)
#define SLOG_WARNING LOG(WARNING)
#define SLOG_INFO LOG(INFO)
#define SLOG_VERBOSE LOG(VERBOSE)
#define SLOG_UNKNOWN LOG(UNKNOWN)

// define application logging setup macros

#define SITINF_LOGGING_CONFIGURE_LOGGING(x) \
  LoggingConfiguration::instance()->configure(x["default-log-file"].as<std::string>(), \
      x["logging-configuration"].as<std::string>()); \

#define SITINF_PRINT_OPTIONS(vm,desc) \
if (vm.count("help,h")) {\
  std::cout << desc << "\n";\
  return 1;\
}\

#define SITINF_PROGRAM_OPTIONS(argc,argv,description,options) \
  boost::program_options::variables_map program_options;\
{\
  std::stringstream description_text;\
  description_text << description << "\n\n" << "Allowed options";\
  boost::program_options::options_description desc(description_text.str());\
  desc.add_options()\
      ("help,h","produce help message")\
\
      options\
\
      ("logging-configuration",\
       boost::program_options::value<std::string>()->default_value(""),\
       "Can be used to set up a logging configuration directly.")\
\
      ("default-log-file",\
       boost::program_options::value<std::string>()->default_value(""),\
       "Can be used to overwrite the default file logging information is written to.")\
\
      ;\
\
  try {\
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), program_options);\
    boost::program_options::notify(program_options);\
\
    sitinf::utils::LoggingConfiguration::instance()->configure(program_options["default-log-file"].as<std::string>(), \
    program_options["logging-configuration"].as<std::string>()); \
\
    std::stringstream arguments;\
    for(int i = 0; i < argc; ++i){\
      arguments << argv[i] << " ";\
    }\
    SLOG_INFO << "Program started with line: " << arguments.str();\
\
    if (program_options.count("help")) {\
      std::cout << desc << "\n";\
      return 1;\
    }\
\
  } catch (boost::program_options::error& e) {\
    sitinf::utils::LoggingConfiguration::instance()->configure();\
    std::stringstream arguments;\
    for(int i = 0; i < argc; ++i){\
      arguments << argv[i] << " ";\
    }\
    SLOG_ERROR << "Could not parse program options: Line: " << arguments.str() << " Error: " << e.what();\
    std::cout << "Could not parse program options: " << e.what();\
    std::cout << "\n\n" << desc << "\n";\
    return 1;\
  }\
\
\
}\


namespace sitinf {
  namespace utils {
    class LoggingConfiguration {

    public:
      static std::string defaultLogFile();

      static LoggingConfiguration* instance();
      static const el::Logger* getLogger(const std::string& name);
      void configure(const std::string& default_log_file = "",
                     const std::string& config_path = "",
                     const std::string& env_name_home = "HOME",
                     const std::string& env_name_config_path = "SITINF_LOGGING_CONF",
                     const std::string& default_config_name = "sitinf_logging.conf");

      ~LoggingConfiguration();
    private:
      LoggingConfiguration();
      el::Configurations configurations;
    };

  } // namespace utils
} // namespace sitinf


