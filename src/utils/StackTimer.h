/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/StackTimer.h                                 **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <string>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace sitinf {
  namespace utils {

    /**
     * @brief The StackTimer class a simple implementation of time measurement.
     *
     * On destruction an instance of this class will print its lifespan in microseconds.
     */
    class StackTimer
    {
    public:

      /**
       * @brief StackTimer creates an instance of StackTimer saving the time of creation
       * @param name the name if this timer
       */
      StackTimer(std::string name);

      /**
        * @brief prints the amount of microseconds between creation and destruction and frees all resources.
        */
      ~StackTimer();

    private:
      /**
       * @brief m_Name the name of the timer
       */
      std::string m_Name;
      /**
       * @brief m_Start the creation time of the timer.
       */
      boost::posix_time::ptime m_Start;
    };

  } // namespace utils
} // namespace sitinf
