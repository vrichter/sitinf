/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/Exception.h                                  **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <exception>
#include <stdexcept>
#include <string>
#include <sstream>

namespace sitinf {
  namespace utils {

    /**
     * @brief The Exception class is the parent of all Exceptions thrown by sitinf classes.
     */
    class Exception : public std::exception {
    public:
      /**
       * @brief A default constructor from a char array.
       *        Saves the error message in a member variable.
       * @param error the error message to be returned from what()
       */
      Exception(const char* error);

      /**
       * @brief A default constructor from a std::string.
       *        Saves the error message in a member variable.
       * @param error the error message to be returned from what()
       */
      Exception(const std::string error);

      /**
       * A virtual destructor to be reimplemented in subclasses.
       */
      virtual ~Exception() throw();

      /**
       * @brief returns the error message of this Exception
       * @return a message describing the reason why this Exception was thrown.
       */
      virtual const char* what() const throw();

      /**
       * @brief setError overrides the internally saved error.
       * @param error the new error value.
       */
      virtual void setError(const std::string& error) throw();

    private:
      std::string error;
    };

   template<typename T>
    class UniqueIdException : public Exception {
      public:
        UniqueIdException(T id) : Exception("") {
          std::stringstream message;
          message << "Multiple use of unique id '" << id << "'.";
          setError(message.str());
        }
    };

#define SITINF_DEFINE_EXCEPTION(classname) \
  class classname : public Exception { \
  public: \
  classname(const char* error) : Exception(error) {} \
  classname(const std::string error) : Exception(error) {}\
  };

    // define all exception types
    SITINF_DEFINE_EXCEPTION(IOException)
    SITINF_DEFINE_EXCEPTION(NullPointerException)
    SITINF_DEFINE_EXCEPTION(NetworkInconsistentException)
    SITINF_DEFINE_EXCEPTION(ConfigurationException)
    SITINF_DEFINE_EXCEPTION(InconsistentParameterException)
    SITINF_DEFINE_EXCEPTION(InconsistentSensorException)
    SITINF_DEFINE_EXCEPTION(IllegalArgumentException)
    SITINF_DEFINE_EXCEPTION(NotFoundException)
    SITINF_DEFINE_EXCEPTION(UnexpectedException)
    SITINF_DEFINE_EXCEPTION(NotImplementedException)
    SITINF_DEFINE_EXCEPTION(GraphEmptyException)
    SITINF_DEFINE_EXCEPTION(GraphNotLayedOutException)
    SITINF_DEFINE_EXCEPTION(ParsingException)
    SITINF_DEFINE_EXCEPTION(EOFException)
    SITINF_DEFINE_EXCEPTION(CurrentlyNoDataAvailableException)
    SITINF_DEFINE_EXCEPTION(NoMoreDataAvailableException)
    SITINF_DEFINE_EXCEPTION(NumericException)
    SITINF_DEFINE_EXCEPTION(StateException)
    SITINF_DEFINE_EXCEPTION(UniqueFactoryFunctionNameException)

#undef SITINF_DEFINE_EXCEPTION

  } // namespace utils
} // namespace sitinf
