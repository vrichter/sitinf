/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/TimedVector.h                                **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Subject.h>
#include <utils/NotifyThread.h>

namespace sitinf {
namespace utils {

  /**
   * @brief The TimedValue stores a T and a corresponding timestamp.
   *
   * @tparam T The type of Data this struct stores.
   */
  template<typename T>
  struct TimedValue {
    T value;
    boost::posix_time::ptime timestamp;

    /**
     * @brief TimedValue creates a TimedValue with timestamp set to now.
     * @param from is copied into the value member.
     */
    TimedValue(T from) : value(from), timestamp(boost::get_system_time()) {}
  };

  /**
   * @brief The TimedVector holds its elements only for a certain time and notifies all changes.
   *
   * An added T will be removed after max_age has passed. The insertion is notified
   * as well as the deletion. The notification carries a copy of the whole inner vector.
   *
   */
  template<typename T>
  class TimedVector : public Subject<const std::vector<T>&>  {
  public:

    /**
     * @brief TimedVector creates empty TimedVector.
     * @param max_age The duration after which new elements are deleted.
     */
    TimedVector(boost::posix_time::time_duration max_age)
      : m_MaxAge(max_age),
        m_Thread([this] () { this -> removeOldValues(); })
    {}

    /**
     * @brief insert adds a new element. The change is notified.
     * @param value
     */
    void insert(T value){
      utils::RecursiveLocker (m_Lock);
      m_Values.push_back(TimedValue<T>(value));
      m_Thread.call_in(0);
    }

    /**
     * @brief values access a vector of all currently valid elements.
     * @return may be empty. same order as insertions.
     */
    std::vector<T> values(){
      utils::RecursiveLocker (m_Lock);
      std::vector<T> result;
      result.reserve(m_Values.size());
      for(auto val : m_Values){
        result.push_back(val.value);
      }
      return result;
    }

  private:
    void removeOldValues(){
      utils::RecursiveLocker lock(m_Lock);
      auto timestamp = boost::get_system_time();
      while(m_Values.size() > 0
            && (timestamp - m_Values.front().timestamp) > m_MaxAge)
      {
        m_Values.pop_front();
      }
      this->notify(values());
      if(m_Values.size()){
        m_Thread.call_at(m_Values.front().timestamp + m_MaxAge);
      } else {
        m_Thread.stop();
      }
    }

  private:
      utils::RecursiveMutex m_Lock;
      boost::posix_time::time_duration m_MaxAge;
      std::deque<utils::TimedValue<T>> m_Values;
      utils::NotifyThread m_Thread;
  };

} // namespace utils
} // namespace sitinf
