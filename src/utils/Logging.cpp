/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/Logging.cpp                                  **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/File.h>
#include <utils/Exception.h>
#include <utils/Types.h>
#include <utils/Macros.h>

#define _ELPP_STACKTRACE_ON_CRASH
#define _ELPP_DEFAULT_LOG_FILE "/dev/null"
#define _LOGGER "utils.Logging"
#include <utils/Logging.h>

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;

static const el::Logger* _logger = LoggingConfiguration::getLogger(_LOGGER);
el::base::debug::CrashHandler elCrashHandler(_ELPP_USE_DEF_CRASH_HANDLER);

std::string localConfigPath(const std::string& LOG_CONFIG){
  return std::string("./") + LOG_CONFIG;
}

std::string homeConfigPath(const std::string& HOME, const std::string& LOG_CONFIG){
  std::string home = std::getenv(HOME.c_str());
  return home + std::string("/") + LOG_CONFIG;
}

bool fileExists(const std::string& name){
  try{
    File local(name,false,true,File::regular);
    return true;
  } catch (IOException& e){
    // file does not exist or is not a regular file
    return false;
  }
}

LoggingConfiguration* LoggingConfiguration::instance(){
  static LoggingConfiguration config;
  return &config;
}

std::string LoggingConfiguration::defaultLogFile(){
  return "/dev/null";
}

const el::Logger* LoggingConfiguration::getLogger(const std::string &name){
  LoggingConfiguration::instance();
  el::Logger* logger = el::Loggers::getLogger(name);
  SLOG_TRACE << "Logger created '" << name << "'";
  return logger;
}

void rolloutHandler(const char* filename, std::size_t size) {
  // ! can't do logging in here. because logger is locked.
  std::cout << "called rollout handler with " << filename << " because it got bigger than " << size << std::endl;
  static Mutex mutex;
  static std::map<std::string,uint> index_map;
  Locker lock(mutex);
  // check and init/increment index for filename
  std::string nameString(filename);
  uint index = 0;
  if(index_map.find(nameString) == index_map.end()){
    index = index_map[nameString] = 0;
  } else {
    index = index_map[nameString]++;
  }

  // force a maximum of 10 backup logs. overwrite in a loop.
  if(index > 9){
    index = 0;
  }

  // zip log file and move to filename_index.gz (dirty commandline calls)
  std::stringstream ss;
  ss << "gzip " << filename;
  int result = system(ss.str().c_str());
  if(result != 0){
    SLOG_WARNING << "error while zipping log file '" << filename << "'. return value: " << result;
  }
  ss.str(std::string());
  ss.clear();
  ss << "mv " << filename << ".gz " << filename << "_" <<  index << ".gz";
  result = std::system(ss.str().c_str());
  if(result != 0){
    SLOG_WARNING << "error while moving zipped file to '" << ss.str() << "'. return value: " << result;
  }
}

LoggingConfiguration::~LoggingConfiguration(){
  el::Helpers::uninstallPreRollOutCallback();
}

LoggingConfiguration::LoggingConfiguration()
{
#if _ELPP_ASYNC_LOGGING
  el::Helpers::setStorage(el::base::type::StoragePointer(new el::base::Storage(el::LogBuilderPtr(new el::base::DefaultLogBuilder()), new el::base::AsyncDispatchWorker())));
#else
  el::Helpers::setStorage(el::base::type::StoragePointer(new el::base::Storage(el::LogBuilderPtr(new el::base::DefaultLogBuilder()))));
#endif  // _ELPP_ASYNC_LOGGING
  // init logger Storage. This must happen before loggers are created and used.
  el::Loggers::getLogger(_LOGGER);
  configurations.setToDefault();
  // default config. used when no configuration from file is available. print everything to std::out.
  configurations.set(el::Level::Global, el::ConfigurationType::Format,  "%datetime %logger [(%fbase:%line) [ %level]:  %msg");
  configurations.set(el::Level::Global, el::ConfigurationType::Enabled, "false");
  configurations.set(el::Level::Global, el::ConfigurationType::ToStandardOutput, "false");
  configurations.set(el::Level::Global, el::ConfigurationType::ToFile, "false");
  ::el::Loggers::setDefaultConfigurations(configurations, true);
  //SLOG_INFO << "Default logging configuration loaded.";
  el::Loggers::addFlag(el::LoggingFlag::StrictLogFileSizeCheck);
  el::Helpers::installPreRollOutCallback(rolloutHandler);
}

void LoggingConfiguration::configure(const std::string& default_log_file,
                                             const std::string& config_path,
                                             const std::string& env_name_home,
                                             const std::string& env_name_config_path,
                                             const std::string& default_config_name)
{
  // try to load configuration from environment -> working directory -> home
  char* envPath = std::getenv(env_name_config_path.c_str());
  // check which path is to use
  std::string path = "";
  std::string hint;
  if(!config_path.empty() && fileExists(config_path)){
     path = config_path;
     hint = "passed configuration path '" + config_path + "'";
  } else if(envPath && !std::string(envPath).empty() && fileExists(std::string(envPath))) {
    path = envPath;
    hint = "path defined in '" + std::string(env_name_config_path) + "'";
  } else if(fileExists(localConfigPath(default_config_name))){
    path = localConfigPath(default_config_name).c_str();
    hint = "working directory.";
  } else if(fileExists(homeConfigPath(env_name_home,default_config_name))){
    path = homeConfigPath(env_name_home,default_config_name).c_str();
    hint = "home directory.";
  }

  // set some flags
  el::Loggers::addFlag(el::LoggingFlag::ColoredTerminalOutput);
  el::Loggers::addFlag(el::LoggingFlag::LogDetailedCrashReason);
  el::Loggers::addFlag(el::LoggingFlag::StrictLogFileSizeCheck);

  // override default output file with passed name. Forces all loggers to use default_log_file
  int n = 2;
  const char* args[2];
  std::string name = "main"; // is ignored
  std::string param = "--default-log-file=";
  param += (default_log_file.empty()) ? std::string("/dev/null/") : default_log_file;
  args[0] = name.c_str();
  args[1] = param.c_str();

  ::el::Helpers::setArgs(n,args);

  if (!path.empty()) {
    ::el::Loggers::configureFromGlobal(path.c_str());
    /*
     * Set default configuration to default from file. This overrides existing configurations with the default.
     * This is needed to set previously created loggers to the configurations default settings.
     * On the other hand this resets all logger configurations from the config file to default.
     */
    ::el::Loggers::setDefaultConfigurations(*el::Loggers::getLogger("default")->configurations(), true);
    /*
     * By loading the configuration from file again, previously overridden special logger configurations
     * are restored.
     */
    ::el::Loggers::configureFromGlobal(path.c_str());

    SLOG_INFO << "Using configuration from " << hint;
  } else {
    // keep empty configuration...
    /* Levels are:
      Trace 	Information that can be useful to back-trace certain events - mostly useful than debug logs.
      Debug 	Informational events most useful for developers to debug application. Only applicable if NDEBUG is not defined (for non-VC++) or _DEBUG is defined (for VC++).
      Fatal 	Very severe error event that will presumably lead the application to abort.
      Error 	Error information but will continue application to keep running.
      Warning 	Information representing errors in application but application will keep running.
      Info 	Mainly useful to represent current progress of application.
      Verbose 	Information that can be highly useful and vary with verbose logging level. Verbose logging is not applicable to hierarchical logging.
      Unknown Only applicable to hierarchical logging and is used to turn off logging completely.
    */
    SLOG_INFO << "Configuration not found. Using default logging configuration for all loggers.";
  }

  SLOG_INFO << "Logger configuration complete.";
}

