/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/NonCopyable.h                                **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

namespace sitinf {
  namespace utils {

    /**
     * @brief The NonCopyable class prevents copy and move constructors and should be
     * inherited by polymorphic types.
     */
    class NonCopyable {

    public:

      /**
       * @brief NonCopyable creates an new instance.
       *
       * Default constuctor is okay.
       */
      NonCopyable() = default;

      /**
       * @brief ~NonCopyable virtual destructor for nice cleanup.
       */
      virtual ~NonCopyable() = default;

    private:

      // all other constuctors are prohibited by making them private.

      /**
       * @brief NonCopyable private copy constructor. Hidden for polymorph classes.
       * @param other
       */
      NonCopyable (const NonCopyable& other) = delete;

      /**
       * @brief NonCopyable move constructor. Hidden for polymorph classes.
       * @param other
       */
      NonCopyable (NonCopyable&& other) = delete;

      /**
       * @brief operator = Copy assignment. Hidden for polymorph classes.
       * @param other
       * @return
       */
      NonCopyable& operator= (const NonCopyable& other) = delete;

      /**
       * @brief operator = move assignment. Hidden for polymorph classes.
       * @param other
       * @return
       */
      NonCopyable& operator= (NonCopyable&& other) = delete;

    };

  } // namespace utils
} // namespace sitinf
