/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/YamlUtils.h                                  **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Types.h>
#include <utils/Exception.h>

#include <yaml-cpp/yaml.h>

namespace sitinf {
  namespace utils {
    namespace yaml {

    template<typename T>
    T read(YAML::Node node, const std::string name, T fallback){
      if(node[name]){
        try {
          return node[name].as<T>();
        } catch (YAML::Exception& e){
          throw ParsingException("Can not read field '" + name + "' from node. Error: " + e.what());
        }
      } else {
        return fallback;
      }
    }

    template<typename T>
    T read(YAML::Node node, const std::string name){
      if(node[name]){
        try {
          return node[name].as<T>();
        } catch (YAML::Exception& e){
          throw ParsingException("Can not read field '" + name + "' from node. Error: " + e.what());
        }
      } else {
        throw ParsingException("Missing required field '" + name + "' in node.");
      }
    }

    } // namespace yaml
  } // namespace utils
} // namespace sitinf
