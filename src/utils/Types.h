/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/Types.h                                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

// this header is used to define some types

#include <complex>
#include <iostream>
#include <boost/thread.hpp>
#include <utils/StackTimer.h>
#include <boost/shared_ptr.hpp>
#include <boost/optional.hpp>

namespace sitinf{
  namespace utils {

    /**
     * @brief real internal floating point data type
     */
    typedef double real;

    /**
     * @brief complex internal complex data type
     */
    typedef std::complex<real> complex;

    /**
     * @brief uint internal unsigned integral data type
     */
    typedef unsigned int uint;

    /**
     * @brief sint internal signed integral data type
     */
    typedef signed int sint;

    /**
     * @brief ulong internal integral data type of
     */
    typedef uint64_t uint64;

    /**
     * @brief slong internal signed long integral data type
     */
    typedef int64_t sint64;

    /**
     * @brief Mutex the Mutex type used in this project
     */
    typedef boost::mutex Mutex;

    /**
     * @brief RecursiveMutex the recursive mutex type used in this project
     */
    typedef boost::recursive_mutex RecursiveMutex;

    /**
     * @brief Locker for the used mutex
     */
    typedef boost::unique_lock<Mutex> Locker;

    /**
     * @brief RecursiveLocker for the used recursive mutex
     */
    typedef boost::unique_lock<RecursiveMutex> RecursiveLocker;

    /**
     * @brief Thread used thread type
     */
    typedef boost::thread Thread;

    /**
     * @brief milliseconds
     */
    typedef boost::posix_time::milliseconds milliseconds;

    /**
     * @brief system_time
     */
    typedef boost::system_time system_time;

    /**
     * A struct to get a template alias for shared ptrs.
     */
    template<class T>
    struct Shared
    {
      typedef boost::shared_ptr<T> Ptr;
    };

    /**
     * A struct to get a template alias for unique ptrs.
     */
    template<class T>
    struct Unique
    {
      typedef std::unique_ptr<T> Ptr;
    };

    /**
     * A struct to get a template alias for weak ptrs.
     */
    template<class T>
    struct Weak
    {
      typedef boost::weak_ptr<T> Ptr;
    };

    template<typename T>
    struct Optional : public boost::optional<T>{};

  } // namespace utils
} // namespace sitinf
