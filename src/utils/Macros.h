/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/Macros.h                                     **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

// this header is used to define some macros.
// please do not use it in other headers.

#include <iostream>
#include <utils/StackTimer.h>

// exits the application after printing error
#define SNOT_IMPLEMENTED SLOG_FATAL

// throws exception x with streamed error y
#define STHROW(x,y)\
{ \
  std::stringstream s;\
  s << y;\
  SLOG_TRACE << "Throwing exception with message: " << s.str();\
  throw x(s.str());\
}

// throws x(y) if first b is false
#define SASSERT_THROW(b,x,y){if(!(b)){STHROW(x,y)}}

// returns if b is false
#define SASSERT_RETURN(b){if(!(b)){return;}}

// returns r if b is false
#define SASSERT_RETURN_DATA(b,r){if(!(b)){return r;}}
#define SASSERT_RETURN_FALSE(b){if(!(b)){return false;}}
#define SASSERT_RETURN_TRUE(b){if(!(b)){return true;}}
#define SASSERT_CONTINUE(b){if(!(b)){continue;}}

// tries x. catches everthing and rethrows it
#define STRY_RETHROW(x)\
{ \
  try { \
    x; \
  } catch (const sitinf::utils::Exception& e){ \
    STHROW(sitinf::utils::Exception,"Rethrowing " << e.what()); \
  } catch (const std::exception& e){ \
    STHROW(sitinf::utils::Exception,"Rethrowing " << e.what()); \
  } catch (...) { \
    STHROW(sitinf::utils::Exception,"Rethrowing unknown exception."); \
  }\
}

// tries x. catches everthing and rethrows it with a message
#define STRY_RETHROW_MESSAGE(x,m)\
{ \
  try { \
    x; \
  } catch (const sitinf::utils::Exception& e){ \
    STHROW(sitinf::utils::Exception, m << " Error: " << e.what()); \
  } catch (const std::exception& e){ \
    STHROW(sitinf::utils::Exception, m << " Error: " << e.what()); \
  } catch (...) { \
    STHROW(sitinf::utils::Exception,"Rethrowing unknown exception."); \
  }\
}

// tries x prints y on exception
#define STRY_PRINT(x,y)\
{ \
  try { \
    STRY_RETHROW(x); \
  } catch (const sitinf::utils::Exception& e){ \
    SLOG_TRACE << y << "\nGot: " << e.what(); \
  } catch (...) { \
    SLOG_WARNING << y << "\n Got unknown exception. ST: " << el::base::debug::StackTrace();\
  }\
}
