/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/Utils.h                                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Types.h>
#include <boost/algorithm/string.hpp>
#include <functional>

namespace sitinf {
  namespace utils {

    /**
     * This function creates uniform random unsigned integers using boost
     * random number generators.
     */
    uint random_uint();

    /**
     * @brief a template function calculates the sum of the elements of a vector.
     */
    template <typename T>
    T sum(const std::vector<T>& vector){
      T result = 0;
      for(auto val : vector) {
        result += val;
      }
      return result;
    }

    /**
     * @brief a template function calculates the protuct of the elements of a vector.
     */
    template <typename T>
    T prod(const std::vector<T>& vector){
      if(vector.empty()) return 0;
      T ret = vector.front();
      for(uint i = 1; i < vector.size(); ++i){
        ret *= vector[i];
      }
      return ret;
    }


    template <typename T>
    double mean(const std::vector<T>& vector){
      if(vector.empty()){
        return 0;
      } else {
        return sum(vector) / (double) vector.size();
      }
    }

    template <typename T>
    double variance(const std::vector<T>& vector, double mean){
      if(vector.empty()){
        return 0;
      } else {
        double sum_value = 0;
        for(auto val : vector){
           sum_value += std::pow(val-mean,2);
        }
        return sum_value / vector.size();
      }
    }

    template <typename T>
    double variance(const std::vector<T>& vector){
      return variance<T>(vector,mean(vector));
    }

    /**
     * @brief concat creates a string representation of a vector
     */
    template <typename T>
    std::string concat(const std::vector<T>& vector,
                       char vector_separator='|',
                       char vector_begin='(',
                       char vector_end=')')
    {
      std::stringstream ret;
      ret << vector_begin << " ";
      if(vector.empty()){
        ret << vector_end;
      } else {
        ret << vector.front();
        for(uint i = 1; i < vector.size(); ++i){
          ret << " " << vector_separator <<  " " << vector[i];
        }
        ret << " " << vector_end;
      }
      return ret.str();
    }


    template <typename T>
    std::vector<T> split_parse(const std::string& vector,const std::string& split_by=" ,;/"){
      std::vector<T> result;
      std::vector <std::string> elements;
      boost::split(elements,vector,boost::is_any_of(split_by));
      for(auto element : elements){
        if(element.size() > 0){
          result.push_back(T());
          std::stringstream str(element);
          str >> result.back();
        }
      }
      return result;
    }

    /**
     * @brief currentTimeMicros can be used to get the time since 01.01.1970 UTC in microseconds
     * @return the current time in microseconds
     */
    uint64 currentTimeMicros();

    /**
     * @brief currentTimeMillis can be used to get the time since 01.01.1970 UTC in milliseconds
     * @return the current time in milliseconds
     */
    uint64 currentTimeMillis();

  } // namespace utils
} // namespace sitinf
