/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/pattern/FunctionRegistry.h                         **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <functional>
#include <utils/Types.h>
#include <utils/Exception.h>

namespace sitinf {
  namespace utils {

    /**
     * @brief The FunctionRegistry template is used to save and retrieve functions using an id.
     */
    template<typename Product,
             typename FunctionIdType,
             typename Function>
    class FunctionRegistry {

    public:

      /**
       * @brief registerFunction adds a function to the function registry.
       * @param id a unique id of this function. typically the name of the implementation.
       * @param function a function that is worth being known globally.
       * @return false if function could not be registered because id is already used.
       */
      bool registerFunction(const FunctionIdType& id, Function function){
        utils::Locker lock(m_Lock);
        if(m_Functions.find(id) == m_Functions.end()){
          m_Functions[id] = function;
          return true;
        } else {
          return false;
        }
      }

      /**
       * @brief hasFunction can be used to check if a function for an id is already registered.
       * @param id the id of the funciton.
       * @return true when a function with the passed id is registered.
       */
      bool hasFunction(const FunctionIdType& id){
        utils::Locker lock(m_Lock);
        return m_Functions.find(id) != m_Functions.end();
      }

      /**
       * @brief getFunction access registered factory functions.
       * @param id the id of the required function.
       * @return a function pointer. nullptr when not available.
       */
      Function getFunction(const FunctionIdType& id){
        utils::Locker lock(m_Lock);
        auto it = m_Functions.find(id);
        if(it != m_Functions.end()){
          return it->second;
        } else {
          return nullptr;
        }
      }

      /**
       * @brief registeredMethods access registered function ids.
       * @return a vector of registered function ids
       */
      std::set<FunctionIdType> registeredMethods(){
        utils::Locker lock(m_Lock);
        std::set<FunctionIdType> ids;
        for(auto it : m_Functions){
          ids.insert(it.first);
        }
      }

    private:
      /**
       * @brief m_Functions inner function map.
       */
      std::map<FunctionIdType,Function> m_Functions;

      /**
       * @brief m_Lock used for concurrency.
       */
      utils::Mutex m_Lock;

    };

    template<
        typename FunctionId,
        typename Function,
        typename Factory
        >
    class StaticRegistrar {
    public:

      /**
       * @brief Register registeres a factory function in a global registry.
       * @throw UniqueIdException<IdType> when the same id is registered multiple times.
       */
      StaticRegistrar(FunctionId id, Function function){
        bool registered = Factory::registry().registerFunction(id,function);
        if(!registered){
          throw utils::UniqueIdException<FunctionId>(id);
        }
      }
    };

    template<
      typename ProductType,
      typename TypeIdType = std::string,
      typename FunctionType = std::function<ProductType(TypeIdType)>
      >
    class Factory {
    public:

      typedef boost::shared_ptr<Factory<ProductType,TypeIdType,FunctionType>> Ptr;
      typedef TypeIdType TypeTd;
      typedef FunctionType Function;

      typedef utils::FunctionRegistry<ProductType,TypeIdType,FunctionType> Registry;
      typedef utils::StaticRegistrar<TypeIdType,FunctionType,Factory> Registrar;

      static Registry& registry(){
        static Registry registry;
        return registry;
      }

    };

  } // namespace utils
} // namespace sitinf
