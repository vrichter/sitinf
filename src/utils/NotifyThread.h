/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/NotifyThread.h                               **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <functional>
#include <future>

#include <utils/Types.h>
#include <utils/Utils.h>
#include <utils/NonCopyable.h>
#include <utils/Exception.h>

namespace sitinf {
  namespace utils {

    class NotifyThread : public utils::NonCopyable {
    public:

      typedef boost::posix_time::time_duration Duration;
      typedef boost::posix_time::ptime Time;

      NotifyThread(std::function<void()> callback);

      virtual ~NotifyThread();

      void call_in(utils::uint64 wait_milliseconds);

      void call_at(utils::uint64 millis_sice_epoch);

      void call_in(const Duration& duration);

      void call_at(const Time& time);

      void stop();

    private:
      utils::RecursiveMutex m_Lock;
      boost::condition_variable m_Condition;
      boost::posix_time::ptime m_WaitUntilSystemTime;
      std::function<void()> m_Callback;
      utils::Thread m_Thread; // must be last

      void wait_loop();

    };

  } // namespace utils
} // namespace sitinf

