/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/Observable.h                                 **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Types.h>
#include <utils/Utils.h>

#include <boost/bind.hpp>
#include <boost/function.hpp>

#include <map>

namespace sitinf {
  namespace utils {

    /**
     * The template for observable objects.
     *
     * Instances of this provide functionality to inform listeners about
     * a changed internal state and pass information.
     */
    template<typename CALLBACK = boost::function<void()> >
    class Observable {
    public:

      /**
       * Creates an Observable instance.
       */
      Observable()
      { /* currently nothing to do */ }

      /**
       * A virtual destructor to be reimplemented in subclasses.
       */
      virtual ~Observable() {}

      /**
       * Adds the callback to the list of callbacks called when notify is called.
       *
       * @param callback the function to be called on update
       * @return the id of the added callback. This is needed to be able to remove it afterwards.
       */
      uint addObserver(CALLBACK callback){
        RecursiveLocker l(m_Lock);
        // create a random id that is not in use
        uint id;
        do {
          id = utils::random_uint();
        } while (m_Callbacks.find(id) != m_Callbacks.end());
        // add callback
        m_Callbacks[id] = callback;
        // return id
        return id;
      }


      /**
       * @brief Removes an observer.
       *
       * @param id the observer id previously returned by addObserver()
       * @return Whether the id was found and observer removed
       */
      bool removeObserver(uint id){
        RecursiveLocker l(m_Lock);
        // erase returns the amount of removed callbacks.
        return m_Callbacks.erase(id) > 0;
      }

      /**
       * calls notify function with every callback.
       */
      void notifyObservers(){
        RecursiveLocker l(m_Lock);
        typename std::map<uint, CALLBACK >::iterator it;
        for(it = m_Callbacks.begin(); it != m_Callbacks.end(); ++it){
          try{
            // call notify function with every observer callback
            notify(it->second);
          } catch (const std::exception& e){
            std::cerr << " callback function threw an exception: " << e.what() << std::endl;
          } catch (...){
            std::cerr << "callback function threw an unknown exception" << std::endl;
          }
        }
      }

      /**
       * This function needs to be reimplemented for all instantiations of the
       * Observable. Call the observers callback function.
       */
      virtual void notify(CALLBACK callback) = 0;

    protected:
      /**
       * @brief m_Lock used to enforce synchronization
       */
      RecursiveMutex m_Lock;
    private:
      /**
       * @brief m_Callbacks the map of registered observers
       */
      std::map<uint,CALLBACK > m_Callbacks;
    };

    /**
     * @brief The Notifyer class a simple implememtation of observable calling
     * void functions to notify observers.
     */
    class Notifyer : public Observable<boost::function<void()> > {
      /**
       * just calls the passed function callback.
       */
      virtual void notify(boost::function<void()> callback){
        callback();
      }
    };

  } // namespace utils
} // namespace sitinf

