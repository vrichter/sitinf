/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/StackTimer.cpp                               **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <utils/StackTimer.h>
#include <map>

#include <boost/date_time/posix_time/posix_time.hpp>

using namespace sitinf;
using namespace sitinf::utils;

StackTimer::StackTimer(std::string name)
  : m_Name(name), m_Start(boost::posix_time::microsec_clock::local_time())
{
}

StackTimer::~StackTimer(){
  std::cout << m_Name << " - " << (boost::posix_time::microsec_clock::local_time()-m_Start).total_microseconds() << std::endl;
}
