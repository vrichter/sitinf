/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/Yavl.h                                       **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

/********************************************************************
**                                                                 **
** This is a (adapted) copy of the MIT-licenced yavl-cpp           **
** source from: https://code.google.com/p/yavl-cpp/                **
**                                                                 **
********************************************************************/

#pragma once

#include <yaml-cpp/yaml.h>
#include <vector>
#include <string>
#include <ostream>

namespace YAVL
{

  typedef std::vector<std::string> Path;

  // really sucks that I have to do this sort of crap since I can't
  // pass a type as an argument to a function.
  template <typename T>
  std::string ctype2str()
  {
    return "FAIL";
  }

  class Exception {
  public:
    std::string why;
    Path gr_path;
    Path doc_path;
    Exception(const std::string _why,
      const Path& _gr_path,
      const Path& _doc_path) :
        why(_why), gr_path(_gr_path), doc_path(_doc_path) {}
  };

  typedef std::vector<Exception> Errors;

  class Validator {
    const YAML::Node& gr;
    const YAML::Node& doc;
    Path gr_path;
    Path doc_path;
    Errors errors;

    int num_keys(const YAML::Node& doc);
    const std::string& type2str(YAML::NodeType::value t);
    bool validate_map(const YAML::Node &mapNode, const YAML::Node &doc);
    bool validate_leaf(const YAML::Node &gr, const YAML::Node &doc);
    bool validate_list(const YAML::Node &gr, const YAML::Node &doc);
    bool validate_doc(const YAML::Node &gr, const YAML::Node &doc);

    void gen_error(const Exception& err) {
      errors.push_back(err);
    }

    template<typename T>
    void attempt_to_convert(const YAML::Node& scalar_node, bool& ok) {
      try {
        scalar_node.as<T>();
        ok = true;
      } catch (const YAML::InvalidScalar& e) {
        std::string s = scalar_node.as<std::string>();
        std::string reason = "unable to convert '" + s + "' to '" + YAVL::ctype2str<T>() + "'.";
        gen_error(Exception(reason, gr_path, doc_path));
        ok = false;
      }
    }

  public:
    Validator(const YAML::Node& _gr, const YAML::Node& _doc) :
      gr(_gr), doc(_doc) {}
    bool validate() {
      return validate_doc(gr, doc);
    }
    const Errors& get_errors() {
      return errors;
    }
  };
}

std::ostream& operator << (std::ostream& os, const YAVL::Path& path);
std::ostream& operator << (std::ostream& os, const YAVL::Exception& v);
std::ostream& operator << (std::ostream& os, const YAVL::Errors& v);
