/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/NotifyThread.cpp                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <utils/NotifyThread.h>
#include <utils/Macros.h>

#define _LOGGER "utils.NotifyThread"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;



NotifyThread::NotifyThread(std::function<void()> callback)
  : m_WaitUntilSystemTime(boost::posix_time::pos_infin),
    m_Callback(callback),
    m_Thread()
{
  m_Thread = utils::Thread(&NotifyThread::wait_loop, this);
}

NotifyThread::~NotifyThread(){
  m_Thread.interrupt();
  m_Thread.join();
}

void NotifyThread::call_at(const Time& time){
  RecursiveLocker l(m_Lock);
  m_WaitUntilSystemTime = time;
  m_Condition.notify_all();
}

void NotifyThread::call_in(const Duration& duration){
  call_at(boost::get_system_time() + duration);
}

void NotifyThread::call_at(utils::uint64 millis_since_epoch){
  static boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
  RecursiveLocker l(m_Lock);
  m_WaitUntilSystemTime = epoch + boost::posix_time::milliseconds(millis_since_epoch);
  m_Condition.notify_all();
}

void NotifyThread::call_in(utils::uint64 wait_milliseconds){
  RecursiveLocker l(m_Lock);
  call_at(boost::get_system_time() + boost::posix_time::milliseconds(wait_milliseconds));
}

void NotifyThread::stop(){
  RecursiveLocker l(m_Lock);
  m_WaitUntilSystemTime = boost::posix_time::pos_infin;
  m_Condition.notify_all();
}

void NotifyThread::wait_loop(){
  // the wait-mutex
  Mutex lock;
  // set time for timeout
  while(true){
    // lock the mutex
    Locker locker(lock);
    // calling wait releases the lock and blocks.
    SLOG_TRACE << this << "Sleepinng until " << m_WaitUntilSystemTime;
    bool notified = m_Condition.timed_wait(locker,m_WaitUntilSystemTime);
    RecursiveLocker memberLock(m_Lock);
    if(!notified) {
      if(boost::get_system_time() > m_WaitUntilSystemTime){
        // the time is realy after timeout. notify
        m_WaitUntilSystemTime = boost::posix_time::pos_infin;
        m_Callback();
      } else {
        SLOG_WARNING << "Returned from sleep before requested sleep time.";
      }
    }
    // in all other cases we cas just re-enter the loop
  }
  // this point is not reachable. The thread will be stopped by interrupt.
}
