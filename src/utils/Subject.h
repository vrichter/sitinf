/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/Subject.h                                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Types.h>
#include <utils/Utils.h>
#include <functional>

#include <set>

namespace sitinf {
  namespace utils {

    /// declaration so it can be used in Observer
    template <typename T = std::nullptr_t>
    class Subject;

    /**
     * @brief The Observer interface needs to be implemented by all Subject observers.
     */
    template <typename T = std::nullptr_t>
    class Observer {
    public:

      /**
       * @brief update is called every time the Subject changes.
       *
       * @param subject a pointer to the subject that notifies ist Observers. So updates from multiple
       * Subjects can be distinguished.
       * @param data holds the new information from Subject
       */
      virtual void update(const Subject<T>* subject, T data) = 0;
    };

    /**
     * @brief Observer implementation with callback function.
     */
    template <typename T = std::nullptr_t>
    class ObserverCallback : public Observer<T> {
    public:

      ObserverCallback(std::function<void(T)> callback)
        : m_Callback(std::move(callback)){}

      virtual void update(const Subject<T>* subject, T data) final {
        m_Callback(data);
      }

    private:
      std::function<void(T)> m_Callback;
    };

    /**
     * The notifying Subject with data implementation.
     *
     * This provides functionality to inform listeners about
     * a changed internal state passing new information directly to the observer.
     */
    template <typename T>
    class Subject {
    public:

      /**
       * @brief SubjectType a Typedef for this.
       */
      typedef Subject<T> SubjectType;

      /**
       * @brief ObserverType the Observer type of this Subject.
       */
      typedef Observer<T> ObserverType;

      /**
       * @brief DataType typedef.
       */
      typedef T SubjectDataType;

      /**
       * Creates an Subject instance.
       */
      Subject(){}

      /**
       * A virtual destructor to be reimplemented in subclasses.
       */
      virtual ~Subject(){
        RecursiveLocker l(m_Lock);
      }

      /**
       * Adds the observer to the list of notified observers.
       *
       * @param observer a pointer to the observer
       */
      void addObserver(ObserverType* observer){
        RecursiveLocker l(m_Lock);
        m_Observers.insert(observer);
      }

      /**
       * Removes an observer from the notify list.
       *
       * @param observer a pointer to the observer to remove
       */
      void removeObserver(ObserverType* observer){
        RecursiveLocker l(m_Lock);
        m_Observers.erase(observer);
      }

      ObserverCallback<T> createCallbackObserver(std::function<void(T)> callback){
        return ObserverCallback<T>(callback);
      }

      /**
       * @brief notify notifies all observers.
       */
      virtual void notify(T data) final {
        if(!observerCount()){
          return;
        }
        RecursiveLocker l(m_Lock);
        for(auto observer : m_Observers){
          try{
            observer -> update(this,data);
          } catch (const std::exception& e){
            std::cerr << "Error: Observer::notify should not throw. Catched: " << e.what() << std::endl;
          } catch (...) {
            std::cerr << "Error: Observer::notify should not throw. Catched unknown exception." << std::endl;
          }
        }
      }

      /**
       * @brief observerCount can be ues to obtain the current count of observers.
       * @return the current number of observers.
       */
      virtual uint observerCount() const final {
        return m_Observers.size();
      }

    private:
      /**
       * @brief m_Lock used to enforce synchronization
       */
      RecursiveMutex m_Lock;
      /**
       * @brief m_Observers the set of registered Observers
       */
      std::set<ObserverType*> m_Observers;
    };

  } // namespace utils
} // namespace sitinf

