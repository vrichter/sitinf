/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/Adapter.h                                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <utils/Converter.h>
#include <utils/Subject.h>

namespace sitinf {
  namespace utils {

    template<typename IN,typename OUT>
    class Adapter : public Subject<OUT>, public utils::Observer<IN> {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<Adapter<IN,OUT> > Ptr;

      Adapter(typename Subject<IN>::Ptr subject, typename utils::Converter<IN,OUT>::Ptr converter)
        : m_Subject(subject), m_Converter(converter)
      {
        m_Subject->addObserver(this);
      }

      static Adapter<IN,OUT>::Ptr New(typename Subject<IN>::Ptr source,
                                      typename utils::Converter<IN,OUT>::Ptr converter)
      {
        return Adapter<IN,OUT>::Ptr(new Adapter<IN,OUT>(source,converter));
      }

      virtual void update(const utils::Subject<IN>* subject, IN data) final {
        this->notify(m_Converter->convert(data));
      }

    private:
      typename Subject<IN>::Ptr m_Subject;
      typename utils::Converter<IN,OUT>::Ptr m_Converter;
    };

  } // namespace uttils
} // namespace sitinf
