/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/ThreadPool.h                                 **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Types.h>
#include <utils/Utils.h>
#include <utils/Exception.h>
#include <future>

namespace sitinf {
  namespace utils {

    template <typename T = void>
    class ThreadPool {
    public:

      typedef std::packaged_task<T()> Task;
      typedef std::vector<Task> TaskSet;
      typedef std::vector<T> ResultSet;
      typedef std::future<ResultSet> FutureResultSet;

      ThreadPool(std::vector<Task>& tasks,
                 utils::uint threads=std::thread::hardware_concurrency(),
                 utils::uint sleep_micro_seconds=100)
        : m_NumThreads(threads), m_Sleep(sleep_micro_seconds), m_Tasks(std::move(tasks))
      {
        if(threads == 0) m_NumThreads = 1;
        for(uint i = 0; i < m_Tasks.size(); ++i){
          if(!m_Tasks[i].valid()){
            throw utils::IllegalArgumentException("Invalid task passed to ThreadPool.");
          }
        }
      }

      virtual ~ThreadPool(){
      }

     FutureResultSet run(){
        return std::async(&ThreadPool::runBlocking,this);
      }

      ResultSet runBlocking(){
        // init future objects
        std::vector<std::future<T> > threads;
        threads.reserve(m_Tasks.size());
        for(uint i = 0; i < m_Tasks.size(); ++i) {
          threads.push_back(std::move(m_Tasks[i].get_future()));
        }
        // start first batch of threads
        std::vector<uint> running;
        for(uint i = 0; i < m_NumThreads; ++i){
          if(m_Tasks.empty()){
            break;
          } else {
            std::thread(std::move(m_Tasks.back())).detach();
            m_Tasks.pop_back();
            running.push_back(m_Tasks.size());
          }
        }
        // run all other threads
        while(!m_Tasks.empty()){
          // remove all threads that are done
          auto it = running.begin();
          while(it != running.end()){
            switch (threads[*it].wait_for(std::chrono::microseconds(1))){
              case std::future_status::ready:
                it = running.erase(it);
                break;
              case std::future_status::timeout:
                ++it;
                break; // do noting
              case std::future_status::deferred:
                throw UnexpectedException("The future of a running thread should not be deferred.");
            }
          }
          // start threads until m_NumThreads are running or done
          if(running.size() < m_NumThreads) {
            while(running.size() < m_NumThreads) {
              if (m_Tasks.empty()) {
                break;
              } else {
                std::thread(std::move(m_Tasks.back())).detach();
                m_Tasks.pop_back();
                running.push_back(m_Tasks.size());
              }
            }
          } else {
            // no thread done jet. wait a little longer
            threads[running.front()].wait_for(std::chrono::microseconds(m_Sleep));
          }
        }

        // all threads are run. wait for the remaining and return the results
        for(auto thread : running){
          threads[thread].wait();
        }
        std::vector<T> result;
        result.reserve(threads.size());
        for(uint i = 0; i < threads.size(); ++i){
          result.push_back(threads[i].get());
        }
        return result;
      }

    private:
      utils::uint m_NumThreads;
      utils::uint m_Sleep;
      std::vector<Task> m_Tasks;
    };

  } // namespace utils
} // namespace sitinf

