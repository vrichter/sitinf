/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/utils/Utils.cpp                                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <utils/Utils.h>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>

#include <boost/date_time/microsec_time_clock.hpp>
#include <boost/date_time/gregorian/greg_date.hpp>

#include <ctime>

#define _LOGGER "utils.Utils"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::utils;

boost::mt19937 generator;

// creates random unsigned integers
uint utils::random_uint(){
  boost::uniform_int<> dist(0, std::numeric_limits<int>::max());
  boost::variate_generator<boost::mt19937&, boost::uniform_int<> > value(generator, dist);
  return value();
}

boost::posix_time::time_duration currentTimeDiff(){
  static boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));
  return boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time() - epoch;
}

uint64 utils::currentTimeMicros(){
  return currentTimeDiff().total_microseconds();
}

uint64 utils::currentTimeMillis(){
  return currentTimeDiff().total_milliseconds();
}
