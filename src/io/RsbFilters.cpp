/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/RsbFilters.cpp                                  **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/RsbFilters.h>
#include <utils/Exception.h>
#include <utils/Macros.h>

#define _LOGGER "io.RsbFilters"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;

ScopeFilter::ScopeFilter(const std::string& scope, bool sub_scopes)
{
  try{
    m_Scope = rsb::Scope(scope);
  } catch (const std::invalid_argument& e){
    STHROW(IOException,"Cant parse scope string: '" << scope << "' into rsb::Scope.\nGot: " << e.what());
  }
  m_SubScopes = sub_scopes;
}

ScopeFilter::ScopeFilter(const rsb::Scope& scope, bool sub_scopes)
  : m_Scope(scope), m_SubScopes(sub_scopes){}

bool ScopeFilter::match(rsb::EventPtr event){
  return(event->getScope() == m_Scope || (m_SubScopes && event->getScope().isSubScopeOf(m_Scope)));
}

SchemaAndByteArrayTypeFilter::SchemaAndByteArrayTypeFilter(const std::string& type)
  : m_Type(type)
{
  // add leading point if not present.
  if(m_Type.front() != '.') m_Type = std::string(".") + m_Type;
}

bool SchemaAndByteArrayTypeFilter::match(rsb::EventPtr event)
{
  if(event->getType() != "schemaandbytearray"){
    return false;
  }
  boost::shared_ptr<rsb::AnnotatedData> data = boost::static_pointer_cast<rsb::AnnotatedData>(event->getData());
  return data->first == m_Type;
}
