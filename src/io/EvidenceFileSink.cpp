/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/EvidenceFileSink.cpp                            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/EvidenceFileSink.h>
#include <utils/Types.h>
#include <utils/Exception.h>
#include <utils/Macros.h>
#include <utils/Converter.h>

#include <boost/date_time/microsec_time_clock.hpp>
#include <boost/date_time/gregorian/greg_date.hpp>

#include <rst/bayesnetwork/BayesNetworkEvidence.pb.h>

#define _LOGGER "io.EvidenceFileSink"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;
using namespace sitinf::network;

rst::timing::Timestamp* createTimeNow(){
  rst::timing::Timestamp* time = rst::timing::Timestamp::default_instance().New();
  time->set_time(utils::currentTimeMicros());
  return time;
}

EvidenceFileSink::EvidenceFileSink(const std::string& filename)
  : m_File(filename,true), m_LastMessage(rst::bayesnetwork::BayesNetworkEvidence::default_instance().New())
{
  m_LastMessage->clear_time();
  m_LastMessage->set_allocated_time(createTimeNow());
}

EvidenceFileSink::~EvidenceFileSink(){
  publish(AspectStateEvidence()); // end with empty eidence
}

void EvidenceFileSink::publish(const AspectStateEvidence& data) {
  if(!data.equals(&m_LastEvidence)){
    m_LastEvidence = data;
    publish();
  }
}

namespace sitinf {
  namespace utils {
template<>
class Converter<network::AspectStateEvidence, const rst::classification::ClassificationResultMap*>{
public:
  virtual network::AspectStateEvidence convert(const rst::classification::ClassificationResultMap* data) {
    AspectStateEvidence result;
    if(data != nullptr){
      for(auto aspect : data->aspects()){
        result.newEvidence(aspect.name(),aspect.result().decided_class());
      }
    }
    return result;
  }
};
  }
}

void EvidenceFileSink::publish(Shared<rst::classification::ClassificationResultMap>::Ptr data){
  static Converter<AspectStateEvidence, const rst::classification::ClassificationResultMap*> converter;
  AspectStateEvidence override = converter.convert(data.get());
  if(!override.equals(&m_LastOverride)){
    m_LastOverride = override;
    publish();
  }
}

void EvidenceFileSink::publish(){
  SLOG_TRACE << "creating evidence rst representation";
  // set up new evidence
  static Converter<Shared<rst::bayesnetwork::BayesNetworkEvidence>::Ptr,const AspectStateEvidence&> converter;
  SLOG_TRACE << "Evidence: " << m_LastEvidence.toString();
  SLOG_TRACE << "Learning override: " << m_LastOverride.toString();
  AspectStateEvidence data = m_LastEvidence;
  data.override(&m_LastOverride);
  auto evidence = converter.convert(data);
  evidence->clear_time();
  evidence->set_allocated_time(createTimeNow());

  // print last evidence and update
  m_LastMessage->set_duration_microseconds(evidence->time().time() - m_LastMessage->time().time()); // difference to new
  m_File << m_LastMessage->ShortDebugString() << std::endl;
  m_LastMessage = evidence;
}


BayesNetworkEvidenceStreamSink::BayesNetworkEvidenceStreamSink(std::ostream* stream)
  : m_Stream(stream) {}

BayesNetworkEvidenceStreamSink::~BayesNetworkEvidenceStreamSink() {}

void BayesNetworkEvidenceStreamSink::publish(const rst::bayesnetwork::BayesNetworkEvidence* data) {
  *m_Stream << data->ShortDebugString() << std::endl;
}
