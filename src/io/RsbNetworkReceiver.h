/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/RsbNetworkReceiver.h                            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <rsb/Factory.h>
#include <rsb/Listener.h>
#include <rsb/patterns/RemoteServer.h>
#include <rst/bayesnetwork/BayesNetwork.pb.h>
#include <utils/Subject.h>

namespace sitinf {
  namespace io {

    /**
     * @brief The RsbNetworkReceiver class can be used to get BayesNetwork configurations via rsb.
     *
     * First the receiver will ask for a Network using a server call and then wait signal updates
     * every time it gets informed about new configurations.
     */
    class RsbNetworkReceiver : public utils::Subject<utils::Shared<rst::bayesnetwork::BayesNetwork>::Ptr > {

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<RsbNetworkReceiver> Ptr;

      /**
       * @brief BayesNetworkPtr a shared pointer to a BayesNetwork
       */
      typedef boost::shared_ptr<rst::bayesnetwork::BayesNetwork> BayesNetworkPtr;

      /**
       * @brief CallbackFunction a function getting a BayesNetworkPtr and returning nothing.
       */
      typedef boost::function<void(BayesNetworkPtr)> CallbackFunction;

      /**
      * @brief FutureBayesNetwork a DataFuture of BayesNetworks
      */
      typedef rsb::patterns::RemoteServer::DataFuture<rst::bayesnetwork::BayesNetwork> FutureBayesNetwork;

      /**
       * @brief RsbNetworkReceiver creates a remote server and listener and signals the received networks.
       *
       * @param scope the scope to be used for requesting and listening.
       * @param remote_function the remote server function to be called.
       */
      RsbNetworkReceiver(const std::string scope, const std::string remote_function="get");

      /**
       * @brief currentNetwork accessor to the currently known Network as prototype
       * @return a shared pointer to a Network message or to null if none is known.
       */
      BayesNetworkPtr currentNetwork();

    private:
      /**
       * @brief m_Lock used to synchronize calls
       */
      utils::Mutex m_Lock;
      /**
       * @brief m_Scope the scope to listen fro updates
       */
      rsb::Scope m_Scope;
      /**
       * @brief m_Listener listens for network updates
       */
      rsb::ListenerPtr m_Listener;
      /**
       * @brief m_RemoteServer is used to get the first network configuration.
       */
      rsb::patterns::RemoteServerPtr m_RemoteServer;
      /**
       * @brief m_ServerWait a thread awaiting the server to time out when it is not running.
       */
      boost::thread m_ServerWait;
      /**
       * @brief m_Timestamp the timestamp of the last network update.
       */
      boost::posix_time::ptime m_Timestamp;

      /**
       * @brief m_CurrentData holds the current Network
       */
      BayesNetworkPtr m_CurrentData;


      /**
       * @brief handle internal handle function calling the callback method.
       * @param data the rsb message.
       */
      void handle(BayesNetworkPtr data);

      /**
       * @brief eventHandle internal handle function calling the callback method.
       *
       * this functions checks for correct scope and type
       *
       * @param event the rsb event.
       */
      void eventHandle(rsb::EventPtr);

      /**
       * @brief waitHandle waits until data returns the BayesNetwork
       *        or breaks then calls the handle.
       * @param data the future data handle.
       */
      void waitHandle(FutureBayesNetwork data);

    };

  } // namespace io
} // sitinf
