/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/SituationListener.h                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <utils/Types.h>
#include <rsb/Listener.h>
#include <rst/classification/ClassificationResultMap.pb.h>

namespace sitinf {
  namespace io {

    /**
     * @brief The SituationListener class listens to Situation results on a
     * scope and provides the most current result.
     */
    class SituationListener {

    public:

      /**
       * @brief Situation shtorcut to situation message type.
       */
      typedef rst::classification::ClassificationResultMap Situation;

      /**
       * @brief SituationPtr a boost pointer to a situation message.
       */
      typedef boost::shared_ptr<Situation> SituationPtr;

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<SituationListener> Ptr;

      /**
       * @brief SituationListener uses the passed scope to create an rsb listener
       *        listeneing for situation results.
       * @param scope the rsb scope where situation results are published
       */
      SituationListener(std::string scope);

      /**
       * @brief A virtual destructor to be reimplemented in subclasses for nice cleanup.
       */
      virtual ~SituationListener();

      /**
       * @brief getLastResult returns the last receives situation or an empty pointer.
       * @return last result or an empty pointer
       */
      SituationPtr getLastResult();

    protected:
      /**
       * @brief m_Lock A lock for thread safety.
       */
      utils::Mutex m_Lock;
      /**
       * @brief m_Listener to listen to Situation objects.
       */
      rsb::ListenerPtr m_Listener;
      /**
       * @brief m_Result A SituationPtr representing the last result.
       */
      SituationPtr m_Result;
      /**
       * @brief handle is registered to Listener and handles its events.
       * @param data Situation data from an rsb event
       */
      void handle(SituationPtr data);
    };

  } // namespace io
} // namespace sitinf
