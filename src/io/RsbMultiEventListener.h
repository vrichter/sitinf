/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/RsbMultiEventListener.h                         **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <tuple>
#include <boost/shared_ptr.hpp>
#include <io/Source.h>
#include <rsb/Factory.h>
#include <rsb/Informer.h>
#include <rsb/MetaData.h>
#include <rsb/converter/ProtocolBufferConverter.h>
#include <rsb/converter/Repository.h>
#include <io/RsbFilters.h>
#include <boost/algorithm/string/replace.hpp>

namespace sitinf {
  namespace io {

    struct CreationTimeIdGenerator {
      static uint64_t generate(rsb::EventPtr event){
        return event->getMetaData().getCreateTime();
      }
    };

    template<uint keepMax = 5>
    struct KeepRecentIncomplete {
      static bool full(std::vector<rsb::EventPtr>& data){
        for(auto it : data){
          if(it == nullptr){
            return false;
          }
        }
        return true;
      }

      static void clean(std::map<uint64_t,std::vector<rsb::EventPtr>>& data){
        auto it = data.begin();
        while (it != data.end()) {
          if(full(it->second)) {
             it = data.erase(it);
          } else {
             ++it;
          }
        }
        while(data.size() > keepMax){
          data.erase(data.begin());
        }
      }
    };

    template<typename EventIdGenerator = CreationTimeIdGenerator,
             typename RetentionStrategy = KeepRecentIncomplete<5>>
    class RsbMultiEventListener : public Source<std::vector<rsb::EventPtr>>{
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef std::shared_ptr<RsbMultiEventListener> Ptr;

      RsbMultiEventListener(const std::vector<rsb::Scope>& scopes)
      {
        for(uint i = 0; i < scopes.size(); ++i){
          m_Listeners.push_back(rsb::getFactory().createListener(scopes[i]));
          m_Handlers.push_back(rsb::HandlerPtr(new rsb::EventFunctionHandler(
                                                 [i,this](rsb::EventPtr event){ this->eventHandle(i,event); }
          )));
          m_Listeners.back()->addHandler(m_Handlers.back());
        }
      }

      virtual ~RsbMultiEventListener<EventIdGenerator,RetentionStrategy>(){
        for(uint i = 0; i < m_Listeners.size(); ++i){
          m_Listeners[i]->removeHandler(m_Handlers[i],true);
        }
      }

      std::vector<rsb::EventPtr> current() override {
        return m_CurrentData;
      }

    private:
      /**
       * @brief m_Lock used to synchronize calls
       */
      utils::Mutex m_Lock;
      /**
       * @brief m_Listeners listen for data updates
       */
      std::vector<rsb::ListenerPtr> m_Listeners;
      /**
       * @brief m_Handlers handler instances passing events to the handle functions
       */
      std::vector<rsb::HandlerPtr> m_Handlers;
      /**
       * @brief m_CurrentData holds the current data
       */
      std::vector<rsb::EventPtr> m_CurrentData;
      /**
       * @brief m_PendingData holds the data that is curently being combined
       */
      std::map<uint64_t,std::vector<rsb::EventPtr>> m_PendingData;

      /**
       * @brief eventHandle internal handle putting events into their slots.
       *
       * @param id distinguishes btw. the event slots.
       * @param event the rsb event.
       */
      void eventHandle(uint id, rsb::EventPtr event){
        assert(id <= m_Listeners.size());
        boost::unique_lock<boost::mutex> l(m_Lock);
        auto it = get_or_create(EventIdGenerator::generate(event));
        assert(it->second[id] == nullptr);
        it->second[id] = event;
        if(complete(it->second)){
          m_CurrentData = it->second;
          notify(m_CurrentData);
        }
        RetentionStrategy::clean(m_PendingData);
      }

    private:
      std::map<uint64_t,std::vector<rsb::EventPtr>>::iterator get_or_create(uint64_t event_id){
        auto it = m_PendingData.find(event_id);
        if(it == m_PendingData.end()){
          return m_PendingData.insert(std::make_pair(event_id,std::vector<rsb::EventPtr>(m_Listeners.size(),nullptr))).first;
        } else {
          return it;
        }
      }
      bool complete(std::vector<rsb::EventPtr>& event_set){
        for(auto e : event_set){
          if(e == nullptr){
            return false;
          }
        }
        return true;
      }

    };

  } // namespace io
} // namespace sitinf
