/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/RsbNetworkPublisher.cpp                         **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/RsbNetworkPublisher.h>
#include <utils/Exception.h>
#include <utils/Types.h>

#include <rsb/Factory.h>
#include <rsb/converter/ProtocolBufferConverter.h>
#include <rsb/converter/Repository.h>

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;
using namespace sitinf::network;

/**
 * @brief The NetworkCallback class holds and publishes a pointer to a network.
 */
class NetworkCallback: public rsb::patterns::LocalServer::Callback<std::string, rst::bayesnetwork::BayesNetwork> {
public:
    NetworkCallback(RsbNetworkPublisher* pub)
      : m_Publisher(pub) {}

    boost::shared_ptr<rst::bayesnetwork::BayesNetwork> call(const std::string &methodName, boost::shared_ptr<std::string> /*ignored*/) {
        return m_Publisher->currentMessage();
    }

private:
    RsbNetworkPublisher* m_Publisher;
};

RsbNetworkPublisher::RsbNetworkPublisher(const std::string scope, const BayesianNetwork::Ptr network)
{
  // register converter
  boost::shared_ptr<rsb::converter::ProtocolBufferConverter<rst::bayesnetwork::BayesNetwork> >
      converter(new rsb::converter::ProtocolBufferConverter<rst::bayesnetwork::BayesNetwork>());
  rsb::converter::converterRepository<std::string>()->registerConverter(converter);
  // create informer
  rsb::Scope scp(scope);
  m_Informer = rsb::getFactory().createInformer<rst::bayesnetwork::BayesNetwork>(scp);
  // create server
  m_Server = rsb::getFactory().createLocalServer(scp);
  // create and register callback
  // register method
  m_Server->registerMethod("getNetwork", rsb::patterns::LocalServer::CallbackPtr(
                             new NetworkCallback(this)));
  updateNetwork(network);
}

void RsbNetworkPublisher::updateNetwork(const BayesianNetwork::Ptr network){
 // create a message
 boost::shared_ptr<rst::bayesnetwork::BayesNetwork> message = network->asRstMessage();
 Locker l(m_Lock);
 m_Message = message;
 // inform about the new network
 m_Informer->publish(m_Message);
}

boost::shared_ptr<rst::bayesnetwork::BayesNetwork> RsbNetworkPublisher::currentMessage(){
  Locker l(m_Lock);
  return m_Message;
}
