/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/SituationListener.cpp                           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/SituationListener.h>
#include <utils/Exception.h>

#include <rsb/Factory.h>
#include <rsb/filter/Filter.h>

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;

class SituationListenerFilter : public rsb::filter::Filter{
  public:
  SituationListenerFilter(std::string scope, std::string type, bool sub_scopes)
    : m_Scope(scope), m_Type(type), m_SubScopes(sub_scopes)
  {}

  virtual bool match(rsb::EventPtr event){
    return event->getType() == m_Type
        && (event->getScope() == m_Scope || (m_SubScopes && event->getScope().isSubScopeOf(m_Scope)));
  }

  private:
    rsb::Scope m_Scope;
    std::string m_Type;
    bool m_SubScopes;
};

SituationListener::SituationListener(std::string scope)
  : m_Result(new Situation())
{
  m_Listener = rsb::getFactory().createListener(scope);
  rsb::filter::FilterPtr filter = rsb::filter::FilterPtr(
                                    new SituationListenerFilter(
                                      scope,
                                      "rst::classification::ClassificationResultMap",
                                      false
                                      )
                                    );
  m_Listener->addFilter(filter);
  m_Listener->addHandler(rsb::HandlerPtr(new rsb::DataFunctionHandler<Situation>(
                                          boost::bind(&SituationListener::handle,this,_1)
                                          )));
}

SituationListener::~SituationListener(){/* currently nothing to do ^*/}

SituationListener::SituationPtr SituationListener::getLastResult(){
  Locker l(m_Lock);
  return m_Result;
}

void SituationListener::handle(SituationPtr data){
  Locker l(m_Lock);
  m_Result = data;
}
