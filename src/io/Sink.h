/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/Sink.h                                          **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <utils/Subject.h>
#include <utils/Converter.h>

namespace sitinf {
  namespace io {

    template<typename T>
    class Sink : public utils::Observer<T>
    {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<Sink<T> > Ptr;

      virtual void publish(T data) = 0;

      virtual void update(const utils::Subject<T>* subject, T data) override {
        // ignore subject, publish
        publish(data);
      }

    };

    template<typename T>
    class NoOpSink : public Sink<T>
    {
    public:
      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<NoOpSink<T> > Ptr;

      void publish(T data) override {}

    };


    template<typename IN,typename OUT>
    class SinkAdapter : public Sink<IN>, utils::Subject<OUT>{
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<SinkAdapter<IN,OUT> > Ptr;

      SinkAdapter(typename Sink<OUT>::Ptr sink, typename utils::Shared<utils::Converter<OUT,IN>>::Ptr converter)
        : m_Sink(sink), m_Converter(converter)
      {
        this->addObserver(m_Sink.get());
      }

      virtual ~SinkAdapter<IN,OUT>(){
        this->removeObserver(m_Sink.get());
      }

      static SinkAdapter<IN,OUT>::Ptr New(typename Sink<OUT>::Ptr sink,
                                          typename utils::Shared<utils::Converter<OUT,IN> >::Ptr converter)
      {
        return SinkAdapter<IN,OUT>::Ptr(new SinkAdapter<IN,OUT>(sink,converter));
      }

      virtual void publish(IN data) final {
        return this->notify(m_Converter->convert(data));
      }

    private:
      typename Sink<OUT>::Ptr m_Sink;
      typename utils::Shared<utils::Converter<OUT,IN>>::Ptr m_Converter;
    };


    class SinkAdapterFactory{

    public:

      template<typename IN,typename OUT>
      static typename Sink<IN>::Ptr create(typename Sink<OUT>::Ptr sink){
        typename utils::Shared<utils::Converter<OUT,IN>>::Ptr converter(new utils::Converter<OUT,IN>());
        return typename SinkAdapter<IN,OUT>::Ptr(new SinkAdapter<IN,OUT>(sink,converter));
      }
    };

  } // namespace io
} // namespace sitinf
