/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/RsbSituationReceiver.h                          **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <rsb/Factory.h>
#include <rsb/Listener.h>
#include <rst/classification/ClassificationResultMap.pb.h>
#include <utils/Subject.h>

namespace sitinf {
  namespace io {

    /**
     * @brief The RsbSituationReceiver class can be used to receive Situation classifications via rsb.
     */
    class RsbSituationReceiver :
        public utils::Subject<const utils::Shared<rst::classification::ClassificationResultMap>::Ptr>
    {

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<RsbSituationReceiver> Ptr;

      /**
       * @brief puts Situation into this namespace
       */
      typedef rst::classification::ClassificationResultMap Situation;

      /**
       * @brief SituationPtr a shared pointer to a Situation
       */
      typedef utils::Shared<Situation>::Ptr SituationPtr;

      /**
       * @brief RsbSituationReceiver creates a listener and signals the received situations.
       *
       * @param scope the scope to be used for listening.
       */
      RsbSituationReceiver(const std::string scope);

      /**
       * @brief currentSituaton accessor to the currently known situation as prototype
       * @return a shared pointer to the a situation message or to null if situation unknown.
       */
      SituationPtr currentSituaton();

    private:
      /**
       * @brief m_Scope the scope to listen to for situation classifications
       */
      rsb::Scope m_Scope;
      /**
       * @brief m_Listener listens on scope for classification messages
       */
      rsb::ListenerPtr m_Listener;

      /**
       * @brief m_CurrentData holds the current situation
       */
      SituationPtr m_CurrentData;

      /**
       * @brief eventHandle internal handle function calling the callback method.
       *
       * this functions checks for correct scope and type
       *
       * @param event the rsb event.
       */
      void eventHandle(rsb::EventPtr);

    };

  } // namespace io
} // sitinf
