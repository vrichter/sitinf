/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/RsbSink.h                                       **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <io/Sink.h>
#include <rsb/Factory.h>
#include <rsb/Informer.h>
#include <rsb/patterns/LocalServer.h>
#include <rsb/converter/ProtocolBufferConverter.h>
#include <rsb/converter/Repository.h>
#include <io/RsbFilters.h>
#include <utils/Exception.h>

namespace sitinf {
  namespace io {

    template <typename T>
    class RsbSink : public Sink<boost::shared_ptr<T> >
    {
    protected:

      /**
       * @brief The TCallback class holds and publishes T's.
       */
      class TCallback: public rsb::patterns::LocalServer::Callback<std::string, T> {
      public:
        TCallback(boost::shared_ptr<T> current)
        {
          updateCurrent(current);
        }

        boost::shared_ptr<T> call(const std::string &methodName, boost::shared_ptr<std::string> /*ignored*/) {
          return m_Current;
        }

        void updateCurrent(boost::shared_ptr<T> current){
          if(current.get()){
            m_Current = current;
          } else {
            throw utils::NullPointerException("Callback function needs a non-null shared pointer");
          }
        }

      private:
        boost::shared_ptr<T> m_Current;
      };

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<RsbSink<T> > Ptr;

      RsbSink(const rsb::Scope& scope,
              boost::shared_ptr<T> data=boost::shared_ptr<T>(),
              const std::string local_function="get")
        : m_Scope(scope), m_LocalFunction(local_function)
      {
        // try to register converter if not already done
        try {
          boost::shared_ptr<rsb::converter::ProtocolBufferConverter<T> >
              converter(new rsb::converter::ProtocolBufferConverter<T>());
          rsb::converter::converterRepository<std::string>()->registerConverter(converter);
        } catch (const std::exception& e){
          // already available do nothing
        }
        if(data.get()){
          init(data);
        }
      }

      void init(boost::shared_ptr<T>& data){
        // create informer
        m_Informer = rsb::getFactory().createInformer<T>(m_Scope);
        // publish passed data
        m_Informer->publish(data);
        // create server
        m_Server = rsb::getFactory().createLocalServer(m_Scope);
        // create and register callback
        m_Callback = typename utils::Shared<TCallback>::Ptr(new TCallback(data));
        m_Server->registerMethod(m_LocalFunction, m_Callback);
      }

      static RsbSink<T>::Ptr New(const rsb::Scope& scope,
                          boost::shared_ptr<T> data=boost::shared_ptr<T>(),
                          const std::string local_function="get")
      {
        return RsbSink<T>::Ptr(new RsbSink<T>(scope,data,local_function));
      }

      ~RsbSink(){}

      /**
       * @brief publish sets corrent to data and published it.
       * @param data the data to be used as current
       */
      void publish(boost::shared_ptr<T> data) override {
        if(!data.get()) return;
        if(!m_Informer.get()) init(data);
        m_Callback->updateCurrent(data);
        m_Informer->publish(data);
      }

      const rsb::Scope& scope(){
        return m_Scope;
      }

    private:
      /**
       * @brief m_Lock used to synchronize calls.
       */
      utils::Mutex m_Lock;
      /**
       * @brief m_Informer is used to publish current T's.
       */
      typename rsb::Informer<T>::Ptr m_Informer;
      /**
       * @brief m_Server answers RPC call for current T's.
       */
      rsb::patterns::LocalServerPtr m_Server;
      /**
       * @brief m_Callback the local server callback returning current T's.
       */
      typename utils::Shared<TCallback>::Ptr m_Callback;
      /**
       * @brief m_Scope the scope on which this RsbSink works.
       */
      rsb::Scope m_Scope;
      /**
       * @brief m_LocalFunction the function to which the server reacts.
       */
      std::string m_LocalFunction;
    };

  } // namespace io
} // namespace sitinf
