/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/SituationInformer.cpp                           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/SituationInformer.h>
#include <utils/Exception.h>
#include <inference/BayesianInference.h>

#include <rsb/converter/Repository.h>
#include <rsb/converter/ProtocolBufferConverter.h>
#include <rsb/Factory.h>
#include <utils/Macros.h>

#define _LOGGER "io.SituationInformer"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;
using namespace sitinf::network;
using namespace sitinf::inference;

// some typedefs for lesser writing
typedef rst::classification::ClassificationResultMap Situation;
typedef boost::shared_ptr<Situation> SituationPtr;
typedef rst::classification::ClassificationResultMap_Entry Entry;
typedef rst::classification::ClassificationResult ClassificationResult;
typedef rst::classification::ClassificationResult::ClassWithProbability ClassWithProbability;

SituationInformer::SituationInformer(std::string scope)
  : m_Scope(scope)
{
  // register converter for situation messages
  boost::shared_ptr<rsb::converter::ProtocolBufferConverter<Situation> >
      converter(new rsb::converter::ProtocolBufferConverter<Situation>());
  rsb::converter::converterRepository<std::string>()->registerConverter(converter);
  // create the informer
  rsb::Factory& factory = rsb::getFactory();
  m_Informer = factory.createInformer<Situation>(scope);
}

SituationInformer::~SituationInformer(){ /* currently nothing to do */}

SituationPtr createSituationMessage(
    const BayesianNetwork::Ptr network,
    const inference::InferenceResult::Ptr result,
    std::map<std::string,std::string>* outcomes=NULL)
{
  // create the Situation object
  SituationPtr situation(new Situation());
  // iterate over the results
  for(std::map<std::string,inference::InferenceResult::Outcomes>::iterator
      it = result->getOutcomes().begin(); it != result->getOutcomes().end(); ++it)
  {
    // get variable
    BayesianNetwork::Variable variable = network->variable(it->first);
    // create aspect classification
    Entry* aspect = new Entry();
    inference::InferenceResult::Outcomes& resultOutcome = it->second;
    // set aspect name from result
    aspect->set_name(it->first);
    // create classification
    ClassificationResult* state = aspect->mutable_result();
    // set decided class from max
    state->set_decided_class(variable.m_Outcomes.at(resultOutcome.m_MaxOutcome));
    // set class probabilities from outcomes
    for(uint j = 0; j < resultOutcome.m_Probabilities.size(); ++j){
      ClassWithProbability* outcome = state->add_classes();
      // class name is the outcome of the variable
      outcome->set_name(variable.m_Outcomes.at(j));
      // confidence is the corresponding belief
      outcome->set_confidence(resultOutcome.m_Probabilities.at(j));
    }
    // check map
    if(outcomes){
      // outcomes is available check whether
      if(outcomes->find(variable.m_Name) == outcomes->end() // variable not saved
         || outcomes->at(variable.m_Name) != variable.m_Outcomes.at(resultOutcome.m_MaxOutcome)) // or outcome changed
      {
        // variable outcome is new add to aspects, passing ownership
        situation->mutable_aspects()->AddAllocated(aspect);
        // update map
        outcomes->operator [](variable.m_Name) = variable.m_Outcomes.at(resultOutcome.m_MaxOutcome);
      } else {
        // variable outcome is known and has not changed. no publishing. delete aspect
        delete aspect;
      }
    } else {
      // outcomes save not passed publish all aspects. passing ownership.
      situation->mutable_aspects()->AddAllocated(aspect);
    }
  }
  return situation;
}

void SituationInformer::publish(const BayesianNetwork::Ptr network,
                                const InferenceResult::Ptr state,
                                const std::string& subscope)
{
  SituationPtr sit = createSituationMessage(network,state);
  //SLOG_TRACE << "Updated Situation:\n" << sit->DebugString()
  Locker l(m_Lock);
  if(subscope != ""){
    rsb::EventPtr event(new rsb::Event());
    event->setData(sit);
    event->setType("rst::classification::ClassificationResultMap");
    event->setScope(rsb::Scope(m_Scope + std::string("/") + subscope));
    m_Informer->publish(event);
  } else {
    m_Informer->publish(sit);
  }
}

FilteredSituationInformer::FilteredSituationInformer(std::string scope)
  : SituationInformer(scope), m_LastSubscope("")
{ /* currently nothing to do */}

FilteredSituationInformer::~FilteredSituationInformer(){ /* currently nothing to do */}

void FilteredSituationInformer::publish(const BayesianNetwork::Ptr network,
                                        const InferenceResult::Ptr state,
                                        const std::string& subscope)
{
  Locker l(m_Lock);
  SituationPtr sit = createSituationMessage(network,state,&m_MaxOutcomes);
  if(sit->aspects_size() || subscope != m_LastSubscope){
    SLOG_TRACE << "Updated Situation:\n" << sit->DebugString();
    // publish only when now empty
    m_Informer->publish(sit);
    m_LastSubscope = subscope;
  } else {
    SLOG_TRACE << "Situation not changed. Not publishing.";
  }
}


