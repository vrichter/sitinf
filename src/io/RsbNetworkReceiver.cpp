/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/RsbNetworkReceiver.cpp                          **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/RsbNetworkReceiver.h>
#include <utils/Exception.h>

#include <rsb/Factory.h>
#include <rsb/converter/ProtocolBufferConverter.h>
#include <rsb/converter/Repository.h>
#include <rsb/filter/ScopeFilter.h>

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;

RsbNetworkReceiver::RsbNetworkReceiver(const std::string scope, const std::string remote_function)
  : m_Scope(scope), m_Timestamp(boost::posix_time::microsec_clock::local_time())
{
  Locker lock(m_Lock);
  // register converter
  boost::shared_ptr<rsb::converter::ProtocolBufferConverter<rst::bayesnetwork::BayesNetwork> >
      converter(new rsb::converter::ProtocolBufferConverter<rst::bayesnetwork::BayesNetwork>());
  rsb::converter::converterRepository<std::string>()->registerConverter(converter);
  // create listener
  m_Listener = rsb::getFactory().createListener(m_Scope);
  m_Listener->addFilter(rsb::filter::FilterPtr(new rsb::filter::ScopeFilter(m_Scope)));

  // register handle
  m_Listener->addHandler(rsb::HandlerPtr(new rsb::EventFunctionHandler(
                                           boost::bind(&RsbNetworkReceiver::eventHandle,this,_1)
                                           )));
  // create remote server
  m_RemoteServer = rsb::getFactory().createRemoteServer(m_Scope);
  // call method on server
  rsb::patterns::RemoteServer::DataFuture<rst::bayesnetwork::BayesNetwork>
      data = m_RemoteServer->callAsync<rst::bayesnetwork::BayesNetwork>(remote_function,boost::shared_ptr<std::string>(new std::string("ignored")));
  m_ServerWait = boost::thread(boost::bind(&RsbNetworkReceiver::waitHandle, this, data));
}

RsbNetworkReceiver::BayesNetworkPtr RsbNetworkReceiver::currentNetwork(){
  return m_CurrentData;
}

void RsbNetworkReceiver::handle(BayesNetworkPtr data){
  if(!data.get()) return;
  boost::posix_time::ptime current = boost::posix_time::microsec_clock::local_time();
  boost::unique_lock<boost::mutex> l(m_Lock);
  if(current < m_Timestamp){
    return; //  ignore old data
  } else {
    m_Timestamp = current;
  }
  m_CurrentData = data;
  notify(m_CurrentData);
}

void RsbNetworkReceiver::eventHandle(rsb::EventPtr event){
  // check scope and type
  if(event->getScope() == m_Scope && event->getType() == "rst::bayesnetwork::BayesNetwork"){
    handle(boost::static_pointer_cast<rst::bayesnetwork::BayesNetwork>(event->getData()));
  }
}

void RsbNetworkReceiver::waitHandle(FutureBayesNetwork data){
  try{
  // wait for 2 seconds
  BayesNetworkPtr value = data.get(30);
  // pass returned data
  if(value.get()){
    handle(value);
  }
  } catch (const rsc::threading::FutureTimeoutException& e) {
    // no problem. server may be down.
    if(m_CurrentData.get()){
      // network already received via listener.
    } else {
      std::cout << "Could not get network from situation after 30 seconds. Maybe the server is down. Scope: '"
                << m_RemoteServer->getScope()->toString() << "'" << std::endl;
    }
  }
}
