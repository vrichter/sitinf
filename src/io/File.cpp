/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/File.cpp                                        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/File.h>
#include <utils/Exception.h>

#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <utils/Macros.h>

#define _LOGGER "io.File"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;

File::File(const std::string& filename, bool throw_existing, bool throw_missing, Type type){
  std::string error = "";
  if(!boost::filesystem::exists(filename)){
    if(throw_missing){
      error = "does not exist.";
    }
  } else { // file exists
    if(throw_existing){
      error = "already exists.";
    } else  {
      switch (type){
        case regular:
          if(!boost::filesystem::is_regular_file(filename)) error = "is not a regular file.";
          break;
        case other:
          if(!boost::filesystem::is_other(filename)) error = "is not of type 'other'.";
          break;
        case directory:
          if(!boost::filesystem::is_directory(filename)) error = "is not a directory.";
          break;
        case all:
          break;
        default:
          STHROW(IOException,"Unknown File::Type enum '" << type << "'");
      }
    }
    if(!error.empty()){
      STHROW(IOException, "File '" << filename << "' " << error);
    }
  }
}

ReadFile::ReadFile(const std::string &filename)
  : File(filename,false,true,regular), std::ifstream(filename.c_str(),std::ios_base::in)
{
  SASSERT_THROW(this->is_open(),IOException,"Cant open file '" << filename << "' for reading.");
}

inline std::ios_base::openmode create_openmode(bool append, bool binary){
  std::ios_base::openmode ret = std::ios_base::out;
  if(append) ret = ret | std::ios_base::app;
  if(binary) ret = ret | std::ios_base::binary;
  return ret;
}

WriteFile::WriteFile(const std::string &filename, bool open_existing, bool create_new, bool append, bool binary)
  : File(filename,!open_existing,!create_new,regular), std::ofstream(filename,create_openmode(append,binary))
{
  SASSERT_THROW(this->is_open(),IOException,"Cant open file '" << filename << "' for writing.");
}

Directory::Directory(const std::string& path)
  : File(path,false,true,directory), m_Path(path)
{}

std::map<File::Type,std::vector<std::string>> Directory::files(const std::string& filter){
  namespace fs = boost::filesystem;
  std::map<File::Type,std::vector<std::string>> result;

  fs::directory_iterator end;
  for(fs::directory_iterator files(m_Path);
      files != end; // != end
      ++files) {
    boost::smatch match;
    if(!boost::regex_match(files->path().string(),match,boost::regex(filter))){
      continue;
    }
    try {
      if (fs::is_directory(files->status())){
        result[File::directory].push_back(files->path().string());
      }
      else if (fs::is_regular_file(files->status())) {
        result[File::regular].push_back(files->path().string());
      }
      else{
        result[File::other].push_back(files->path().string());
      }
    }
    catch ( const std::exception & ex ) {
      SLOG_WARNING << "File with path '" << files->path().string() << "' is of unknown type.";
    }
  }
  return result;
}
