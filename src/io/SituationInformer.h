/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/SituationInformer.h                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <network/BayesianNetwork.h>
#include <rsb/Informer.h>
#include <rst/classification/ClassificationResultMap.pb.h>

namespace sitinf {

namespace inference {
  /**
   * forward declaration of the InferenceResult type.
   */
  struct InferenceResult;
} // namespace inference

  namespace io {

    /**
     * @brief The SituationInformer class is used to publish the current state
     * of a BayesNetwork via rsb.
     */
    class SituationInformer {

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<SituationInformer> Ptr;

      /**
       * @brief SituationInformer uses the passed scope to create an rsb publisher
       *        for later publishing of BayesNetwork states.
       * @param scope the rsb scope to be used for publishing
       */
      SituationInformer(std::string scope);

      /**
       * @brief A virtual destructor to be reimplemented in subclasses for nice cleanup.
       */
      virtual ~SituationInformer();

      /**
       * @brief Creates a Situation message and publishes the passed beliefs.
       *
       * @param network The BayesianNetwork describing the Situation.
       * @param state The state of the network as InferenceResult.
       * @param subscope publish data on subscope. no subscope if left empty.
       */
      virtual void publish(const network::BayesianNetwork::Ptr network,
                           const boost::shared_ptr<inference::InferenceResult> state,
                           const std::string& subscope = "");

    protected:
      /**
       * @brief m_Lock A lock for thread safety.
       */
      utils::Mutex m_Lock;
      /**
       * @brief m_Informer to publish Situation objects.
       */
      rsb::Informer<rst::classification::ClassificationResultMap>::Ptr m_Informer;

      /**
       * @brief m_Scope the root scope to use for publishing data.
       */
      std::string m_Scope;
    };

    /**
     * @brief The FilteredSituationInformer class is used to publish state updates
     * of a BayesNetwork via rsb.
     */
    class FilteredSituationInformer : public SituationInformer {

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<FilteredSituationInformer> Ptr;

      /**
       * @brief FilteredSituationInformer uses the passed scope to create an rsb publisher
       *        for later publishing of BayesNetwork states.
       * @param scope the rsb scope to be used for publishing
       */
      FilteredSituationInformer(std::string scope);

      /**
       * @brief A virtual destructor to be reimplemented in subclasses for nice cleanup.
       */
      virtual ~FilteredSituationInformer();

      /**
       * @brief Creates a Situation message and publishes the passed beliefs.
       *
       * @param network The BayesianNetwork describing the Situation.
       * @param state The states of the network as InferenceResult.
       * @param subscope publish data on subscope. no subscope if left empty.
       */
      virtual void publish(const network::BayesianNetwork::Ptr network,
                           const boost::shared_ptr<inference::InferenceResult> state,
                           const std::string& subscope) override;

    private:
      /**
       * @brief m_Maximums holds for every known variable name the last known
       * maximum probable outcome.
       */
      std::map<std::string,std::string> m_MaxOutcomes;
      std::string m_LastSubscope;

    };

  } // namespace io
} // namespace sitinf
