/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/MessageParser.cpp                               **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/MessageParser.h>
#include <utils/Exception.h>
#include <google/protobuf/dynamic_message.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/text_format.h>

#include <utils/Macros.h>

#define _LOGGER "io.MessageParser"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;

MessageParser::MessageParser(){ /* currently nothing to do */ }

Shared<google::protobuf::Message>::Ptr MessageParser::parseWireFormat(std::istream* binary_data,
    const google::protobuf::Descriptor* descriptor)
{
  SASSERT_THROW(descriptor,NullPointerException,"Passed descriptor pointer is NULL");
  SASSERT_THROW(binary_data,NullPointerException,"Passed stream pointer is NULL");
  // create the message pointer
  Shared<google::protobuf::Message>::Ptr message(m_Factory.GetPrototype(descriptor)->New());
  // parse data
  message->ParseFromIstream(binary_data);
  SASSERT_THROW(message->ParseFromIstream(binary_data),IOException,
                      "Could not parse passed stream as binary wiredata of type '" << descriptor->full_name() << "'");
  return message;
}

Shared<google::protobuf::Message>::Ptr MessageParser::parseJsonFormat(std::istream* json_data,
    const google::protobuf::Descriptor* descriptor)
{
  SASSERT_THROW(descriptor,NullPointerException,"Passed descriptor pointer is NULL");
  SASSERT_THROW(json_data,NullPointerException,"Passed stream pointer is NULL");
  // create an protobuf input stream from passed string
  google::protobuf::io::IstreamInputStream stream(json_data);
  // create the mesage pointer
  Shared<google::protobuf::Message>::Ptr message(m_Factory.GetPrototype(descriptor)->New());
  // parse message from stream
  SASSERT_THROW(google::protobuf::TextFormat::Parse(&stream,message.get()),IOException,
                      "Could not parse passed stream as json data of type '" << descriptor->full_name() << "'");
  return message;
}
