/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/File.h                                          **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <string>
#include <fstream>
#include <map>
#include <vector>

namespace sitinf {
  namespace io {

    /**
     * @brief The File class base class enforces basic checks before opening files.
     */
    class File {

    public:

      enum Type {
        regular,
        directory,
        other,
        all
      };

      /**
       * @brief File constructor checks whether passed file exists and is regular.
       * @param filename the path and name of the file
       * @param throw_existing if this is true and the file exists an IOException is thrown
       * @param throw_missing if this is true and the file is missing an IOException is thrown
       * @param type the expected file type. Throws an IOException when not matching.
       */
      File(const std::string& filename, bool throw_existing, bool throw_missing, Type type);

    private:

      /**
       * @brief File empty constuctor is private preventing creation of files without name.
       */
      File();

    };

    /**
     * @brief The ReadFile class can be used to read files as input file stream.
     */
    class ReadFile : public File, public std::ifstream {
    public:
      /**
       * @brief ReadFile ensures that the file exists and can be opened.
       * @param filename the path and name of the file
       */
      ReadFile(const std::string& filename);

    };

    /**
     * @brief The WriteFile class can be used to write data into a file via an ofstream.
     */
    class WriteFile : public File, public std::ofstream {
    public:
      /**
       * @brief WriteFile ensures that the file exists and can be opened.
       * @param filename the path and name of the file
       * @param open_existing if it is ok to open existing files
       * @param create_new if it is ok to create new files
       * @param append whether to append to file instead of overwriting
       * @param binary whether to write in binary mode
       */
      WriteFile(const std::string& filename,
                bool open_existing=false,
                bool create_new=true,
                bool append=true,
                bool binary=false);

    };

    class Directory : public File {

    public:
      Directory(const std::string& path);

      std::map<File::Type,std::vector<std::string>> files(const std::string& filter=".*");

    private:
      std::string m_Path;
    };

  } // namespace io
} // namespace sitinf
