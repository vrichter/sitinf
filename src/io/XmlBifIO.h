/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/XmlBifIO.h                                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <string>

#include <network/BayesianNetwork.h>

namespace sitinf {
  namespace io {

    /**
     * This class provides functions to read and write BayesianNetwork into XMLBIF documents.
     *
     * The read/created documents are of the <a href="http://www.cs.cmu.edu/~fgcozman/Research/InterchangeFormat/">Interchange Format for Bayesian Networks (XMLBIF)</a>,
     * which has the format:
   \verbatim
       <!DOCTYPE BIF [
         <!ELEMENT BIF ( NETWORK )*>
           <!ATTLIST BIF VERSION CDATA #REQUIRED>
         <!ELEMENT NETWORK ( NAME, ( PROPERTY | VARIABLE | DEFINITION )* )>
         <!ELEMENT NAME (#PCDATA)>
         <!ELEMENT VARIABLE ( NAME, ( OUTCOME |  PROPERTY )* ) >
           <!ATTLIST VARIABLE TYPE (nature|decision|utility) "nature">
         <!ELEMENT OUTCOME (#PCDATA)>
         <!ELEMENT DEFINITION ( FOR | GIVEN | TABLE | PROPERTY )* >
         <!ELEMENT FOR (#PCDATA)>
         <!ELEMENT GIVEN (#PCDATA)>
         <!ELEMENT TABLE (#PCDATA)>
         <!ELEMENT PROPERTY (#PCDATA)>
         ]>
   \endverbatim
     */
    class XmlBifIO {

    public:

      /**
       * @brief readFile reads a BayesianNetwork from an xml file
       * @param filename the path to the xml file
       * @return a pointer to the BayesianNetwork
       */
      static network::BayesianNetwork::Ptr readFile(const std::string& filename);

      /**
       * @brief readStream reads a BayesianNetwork from an xml input stream
       * @param input_stream the stream providing xml data
       * @return a pointer to the BayesianNetwork
       */
      static network::BayesianNetwork::Ptr readStream(std::istream& input_stream);

      /**
       * @brief writeFile writes an xml file describing the passed BayesianNetwork
       * @param filename the name of the xml file to write
       * @param bayesian_network the network to save
       */
      static void writeFile(const std::string& filename, const network::BayesianNetwork& bayesian_network);

      /**
       * @brief writeStream writes an xml descrption of the passed BayesianNetwork into a stream
       * @param output_stream the stream to write to
       * @param bayesian_network the network to save
       */
      static void writeStream(std::ostream& output_stream, const network::BayesianNetwork& bayesian_network);
    };
  } // namespace io
} // namespace sitinf
