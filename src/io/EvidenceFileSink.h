/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/EvidenceFileSink.h                              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <io/File.h>
#include <io/Sink.h>
#include <network/AspectStateEvidence.h>
#include <rst/bayesnetwork/BayesNetworkEvidence.pb.h>

namespace sitinf {
  namespace io {

    class EvidenceFileSink :
        public Sink<const network::AspectStateEvidence& >,
        public Sink<utils::Shared<rst::classification::ClassificationResultMap>::Ptr >
    {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<EvidenceFileSink> Ptr;

      EvidenceFileSink(const std::string& filename);

      ~EvidenceFileSink();

      virtual void publish(const network::AspectStateEvidence& data) override;
      virtual void publish(utils::Shared<rst::classification::ClassificationResultMap>::Ptr data) override;

    private:
      void publish();
      WriteFile m_File;
      network::AspectStateEvidence m_LastEvidence;
      network::AspectStateEvidence m_LastOverride;
      boost::shared_ptr<rst::bayesnetwork::BayesNetworkEvidence> m_LastMessage;
    };

    class BayesNetworkEvidenceStreamSink : public Sink<const rst::bayesnetwork::BayesNetworkEvidence* > {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<BayesNetworkEvidenceStreamSink> Ptr;

      BayesNetworkEvidenceStreamSink(std::ostream* stream);

      ~BayesNetworkEvidenceStreamSink();

      virtual void publish(const rst::bayesnetwork::BayesNetworkEvidence* data) override;

    private:
      std::ostream* m_Stream;

    };

  } // namespace io
} // namespace sitinf
