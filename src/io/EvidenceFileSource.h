/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/EvidenceFileSource.h                            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <io/Source.h>
#include <network/AspectStateEvidence.h>
#include <io/File.h>
#include <rst/bayesnetwork/BayesNetworkEvidence.pb.h>

namespace sitinf {

  class EvidenceFileSource : public io::PullSource<network::WeightedAspectStateEvidence>
  {
  public:

    /**
     * @brief Ptr typedef for shared pointer type
     */
    typedef boost::shared_ptr<EvidenceFileSource> Ptr;

    EvidenceFileSource(const std::string& filename);

    virtual ~EvidenceFileSource();

  protected:

    /**
     * @brief aquireNext reads the next AspectStateEvidence
     * @return the next parsable AspectStateEvidence
     * @throw NoMoreDataAvailableException when the stream ended.
     */
    virtual network::WeightedAspectStateEvidence aquireNext() override;

  private:
    std::string m_Filename;
    uint m_Line;
    io::ReadFile m_File;

  };

  class BayesNetworkEvidenceStreamSource : public io::PullSource<rst::bayesnetwork::BayesNetworkEvidence>{
  public:
    /**
     * @brief Ptr typedef for shared pointer type
     */
    typedef boost::shared_ptr<BayesNetworkEvidenceStreamSource> Ptr;

    BayesNetworkEvidenceStreamSource(std::istream* stream);

    virtual ~BayesNetworkEvidenceStreamSource();

  protected:

    /**
     * @brief aquireNext reads the next BayesNetworkEvidence
     * @return the next parsable BayesNetworkEvidence
     * @throw NoMoreDataAvailableException when the stream ended.
     */
    virtual rst::bayesnetwork::BayesNetworkEvidence aquireNext() override;

  private:
    std::istream* m_Stream;
    uint m_Line;

  };

} // namespace sitinf
