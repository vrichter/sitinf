/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/ProtoFilesImporter.h                            **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <google/protobuf/compiler/importer.h>
#include <boost/shared_ptr.hpp>

namespace sitinf {
  namespace io {

    /**
     * @brief The PrintingErrorCollector class is an ErrorCollector implementation needed by the ProtoFilesImporter.
     * It prints all errors to std::cout
     */
    class PrintingErrorCollector : public google::protobuf::compiler::MultiFileErrorCollector {

      /**
       * @brief AddError prints an error to std::cout
       * @param filename of the file which caused the error
       * @param line in which the error happened
       * @param column in which the error happened
       * @param message the error message
       */
      void AddError(const std::string & filename, int line, int column, const std::string & message);
    };

    /**
     * @brief The ProtoFilesImporter class can be used for dynamic loading of
     * protobuf::Descriptors from proto files in the filesystem.
     */
    class ProtoFilesImporter {
    public:

      /**
       * @brief Ptr a shared pointer to this class
       */
      typedef boost::shared_ptr<ProtoFilesImporter> Ptr;

      /**
       * @brief ProtoFilesImporter sets up internal data for Descriptor loading.
       * @param path to the root of the proto files. May be multiple paths split by 'delimiter'.
       * @param delimiter set of delimiter-characters to split multiple paths in the passed path string.
       */
      ProtoFilesImporter(const std::string& path, const std::string& delimiter = ":");

      /**
       * @brief ProtoFilesImporter sets up internal data for Descriptor loading.
       *
       * This constructor uses prefix or a default path as proto root folder depending
       * on the existence of a prefix environment-variable.
       */
      ProtoFilesImporter();

      /**
       * @brief findDescriptor searches for a Descriptor for the type corresponding to the
       * passed typeName
       * @param typeName the name of the prototype
       * @return the descriptor of the type or NULL if not found
       */
      const google::protobuf::Descriptor* findDescriptor(const std::string& typeName);

      /**
       * @brief isBasicType can be used to check whether type is one of protobufs basic types
       * @param type the name of the type
       * @return true if type is a basic type
       */
      static bool isBasicType(const std::string& type);

    private:
      /**
       * @brief m_SourceTree maintains the source files found in path
       */
      google::protobuf::compiler::DiskSourceTree m_SourceTree;
      /**
       * @brief m_ErrorCollector handles error messages
       */
      PrintingErrorCollector m_ErrorCollector;
      /**
       * @brief m_Importer finds and compiles needed proto files
       */
      google::protobuf::compiler::Importer m_Importer;
      /**
       * @brief m_Imported maintains the set of imported Descriptors types to they are
       * imported only once.
       */
      std::set<std::string> m_Imported;
    };

  } // namespace io
} // namespace sitinf
