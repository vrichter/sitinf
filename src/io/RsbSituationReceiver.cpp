/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/RsbSituationReceiver.cpp                        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/RsbSituationReceiver.h>
#include <utils/Exception.h>

#include <rsb/Factory.h>
#include <rsb/converter/ProtocolBufferConverter.h>
#include <rsb/converter/Repository.h>

using namespace sitinf;
using namespace sitinf::io;

RsbSituationReceiver::RsbSituationReceiver(const std::string scope)
  : m_Scope(scope)
{
  // register converter
  boost::shared_ptr<rsb::converter::ProtocolBufferConverter<Situation> >
      converter(new rsb::converter::ProtocolBufferConverter<Situation>());
  rsb::converter::converterRepository<std::string>()->registerConverter(converter);
  // create listener
  m_Listener = rsb::getFactory().createListener(m_Scope);

  // register handle
  m_Listener->addHandler(rsb::HandlerPtr(new rsb::EventFunctionHandler(
                                           boost::bind(&RsbSituationReceiver::eventHandle,this,_1)
                                           )));
}

RsbSituationReceiver::SituationPtr RsbSituationReceiver::currentSituaton(){
  return m_CurrentData;
}

void RsbSituationReceiver::eventHandle(rsb::EventPtr event){
  // check scope and type
  if(event->getScope() == m_Scope && event->getType() == "rst::classification::ClassificationResultMap"
     && event->getData().get())
  {
    m_CurrentData = boost::static_pointer_cast<Situation>(event->getData());
    notify(m_CurrentData);
  }
}
