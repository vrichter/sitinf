/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/EvidenceFileSource.cpp                          **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <io/EvidenceFileSource.h>
#include <utils/Types.h>
#include <utils/Exception.h>
#include <utils/Macros.h>
#include <google/protobuf/text_format.h>

#define _LOGGER "io.EvidenceFileSource"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::utils;
using namespace sitinf::network;

EvidenceFileSource::EvidenceFileSource(const std::string& filename)
  : m_Filename(filename), m_Line(0), m_File(filename) {}

EvidenceFileSource::~EvidenceFileSource(){}

WeightedAspectStateEvidence EvidenceFileSource::aquireNext() {
  // WeightedAspectStateEvidence is coded as one BayesNetworkEvidence in every line
    rst::bayesnetwork::BayesNetworkEvidence evidence;
    std::string stateData;
    stateData.reserve(1024);
    while(true){
      ++m_Line;
      stateData.clear();
      if(!m_File.good()){
        STHROW(NoMoreDataAvailableException,"There is no more data available in this Source.");
      }
      std::getline(m_File,stateData);
      if(stateData.empty()){
        SLOG_TRACE << "Ignoring empty evidence line from " << m_Filename << ":" << m_Line;
        continue;
      }
      bool error = !google::protobuf::TextFormat::ParseFromString(stateData,&evidence);
      if(error){
        // read error
        SLOG_WARNING << "Can not parse evidence from " << m_Filename << ":" << m_Line << ": " << stateData;
        continue;
      } else {
        WeightedAspectStateEvidence ret;
        for (int i = 0; i < evidence.observations_size(); ++i){
          ret.newEvidence(evidence.observations(i).variable(),evidence.observations(i).state());
        }
        if(evidence.has_duration_microseconds()){
          ret.weight(evidence.duration_microseconds() / 1000000.);
        }
        return ret;
      }
    }
}


BayesNetworkEvidenceStreamSource::BayesNetworkEvidenceStreamSource(std::istream* stream)
  : m_Stream(stream) {}

BayesNetworkEvidenceStreamSource::~BayesNetworkEvidenceStreamSource() {}

rst::bayesnetwork::BayesNetworkEvidence BayesNetworkEvidenceStreamSource::aquireNext() {
    rst::bayesnetwork::BayesNetworkEvidence evidence;
    std::string stateData;
    stateData.reserve(1024);
    while(true){
      ++m_Line;
      stateData.clear();
      if(!m_Stream->good()){
        STHROW(NoMoreDataAvailableException,"There is no more data available in this Source.");
      }
      std::getline(*m_Stream,stateData);
      if(stateData.empty()){
        SLOG_TRACE << "Ignoring empty evidence line from stream" << ":" << m_Line;
        continue;
      }
      bool error = !google::protobuf::TextFormat::ParseFromString(stateData,&evidence);
      if(error){
        // read error
        SLOG_WARNING << "Can not parse evidence from stream " << ":" << m_Line << ": " << stateData;
        continue;
      } else {
        return evidence;
      }
    }

}
