/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/Source.h                                        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <utils/Subject.h>
#include <utils/Adapter.h>
#include <utils/Converter.h>

namespace sitinf {
  namespace io {

    template <typename T>
    /**
     * @brief The Source class provides an interface for all classes providing serial data.
     */
    class Source : public utils::Subject<T>
    {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<Source<T> > Ptr;

      /**
       * @brief current can be used to access the current T
       * @return a subclass may return appropriate T instances,
       * null pointers or raise an exception.
       */
      virtual T current() = 0;

    };

    template<typename T>
    /**
     * @brief The NoOpSource class does nothing.
     * Only returns the data passed to the constructor on current().
     */
    class NoOpSource : public Source<T>
    {
    public:
      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<NoOpSource<T> > Ptr;

      /**
       * @brief NoOpSource creates a NoOpSource instance.
       * @param data the data to return on current()
       */
      NoOpSource(const T& data)
        : m_Data(data){}

      /**
       * @brief current returns the data passed in the constructor
       * @return a copy of data
       */
      virtual T current() override final{
        return m_Data;
      }

    private:
      /**
       * @brief m_Data internally held copy of data passed in the constructor.
       */
      T m_Data;

    };

    template <typename T>
    /**
     * @brief The PullSource class is a specialized version of a Source class which needs data to be actively requested.
     *
     * Calling when next() is called on a PullSource it aquires a new data object
     * notifies all Observers and returns the new data.
     */
    class PullSource : public Source<T>
    {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<PullSource<T> > Ptr;

      /**
       * @brief current can be used to access the current T
       * @return will return the locally cached data point.
       */
      virtual T current() final {
        return m_CurrentData;
      }

      /**
       * @brief next actively pulls a new data point from the source and notifies observers.
       *
       * @return the next T of this source
       * @throw NoMoreDataAvailableException when this source dryed out (can not produce more data)
       */
      virtual T next() final {
        m_CurrentData = aquireNext();
        if(Source<T>::observerCount()){
          Source<T>::notify(m_CurrentData);
        }
        return m_CurrentData;
      }

    protected:

      /**
       * @brief aquireNext gets the next data from the source.
       *
       * This needs to be implemented by a subclass to provide pull functionality for a PullSource.
       *
       * @return the new data.
       */
      virtual T aquireNext() = 0;

    private:

      /**
       * @brief m_CurrentData cache for the current T data point.
       */
      T m_CurrentData;
    };

    template<typename OUT,typename IN>
    class SourceAdapter : public Source<OUT>, public utils::Observer<IN> {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<SourceAdapter<OUT,IN> > Ptr;

      SourceAdapter(typename Source<IN>::Ptr source, typename utils::Shared<utils::Converter<OUT,IN>>::Ptr converter)
        : m_Source(source), m_Converter(converter)
      {
        m_Current = m_Converter->convert(source->current());
        m_Source->addObserver(this);
      }

      static SourceAdapter<OUT,IN>::Ptr New(typename Source<IN>::Ptr source,
                                            typename utils::Converter<OUT,IN>::Ptr converter)
      {
        return SourceAdapter<OUT,IN>::Ptr(new SourceAdapter<OUT,IN>(source,converter));
      }

      virtual void update(const utils::Subject<IN>* subject, IN data) final {
        m_Current = m_Converter->convert(data);
        this->notify(m_Current);
      }

      virtual OUT current() final {
        return m_Current;
      }

    private:
      typename Source<IN>::Ptr m_Source;
      typename utils::Shared<utils::Converter<OUT,IN>>::Ptr m_Converter;
      OUT m_Current;
    };


    class SourceAdapterFactory{

    public:

      template<typename OUT,typename IN>
      static typename Source<OUT>::Ptr create(typename Source<IN>::Ptr source){
        typename utils::Shared<utils::Converter<OUT,IN>>::Ptr converter(new utils::Converter<OUT,IN>());
        return typename SourceAdapter<OUT,IN>::Ptr(new SourceAdapter<OUT,IN>(source,converter));
      }
    };
  } // namespace io
} // namespace sitinf
