/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/RsbFilters.h                                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <rsb/filter/Filter.h>
#include <rsb/Scope.h>

namespace sitinf {
  namespace io {

    /**
     * @brief The ScopeFilter class can be used to filter rsb Events for a specific scope.
     */
    class ScopeFilter : public rsb::filter::Filter {
    public:

      /**
       * @brief ScopeFilter constructor sets up filter from string.
       * @param scope the rsb scope on which to accept messages
       * @param sub_scopes whether to accept messages from subscopes
       * @throw IOException when scope can not be parsed.
       */
      ScopeFilter(const std::string& scope, bool sub_scopes);

      /**
       * @brief ScopeFilter constructor using an rsb scope object
       * @param scope the rsb scope on which to accept messages
       * @param sub_scopes whether to accept messages from subscopes
       */
      ScopeFilter(const rsb::Scope& scope, bool sub_scopes);

      /**
       * @brief match checks whether the scpe of the passed event can be accepted.
       * @param event the event to check
       * @return true when this filter lets the event pass.
       */
      virtual bool match(rsb::EventPtr event);

    private:
      /**
       * @brief m_Scope internally held scope descriptor.
       */
      rsb::Scope m_Scope;
      /**
       * @brief m_SubScopes whether to use subscopes.
       */
      bool m_SubScopes;
    };

    /**
     * @brief The SchemaAndByteArrayTypeFilter class filters messages for a specific type.
     */
    class SchemaAndByteArrayTypeFilter : public rsb::filter::Filter {
    public:

      /**
       * @brief SchemaAndByteArrayTypeFilter constructor sets up filter
       * @param type the type to be accepted by this filter.
       */
      SchemaAndByteArrayTypeFilter(const std::string& type);

      /**
       * @brief match checks whether the type of the passed event equals this filters type.
       * @param event the event to check
       * @return true when this filter lets the event pass.
       */
      virtual bool match(rsb::EventPtr event);

    private:
      /**
       * @brief m_Type the message type string.
       */
      std::string m_Type;
    };

  } // namespace io
} // sitinf
