/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/RsbSource.h                                     **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <boost/shared_ptr.hpp>
#include <io/Source.h>
#include <rsb/Factory.h>
#include <rsb/Informer.h>
#include <rsb/converter/ProtocolBufferConverter.h>
#include <rsb/converter/Repository.h>
#include <io/RsbFilters.h>
#include <boost/algorithm/string/replace.hpp>

namespace sitinf {
  namespace io {


    template<typename Type>
    void register_rst(){
      // try to register converter if not already done
      try {
        boost::shared_ptr<rsb::converter::ProtocolBufferConverter<Type> > converter(
          new rsb::converter::ProtocolBufferConverter<Type>());
        rsb::converter::converterRepository<std::string>()->registerConverter(converter);
      } catch (const std::exception& e) {
        // already available do nothing
      }
    }

    template<typename First, typename Second, typename... Rest>
    void register_rst(){
      // register list recursive
      register_rst<First>();
      register_rst<Second, Rest...>();
    }

    template <typename T>
    class RsbSource : public Source<boost::shared_ptr<T> >
    {
    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<RsbSource<T> > Ptr;

      typedef boost::shared_ptr<T> ResultType;

      RsbSource(const rsb::Scope& scope,
                const typename utils::Shared<T>::Ptr default_instance = typename utils::Shared<T>::Ptr(nullptr),
                const std::string remote_function="get")
        : m_Scope(scope), m_CurrentData(default_instance)
      {
        register_rst<T>();
        // create listener
        m_Listener = rsb::getFactory().createListener(m_Scope);
        m_Listener->addFilter(rsb::filter::FilterPtr(new ScopeFilter(m_Scope,false)));

        // register handle
        m_Listener->addHandler(rsb::HandlerPtr(new rsb::EventFunctionHandler(
                                                 boost::bind(&RsbSource<T>::eventHandle,this,_1)
                                               )));
        // create remote server
        m_RemoteServer = rsb::getFactory().createRemoteServer(m_Scope);
        // call method on server
        rsb::patterns::RemoteServer::DataFuture<T> dataFuture =
          m_RemoteServer->callAsync<T>(remote_function,boost::shared_ptr<std::string>(new std::string("ignored")));
        m_ServerWait = boost::thread(boost::bind(&RsbSource<T>::waitHandle, this, dataFuture));
      }

      virtual ~RsbSource(){
      }

      /**
       * @brief New a shortcut to get a shared pointer to a new instance.
       * @param scope the rsb scope to use for data acuisition
       * @param default_instance the default T instance to return when none could be aquired
       * @param remote_function the name of the remote function to use for first acquisition
       * @return a shared pointer to a newly created instance.
       */
      static RsbSource<T>::Ptr New(const rsb::Scope& scope,
                            const typename utils::Shared<T>::Ptr default_instance = typename utils::Shared<T>::Ptr(nullptr),
                            const std::string remote_function="get")
      {
        return RsbSource<T>::Ptr(new RsbSource<T>(scope,default_instance,remote_function));
      }

      ResultType current() override {
        return m_CurrentData;
      }

      const rsb::Scope& scope(){
        return m_Scope;
      }

    private:
      /**
       * @brief m_Lock used to synchronize calls
       */
      utils::Mutex m_Lock;
      /**
       * @brief m_Scope the scope to listen for updates
       */
      rsb::Scope m_Scope;
      /**
       * @brief m_Listener listens for data updates
       */
      rsb::ListenerPtr m_Listener;
      /**
       * @brief m_RemoteServer is used to get the first data.
       */
      rsb::patterns::RemoteServerPtr m_RemoteServer;
      /**
       * @brief m_ServerWait a thread awaiting the server to time out when it is not running.
       */
      boost::thread m_ServerWait;
      /**
       * @brief m_CurrentData holds the current data
       */
      ResultType m_CurrentData;

      /**
       * @brief handle internal handle function calling the callback method.
       * @param data the rsb message.
       * @param replace whether to replace current data if available
       */
      void handle(ResultType data, bool replace=true){
        if(!data.get()) return;
        boost::unique_lock<boost::mutex> l(m_Lock);
        if(!replace && m_CurrentData.get()) {
          // do not replace available data
          return;
        }
        m_CurrentData = data;
        this->notify(m_CurrentData);
      }

      /**
       * @brief eventHandle internal handle function calling the callback method.
       *
       * this functions checks for correct scope and type
       *
       * @param event the rsb event.
       */
      void eventHandle(rsb::EventPtr event){
        // we need the message type name. protobuf returns point separated, rsb colon separated.
        static std::string typeName = "";
        if(typeName.empty()) {
          typeName = T::default_instance().GetTypeName();
          boost::replace_all(typeName,".","::");
        }
        // check scope and type
        if(event->getScope() == m_Scope && event->getType() == typeName) {
          handle(boost::static_pointer_cast<T>(event->getData()));
        }
      }

      /**
       * @brief waitHandle waits until future returns the data or breaks then calls the handle.
       * @param future the future data handle.
       */
      void waitHandle(rsb::patterns::RemoteServer::DataFuture<T> future){
        try{
          // wait for 30 seconds
          ResultType value = future.get(30);
          // pass returned data, always prefer informer data over server
          if(value.get()) {
            handle(value,false);
          }
        } catch (rsc::threading::FutureTimeoutException& e) {
          if(m_CurrentData.get()) {
            // network already received via listener.
          } else {
            // server may be down.
            std::cout << "Could not get public data from server after 30 seconds. Maybe the server is down. Scope: '"
                      << m_RemoteServer->getScope()->toString() << "'" << std::endl;
          }
        }
      }
    };

  } // namespace io
} // namespace sitinf
