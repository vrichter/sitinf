/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/ProtoFilesImporter.cpp                          **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <google/protobuf/descriptor.h>

#include <iostream>
#include <algorithm>

#include <utils/Types.h>

#include <io/ProtoFilesImporter.h>
#include <io/File.h>
#include <utils/Macros.h>
#include <boost/tokenizer.hpp>

#define _LOGGER "io.ProtoFilesImporter"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);

using namespace sitinf;
using namespace sitinf::io;
using namespace google::protobuf;
using namespace google::protobuf::compiler;

std::string findProtoPath(){
  // from environment variable
  const char* PROTOPATH = std::getenv("RST_PROTO_PATH");
  std::string protopath = (PROTOPATH) ? std::string(PROTOPATH) : "";
  if(!protopath.empty()) return protopath;

  // use prefix if available else fallback to default path
  const char* PREFIX = std::getenv("prefix");
  if(!PREFIX) PREFIX = std::getenv("PREFIX");
  std::string prefix = (PREFIX) ? std::string(PREFIX) : "/usr";

  // should be in $prefix/share/rst*/proto
  Directory share(prefix + std::string("/share"));
  auto rst_paths = share.files(".*/share/rst.*");
  for(std::string path : rst_paths[File::Type::directory]){
    Directory rst(path);
    std::vector<std::string> protos = rst.files(".*/share/rst.*/proto")[File::Type::directory];
    if(!protos.empty()){
      // found a proto directory.
      protopath = protos.front();
    }
  }
  return protopath;
}

class BasicTypeSet : public std::set<std::string> {
public:
  BasicTypeSet(){
    insert("double");
    insert("float");
    insert("int32");
    insert("int64");
    insert("uint32");
    insert("uint64");
    insert("sint32");
    insert("sint64");
    insert("fixed32");
    insert("fixed64");
    insert("sfixed32");
    insert("sfixed64");
    insert("bool");
    insert("string");
    insert("bytes");
  }
};

void PrintingErrorCollector::AddError(const std::string& filename, int line, int column, const std::string& message){
  SLOG_WARNING << "Encountered an error while parsing message definition."
                 << " Filename: " << filename
                 << " Line,Column: " << line << "," << column
                 << " Error: " << message << std::endl;
}

ProtoFilesImporter::ProtoFilesImporter(const std::string& path, const std::string& delimiter)
  : m_SourceTree(), m_ErrorCollector(), m_Importer(&m_SourceTree, &m_ErrorCollector)
{
  boost::char_separator<char> separator(delimiter.c_str());
  boost::tokenizer<boost::char_separator<char>> tokens(path, separator);
  for(auto token : tokens){
    if(!token.empty()){
      // add default rst,rst-experimental map paths
      m_SourceTree.MapPath("",token);
      m_SourceTree.MapPath("rst",token+std::string("/experimental/rst"));
      m_SourceTree.MapPath("rst",token+std::string("/sandbox/rst"));
      m_SourceTree.MapPath("rst",token+std::string("/stable/rst"));
      m_SourceTree.MapPath("rst",token+std::string("/deprecated/rst"));
    }
  }
}

ProtoFilesImporter::ProtoFilesImporter() : ProtoFilesImporter(findProtoPath()) {}

const Descriptor* ProtoFilesImporter::findDescriptor(const std::string& typeName){
  // remove point at the begining of typeName (.rst.Type -> rst.Type)
  std::string name = typeName[0] == '.' ? typeName.substr(1) : typeName;

  // check whether already tried to import corresponding file
  if(m_Imported.find(typeName) == m_Imported.end()){
      // create file name from
      std::string fileName = name;
      std::replace(fileName.begin(),fileName.end(),'.','/');
      fileName += std::string(".proto");
      // import
      SLOG_TRACE << "importing file " << fileName;
      m_Importer.Import(fileName);
      // add to imported set
      m_Imported.insert(typeName);
    }

  // return what ever the pool can provide (NULL when not found)
  return m_Importer.pool()->FindMessageTypeByName(name);
}

bool ProtoFilesImporter::isBasicType(const std::string& type){
  const static BasicTypeSet bTypes;
  return bTypes.find(type) != bTypes.end();
}

