/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/AnyTypeParticipantConfig.h                      **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <rsb/ParticipantConfig.h>

namespace sitinf {
  namespace io {

    /**
     * @brief The AnyTypeParticipantConfig class a participant config that can receive
     * messages of any type without registration. The message data is then returned as
     * std::string and typename.
     */
    class AnyTypeParticipantConfig : public rsb::ParticipantConfig {
    public:

      /**
       * @brief AnyTypeParticipantConfig the default instance uses the default ParticipantConfig
       * from rsb::Factory. Registers an always aplicable rsb::converter::SchemaAndByteArrayConverter
       */
      AnyTypeParticipantConfig();

      /**
       * @brief AnyTypeParticipantConfig the uses the passed ParticipantConfig. Registers an
       * always aplicable rsb::converter::SchemaAndByteArrayConverter.
       * @param config an rsb::ParticipantConfig
       */
      AnyTypeParticipantConfig(const rsb::ParticipantConfig& config);

    private:
      /**
       * @brief init internal function to initialize the config.
       */
      void init();
    };

  } // namespace io
} // namespace sitinf
