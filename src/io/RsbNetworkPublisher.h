/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/RsbNetworkPublisher.h                           **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <network/BayesianNetwork.h>

#include <rsb/Factory.h>
#include <rsb/Informer.h>
#include <rsb/patterns/LocalServer.h>
#include <rst/bayesnetwork/BayesNetwork.pb.h>

namespace sitinf {
  namespace io {

    /**
     * @brief The RsbNetworkPublisher class can be used to make a BayesianNetwork public.
     *
     * The network is published via a informer on creation and every time setNetwork is
     * called. Additionally it provides an rsb::patterns::LocalServer to get the network
     * via the rpc-call "getNetwork".
     */
    class RsbNetworkPublisher {

    public:

      /**
       * @brief Ptr typedef for shared pointer type
       */
      typedef boost::shared_ptr<RsbNetworkPublisher> Ptr;

      /**
       * @brief RsbNetworkPublisher creates an informer and Local server and publishes the passed network.
       *
       * @param scope the scope to be used for publishing
       * @param network the network to publish. may be empty.
       */
      RsbNetworkPublisher(const std::string scope, const network::BayesianNetwork::Ptr network=network::BayesianNetwork::Ptr());

      /**
       * @brief updateNetwork sets the internal network to the passed and publishes the new one.
       * @param network a new network.
       */
      void updateNetwork(const network::BayesianNetwork::Ptr network);

      /**
       * @brief currentMessage returns a shared pointer to the current message
       * @return the current message
       */
      boost::shared_ptr<rst::bayesnetwork::BayesNetwork> currentMessage();

    private:
      /**
       * @brief m_Lock used to synchronize calls
       */
      utils::Mutex m_Lock;
      /**
       * @brief m_Message holds the most recent message representation of the network
       */
      boost::shared_ptr<rst::bayesnetwork::BayesNetwork> m_Message;
      /**
       * @brief m_Informer is used to publish network updates
       */
      rsb::Informer<rst::bayesnetwork::BayesNetwork>::Ptr m_Informer;
      /**
       * @brief m_Server answers RPC callf for current network.
       */
      rsb::patterns::LocalServerPtr m_Server;
    };

  } // namespace io
} // namespace sitinf
