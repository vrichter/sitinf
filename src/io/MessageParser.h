/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/MessageParser.h                                 **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <io/ProtoFilesImporter.h>
#include <google/protobuf/message.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/dynamic_message.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/text_format.h>

#include <utils/Types.h>
#include <utils/Exception.h>

namespace sitinf {
  namespace io {

    /**
     * @brief The MessageParser serializes and deserializes Protobuf messages.
     */
    class MessageParser {

    public:

      /**
       * @brief MessageParser default constructor.
       */
      MessageParser();

      /**
       * @brief parseWireFormat parses a message from protobuf descriptor and serialized wireformat data.
       * @param binary_data wiredata held in an input stream
       * @param descriptor a message descriptor
       * @return a shared pointer to the parsed message as protobuf::Message
       */
      utils::Shared<google::protobuf::Message>::Ptr parseWireFormat(std::istream* binary_data,
                                                                    const google::protobuf::Descriptor* descriptor);

      /**
       * @brief parseJsonFormat parses a message from its json representation.
       * @param json_data the message data as a json input stream
       * @param descriptor a message descriptor
       * @return a shared pointer to the parsed message as protobuf::Message
       */
      utils::Shared<google::protobuf::Message>::Ptr parseJsonFormat(std::istream* json_data,
                                                                    const google::protobuf::Descriptor* descriptor);

      template<class T>
      /**
       * @brief parseJsonFormat parses a message from its json representation into a concrete prototype T
       * @param json_data the message data as a json input stream
       * @return a shared pointer to to the parsed message
       */
      typename utils::Shared<T>::Ptr parseJsonFormat(std::istream* json_data){
        if(!json_data){
          throw utils::NullPointerException("Passed stream pointer is NULL");
        }
        // create a protobuf input stream from passed string
        google::protobuf::io::IstreamInputStream stream(json_data);
        // create the mesage pointer
        typename utils::Shared<T>::Ptr message(T::default_instance().New());
        // parse message from stream
        if(!google::protobuf::TextFormat::Parse(&stream,message.get())){
          std::stringstream error;
          error << "Could not parse passed stream as json data of type '" << T::default_instance().GetTypeName() << "'";
          throw utils::IOException(error.str());

        }
        return message;
      }

    private:
      /**
       * @brief m_Factory used to create message instances from descriptors.
       */
      google::protobuf::DynamicMessageFactory m_Factory;

    };

  } // namespace io
} // namespace sitinf
