/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/io/XmlBifIO.cpp                                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <iostream>

#include <boost/version.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string.hpp>

#include <io/XmlBifIO.h>
#include <utils/Exception.h>
#include <utils/Macros.h>

#define _LOGGER "io.XmlBifIO"
#include <utils/Logging.h>
static const el::Logger* _logger = sitinf::utils::LoggingConfiguration::getLogger(_LOGGER);


using namespace sitinf;
using namespace sitinf::io;
using namespace sitinf::network;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;

void tokenize(std::string table, std::vector<double>& dst, std::string token=" "){
  std::vector <std::string> fields;
  boost::split(fields,table,boost::is_any_of(" "));
  for(std::vector<std::string>::const_iterator it = fields.begin(); it != fields.end(); ++it){
    if(it->size() > 0){
      dst.push_back(0.);
      std::stringstream str(*it);
      str >> dst.back();
    }
  }
}

BayesianNetwork::Variable parseVariable(const ptree& nData){
  BayesianNetwork::Variable var;
  for(ptree::const_iterator it = nData.begin(); it != nData.end(); ++it){
    if(it->first == "<xmlattr>"){
      var.m_Type = BayesianNetwork::Variable::toType(it->second.get<std::string>("TYPE"));
    } else if(it->first == "NAME"){
      var.m_Name = it->second.get_value<std::string>();
    } else if(it->first == "OUTCOME"){
      var.m_Outcomes.push_back(it->second.get_value<std::string>());
    } else if(it->first == "PROPERTY"){
      var.m_Property = it->second.get_value<std::string>();
    } else {
      SLOG_ERROR << "Unexpected property in BayesianNetwork '" << it->first << "'";
    }
  }
  return var;
}

BayesianNetwork::Definition parseDefinition(const ptree& nData){
  BayesianNetwork::Definition def;
  for(ptree::const_iterator it = nData.begin(); it != nData.end(); ++it){
    if(it->first == "FOR"){
      def.m_Variables.insert(def.m_Variables.begin(),it->second.get_value<std::string>());
    } else if(it->first == "GIVEN"){
      def.m_Variables.push_back(it->second.get_value<std::string>());
    } else if(it->first == "TABLE"){
      tokenize(it->second.get_value<std::string>(), def.m_Table);
    } else {
      SLOG_ERROR << "Unexpected property in BayesianNetwork '" << it->first << "'";
    }
  }
  return def;
}

BayesianNetwork::Ptr parseNetwork(const ptree& nData){
  std::string comment = "";
  std::string name = "";
  std::vector<BayesianNetwork::Variable> vars;
  std::vector<BayesianNetwork::Definition> defs;

  // get network comment
  boost::optional<std::string> optComment = nData.get_optional<std::string>("<xmlcomment>");
  comment = (optComment) ? *optComment : "";

  // get network name / variables / definitions
  ptree subnet = nData.get_child("BIF.NETWORK");
  for(ptree::const_iterator it = subnet.begin(); it != subnet.end(); ++it){
    if(it->first == "VARIABLE"){
      vars.push_back(parseVariable(it->second));
    } else if(it->first == "DEFINITION"){
      defs.push_back(parseDefinition(it->second));
    } else if(it->first == "NAME"){
      name = it->second.get_value<std::string>();
    } else {
      SLOG_ERROR << "Unexpected property in BayesianNetwork '" << it->first << "'";
    }
  }
  return BayesianNetwork::Ptr(new BayesianNetwork(vars,defs,name,comment));
}

ptree writeNetwork(const BayesianNetwork& network){
  ptree ret;

  // create bif node with version information
  ret.put("BIF.<xmlattr>.VERSION","0.3");

  // write network name
  if(network.name() != ""){
    ret.put("BIF.NETWORK.NAME",network.name());
  }

  // write network comment
  if(network.comment() != ""){
    ret.put("<xmlcomment>",network.comment());
  }

  // write network variables
  const std::vector<BayesianNetwork::Variable>& vars = network.variables();
  for (std::vector<BayesianNetwork::Variable>::const_iterator
       it = vars.begin(); it != vars.end(); ++it){
    const BayesianNetwork::Variable& var = *it;
    ptree varTree;
    // set type
    varTree.put("<xmlattr>.TYPE",BayesianNetwork::Variable::toString(var.m_Type));
    // set name
    varTree.put("NAME",var.m_Name);
    // set outcomes
    std::vector<std::string>::const_iterator it2;
    for(it2 = var.m_Outcomes.begin(); it2 != var.m_Outcomes.end(); ++it2){
      varTree.add("OUTCOME",*it2);
    }
    // set property
    if(var.m_Property != ""){
      varTree.put("PROPERTY",var.m_Property);
    }
    ret.add_child("BIF.NETWORK.VARIABLE",varTree);
  }

  // write network definitions
  const std::vector<BayesianNetwork::Definition>& defs = network.definitions();
  for (std::vector<BayesianNetwork::Definition>::const_iterator
       it = defs.begin(); it != defs.end(); ++it){
    const BayesianNetwork::Definition& def = *it;
    ptree defTree;
    // check variables exist
    if(def.m_Variables.empty()){
      SLOG_ERROR << "Definition must at least contain one variable.";
      continue;
    }
    // add first variable as for
    defTree.put("FOR",def.m_Variables.front());
    // add other variables as given
    for(uint i = 1; i < def.m_Variables.size(); ++i){
      defTree.add("GIVEN",def.m_Variables.at(i));
    }
    // add table
    std::stringstream v;
    for(uint i = 0; i < def.m_Table.size(); ++i){
      v << def.m_Table.at(i) << " ";
    }
    defTree.put("TABLE",v.str());
    ret.add_child("BIF.NETWORK.DEFINITION",defTree);
  }
  return ret;
}

BayesianNetwork::Ptr XmlBifIO::readFile(const std::string& filename){
  ptree tree;
  read_xml(filename, tree);
  return parseNetwork(tree);
}

BayesianNetwork::Ptr XmlBifIO::readStream(std::istream& input_stream) {
  ptree tree;
  read_xml(input_stream, tree);
  return parseNetwork(tree);
}

void XmlBifIO::writeFile(const std::string& filename, const BayesianNetwork& bayesian_network){
  ptree tree = writeNetwork(bayesian_network);
#if BOOST_VERSION >= 105600
  xml_writer_settings<std::string> settings(' ', 2);
#else
  xml_writer_settings<char> settings(' ', 2);
#endif
  write_xml(filename, tree, std::locale(), settings);
}

void XmlBifIO::writeStream(std::ostream& output_stream, const BayesianNetwork& bayesian_network){
  ptree tree = writeNetwork(bayesian_network);
#if BOOST_VERSION >= 105600
  xml_writer_settings<std::string> settings(' ', 2);
#else
  xml_writer_settings<char> settings(' ', 2);
#endif
  write_xml(output_stream, tree, settings);
}
