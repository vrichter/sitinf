# SitInf

SitInf is a small library and toolset for situation recognition.
It uses [libDAI](https://staff.fnwi.uva.nl/j.m.mooij//libDAI/doc/) to infer the current situation from a network and sensor configuration.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* How to run tests
* Deployment instructions

## Network

The Networks corrently supported by SitInf are discrete BayesianNetworks defined in the [XLBIF](http://www.cs.cmu.edu/~fgcozman/Research/InterchangeFormat/ "The Interchange Format for Bayesian Networks") format.

## Sensors

The current state of the network may be clamped by [Sensors](@ref sitinf::sensor::Sensor).

## Communication

SitInf uses [RSB](https://code.cor-lab.de/projects/rsb "Robotics Service Bus") to communicate its currently used network and inference results.

## Applications

1. sitinf-situation is the main application.
- loads a network from xml
- calculates the networks state
- loads a sensor configuration
- clamps network states from sensor information and updates the state under current information
- network states can be overwritten using the sitinf-simulator application
- publishes the used network and current state via RSB

2. sitinf-simulator can be used for simulation and debugging
- aquires the current network from sitinf-situation
- can publish network states to overwrite the current situation states

3. sitinf-viewer network visualization
- aquires the current network from sitinf-situation
- listens for state updates from sitinf-situation
- renders the current network and its state

## Copyright

GNU LESSER GENERAL PUBLIC LICENSE

This project may be used under the terms of the GNU Lesser General
Public License version 3.0 as published by the
Free Software Foundation and appearing in the file LICENSE.LGPL
included in the packaging of this project.  Please review the
following information to ensure the license requirements will
be met: http://www.gnu.org/licenses/lgpl-3.0.txt

